# Ovaalstuudio Booking App

Welcome to the Ovaalstuudio Booking App! This application allows users to book seminar rooms and manage bookings with
ease. It is built using PHP 8 and MySQL/MariaDB, and comes with various features like email notifications, image
uploading, and integration with Simplbooks accounting software.

## Important Links

| Site            | URL                                                                                                                        | Username       | Password |
|-----------------|----------------------------------------------------------------------------------------------------------------------------|----------------|----------|
| Pivotal Tracker | [https://www.pivotaltracker.com/n/projects/1561985](https://www.pivotaltracker.com/n/projects/1561985)                     |                |          |
| Live            | [https://bron.ovaal.ee/](https://bron.ovaal.ee/)                                                                           |                |          |
| Staging         | [https://test.diarainfra.com/broneerimiskeskkond](https://test.diarainfra.com/broneerimiskeskkond)                         | demo@demo.ee   | demo     |
| Simplbooks      | [https://app.simplbooks.com/e7c6d82a64a51fef292b2ef079568575](https://app.simplbooks.com/e7c6d82a64a51fef292b2ef079568575) | infra@diara.ee | makaron  |


## Requirements

- PHP 8.0 or later
- MySQL or MariaDB
- Composer
- Imagemagick PHP extension (optional for development)
- Pivotal Tracker (for user story tracking)
- MailHog (for catching emails in dev)

## Installation

1. Clone the repository:

```bash
git clone https://github.com/your-username/seminar-room-booking-webapp.git
```

2. Navigate to the project directory:

```bash
cd seminar-room-booking-webapp
```

3. Install the required dependencies using Composer:

```bash
composer install
```

4. Create a MySQL/MariaDB database for the application:

```sql
CREATE DATABASE seminar_room_booking;
```

5. Import the provided database dump to your newly created database:

```bash
mysql -u your_username -p seminar_room_booking < doc/database.sql
```

## Configuration

### 1. Copy the sample configuration file:

```bash
cp config.sample.php config.php
```

### 2. Open `config.php` and update the following settings:

- Database connection details (username, password, host, and database name)
- Simplbooks API keys (set for dev environment in the sample config file, needs to be changed in live
- SMTP settings (set to MailHog in the sample config file, needs to be changed in live)

### 3. Install the Imagemagick PHP extension (only needed for uploading images in admin/rooms):

#### For Linux users:

```bash
# For Ubuntu/Debian-based systems
sudo apt-get install php-imagick

# For Alpine-based systems
sudo apk add php82-imagick
```

#### For Windows XAMPP users:

Download the appropriate Imagick binary for your PHP version from the PECL website:

- 1. Visit https://pecl.php.net/package/imagick.
- 2. Find the version that corresponds to your PHP version and click on the "DLL" link to download the zip file.

Extract the downloaded zip file:

1. Right-click the zip file and choose "Extract All" or use your preferred archive extractor tool.
2. Locate the "php_imagick.dll" file in the extracted folder.

Copy the "php_imagick.dll" file to the XAMPP PHP extensions directory:

1. Navigate to your XAMPP installation directory (usually C:\xampp).
2. Open the "php" folder, followed by the "ext" folder.
3. Paste the "php_imagick.dll" file into the "ext" folder.

Configure the PHP.ini file to load the Imagick extension:

1. Go back to the "php" folder in your XAMPP directory.
2. Locate and open the "php.ini" file with a text editor, such as Notepad or Notepad++.
3. Add the following line at the end of the "Dynamic Extensions" section or anywhere after it:

```makefile
extension=php_imagick.dll
```

4. Save and close the "php.ini" file.

Download and install ImageMagick:

1. Visit the ImageMagick download page at https://imagemagick.org/script/download.php.
2. Download the appropriate version for your system (32-bit or 64-bit) under the "Windows Binary Release" section.
3. Run the installer and follow the on-screen instructions.

Restart your XAMPP server:

1. Open the XAMPP control panel.
2. Stop the Apache and MySQL services if they are running.
3. Start the Apache and MySQL services again.
4. Restart your web server to apply the changes.

### Accessing simplbooks test enviorment

PivotalTracker: https://www.pivotaltracker.com/n/projects/1561985

Simplbooks test enviorment:

* URL: https://app.simplbooks.com/e7c6d82a64a51fef292b2ef079568575
* Username: infra@diara.ee
* Password: makaron

## Development

1. Set up Pivotal Tracker for user story tracking:

- Create a project and add stories as needed.
- Update the config.php file with your Pivotal Tracker API key and project ID.

2. Set up MailHog for catching emails in the development environment:

- Install MailHog following the instructions in the [official documentation](https://github.com/mailhog/MailHog).

## Third-Party Integrations

This application integrates with Simplbooks accounting software for generating invoices. You can find the API keys for
the Simplbooks test environment in the `config.php` file.

For more information on Simplbooks, visit their [official website](https://www.simplbooks.com).

## Contributing

If you need to contribute to this project, please follow these steps:

1. Fork the repository
2. Create a new branch for your changes if you are not able to finish the story in one single sitting. Name the branch
   after the story you are working on using the following format: `<story-id>_keyword`. For example, if you are working
   on story #123456789, the branch name should be `123456789_SignUp`.
3. Only commit complete story as a single commit to main branch. Squash your branch commits if needed.
4. Do not commit multiple stories in a single commit.
5. Use the following format for your commit messages: `<story-title> [Delivers #<story-id>]`. For example, if you are
   working on story #123456789 and you are adding a new feature, your commit message should
   be `Add new feature [Delivers #123456789]`.

## License

This project is not publicly licensed.