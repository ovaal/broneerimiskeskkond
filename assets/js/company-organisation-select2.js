var company_id = null;

var company_organisation = $('#registration-organisation');

var company_registry_number_container = $(".company-registry-number-container");
var company_address_container = $(".company-address-container");

// Hide registry number and address until the user searches for a company
company_registry_number_container.hide();
company_address_container.hide();

company_organisation.select2({
    placeholder: COMPANY_NAME,
    allowClear: true,
    minimumInputLength: 4,
    maximumSelectionLength: 1,
    ajax: {
        url: 'companies/get_for_select',
        dataType: 'json',
        data: function (params) {
            return {query: params.term}
        },
        processResults: function (json) {
            return {
                results: json.data
            };
        }
    }
});

company_organisation.on('select2:select', function (e) {
    ajax('companies/get', {
        company_id: $(this).val()
    }, function (json) {
        // Fill in the company data
        $("#registration-registry-number").val(json.data.company_registry_nr);
        $("#registration-address").val(json.data.company_city + json.data.company_street);

        // Show registry number and address
        company_registry_number_container.show();
        company_address_container.show();
    });

    // Set the found company ID
    company_id = $(this).val();
});


// https://gist.github.com/lingceng/d1cd5ca5db9a777b31487769242c422f
// See the issue here: https://github.com/select2/select2/issues/3022

function select2InputTags(queryStr) {
    var $input = $(queryStr)

    var $select = $('<select class="'+ $input.attr('class') + '" multiple="multiple"><select>')
    if ($input.val() != "") {
        $input.val().split(',').forEach(function(item) {
            $select.append('<option value="' + item + '" selected="selected">' + item + '</option>')
        });
    }
    $select.insertAfter($input)
    $input.hide()

    $select.change(function() {
        $input.val($select.val().join(","));
    });

    return $select;
}