var RELOAD = 33;

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    return !(charCode > 31 && (charCode < 48 || charCode > 57));


}

function tryToParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns null, and typeof null === "object",
        // so we must check for that, too. Thankfully, null is falsey, so this suffices:
        if (o && typeof o === "object") {
            return o;
        }
    } catch (e) {
    }

    return false;
}


$(document).ready(function () {

    $('.the-userbox-form').hide();
    $('.the-userbox-form#login').show();

    $('.the-userbox-tabs li').on('click', function () {

        $('.the-userbox-tabs li').each(function () {

            $(this).removeClass('active');

        });

        $('.the-userbox-form').each(function () {

            $(this).hide();

        });

        $(this).addClass('active');

        $('.the-userbox-form#' + $(this).attr('id')).show();

    });

    // minus
    $('.data-input-toggle-number .input-minus').on('click', function (event) {

        event.preventDefault();

        var currentNumber = $(this).parent().find('input').val();

        if (currentNumber < 1) {

            currentNumber = 0;

        } else {

            currentNumber--;

        }

        var elem = $(this).parent().find('input');
        elem.val(currentNumber);

        suppliesToSession(elem);
        calculateBill();

        return false;

    });

    // plus
    $('.data-input-toggle-number .input-plus').on('click', function (event) {

        event.preventDefault();

        var currentNumber = $(this).parent().find('input').val();

        currentNumber++;

        var elem = $(this).parent().find('input');
        elem.val(currentNumber);

        suppliesToSession(elem);
        calculateBill();

        return false;

    });

    // modal
    $('li[data-open-room-modal="true"]').on('click', function () {

        var roomImages = $(this).attr('data-images').split(',');
        var roomTitle = $(this).attr('data-title');
        var roomSubTitle = $(this).attr('data-subtitle');
        var roomDescription = $(this).attr('data-desc');
        var roomPrice = $(this).attr('data-price');
        var roomPriceLabel = $(this).attr('data-price-label');
        var roomArea = $(this).attr('data-area');
        var roomAreaLabel = $(this).attr('data-area-label');
        var roomPlanLabel = $(this).attr('data-plan-label');
        var roomPlanItems = $(this).attr('data-plan-items').split(',');

        var modalContent = '<div class="modal-container"><div class="room-modal">' +
            '<div class="modal-close"></div>' +
            '<div class="modal-images"><div class="modal-images-gallery"></div></div>' +
            '<div class="modal-content"><div class="content-row" id="the-room-view-header">' +
            '<h1>' + roomTitle + '</h1>' +
            '<p>' + roomDescription + '</p>' +
            '</div></div>' +
            '</div></div>';

        $('body').append(modalContent);

        $('.modal-close').on('click', function () {

            $('.modal-container').remove();

        });

    });

});

function ajax(url, options, callback_or_redirect_url, error_callback) {

    $.post(url, options)
        .fail(function (jqXHR, textStatus, errorThrown) {
            console.log('Xhr error: ', jqXHR, textStatus, errorThrown);
            show_error_modal(jqXHR.responseText, true);
        })
        .done(function (response) {
            var json = tryToParseJSON(response);

            // Properly hide loading modal if it's open
            close_modal($("#loading-modal"));

            // Handle situation when server response was not json
            if (json === false) {

                // Show the error (generic if not on localhost)
                show_error_modal(response);

                return false;

            }

            // Decode data if it was encoded
            if (json.encoded) {
                json.data = decode_base64(json.data);
            }

            // Show actual error message to user if status is 4xx
            if (json.status.toString().charAt(0) === '4') {

                // Call error callback if given
                if (typeof error_callback === 'function') {
                    error_callback(json);
                }

                // Show error modal if callback was not given
                else {
                    show_error_modal(json.data, true);
                }
            }

            // Show generic error message
            else if (json.status === 500) {

                // Call error callback if given
                if (typeof error_callback === 'function') {
                    error_callback(json);
                }

                // Show error modal if callback was not given
                else {
                    show_error_modal(json.data);
                }

                return false;
            } else {

                // Call the succes callback function if given
                if (typeof callback_or_redirect_url === 'function') {
                    callback_or_redirect_url(json);
                }

                // Redirect if calback was a string
                else if (typeof callback_or_redirect_url === 'string') {
                    location.href = callback_or_redirect_url;
                }

                // Refresh page if callback was RELOAD
                else if (callback_or_redirect_url === RELOAD) {
                    location.reload();
                }

            }

        });

}

// Fix "when I open a second modal and close it, the first modal becomes unscrollable" issue
$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});

function close_modal(modal) {
    modal.modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

function show_error_modal(error, always) {

    // Replace error message when server did not return anything
    if (error.length === 0) {
        error = "Server returned an empty response"
    }

    // Stringify object type errors (arrays)
    if (error === Object(error)) {
        error = '<pre>' + JSON.stringify(error, null, 4) + '</pre>';
    }

    always = typeof always !== 'undefined';

    // Determine if we are on a dev env
    var localhost = window.location.hostname !== 'bron.ovaal.ee';

    // Show a proper error message depending on the env
    $(".error-modal-body").html(localhost || always ? error : SERVER_ERROR_OTHER);

    // Show the modal
    $("#error-modal").modal('show');

    // Hide potential loading modal
    $('#loading-modal').hide();
    $('body > div.modal-backdrop.fade.in').hide();

}

// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        },
        configurable: true,
        writable: true
    });
}

