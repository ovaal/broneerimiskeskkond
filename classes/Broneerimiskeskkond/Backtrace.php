<?php namespace Broneerimiskeskkond;

class Backtrace
{
    public static function toCsv(array $backtrace): string
    {
        Backtrace::stripNoiseCalls($backtrace);
        Backtrace::addArgumentNames($backtrace);
        Backtrace::stripPathFromFileNames($backtrace);
        Backtrace::stripPathFromClassNames($backtrace);
        Backtrace::reformat($backtrace);
        return array2csv($backtrace);
    }

    public static function stripNoiseCalls(array &$backtrace): void
    {
        foreach ($backtrace as $i => $call) {
            if (self::isApplicationConstructCall($call)
                || self::isIndexTopCall($call)
                || self::isSelfCall($call)) {
                self::removeCall($backtrace, $i);
            }
        }
    }

    private static function isApplicationConstructCall(mixed $call): bool
    {
        return $call->class === 'Halo\Application' && $call->method === '__construct';
    }

    private static function isIndexTopCall($call): bool
    {
        $file = self::removeProjectBasePath($call->file);
        return $file === platformSlashes('/index.php') && $call->method === '[top]';
    }

    private static function removeProjectBasePath(string $fullPathToFile): string
    {
        return str_replace(self::getProjectBasePath(), '', $fullPathToFile);
    }

    private static function getProjectBasePath(): string
    {
        return dirname(__DIR__, 2);
    }

    private static function isSelfCall(mixed $call): bool
    {
        return $call->class === 'Broneerimiskeskkond\Log' && $call->method === 'log';
    }

    private static function removeCall(array &$backtrace, int $indexOfCallToRemove): void
    {
        unset($backtrace[$indexOfCallToRemove]);
    }

    private static function addArgumentNames(&$backtrace): void
    {
        foreach ($backtrace as &$call) {
            $argumentList = getArgumentList($call->method, $call->class);
            $call->arguments = self::getGivenArguments($call->arguments, $argumentList);
        }
    }

    private static function getGivenArguments(array $givenArguments, array $defaultArguments): array
    {
        $result = [];
        $allArgumentNames = array_keys($defaultArguments);
        foreach ($givenArguments as $i => $argumentValue) {

            // Defensive check
            if (!isset($allArgumentNames[$i])) {
                $argumentName = $i;
            }else{
                $argumentName = $allArgumentNames[$i];
            }

            $result = self::addArgumentToResultIfValueIsNotDefault(
                $argumentName,
                $defaultArguments,
                $argumentValue,
                $result);
        }
        return $result;
    }

    private static function addArgumentToResultIfValueIsNotDefault($argumentName, array $defaultArguments, mixed $argumentValue, array $result): array
    {


        // Defensive check
        if (!isset($defaultArguments[$argumentName])) {
            return $result;
        }

        $defaultValue = $defaultArguments[$argumentName];
        if ($argumentValue !== $defaultValue) {
            $result[$argumentName] = $argumentValue;
        }
        return $result;
    }

    public static function stripPathFromFileNames(array &$backtrace): void
    {
        foreach ($backtrace as & $call) {
            $call->file = basename($call->file);
        }
    }

    public static function stripPathFromClassNames(array &$backtrace): void
    {
        foreach ($backtrace as & $call) {
            $call->class = basename(str_replace('\\', '/', $call->class));
        }
    }

    public static function reformat(&$backtrace): void
    {
        foreach ($backtrace as &$frame) {
            $classAndMethod = $frame->class ? "$frame->class::$frame->method" : $frame->method;
            $frame = [$frame->file, $frame->lineNumber, $classAndMethod, $frame->arguments];
        }

    }

    public static function convertToJsonIfArrayOrObject(mixed $value): mixed
    {
        if (is_array($value) || is_object($value)) {
            $value = json_encode($value);
        }
        return $value;
    }

    public static function convertBacktraceToText(array &$backtrace): string
    {
        self::flattenCallsToSingleString($backtrace);
        return implode("\n", $backtrace);
    }

    public static function flattenCallsToSingleString(array &$backtrace): void
    {

        foreach ($backtrace as &$$$i) {
            $i = "$i->file:$i->lineNumber $i->class::$i->method($i->arguments)";
        }
    }

    public static function makeLabel(mixed $name, $value): string
    {
        $name = htmlspecialchars($name);

        if (!is_string($value)) {
            $value = '<i>'.json_encode($value).'</i>';
        } else {
            $value = htmlspecialchars($value);
        }

        return "<br><span>$name:</span><span>$value</span>";
    }

    public static function asHTML(mixed $backtrace): string
    {
        $n = 0;
        $result = '';
        Backtrace::makeFilesBold($backtrace);
        Backtrace::makeArgumentsBeautiful($backtrace);

        foreach ($backtrace as $call) {
            $n++;
            $result .= "<div class=\"backtrace-call\"> $n. $call[0]:$call[1] $call[2]($call[3])</div>";
        }
        return $result;
    }

    public static function makeFilesBold(array &$backtrace): void
    {
        foreach ($backtrace as & $call) {
            $call[0] = '<b>' . $call[0] . '</b>';
            $call[1] = '<b>' . $call[1] . '</b>';
        }
    }

    public static function makeArgumentsBeautiful(array &$backtrace): void
    {
        foreach ($backtrace as &$call) {
            $call[3] = Log::convertArgumentsToLabels($call[3]);
        }
    }

}