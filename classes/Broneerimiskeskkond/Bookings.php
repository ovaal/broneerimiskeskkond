<?php
/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 3.06.2016
 * Time: 14:10
 */

namespace Broneerimiskeskkond;


class Bookings
{
    static function get_all()
    {
        return get_all("SELECT *, orders.user_id FROM bookings LEFT JOIN orders USING (order_id) LEFT JOIN rooms USING (room_id) LEFT JOIN payment_statuses USING (payment_status_id) WHERE orders.deleted = 0");
    }

    static function get_all_by_order_id($order_id)
    {
        return get_all("SELECT *, orders.user_id FROM bookings LEFT JOIN orders USING (order_id) LEFT JOIN rooms USING (room_id) LEFT JOIN payment_statuses USING (payment_status_id) WHERE orders.order_id = $order_id AND orders.deleted = 0 AND bookings.deleted = 0");
    }

    static function find_by_user($user_id)
    {
        return get_all("SELECT * FROM orders LEFT JOIN bookings USING (order_id) LEFT JOIN rooms USING (room_id)WHERE user_id='$user_id'");
    }

    static function get_by_order_id($order_id)
    {
        return get_first("SELECT * FROM bookings LEFT JOIN orders USING (order_id) LEFT JOIN booking_extras USING (booking_id) LEFT JOIN booking_supplies USING(booking_id) LEFT JOIN rooms USING(room_id) LEFT JOIN users USING(user_id) WHERE order_id='$order_id'");
    }

    static function get_by_booking_id($booking_id)
    {
        return get_first("SELECT * FROM bookings LEFT JOIN orders using (order_id) LEFT JOIN booking_extras USING (booking_id) LEFT JOIN booking_supplies USING(booking_id) LEFT JOIN rooms USING(room_id) LEFT JOIN users USING(user_id) WHERE booking_id='$booking_id'");
    }

    static function serialize_by_order_id($order_id)
    {
        $order_info['room'] = get_all("SELECT * FROM rooms LEFT JOIN bookings USING (room_id) LEFT JOIN orders USING (order_id) WHERE bookings.order_id=$order_id");
        $order_info['booking'] = get_first("SELECT
                                                *,
                                                
                                                -- Disable save button when there are 4 days or less until the booking start date
                                                IF((DATE(booking_start) < DATE(DATE_ADD(NOW(), INTERVAL 4 DAY))) OR
                                                   (DATE(DATE_ADD(CURDATE(), INTERVAL 4 DAY)) = DATE(booking_start) AND CURTIME() > '18:00:00')
                                                , 1, 0) read_only
                                              FROM bookings
                                                LEFT JOIN orders USING (order_id)
                                                LEFT JOIN users USING (user_id)
                                              WHERE bookings.order_id =$order_id AND orders.deleted = 0");
        $order_info['extras'] = get_all("SELECT * 
                                        FROM booking_extras 
                                        LEFT JOIN bookings USING (booking_id)
                                        LEFT JOIN orders USING (order_id) 
                                        LEFT JOIN extras USING (extra_id) 
                                        WHERE order_id=$order_id");
        $order_info['supplies'] = get_all("SELECT * 
                                        FROM booking_supplies 
                                        LEFT JOIN bookings USING (booking_id)
                                        LEFT JOIN orders USING (order_id) 
                                        LEFT JOIN supplies USING (supply_id)
                                        WHERE order_id=$order_id");
        $order_info['services'] = get_all("SELECT * 
                                        FROM order_services
                                        WHERE order_id=$order_id");
        return $order_info;
    }

    static function get_order_id_by_date($date, $state_institution = 0)
    {
        return get_all("SELECT order_id
                        FROM bookings
                          LEFT JOIN orders USING (order_id)
                          LEFT JOIN users USING (user_id)
                        WHERE DATE(booking_start) = '$date'
                          AND state_institution = $state_institution
                          AND orders.dont_send_invoice = 0
                          AND orders.deleted = 0
                        GROUP BY order_id");
    }

    static function clear()
    {

        // Log session clearing event to a file for debugging vanishing sessions
        $f_session_clearings = fopen("session_clearings.txt", "a") or die("Unable to write to file!");
        fwrite($f_session_clearings, date('Y-m-d H:i:s') . "UserID: $_SESSION[user_id] " . print_r(debug_backtrace(), 1) . "\n");
        fclose($f_session_clearings);

        $user_id = $_SESSION['user_id'];
        $language = $_SESSION['language'];

        $_SESSION = array();
        $_SESSION['language'] = $language;
        $_SESSION['user_id'] = $user_id;

    }

    static function get_date_without_bookings()
    {
        $date = date('Y-m-d');
        $free = false;

        while ($free == false) {
            // Check if there are any bookings on this date
            $bookings_on_this_date = get_all("SELECT * FROM bookings WHERE (DATE(booking_start) = '$date' OR DATE(booking_end) = '$date') AND deleted = 0");

            // If there are bookings on this date, add one day to date and check again
            if (empty($bookings_on_this_date)) {
                $free = true;
            } else {
                $date = date('Y-m-d', strtotime($date . ' + 1 days'));
            }

        }

        return $date;
    }

    public static function get_id_by_order_id_and_room_id($order_id, $room_id)
    {
        return get_one("
            SELECT booking_id
            FROM orders
            JOIN bookings USING (order_id)
            LEFT JOIN booking_extras USING (booking_id)
            WHERE bookings.room_id = $room_id
            AND order_id = $order_id;
            ");
    }


}
