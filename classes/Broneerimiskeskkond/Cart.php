<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class Cart
{
    public static function get($process = true)
    {

        $cart['rooms'] = Cart::getRooms();
        $cart['services'] = Service::translateNames(Cart::getServices());
        $cart['available_services'] = Service::translateNames(Service::getAll());
        $cart['booking_start_date'] = $_SESSION['booking_date'];
        $cart['booking_start_time'] = $_SESSION['booking_start_time'];
        $cart['booking_end_time'] = $_SESSION['booking_end_time'];
        $cart['booking_duration'] = $_SESSION['booking_duration'];
        $cart['special_room_selected'] = Cart::specialRoomSelected();
        $cart['discount'] = Cart::getDiscountAmount();
        $cart['discount_level'] = User::get()['discount_level'];
        $cart['sum'] = Cart::sum();

        $process ? Extras::processForOrderSummary($cart['rooms'], true) : null;

        return $cart;

    }

    public static function getRoomsPrice()
    {
        $sum = 0;

        if (Cart::specialRoomSelected()) {

            if (empty(Cart::specialRoom())) throw new \Exception('Failed to get Cart::specialRoom()');

            return Cart::specialRoom()['room_price'];

        } else {

            // Add individual room prices together
            foreach ($_SESSION['selected_rooms'] as $selected_room) {
                $sum += $selected_room['room_price'];
            }
        }

        return $sum;
    }

    public static function getRoomsSum()
    {
        return self::getRoomsPrice() * $_SESSION['booking_duration'];
    }


    public static function getRooms()
    {
        $rooms = [];
        $extras = Extras::translateNames(Extras::get());

        if (Cart::specialRoomSelected()) {
            $room = Cart::specialRoom();
            $rooms[] = [
                'room_id' => 101,
                'room_name' => __($room['room_name']),
                'room_price' => (float)$room['room_price']
            ];
        }

        if (!empty($_SESSION['selected_rooms'])) foreach ($_SESSION['selected_rooms'] as $selected_room_id => $selected_room) {

            // Reset (back) to emptyness
            $selected_extras = [];

            // Assemble extras array
            if (isset($selected_room['extras'])) foreach ($selected_room['extras'] as $extra_id => $whatever) {
                $selected_extras[$extra_id] = [
                    'id' => $extra_id,
                    'name' => __($extras[$extra_id]['extra_name']),
                    'price' => (float)$extras[$extra_id]['extra_price'],
                    'selected' => true
                ];
            }

            // Assemble rooms array
            $rooms[$selected_room_id] = [
                'room_id' => $selected_room['room_id'],
                'room_name' => __('Room') . ' ' . $selected_room['room_name'],
                'room_price' => Cart::specialRoomSelected() ? 0 : (float)$selected_room['room_price'],
                'extras' => $selected_extras
            ];
        }

        return $rooms;
    }

    public static function getServices()
    {

        $all_services = Service::getAll();
        $selected_services = [];

        foreach ($all_services as $service_id => $all_service) {
            if (!empty($_SESSION['selected_services'][$service_id])) {
                $selected_services[$service_id] = $all_services[$service_id];
                $selected_services[$service_id]['quantity'] = $_SESSION['selected_services'][$service_id];
            }
        }

        return $selected_services;
    }

    public static function addService($service_id, int $quantity)
    {
        $_SESSION['selected_services'][$service_id] = $quantity;
    }

    public static function addExtra($room_id, $extra_id)
    {
        $_SESSION['selected_rooms'][$room_id]['extras'][$extra_id] = 1;
    }

    public static function removeExtra($room_id, $extra_id)
    {
        if (isset($_SESSION['selected_rooms'][$room_id]['extras'][$extra_id]))
            unset($_SESSION['selected_rooms'][$room_id]['extras'][$extra_id]);
    }

    public static function removeService($service_id)
    {
        if (isset($_SESSION['selected_services'][$service_id])) {
            unset($_SESSION['selected_services'][$service_id]);
        }
    }

    public static function specialRoomSelected()
    {
        return !empty($_SESSION['selected_special_room']);
    }

    public static function clear()
    {
        unset($_SESSION['selected_rooms']);
        unset($_SESSION['selected_special_room']);
    }

    public static function specialRoom()
    {
        return empty($_SESSION['selected_special_room']) ? null : $_SESSION['selected_special_room'];
    }

    public static function getRoomNames()
    {
        $room_names = [];
        // Generate room names
        if (!empty($_SESSION['selected_rooms'])) foreach ($_SESSION['selected_rooms'] as $room_id => $room_price) {
            $selected = Room::get($room_id);
            $room_names[] = $selected['room_name'];
        }

        return implode('", "', $room_names);
    }

    public static function getDiscountAmount()
    {
        return Cart::getRoomsSum() * User::get()['user_discount_percent'] * -1;
    }

    private static function sum()
    {
        $result = 0;
        $result += Cart::getRoomsSum();
        $result += Cart::getExtrasSum();
        $result += Cart::getDiscountAmount();
        $result += Cart::getServicesSum();

        return $result;
    }

    public static function getExtrasSum()
    {
        $result = 0;
        $rooms = Cart::getRooms();

        foreach ($rooms as $room) {
            if (!empty($room['extras'])) {
                foreach ($room['extras'] as $extra) {
                    $result += $extra['price'];
                }
            }
        }
        return $result;
    }

    public static function getServicesSum()
    {
        $result = 0;
        $services = Cart::getServices();
        foreach ($services as $service) {
            $result += $service['service_price'] * $service['quantity'];
        }
        return $result;

    }
}
