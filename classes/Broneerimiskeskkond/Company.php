<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 15.01.2018
 * Time: 15:04
 */

namespace Broneerimiskeskkond;


use SimplBooks\SimplBooks;

class Company
{

    /**
     * @param null $criteria
     * @return array|null
     */
    static function get($criteria = null)
    {
        $where = SQL::getWhere($criteria, "company_id");
        return get_first("SELECT * FROM companies $where");
    }

    /**
     * @param null $criteria
     * @return array
     */
    static function getAll($criteria = null)
    {
        $where = SQL::getWhere($criteria, "company_id");
        return get_all("SELECT * FROM companies $where");
    }

    /**
     * Check if given company exists in SimplBooks with its registry number
     * @param $company_id
     * @return object|false
     */
    public static function inSimplbooks($company_id): object|false
    {
        $company = self::get($company_id);
        return SimplBooks::getClient(["reg_no" => $company["company_registry_nr"]]);
    }

    /**
     * Add company to SimplBooks
     * @param $company_id
     * @param null $user
     * @return integer
     * @throws \Exception
     */
    public static function addToSimplbooks($company_id, $user = null)
    {
        $company = self::get($company_id);

        // Check existence of another company with different registry number but same name
        if (SimplBooks::getClient(["name" => $company['company_name']])) {
            // Generate an unique company name
            $unique_name = $company['company_name'] . ' (' . $company['company_registry_nr'] . ')';

            // Overwrite company name in our local database
            update('companies', ['company_name' => $unique_name], "company_id = $company_id");

            // Get the company with the updated company_name
            $company = self::get($company_id);
        }

        $user = empty($user) ? User::get() : $user;

        // Create new client to SimplBooks
        $client_data = array(
            'Client' => array(
                'e_mail' => $user['email'],
                'name' => $company['company_name'],
                'phone' => $user['phone'],
                'address_street' => $company['company_street'],
                'address_city' => $company['company_city'],
                'address_postal_code' => $company['company_postal_code'],
                'reg_no' => $company['company_registry_nr']
            )
        );

        $simplbooks_client_id = SimplBooks::createClient($client_data)->inserted_id;

        update("companies", ["simplbooks_client_id" => $simplbooks_client_id], "company_id = $company_id");

        return $simplbooks_client_id;
    }

}