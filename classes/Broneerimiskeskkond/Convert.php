<?php

namespace Broneerimiskeskkond;

class Convert
{
    public static function toPlainText($data, $strip_tags = true, $remove_whitespace = true): string
    {
        if ($strip_tags) {
            $data = strip_tags($data);
        }
        if ($remove_whitespace) {
            $data = preg_replace('/\s+/', ' ', $data);
        }

        return $data;
    }

}