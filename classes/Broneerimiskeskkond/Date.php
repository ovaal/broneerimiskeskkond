<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class Date
{
    public static function toHumanReadable($date)
    {
        $timestamp = strtotime($date);
        $day = date("d", $timestamp);
        $month = date("F", ($timestamp));
        $year = date("Y", $timestamp);

        return $day . " " . __($month) . " " . $year;
    }
}