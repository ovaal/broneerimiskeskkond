<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class Designs
{
    public static function get()
    {
        $result = [];

        $designs = get_all("SELECT * FROM designs");

        foreach ($designs as $design) {
            $result[$design['design_id']] = $design;
        }
        return $result;
    }
}