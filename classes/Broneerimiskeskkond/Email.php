<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 02/06/16
 * Time: 17:42
 */

namespace Broneerimiskeskkond;


use PHPMailer\PHPMailer\PHPMailer;

class Email
{
    static function send($to, $subject, $body, $path = null, array $stringAttachments = [], $cc_to = false)
    {
        //Create a new PHPMailer instance
        $mail = new PHPMailer();

        //Tell PHPMailer to use SMTP

        if (SMTP_USE_SENDMAIL) {
            $mail->isSendmail();
        } else {
            $mail->isSMTP();
        }

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isHTML(true);

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = SMTP_DEBUG;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host = SMTP_HOST;

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->CharSet = 'UTF-8';
        $mail->Port = SMTP_PORT;

        if (SMTP_AUTH) {
            $mail->SMTPSecure = SMTP_ENCRYPTION;

            //Whether to use SMTP authentication

            $mail->SMTPAuth = SMTP_AUTH;

            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = SMTP_AUTH_USERNAME;


            //Password to use for SMTP authentication
            $mail->Password = SMTP_AUTH_PASSWORD;
        }

        //Set who the message is to be sent from
        $mail->setFrom(SMTP_FROM);

        //Set who the message is to be sent to
        $mail->addAddress($to);

        //Set the subject line
        $mail->Subject = $subject;

        //Set message body
        $mail->Body = "<html><body>$body</body></html>";
        $mail->AltBody = strip_tags($body);

        // DKIM
        $mail->DKIM_domain = 'ovaal.ee';
        $mail->DKIM_private = 'certs/ovaal_dkim_private_key.pem';
        $mail->DKIM_selector = '1504791110.ovaal';
        $mail->DKIM_passphrase = '';
        $mail->DKIM_identity = $mail->From;

        //Set Carbon Copy recipient
        if ($cc_to) {
            $mail->AddCC($cc_to);
        }

        //Attach attachment, if given
        if (!empty($path)) {

            $mail->addAttachment($path);
        }
        if (!empty($stringAttachments)) {
            foreach ($stringAttachments as $key => $val) {
                $mail->addStringAttachment($val, $key);
            }
        }

        //send the message, check for errors
        $info = json_encode(['$to' => $to, '$subject' => $subject, '$body' => $body, '$path' => $path, '$stringAttachments' => array_keys($stringAttachments), '$cc_to' => $cc_to]);
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            Log::log(
                log_event_id: Log::EMAIL,
                object_type: 'emails',
                object_id: 0,
                new_values: [],
                additional_info: $info . $mail->ErrorInfo,
                isError: true
            );
        } else {
            Log::log(
                log_event_id: Log::EMAIL,
                object_type: 'emails',
                object_id: 0,
                new_values: [],
                additional_info: $info

            );
        }
    }

    static function hasValidDomain($email)
    {
        $regex = "^[a-z\'0-9]+([._-][a-z\'0-9]+)*@([a-z0-9]+([._-][a-z0-9]+))+$";

        if (preg_match("/$regex/", $email, $email_parts) == 1) {

            $domain = $email_parts[2];

            if (checkdnsrr($domain, "MX")) {
                return true;
            }

        }

        return false;
    }


}