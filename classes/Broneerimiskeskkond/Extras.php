<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class Extras
{
    public static function get()
    {
        $result = [];

        $extras = get_all("SELECT * FROM extras");

        foreach ($extras as $extra) {
            $result[$extra['extra_id']] = $extra;
        }
        return $result;
    }

    public static function inject_unselected(&$rooms, $translate = false)
    {
        $room_extras = get_all("
            SELECT * FROM rooms 
            LEFT JOIN room_extras USING (room_id) 
            LEFT JOIN extras USING (extra_id)
            WHERE extra_in_use = 1");

        foreach ($room_extras as $room_extra) {

            if (isset($rooms[$room_extra['room_id']])) {

                if (!isset($rooms[$room_extra['room_id']]['extras'][$room_extra['extra_id']])) {
                    $rooms[$room_extra['room_id']]['extras'][$room_extra['extra_id']] = [
                        'id' => $room_extra['extra_id'],
                        'price' => $room_extra['extra_price'],
                        'name' => $translate ? __($room_extra['extra_name']) : $room_extra['extra_name'],
                        'unit' => $translate ? __($room_extra['extra_unit']) : $room_extra['extra_unit'],
                        'selected' => false
                    ];
                }
            }
        }
    }

    public static function sort_alphabetically(&$rooms)
    {
        foreach ($rooms as &$room) {
            if (isset($room['extras'])) {

                uasort($room['extras'], function ($a, $b) {
                    return $a['name'] <=> $b['name'];
                });

            }
        }
        unset($room); // Prevent overwriting the $rooms last member later on
    }

    public static function removeIndexes(&$rooms)
    {
        // Remove id's from room extras to prevent json_encode from turning it from array to obj
        foreach ($rooms as &$room) {
            if (isset($room['extras']))
                $room['extras'] = array_values($room['extras']);
        }
        unset($room); // Prevent overwriting the $rooms last member later on

    }

    public static function processForOrderSummary(&$rooms, $translate = false)
    {
        Extras::inject_unselected($rooms, $translate);
        Extras::sort_alphabetically($rooms);
        Extras::removeIndexes($rooms);
    }

    public static function translateNames($extras)
    {
        if (empty($extras))
            return [];

        foreach ($extras as &$extra) {
            $extra['extra_name'] = __($extra['extra_name']);
            $extra['extra_unit'] = __($extra['extra_unit']);
        }

        unset($extra);

        return $extras;

    }

}