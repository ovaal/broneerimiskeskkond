<?php
/**
 * Created by PhpStorm.
 * User: Rasmus
 * Date: 25/04/2016
 * Time: 13:18
 */

namespace Broneerimiskeskkond;


use Imagick;

class Images
{

    static function decode_image_data($data)
    {
        return base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
    }

    static function save($filename, $data)
    {

        // Make directory if it doesn't exist
        if (!file_exists(dirname($filename))) {
            mkdir(dirname($filename), 0777, true);
        }

        file_put_contents($filename, $data);
    }

    static function delete($filepath)
    {
        if (file_exists($filepath)) {

            // Avoid permission problems
            chmod($filepath, 0777);

            unlink($filepath);


        } else {

            echo "Error";

        }
    }

    static function insert_first_image($array, $special_room = false)
    {
        $rooms = [];
        foreach ($array as $result) {

            if (!$special_room) {
                $imagefolder = dirname(dirname(__DIR__)) . "/assets/img/rooms/" . $result['room_id'] . "/";
            } else {
                @$imagefolder = dirname(dirname(__DIR__)) . "/assets/img/special_rooms/" . $result['special_room_id'] . "/";
            }

            $images = scandir($imagefolder);

            // Make sure at least one image exists
            if (count($images) > 2) {

                $firstimage = $images[2]; // First image

            } else {

                $placeholderfolder = dirname(dirname(__DIR__)) . "/assets/img/rooms/placeholder/placeholder.png";

                // Copy placeholder image to empty rooms directory
                copy($placeholderfolder, $imagefolder . 'placeholder.png');

                $firstimage = '';

                if (isset($firstimage) && !empty($firstimage)) {
                    $firstimage = $images[2];
                };
            }

            // Insert first image filename into rooms array
            if (!$special_room) {
                $rooms[] = array_merge($result, ['room_first_image_filename' => $firstimage]);
            } else {
                $rooms[] = array_merge($result, ['special_room_first_image_filename' => $firstimage]);
            }
        }

        return $rooms;
    }

    static function getContentAsString($image_file, $format, $w = null, $h = null)
    {
        // Check that file exists
        if (!file_exists($image_file)) {
            throw new \Exception("The file $image_file does not exist");
        }

        try {

            // Create new ImageMagick object from the image
            $im = new \imagick($image_file);

            // Set the format of the image
            $im->setImageFormat($format);

            // Resize image
            if ($w + $h > 0) {
                $im->scaleImage($w, $h, 1);
            }

            // Remove meta data
            $im->stripImage();

            // Set the image compression
            $im->setInterlaceScheme(\Imagick::INTERLACE_PLANE);
            $im->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $im->setImageCompressionQuality(85);

            // Try between 0 or 5 radius. If you find radius of 5
            // produces too blurry  pictures decrease to 0 until you
            // find a good balance between size and quality.
            $im->gaussianBlurImage(0.05, 1);

            // Get image data as string
            $image_data = $im->getImageBlob();

            // Free memory
            $im->clear();

        } catch (\ImagickException $e) {
            if ($e->getCode() == 420) {

                throw new \Exception("Unsupported image type");
            } else {
                throw new \Exception("There was a problem with the image file. " . $e->getCode() . $e->getMessage());
            }
        }

        return $image_data;
    }
}