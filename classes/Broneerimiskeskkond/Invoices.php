<?php

namespace Broneerimiskeskkond;


use Broneerimiskeskkond\PaymentStatus;
use SimplBooks\SimplBooks;
use const false;

class Invoices
{
    static function create_from_order($order_id, $room_price_multiplier, $add_extras, $days_due)
    {
        if ($room_price_multiplier > 1) {
            throw new \Exception('room_price_multiplier must be less than 1');
        }

        $order = Orders::get($order_id);

        if ($order['order_billed_rent_percentage'] / 100 + $room_price_multiplier > 1) {
            $room_price_multiplier = 1 - ($order['order_billed_rent_percentage'] / 100);
        }

        if ($room_price_multiplier < 0 || $room_price_multiplier > 1) {
            throw new \Exception('room_price_multiplier cannot be less than 0 and higher than 1');
        }

        $room_names = implode(', ', Orders::getRoomNames($order['rooms']));

        $user = User::get($order['user_id'] ?? ($_SESSION['user_id'] ?? null), $order['order_id']);

        if (!$user) {
            throw new \Exception('User not found');
        }

        $payment_percentage_text = $room_price_multiplier * 100;
        $percent = ', ' . __('partial payment') . ' ' . $payment_percentage_text . '%';

        // Add rooms to invoice
        if ($room_price_multiplier) {

            if ($order['special_room_selected']) {

                $invoice_rows[] = array(
                    "name" => __($order['special_room_name']) .
                        ' (' . $room_names . ', ' . $order['booking_start_date'] . ' ' .
                        $order['booking_start_time'] . '—' . $order['booking_end_time'] . ')' .
                        $percent,
                    "unit" => "h",
                    "amount" => $order['booking_duration'],
                    "price_per_unit" => $order['special_room_price'] * $room_price_multiplier * (1 - $user['user_discount_percent']),
                    "vat" => VAT_PERCENT * 100
                );

            } else {

                foreach ($order['rooms'] as $room) {

                    $invoice_rows[] = array(
                        "name" => __("Room rent") . "  (" .
                            $room['room_name'] . ', ' . $room['booking_start_date'] . ' ' .
                            $room['booking_start_time'] . '—' . $room['booking_end_time'] . ')' . $percent,
                        "unit" => "h",
                        "amount" => $room['booking_duration'],
                        "price_per_unit" => $room['room_price'] * $room_price_multiplier * (1 - $user['user_discount_percent']),
                        "vat" => VAT_PERCENT * 100

                    );
                };
            }
        }

        if ($add_extras) {

            // Add extras to invoice
            foreach ($order['rooms'] as $room) {
                if (!empty($room['extras'])) foreach ($room['extras'] as $extra) {
                    $invoice_rows[] = array(
                        "name" => __($extra['name']) . " (" . __($room['room_name']) . ")",
                        "unit" => __($extra['unit']),
                        "amount" => 1,
                        "price_per_unit" => $extra['price'],
                        "vat" => VAT_PERCENT * 100
                    );
                };
            }

            // Add designs to invoice
            if (!empty($order['designs'])) foreach ($order['designs'] as $order_design) {
                $invoice_rows[] = array(
                    "name" => __($order_design['design_name']) . " (" . __($order_design['room_name']) . ")",
                    "unit" => __('pcs'),
                    "amount" => 1,
                    "price_per_unit" => $order_design['design_price'],
                    "vat" => VAT_PERCENT * 100
                );
            };

            // Add supplies to invoice
            if (!empty($order['supplies'])) foreach ($order['supplies'] as $order_supply) {
                $invoice_rows[] = array(
                    "name" => __($order_supply['supply_name']) . " (" . __($order_supply['room_name']) . ")",
                    "unit" => __('pcs'),
                    "amount" => $order_supply['quantity'],
                    "price_per_unit" => $order_supply['supply_price'],
                    "vat" => VAT_PERCENT * 100
                );

            };

            // Add services to invoice
            if (!empty($order['services'])) foreach ($order['services'] as $order_services) {

                if ($order_services['service_price'] != 0) {
                    $invoice_rows[] = [
                        "name" => __($order_services['service_name_in_order_summary']),
                        "unit" => empty($order_services['service_unit_singular']) ? __('pcs') : __($order_services['service_unit_singular']),
                        "amount" => $order_services['quantity'],
                        "price_per_unit" => $order_services['service_price'],
                        "vat" => VAT_PERCENT * 100
                    ];
                }

            };
        }

        // Extra row
        if (get_one("SELECT invoice_has_extra_row FROM orders WHERE order_id = $order_id") == 1) {
            $invoice_rows[] = array(
                "name" => "Extension of the rent time",
                "unit" => "h",
                "amount" => 0.5,
                "price_per_unit" => 30,
                "vat" => VAT_PERCENT * 100
            );
        }

        // Do not create invoice if the room has been fully paid for and no more extras have been added
        if (empty($invoice_rows)) {
            return false;
        }

        // Get company ID
        $company_id = get_one("SELECT company_id FROM orders WHERE order_id = $order_id");

        $simplbooks_client = Company::inSimplbooks($company_id);

        // Check if client in SimplBooks exists
        if ($simplbooks_client) {

            $company = Company::get($company_id);

            $simplbooks_client_id = $simplbooks_client->id;

            // Sync companies.simplbooks_client_id with Simplbooks
            if ($company['simplbooks_client_id'] != $simplbooks_client->id) {
                update("companies", ['simplbooks_client_id' => $simplbooks_client->id], "company_id = $company_id");
            }

            // Update the client in SimplBooks
            SimplBooks::updateClient(
                $simplbooks_client_id,
                $company['company_name'],
                $company['company_city'],
                $company['company_street'],
                $company['company_postal_code'],
                $company['company_registry_nr']
            );
        } else {
            $simplbooks_client_id = Company::addToSimplbooks($company_id, $user);
        }

        if (!$simplbooks_client_id) {
            throw new \Exception('Invalid Simplbooks Client ID');
        }

        $additional_info = $order['invoice_additional_info'];

        // Add additional info
        $invoice_additional_info[] = ucfirst(trim($additional_info));
        $invoice_additional_info[] = " Broneeringu ID$order_id";

        // Remove empty elements (if $additional_info is empty), so that implode() does not add extra spaces
        $invoice_additional_info = array_filter($invoice_additional_info);

        // Convert to string
        $invoice_additional_info = implode('. ', $invoice_additional_info);

        // Create new invoice to SimplBooks
        $invoice = SimplBooks::createInvoice(
            $simplbooks_client_id,
            $invoice_rows,
            $days_due,
            $invoice_additional_info
        );

        // Update order billed rent percentage
        Orders::updateBilledRentPercentage(
            $order_id,
            $room_price_multiplier * 100 + $order['order_billed_rent_percentage']
        );

        return $invoice;
    }

    static function create_credit_invoice($invoice_id)
    {
        $invoice_rows = [];
        $original_invoice = SimplBooks::getInvoiceById($invoice_id);
        $simplbooks_client_id = $original_invoice->Invoice->client_id;

        // Credit invoice rows
        foreach ($original_invoice->Task as $invoice_row) {
            $invoice_rows[] = array(
                'warehouse_id' => '0',
                'article_id' => '0',
                'code' => '',
                'name' => "Ruumi rent" . " - " . $invoice_row->name,
                'contents' => '',
                'unit' => 'h',
                'amount' => $invoice_row->amount,
                'price_per_unit' => $invoice_row->price_per_unit - $invoice_row->price_per_unit - $invoice_row->price_per_unit,
                'worker' => '',
                'vat' => VAT_PERCENT * 100);
        };

        // Create new invoice to SimplBooks
        return SimplBooks::createInvoice(
            $simplbooks_client_id,
            $invoice_rows,
            21,
            __("This a credit invoice. Original invoice number is") . " " . $original_invoice->Invoice->number
        );
    }


    static function send($invoice_id, $invoice_number, $order_id, $email_to, $invoice_type)
    {
        $pdf_invoice = SimplBooks::sendPDFToBrowser($invoice_id, true);
        $e_invoice = SimplBooks::getXML($invoice_id, true);

        $attachments = ['Invoice_nr_' . $invoice_number . $order_id . '.pdf' => $pdf_invoice,
            'Invoice_nr_' . $invoice_number . $order_id . '.xml' => $e_invoice];

        $body =
            __("Hello!") . "<br><br>" .
            __('An invoice is attached to this message.') . "<br><br>" .
            __("Thank You.") . "<br><br>" .
            __("Best regards,") . "<br>" .
            "Ovaalstuudio";

        Orders::updateStatus(
            $order_id,
            constant(PaymentStatus::class . '::' . strtoupper($invoice_type) . "_INVOICE_NOT_SENT")
        );

        // Send invoice to client
        Email::send(
            $email_to,
            __("Invoice nr") . ' ' . $invoice_number,
            $body,
            null,
            $attachments,
            SITE_OWNER_EMAIL
        );

        Orders::updateStatus(
            $order_id,
            constant(PaymentStatus::class . '::' . strtoupper($invoice_type) . "_INVOICE_SENT")
        );

    }
}
