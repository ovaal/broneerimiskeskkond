<?php
/**
 * Created by PhpStorm.
 * User: IBM
 * Date: 02/07/2018
 * Time: 16:49
 */

namespace Broneerimiskeskkond;


class Language
{
    private static $languages;

    public static function toggle($language_id, $state)
    {
        update('languages', ['language_enabled' => $state], "language_id = $language_id");
    }

    public static function isEnabled($language)
    {
        return array_key_exists($language, self::getEnabled());
    }

    public static function activeLanguages()
    {
        return self::get(['language_enabled' => 1]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getAlternative()
    {
        $enabled_languages = self::getEnabled();

        // Make sure exactly two languages are active
        if (count($enabled_languages) != 2) {
            throw new \Exception('Unable to get alternative language, because active language count is not 2');
        }
        foreach ($enabled_languages as $language) {
            if ($language['language_short_name'] !== $_SESSION['language']) {
                return $language;
            }
        }
        // This should never happen
        throw new \Exception('Exception #1');
    }

    /**
     * @param array $criteria
     * @return array
     * @throws \Exception
     */
    public static function get($criteria = [])
    {
        return self::filter(self::getAllLanguages(), $criteria);
    }

    /**
     * @param $criteria
     * @param $languages
     * @return array
     * @throws \Exception
     */
    protected static function filter($languages, $criteria)
    {
        if(empty($languages)){
            throw new \Exception('There are no languages in the database');
        }
        $filtered_languages = array_filter($languages, function ($n) use ($criteria) {
            foreach ($criteria as $field => $value) {
                if ($n[$field] != $value) {
                    return false;
                }
            }
            return true;
        });
        return $filtered_languages;
    }

    protected static function getAllLanguages()
    {
        if (empty(self::$languages)) {
            $languages = get_all('SELECT * FROM languages');
            foreach ($languages as $language) {
                self::$languages[$language['language_short_name']] = $language;
            }
        }
        return self::$languages;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function getAll()
    {
        return self::get();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public static function renderLanguageOptions()
    {
        $active_languages = self::getEnabled();

        // Start nested output buffer
        ob_start();

        switch (count($active_languages)) {

            // Return nothing if language count is 0 or 1
            case 0:
            case 1:
                break;

            // Return link to alternative language if count is 2
            case 2:
                $alternative_language = self::getAlternative();
                require 'templates/_partials/language_menu_link.php';
                break;

            // Return dropdown of languages if count is > 2
            default:
                require 'templates/_partials/language_menu_dropdown.php';
        }

        // Return contents of nested output buffer
        return ob_get_clean();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function getEnabled()
    {
        return self::get(['language_enabled' => 1]);
    }
}