<?php namespace Broneerimiskeskkond;

class Log
{
    const ADD = 1;
    const EDIT = 2;
    const DELETE = 3;
    const RPC = 4; // Remote Procedure Call
    const EMAIL = 5; // Remote Procedure Call

    const Labels = [
        self::ADD => 'success',
        self::EDIT => 'info',
        self::DELETE => 'danger',
        self::RPC => 'default',
        self::EMAIL => 'info'
    ];

    static function get(array $criteria = null)
    {
        $where = SQL::getWhere($criteria, "object_id");

        $logs = get_all("
            SELECT logged_at,
                   object_type,
                   changed_fields,
                   object_id,
                   additional_info,
                   backtrace,
                   user_id,
                   first_and_last_name,
                   log_event_id,
                   log_event_name,
                   creator.user_id             creator_id,
                   creator.first_and_last_name creator_name
            FROM log
                     LEFT JOIN users creator USING (user_id)
                     LEFT JOIN `log_events` USING (log_event_id)
                $where
            ORDER BY log_id
            DESC");

        Log::convertBacktracesFromCsvToArray($logs);

        return $logs;
    }

    public static function convertBacktracesFromCsvToArray(&$logs): void
    {
        foreach ($logs as &$log) {
            self::convertNewlineDelimitedCsvStringToArrayOfCsvStrings($log['backtrace']);
            foreach ($log['backtrace'] as &$call) {
                if (!empty($call)) {
                    $call = self::convertCsvStringToArray(trim($call));

                    self::convertArgumentsFromJsonToArray($call);
                }
            }
        }
    }

    public static function convertNewlineDelimitedCsvStringToArrayOfCsvStrings(&$csvString): void
    {
        $csvString = explode("\n", $csvString);
    }

    public static function convertCsvStringToArray(string $csvString): array
    {
        return str_getcsv(
            $csvString,
            $separator = ",",
            $enclosure = "\"",
            $escape = "\\"
        );
    }

    public static function convertJsonToArray(string $jsonString): array|null|object
    {
        return json_decode($jsonString);
    }

    static function log(int $log_event_id, string $object_type, ?int $object_id, array $new_values = [], ?int $user_id = null, string $additional_info = null, bool $isError = false)
    {
        insert("log", [
            "logged_at" => date('Y-m-d H:i:s'),
            "user_id" => $user_id > 0 ? $user_id : ($_SESSION['user_id'] ?? null),
            "log_event_id" => $log_event_id,
            "object_type" => $object_type,
            "object_id" => $object_id,
            "changed_fields" => self::constructChangedFields($new_values, $object_id, $object_type),
            "additional_info" => $additional_info,
            "error" => $isError ? 1 : 0,
            "backtrace" => Backtrace::toCsv(\Spatie\Backtrace\Backtrace::create()
                ->applicationPath(dirname(__DIR__, 2))
                ->withArguments()
                ->frames())
        ]);

        self::deleteOldLogs();
    }

    public static function constructChangedFields(array $changed_fields, $object_id, $object_type): ?string
    {
        self::encodeValuesToJson($changed_fields);

        if ($object_id !== 0) {

            $old_fields = self::buildOldFields($changed_fields, $object_type, $object_id);

            if (!empty($old_fields)) {
                $changed_fields = self::buildActuallyChangedFields($changed_fields, $old_fields);
            }
        }

        return $changed_fields ? http_build_query($changed_fields) : null;
    }

    private static function encodeValuesToJson(array &$array): void
    {
        foreach ($array as &$value) {
            $value = json_encode($value);
        }
    }

    public static function getLastLogEntryForObjectContainingSpecifiedField($object_type, $object_id, string $field_name): mixed
    {
        $operator = is_null($object_id) ? 'is null' : '=';
        $field_name_encoded = urlencode($field_name);
        return get_one("SELECT changed_fields 
                                        FROM log 
                                        WHERE object_type = '" . addslashes($object_type) . "'
                                          AND object_id $operator $object_id
                                          AND changed_fields LIKE '%$field_name_encoded=%'
                                        ORDER BY log_id DESC LIMIT 1");
    }

    private static function decodeFields($fieldsAsUrlEncodedText): array
    {
        if (empty($fieldsAsUrlEncodedText)) {
            return [];
        }

        $result = [];

        $fieldsAsUrlEncodedText = str_replace('+', ' ', $fieldsAsUrlEncodedText);
        $fieldsAsUrlEncodedText = explode('&', rawurldecode($fieldsAsUrlEncodedText));

        foreach ($fieldsAsUrlEncodedText as $field) {
            list($field_name, $field_value) = explode('=', $field);
            $result[$field_name] = $field_value;
        }

        return $result;
    }

    public static function getChangedFields($changed_fields): string
    {
        $fields = [];

        $changed_fields = self::decodeFields($changed_fields);

        foreach ($changed_fields as $field => $value) {

            $fields[] = $field . ': ' . $value;
        }

        return implode("\n", $fields);
    }

    public static function convertArgumentsToLabels(mixed $arguments): string
    {
        $result = '';
        if (!empty($arguments)) {
            foreach ($arguments as $argumentName => $argumentValue) {
                $argumentValue = Backtrace::convertToJsonIfArrayOrObject($argumentValue);
                $result .= Backtrace::makeLabel($argumentName, $argumentValue);
            }
        }
        return trim($result);
    }

    public static function makeKeysBold(mixed $additional_info): string
    {
        $additional_info = explode("\n", $additional_info);
        foreach ($additional_info as &$item) {
            $item = preg_replace('/([\w_\[\]-]+): (.*)/', '<b>$1:</b> $2', $item);
        }
        return implode("\n", $additional_info);
    }

    public static function deleteOldLogs(): void
    {
        delete("log", "logged_at < DATE_ADD(NOW(), INTERVAL -6 MONTH)");
    }

    private static function buildActuallyChangedFields(array $changed_fields, array $old_fields): array
    {
        $actually_changed_fields = [];

        foreach ($changed_fields as $field_name => $field_value) {

            if (self::fieldExistsInLogsWithDifferentValue($old_fields, $field_name, $field_value)) {
                self::addToListOfActuallyChangedFields( $actually_changed_fields, $field_name,$field_value);
            }
        }
        return $actually_changed_fields;
    }

    private static function buildOldFields(array $changed_fields, $object_type, $object_id): array
    {
        foreach (array_keys($changed_fields) as $field_name) {

            $ChangedFieldsAsTextOfLastLogEntryContainingField = self::getLastLogEntryForObjectContainingSpecifiedField(
                $object_type,
                $object_id,
                $field_name);

            if (!$ChangedFieldsAsTextOfLastLogEntryContainingField) {
                $old_fields[$field_name] = false;
                continue;
            }

            $lastLogEntryChangedFields =
                self::decodeFields($ChangedFieldsAsTextOfLastLogEntryContainingField);

            $old_fields[$field_name] = $lastLogEntryChangedFields[$field_name] ?? null;
        }
        return $old_fields;
    }

    private static function convertArgumentsFromJsonToArray(array &$call): void
    {
        $call[3] = self::convertJsonToArray($call[3]);
    }

    private static function fieldExistsInLogsWithDifferentValue(array $old_fields, int|string $field_name, mixed $field_value): bool
    {
        return isset($old_fields[$field_name]) && $old_fields[$field_name] !== $field_value;
    }

    private static function addToListOfActuallyChangedFields(array &$actually_changed_fields, int|string $field_name, mixed $field_value): void
    {
        $actually_changed_fields[$field_name] = $field_value;
    }


}