<?php

namespace Broneerimiskeskkond;

class Orders
{
    private static $existing_orders = null;

    static function get_orders_by_date($date)
    {
        return get_all("SELECT DISTINCT order_id, email, send_reminder
                             FROM bookings 
                             JOIN orders USING (order_id) 
                             JOIN users USING (user_id) 
                             WHERE booking_start BETWEEN '{$date}' AND '{$date} 23:59:59' AND orders.deleted = 0 AND bookings.deleted=0");
    }

    static function find_by_user($user_id)
    {
        // Serve from cache, if possible
        if (!is_null(self::$existing_orders))
            return self::$existing_orders;

        $orders = [];
        $rows = get_all("
            SELECT *,
                   date(booking_start) AS event_date,
                concat(TIME_FORMAT(booking_start, '%H:%i'), ' - ', TIME_FORMAT(booking_end, '%H:%i')) AS booking_time,
                service_id
            FROM orders
            LEFT JOIN order_services USING (order_id)
            LEFT JOIN bookings USING (order_id)
            LEFT JOIN rooms USING (room_id)
            WHERE user_id = '$user_id'
            AND orders.deleted = 0
            AND bookings.deleted = 0
            AND date_add(DATE(booking_start), INTERVAL 7 DAY) > DATE(NOW())
            ORDER BY order_id DESC");

        //Reorganize orders by order_id

        foreach ($rows as $row) {
            $orders[$row['order_id']]['order_id'] = $row['order_id'];
            $orders[$row['order_id']]['room_names'][$row['room_id']] = $row['room_name'];
            $orders[$row['order_id']]['event_date'] = $row['event_date'];
            $orders[$row['order_id']]['booking_time'] = $row['booking_time'];
            $orders[$row['order_id']]['coordinator_name'] = $row['coordinator_name'];
        }

        // Serialize room names
        foreach ($orders as &$order) {
            $order['room_names'] = implode(', ', $order['room_names']);
        }

        // Cache the result
        self::$existing_orders = $orders;

        return $orders;
    }

    static function get_bookings_data($user_id)
    {
        $booking_data = get_all("SELECT order_id, booking_start, booking_end, room_name
                                      FROM orders LEFT JOIN bookings USING (order_id) LEFT JOIN rooms USING (room_id)
                                      WHERE user_id='$user_id' AND orders.deleted = 0 AND bookings.deleted = 0
                                      ORDER BY order_id desc");
        return $booking_data;
    }

    static function get_deposit_price($order_id)
    {
        return get_one("SELECT sum((unix_timestamp(bookings.booking_end) - unix_timestamp(bookings.booking_start)) / 3600 * room_price) * 0.25 
                             FROM bookings 
                             JOIN orders USING (order_id)
                             JOIN rooms USING (room_id)
                             WHERE order_id = $order_id");
    }

    static function create($company_id, $coordinator = '', $invoice_additional_info = '')
    {
        if (empty($_SESSION['booking_date'])) {
            throw new \Exception('Cannot create order because session did not contain booking_date');
        }

        $extras = Extras::translateNames(Extras::get());
        $designs = Designs::get();
        $supplies = Supplies::get();
        $services = Service::translateNames(Service::getAll());


        $booking_date = $_SESSION['booking_date'];

        // Orders table
        $order['order_made'] = date("Y-m-d H:i:s");
        $order['user_id'] = $_SESSION['user_id'];
        $order['payment_status_id'] = PaymentStatus::NO_INVOICE_SENT;
        $order['coordinator_name'] = $coordinator;
        $order['total_number_of_people'] = $_SESSION['number_of_people'];
        $order['special_room_selected'] = Cart::specialRoomSelected() ? 1 : 0;
        $order['company_id'] = $company_id;
        $order['invoice_additional_info'] = $invoice_additional_info;
        $order['special_room_name'] = empty($order['special_room_selected']) ? null : $_SESSION['selected_special_room']['room_name'];
        $order['special_room_price'] = Cart::specialRoomSelected() ? Cart::getRoomsPrice() : 0;
        $order['discount_percent'] = User::get()['user_discount_percent'];
        $order['discount_level'] = User::get()['discount_level'];
        $order_id = insert('orders', $order);

        if (!empty($_SESSION['selected_services'])) {
            foreach ($_SESSION['selected_services'] as $service_id => $service_quantity) {
                $s = $services[$service_id];
                insert('order_services', [
                    'order_id' => $order_id,
                    'order_service_name' => $s['service_name_in_order_summary'],
                    'service_id' => $service_id,
                    'order_service_price' => $s['service_price'],
                    'order_service_quantity' => (int)$service_quantity,
                    'order_service_unit' => $service_quantity == 1 ? $s['service_unit_singular'] : $s['service_unit_plural']
                ]);
            }
        }

        // Bookings table
        foreach ($_SESSION['selected_rooms'] as $selected_room) {

            $room_id = $selected_room['room_id'];

            $booking['room_id'] = $room_id;
            $booking['booking_start'] = "$booking_date $selected_room[booking_start_time]";
            $booking['booking_end'] = "$booking_date $selected_room[booking_end_time]";
            $booking['order_id'] = $order_id;
            $booking['room_type'] = $_SESSION['selected_rooms'][$room_id]['room_type'];
            $booking['booking_room_price'] = $_SESSION['selected_rooms'][$room_id]['room_price'];

            $booking_id = insert('bookings', $booking);

            if (isset($selected_room['supplies'])) {
                $booking_supplies = $selected_room['supplies'];
                foreach ($booking_supplies as $supply_id => $quantity) {
                    insert('booking_supplies', [
                        'booking_id' => $booking_id,
                        'supply_id' => $supply_id,
                        'quantity' => $quantity,
                        'booking_supply_price' => $supplies[$supply_id]['supply_price']
                    ]);
                }
            }

            if (isset($selected_room['extras'])) {
                $booking_extras = $selected_room['extras'];
                foreach ($booking_extras as $extra_id => $quantity) {
                    insert('booking_extras', [
                        'booking_id' => $booking_id,
                        'extra_id' => $extra_id,
                        'booking_extra_price' => $extras[$extra_id]['extra_price'],
                        'booking_extra_name' => $extras[$extra_id]['extra_name'],
                        'booking_extra_unit' => $extras[$extra_id]['extra_unit']
                    ]);
                }
            }

            if (isset($selected_room['designs'])) {
                $booking_designs = $selected_room['designs'];
                foreach ($booking_designs as $design_id => $price) {
                    insert('booking_designs', [
                        'booking_id' => $booking_id,
                        'design_id' => $design_id,
                        'booking_design_price' => $designs[$design_id]['design_price']
                    ]);
                }
            }
        }

        // Associate user with selected company
        update('users', [
            'company_id' => $company_id
        ], "user_id = $_SESSION[user_id]");

        return $order_id;
    }

    static function get($order_id)
    {
        $order['sum'] = 0;
        $order['order_service_price'] = 0;
        $order['order_service_time'] = 0;
        $order['services'] = [];

        $data = get_all("SELECT
                                *,
                                (unix_timestamp(bookings.booking_end) - unix_timestamp(bookings.booking_start)) / 3600 AS booking_duration,
                                date(booking_start)                                     AS booking_start_date,
                                date(booking_end)                                       AS booking_end_date,
                                TIME_FORMAT(booking_start, '%H:%i')                     AS booking_start_time,
                                TIME_FORMAT(booking_end, '%H:%i')                       AS booking_end_time
                              FROM orders
                                LEFT JOIN companies USING (company_id)
                                LEFT JOIN users USING (user_id)
                                LEFT JOIN bookings USING (order_id)
                                LEFT JOIN rooms USING (room_id)
                                LEFT JOIN booking_supplies USING (booking_id)
                                LEFT JOIN supplies USING (supply_id)
                                LEFT JOIN booking_extras USING (booking_id)
                                LEFT JOIN extras USING (extra_id)
                                LEFT JOIN booking_designs USING (booking_id)
                                LEFT JOIN designs USING (design_id)
                                LEFT JOIN payment_statuses USING (payment_status_id)
                                LEFT JOIN order_services USING (order_id)
                                LEFT JOIN services USING (service_id)
                              WHERE
                                orders.order_id = $order_id AND bookings.deleted=0 AND orders.deleted=0");


        if (empty($data)) {
            throw new \Exception('No such order');
        }

        foreach ($data as $row) {
            $order['order_id'] = $row['order_id'];
            $order['invoice_additional_info'] = $row['invoice_additional_info'];
            $order['booking_duration'] = (float)$row['booking_duration'];
            $order['booking_end_time'] = $row['booking_end_time'];
            $order['booking_start_date'] = $row['booking_start_date'];
            $order['booking_start_time'] = $row['booking_start_time'];
            $order['coordinator_name'] = $row['coordinator_name'];
            $order['special_room_selected'] = $row['special_room_selected'];
            $order['payment_status_id'] = $row['payment_status_id'];
            $order['payment_status_name'] = $row['payment_status_name'];
            $order['deposit_invoice_id'] = $row['deposit_invoice_id'];
            $order['deposit_invoice_number'] = $row['deposit_invoice_number'];
            $order['user_id'] = $row['user_id'];
            $order['invoice_email'] = $row['invoice_email'] ?: $row['email'];
            $order['discount_percent'] = $row['discount_percent'];
            $order['discount_level'] = $row['discount_level'];
            $order['user_name'] = $row['first_and_last_name'];
            $order['user_organisation'] = $row['company_name'];
            $order['special_room_name'] = $row['special_room_name'];
            $order['special_room_price'] = $row['special_room_price'];
            $order['company_name'] = $row['company_name'];
            $order['company_id'] = $row['company_id'];
            $order['order_billed_rent_percentage'] = $row['order_billed_rent_percentage'];
            $order['order_remarks'] = $row['order_remarks'];
            $order['total_number_of_people'] = $row['total_number_of_people'];

            // Rooms
            $order['rooms'][$row['room_id']]['room_id'] = $row['room_id'];
            $order['rooms'][$row['room_id']]['room_name'] = $row['room_name'];
            $order['rooms'][$row['room_id']]['room_price'] = (float)$row['booking_room_price'];
            $order['rooms'][$row['room_id']]['booking_id'] = $row['booking_id'];
            $order['rooms'][$row['room_id']]['booking_start_date'] = $row['booking_start_date'];
            $order['rooms'][$row['room_id']]['booking_end_date'] = $row['booking_end_date'];
            $order['rooms'][$row['room_id']]['booking_start_time'] = $row['booking_start_time'];
            $order['rooms'][$row['room_id']]['booking_end_time'] = $row['booking_end_time'];
            $order['rooms'][$row['room_id']]['booking_duration'] = $row['booking_duration'];


            // Room extras
            if (!empty($row['extra_id'])) {
                $order['rooms'][$row['room_id']]['extras'][$row['extra_id']] = [
                    'id' => $row['extra_id'],
                    'name' => $row['booking_extra_name'],
                    'price' => (float)$row['booking_extra_price'],
                    'selected' => true,
                    'unit' => $row['booking_extra_unit']
                ];
            }

            // Room designs
            if (!empty($row['design_id'])) {
                $order['rooms'][$row['room_id']]['designs'][$row['design_id']]['name'] = $row['design_name'];
                $order['rooms'][$row['room_id']]['designs'][$row['design_id']]['price'] = (float)$row['booking_design_price'];
            }

            // Room supplies
            if (!empty($row['supply_id'])) {
                $order['rooms'][$row['room_id']]['supplies'][$row['supply_id']]['name'] = $row['supply_name'];
                $order['rooms'][$row['room_id']]['supplies'][$row['supply_id']]['amount'] = $row['quantity'];
                $order['rooms'][$row['room_id']]['supplies'][$row['supply_id']]['price'] = (float)$row['booking_supply_price'];
            }

            // Services
            if (!empty($row['service_id'])) {
                $order['services'][$row['service_id']]['service_id'] = $row['service_id'];
                $order['services'][$row['service_id']]['service_name'] = $row['order_service_name'];
                $order['services'][$row['service_id']]['service_name_in_order_summary'] = $row['order_service_name'];
                $order['services'][$row['service_id']]['service_price'] = $row['order_service_price'];
                $order['services'][$row['service_id']]['quantity'] = $row['order_service_quantity'];
                $order['services'][$row['service_id']]['service_group_id'] = $row['service_group_id'];
                $order['services'][$row['service_id']]['service_unit_plural'] = $row['service_unit_plural'];
                $order['services'][$row['service_id']]['service_unit_singular'] = $row['service_unit_singular'];
            }
        }

        // Calculate room sum
        if ($order['special_room_selected'] == 1) {

            // Set whole studio price
            $order['sum'] = $order['special_room_price'] * $order['booking_duration'];

            // Reset individual room prices to zero
            foreach ($order['rooms'] as &$room) {
                $room['room_price'] = 0;
            }

            // Prevent overwriting the last member of the rooms array in the next foreach
            // See the warning at https://www.php.net/manual/en/control-structures.foreach.php)
            unset($room);

        } else {

            foreach ($order['rooms'] as $room) {

                $order['sum'] += $room['booking_duration'] * $room['room_price'];
            }
        }

        // Set different room name and price in case of whole studio
        if ($order['special_room_selected'] == 1) {

            // Prepend a new fake room as the first room
            array_unshift($order['rooms'], [
                'room_id' => 101,
                'room_name' => $order['special_room_name'],
                'room_price' => $order['special_room_price']
            ]);
        }

        // Calculate discount from rooms sum
        $order['discount'] = $order['sum'] * $order['discount_percent'] * -1;

        // Apply discount to order sum
        $order['sum'] += $order['discount'];

        // Add extras/supplies/designs
        foreach ($order['rooms'] as $room) {
            if (isset($room['extras'])) {
                foreach ($room['extras'] as $extra) {
                    $order['sum'] += (float)$extra['price'];
                }
            }

            if (isset($room['designs'])) {
                foreach ($room['designs'] as $design) {
                    $order['sum'] += (float)$design['price'];
                }
            }

            if (isset($room['supplies'])) {
                foreach ($room['supplies'] as $supply) {
                    $order['sum'] += $supply['price'] * $supply['amount'];
                }
            }
        }

        foreach ($order['services'] as $service) {
            $order['sum'] += $service['service_price'] * $service['quantity'];
        }
        return $order;
    }

    public static function removeService($service_id, $order_id)
    {
        // TODO: Add security and time check before removing service from order
        q("DELETE FROM order_services WHERE order_id = $order_id AND service_id = $service_id");
    }

    public static function addService($service_id, $quantity, $order_id)
    {
        $services = Service::getAll();

        // Save the service to order
        insert('order_services', [
            'order_id' => $order_id,
            'service_id' => $service_id,
            'order_service_name' => $services[$service_id]['service_name'],
            'order_service_price' => $services[$service_id]['service_price'],
            'order_service_quantity' => $quantity,
        ]);
    }

    public static function getRoomNames($rooms)
    {
        $result = [];

        foreach ($rooms as $room) {
            $result[] = $room['room_name'];
        }

        return $result;
    }

    public static function addExtra($room_id, $extra_id, $order_id)
    {

        validate($room_id);
        validate($extra_id);
        validate($order_id);

        // Get booking_id for specified room in specified order
        $booking_id = Bookings::get_id_by_order_id_and_room_id($order_id, $room_id);

        validate($booking_id);

        $extra = Extras::translateNames(Extras::get())[$extra_id];

        insert('booking_extras', [
            'booking_id' => $booking_id,
            'extra_id' => $extra_id,
            'booking_extra_price' => $extra['extra_price'],
            'booking_extra_name' => $extra['extra_name'],
            'booking_extra_unit' => $extra['extra_unit']
        ]);
    }

    public static function removeExtra($room_id, $extra_id, $order_id)
    {

        validate($room_id);
        validate($extra_id);
        validate($order_id);

        // Get booking_id for specified room in specified order
        $booking_id = Bookings::get_id_by_order_id_and_room_id($order_id, $room_id);

        validate($booking_id);

        // Delete extra
        q("DELETE FROM booking_extras WHERE booking_id = $booking_id AND extra_id = $extra_id");

    }

    static function getAll(array $criteria = [], $include_deleted = false)
    {
        if (!$include_deleted) {
            $criteria += ['orders.deleted' => 0];
        }

        $where = SQL::getWhere($criteria, "order_id");
        return get_all("SELECT * FROM orders 
            LEFT JOIN bookings USING (order_id) 
            LEFT JOIN users USING (user_id) 
            $where 
            GROUP BY order_id");
    }

    /**
     * @param $order_id
     * @param $new_billed_rent_percentage
     * @return void
     */
    public static function updateBilledRentPercentage($order_id, $new_billed_rent_percentage): void
    {
        update(
            'orders',
            ['order_billed_rent_percentage' => $new_billed_rent_percentage],
            "order_id=$order_id"
        );
    }

    /**
     * @param int $order_id
     * @param $status_id
     * @return void
     */
    public static function updateStatus(int $order_id, $status_id): void
    {
        update('orders', ['payment_status_id' => $status_id], "order_id = $order_id");
    }

    public static function addRemark(mixed $order_id, mixed $remark)
    {
        update('orders', ['order_remarks' => $remark], "order_id = $order_id");
    }

    /**
     * Returns all orders with their rooms, extras, designs, supplies and services
     * @param array $criteria - array of criteria to filter orders by
     * @param string|null $search - search string
     * @param string $order_by - order by column
     * @param string $order_direction - order direction
     * @return array - array of orders
     */
    public static function get_structured(array $criteria = [], string $search = null, string $order_by = 'orders.order_id', string $order_direction = 'DESC'): array
    {

        // Generate search condition
        if ($search) {
            $search = addslashes($search);
            $criteria[] = " (
                orders.order_id LIKE '%$search%'
                OR order_made LIKE '%$search%'
                OR booking_start LIKE '%$search%'
                OR first_and_last_name LIKE '%$search%'
                OR coordinator_name LIKE '%$search%'
                OR invoice_additional_info LIKE '%$search%'
                OR company_name LIKE '%$search%'
            )";
        }

        $criteria[] = "orders.deleted = 0";
        $where = SQL::getWhere($criteria, "order_id");
        $orders = get_all("
            SELECT orders.*, users.*, payment_statuses.*, bookings.*, rooms.*, companies.*
            FROM orders
            LEFT JOIN users USING (user_id)
            LEFT JOIN payment_statuses USING (payment_status_id)
            LEFT JOIN bookings ON orders.order_id = bookings.order_id
            LEFT JOIN rooms USING (room_id)
            LEFT JOIN companies ON orders.company_id = companies.company_id
            $where
            ORDER BY $order_by $order_direction
        ");

        // Re-organize rooms under 'rooms'
        foreach ($orders as $order) {
            $result[$order['order_id']]['payment_status_id'] = $order['payment_status_id'];
            $result[$order['order_id']]['payment_status_name'] = $order['payment_status_name'];
            $result[$order['order_id']]['order_id'] = $order['order_id'];
            $result[$order['order_id']]['user_id'] = $order['user_id'];
            $result[$order['order_id']]['order_made'] = $order['order_made'];
            $result[$order['order_id']]['deleted'] = $order['deleted'];
            $result[$order['order_id']]['email'] = $order['email'];
            $result[$order['order_id']]['coordinator_name'] = $order['coordinator_name'];
            $result[$order['order_id']]['first_and_last_name'] = $order['first_and_last_name'];
            $result[$order['order_id']]['organisation'] = $order['company_name'];
            $result[$order['order_id']]['state_institution'] = $order['state_institution'];
            $result[$order['order_id']]['dont_send_invoice'] = $order['dont_send_invoice'];
            $result[$order['order_id']]['booking_id'] = $order['booking_id'];
            $result[$order['order_id']]['booking_start'] = $order['booking_start'];
            $result[$order['order_id']]['booking_end'] = $order['booking_end'];
            $result[$order['order_id']]['rooms'][$order['room_id']] = $order;
            $result[$order['order_id']]['total_number_of_people'] = $order['total_number_of_people'];
            $result[$order['order_id']]['invoice_additional_info'] = $order['invoice_additional_info'];
            $result[$order['order_id']]['send_reminder'] = $order['send_reminder'];
            $result[$order['order_id']]['order_billed_rent_percentage'] = $order['order_billed_rent_percentage'];
        }

        return $result ?? [];
    }


}
