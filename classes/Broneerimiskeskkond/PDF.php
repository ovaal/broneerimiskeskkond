<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 02/06/16
 * Time: 17:45
 */

namespace Broneerimiskeskkond;


class PDF
{
    const FILE = 'F';
    const DOWNLOAD = 'D';

    static function create($title, $data, $user, $file, $destination)
    {

        // Deny access to other user's orders
        if (!$user->is_admin && $data['user_id'] != $user->user_id) {
            error_out(__('Permission denied'));
        }

        // Specify quantities (in parentheses) in service names which have other quantity than 1
        $data['services'] = Service::addQuantitySpecificationToservice($data['services']);

        ob_start();
        include 'templates/pdf/order_summary.php';
        $html = ob_get_clean();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->Output($file, $destination);

    }


}
