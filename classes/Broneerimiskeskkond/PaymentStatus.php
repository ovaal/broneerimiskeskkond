<?php

namespace Broneerimiskeskkond;

class PaymentStatus
{
    public const NO_INVOICE_SENT= 1; // Used for state institutions when letter of guarantee request email has been sent.
    public const DEPOSIT_INVOICE_SENT = 2; // When initial booking payment invoice has been sent (25%-100% rent)
    public const FINAL_INVOICE_SENT = 3; // When final invoice has been sent(remaining rent and extras)
    public const ORDER_CANCELLED = 4; // Currently not in use
    public const INVOICE_IN_MAKSEKESKUS = 5; // Currently not in use
    public const DEPOSIT_INVOICE_NOT_SENT = 6; // When system is about to send deposit invoice
    public const FINAL_INVOICE_NOT_SENT = 7; // When system is about to send final invoice

    // Historical
    public const INVOICE_COMPLETED = 1;
    public const INVOICE_WAITING_FOR_EVENT = 2;
    public const INVOICE_CANCELLED_HISTORICAL = 3;
    public const INVOICE_TO_SEND = 4;
    public const INVOICE_SENT = 5;
    public const INVOICE_IN_MAKSEKESKUS_HISTORICAL = 6;
    public const INVOICE_NEW = 7;
}