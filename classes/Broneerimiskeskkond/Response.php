<?php

namespace Broneerimiskeskkond;

use HTMLPurifier;
use HTMLPurifier_Config;
use JetBrains\PhpStorm\NoReturn;
use League\HTMLToMarkdown\HtmlConverter;
use Markdownify\ConverterExtra;

class Response
{

    /**
     * @return bool
     */
    #[NoReturn] public static function showHtmlErrorPage($errorMessage): void
    {
        $errors[] = $errorMessage;
        require 'templates/error_template.php';
        exit();

    }

    #[NoReturn] public static function outputAsPlainText($data, $strip_tags = true, $remove_whitespace = true, $remove_newlines = false): void
    {
        $converter = new ConverterExtra;
        $data = $converter->parseString($data);

        if ($strip_tags) {
            $data = strip_tags($data);
        }

        if ($remove_newlines) {
            $data = preg_replace('/(\r?\n)+/', ' ', $data);
        }

        if ($remove_whitespace) {
            $data = preg_replace('/\t+/', ' ', $data);
            $data = preg_replace('/ +/', ' ', $data);
        }

        exit($data);
    }

    /**
     * @param array $data
     * @return void
     */
    #[NoReturn] public static function outputAsJson(array $data): void
    {
        exit(json_encode($data));
    }

}