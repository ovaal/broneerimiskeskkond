<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 19.04.2016
 * Time: 13:38
 */

namespace Broneerimiskeskkond;


class Room
{

    static function get_available_rooms($date, $start_time, $end_time, $number_of_people, $special_room_id = false)
    {
        // Require all arguments
        if (empty($date) or empty($start_time) or empty($end_time) or empty($number_of_people)) {
            if (!$special_room_id) {
                $rooms = get_all("SELECT * FROM rooms WHERE room_visible = 1 ORDER BY room_max_persons DESC");
                return Images::insert_first_image($rooms, $special_room_id);
            } else {
                $sql = "SELECT * FROM special_rooms WHERE special_room_id = $special_room_id";
                return Images::insert_first_image(get_all($sql), $special_room_id);
            }
        }

        $where = array();


        // Build WHERE part
        $booking_date = addslashes($date);
        $booking_start_time = addslashes($start_time);
        $booking_end_time = addslashes($end_time);
        $booking_start = "$booking_date $booking_start_time";
        $booking_end = "$booking_date $booking_end_time";
        $where[] = 'room_visible = 1';
        $where[] = "room_id != 3 AND (DATE(booking_start) = '$booking_date' OR 
        DATE(booking_start) != '$booking_date') AND NOT 
        ('$booking_start' < booking_end AND 
        '$booking_end' > booking_start)";

        // Stringify $where
        $where = implode(' and ', $where);

        $where .= " AND rooms.room_max_persons >= $number_of_people OR 
        (booking_id IS NULL AND rooms.room_max_persons >= $number_of_people)";

        // Prepend WHERE to $where
        if (!empty ($where)) {
            $where = 'WHERE ' . $where;
        }

        // Execute query
        if (!$special_room_id) {
            $sql = "SELECT * FROM rooms LEFT JOIN bookings USING (room_id) $where GROUP BY room_id ORDER BY room_max_persons DESC";
        } else {
            $sql = "SELECT * FROM special_rooms WHERE special_room_id = $special_room_id";
        }

        return Images::insert_first_image(get_all($sql), $special_room_id);
    }


    static function get_all($date, $start_time, $end_time, $special_room_id = false)
    {
        // Require all arguments
        if (empty($date) or empty($start_time) or empty($end_time)) {
            if (!$special_room_id) {
                $rooms = get_all("SELECT * FROM rooms ORDER BY room_max_persons DESC");
                return Images::insert_first_image($rooms, $special_room_id);
            } else {
                $sql = "SELECT * FROM special_rooms WHERE special_room_id = $special_room_id";
                return Images::insert_first_image(get_all($sql), $special_room_id);
            }
        }

        // Build WHERE part
        $where = "WHERE room_id != 3 OR booking_id IS NULL";

        // Execute query
        if (!$special_room_id) {
            $sql = "SELECT * FROM rooms LEFT JOIN bookings USING (room_id) $where GROUP BY room_id ORDER BY room_max_persons DESC";
        } else {
            $sql = "SELECT * FROM special_rooms WHERE special_room_id = $special_room_id";
        }

        return Images::insert_first_image(get_all($sql), $special_room_id);
    }

    public static function get($room_id)
    {
        $room_id = addslashes($room_id);
        return get_first("SELECT * FROM rooms WHERE room_id = $room_id");
    }

    public static function get_special($room_id)
    {
        $room_id = addslashes($room_id);
        $room = get_first("SELECT * FROM special_rooms WHERE special_room_id = $room_id");

        // The rooms/3 view doesn't know if it shows normal or special room, so we must harmonize the room fields
        return [
            'room_id' => $room['special_room_id'],
            'room_name' => $room['special_room_name'],
            'room_max_persons' => $room['special_room_max_persons'],
            'room_description' => $room['special_room_description'],
            'room_message' => $room['special_room_message'],
            'room_slogan' => $room['special_room_slogan'],
            'room_price' => $room['special_room_price'],
            'room_size' => $room['special_room_size'],
            'room_seats' => $room['special_room_seats'],
            'room_seat_type' => $room['special_room_seat_type'],
            'room_class' => $room['special_room_class'],
            'room_visible' => 1
        ];
    }

    public static function save_search_parameters_to_session()
    {
        $_SESSION['booking_date'] = self::getSessionBookingDate();
        $_SESSION['booking_start_time'] = empty($_REQUEST['booking_start_time']) ? (empty($_SESSION['booking_start_time']) ? "08:00" : $_SESSION['booking_start_time']) : $_REQUEST['booking_start_time'];
        $_SESSION['booking_end_time'] = empty($_REQUEST['booking_end_time']) ? (empty($_SESSION['booking_end_time']) ? "18:00" : $_SESSION['booking_end_time']) : $_REQUEST['booking_end_time'];
        $_SESSION['number_of_people'] = empty($_REQUEST['number_of_people']) ? (empty($_SESSION['number_of_people']) ? "" : $_SESSION['number_of_people']) : $_REQUEST['number_of_people'];

        // Booking duration in hours (for moment.js)
        $_SESSION['booking_duration'] = (strtotime($_SESSION['booking_end_time']) - strtotime($_SESSION['booking_start_time'])) / 3600;

    }

    public static function get_extras($room_id = NULL)
    {
        if ($room_id == NULL) {
            $extras = get_all("SELECT * FROM extras WHERE extra_in_use=1");
        } else {
            $extras = get_all("SELECT * FROM room_extras LEFT JOIN extras USING (extra_id) WHERE extra_in_use=1 AND room_id=$room_id");
        }

        return $extras;
    }

    public static function get_designs($room_id = NULL)
    {
        if ($room_id == NULL) {
            $designs = get_all("SELECT * FROM designs WHERE design_in_use=1");
        } else {
            $designs = get_all("SELECT * FROM room_designs LEFT JOIN designs USING (design_id) WHERE design_in_use=1 AND room_id=$room_id");
        }

        return $designs;
    }

    public static function get_supplies()
    {
        return get_all("SELECT * FROM supplies WHERE supply_in_use=1");
    }

    public static function get_booking_time_options($start_hour, $start_min, $end_hour, $end_min)
    {
        $options = [];
        for ($hours = $start_hour; $hours <= $end_hour; $hours++) {
            $this_hour_start_min = $hours == $start_hour ? $start_min : 0;
            $this_hour_end_min = $hours == $end_hour ? $end_min : 59;
            for ($mins = $this_hour_start_min; $mins <= $this_hour_end_min; $mins += 30)
                $options[] = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':'
                    . str_pad($mins, 2, '0', STR_PAD_LEFT);
        }
        return $options;

    }

    public static function get_all_bookings_js($room_id)
    {
        return get_all("SELECT 'Booked' title, 
                        booking_start start, 
                        booking_end end 
                        FROM bookings
                          LEFT JOIN orders USING (order_id)
                        WHERE room_id = $room_id
                          AND (orders.deleted = 0 OR orders.deleted IS NULL)
                          AND bookings.deleted = 0");
    }

    public static function get_available_bookings($room_id)
    {
        // List of times to book in (e.g: 8:00 - 18:00)
        $booking_time_list = \Broneerimiskeskkond\Room::get_booking_time_options(8, 00, 18, 00);

        // Bookings for selected date
        $bookings = get_all("
                SELECT
                  substring_index(TIME(booking_start),':',2) AS start,
                  substring_index(TIME(booking_end),':',2) AS end
                FROM bookings 
                  LEFT JOIN orders USING (order_id)
                WHERE 
                  DATE(booking_start)= '{$_SESSION['booking_date']}' 
                  AND room_id = $room_id 
                  AND orders.deleted = 0
                GROUP BY DATE(booking_start)
                ORDER BY booking_start");

        foreach ($bookings as $booking) {
            // Search array keys from the booking times list
            $booking_start_key = array_search($booking['start'], $booking_time_list);
            $booking_end_key = array_search($booking['end'], $booking_time_list);

            // Unset the booked times from the booking times list
            foreach (range($booking_start_key, $booking_end_key) as $key) {
                unset($booking_time_list[$key]);
            }
        }

        $previous_key = null;
        $available_time_ranges = [];
        $consecutive_range_of_time_keys = [];

        // Slice array by consecutive sequences
        foreach (array_keys($booking_time_list) as $current_key) {
            // Put current key into $consecutive_range_of_time_keys when there is no gap with previous time option
            if ($current_key == $previous_key + 1) {
                $consecutive_range_of_time_keys[] = $current_key;
            } else {
                $available_time_ranges[] = $consecutive_range_of_time_keys;

                // Start new range of keys
                $consecutive_range_of_time_keys = array($current_key);
            }

            $previous_key = $current_key;
        }

        $available_time_ranges[] = $consecutive_range_of_time_keys;

        if (isset($available_time_ranges[2])) {
            unset($available_time_ranges[2]);
        }

        unset($available_time_ranges[0]); // Unset the empty member

        $there_are_bookable_time_ranges = false;
        foreach ($available_time_ranges as $available_time_range) {

            $first_time_key = current($available_time_range);
            $last_time_key = end($available_time_range);

            // If start time is not 08:00, add 30 minutes to it
            if ($booking_time_list[$first_time_key] != '08:00') {
                $first_time_key += 1;
            }

            // If end time is not 18:00, remove 30 minutes from it
            if ($booking_time_list[$last_time_key] != '18:00') {
                $last_time_key -= 1;
            }

            $interval_minutes = (strtotime($booking_time_list[$last_time_key]) - strtotime($booking_time_list[$first_time_key])) / 60;

            if (count($bookings) == 0) { // If no bookings are made
                echo '<p>07:00 - 23:59</p>';
            } elseif ($interval_minutes >= 240) { // Don't let the interval in minutes be less than 240minutes
                $there_are_bookable_time_ranges = true;
                echo '<p>' . implode(" - ", array($booking_time_list[$first_time_key], $booking_time_list[$last_time_key])) . '</p>';
            }
        }

        if (count($bookings) != 0 && !$there_are_bookable_time_ranges) {
            echo '<div class="bookedAllDay">' . __('The room is booked for the whole day!') . '</div>';
        }

    }

    static function get_current_prices($duration = 1)
    {
        $sum = 0;
        if (!empty($_SESSION['selected_special_room'])) {
            $room_id = (int)$_SESSION['selected_special_room']['room_id'];
            $sum = round(get_one("SELECT special_room_price FROM special_rooms WHERE special_room_id = $room_id") * $duration, 2);
        } else {
            foreach ($_SESSION['selected_rooms'] as $selected_room) {
                $sum += $selected_room['room_price'] * $duration;
            }
        }
        return $sum;

    }

    static function get_session_extra_price()
    {
        $extras = \Broneerimiskeskkond\Room::get_extras();
        $extra_price_array = [];
        foreach ($_SESSION['selected_rooms'] as $selected_room) {
            $room_id = $selected_room['room_id'];
            foreach ($extras as $extra) {
                if (isset($_SESSION['selected_rooms'][$room_id]['extras'][$extra['extra_id']])) {
                    $extra_price_array[] = $extra['extra_price'];
                }
            }
        }

        return array_sum($extra_price_array);

    }

    static function get_session_supply_price()
    {
        $supplies = \Broneerimiskeskkond\Room::get_supplies();
        $supply_price_array = [];
        foreach ($_SESSION['selected_rooms'] as $selected_room) {
            $room_id = $selected_room['room_id'];
            foreach ($supplies as $supply) {
                if (isset($_SESSION['selected_rooms'][$room_id]['supplies'][$supply['supply_id']])) {
                    $supply_price_array[] = $supply['supply_price'] * $_SESSION['selected_rooms'][$room_id]['supplies'][$supply['supply_id']];
                }
            }
        }
        return array_sum($supply_price_array);

    }

    public static function get_all_images($room_id, $special_room = false)
    {
        if ($special_room) {
            $directory = dirname(dirname(__DIR__)) . "/assets/img/special_rooms/" . $room_id;
        } else {
            $directory = dirname(dirname(__DIR__)) . "/assets/img/rooms/" . $room_id;
        }

        if (file_exists($directory)) {
            $images = array_diff(scandir($directory), array('.', '..'));
        } else
            $images = array();

        return $images;
    }

    public static function save_extras($extra_id, $value, $room_id)
    {

        if (isset($_SESSION['selected_rooms'][$room_id]['extras'][$extra_id])) {
            unset($_SESSION['selected_rooms'][$room_id]['extras'][$extra_id]);
        } else {
            $_SESSION['selected_rooms'][$room_id]['extras'][$extra_id] = $value;
        }

    }

    public static function save_designs($design_id, $value, $room_id)
    {

        if (isset($_SESSION['selected_rooms'][$room_id]['designs'][$design_id])) {
            unset($_SESSION['selected_rooms'][$room_id]['designs'][$design_id]);
        } else {
            $_SESSION['selected_rooms'][$room_id]['designs'][$design_id] = $value;
        }

    }

    public static function save_supplies($supply_id, $quantity, $room_id)
    {

        if (isset($_SESSION['selected_rooms'][$room_id]['supplies'][$supply_id]) && $quantity == 0) {
            unset($_SESSION['selected_rooms'][$room_id]['supplies'][$supply_id]);
        } else {
            $_SESSION['selected_rooms'][$room_id]['supplies'][$supply_id] = $quantity;
        }

    }

    public static function save_selected_rooms($room_id)
    {
        $room = get_first("SELECT room_id, room_name, room_price FROM rooms WHERE room_id = $room_id");
        $main_room_ids = array(1, 6);
        $extra_room_ids = array(2, 4, 5);

        $_SESSION['selected_rooms'][$room_id]['room_id'] = $room['room_id'];
        $_SESSION['selected_rooms'][$room_id]['room_name'] = $room['room_name'];

        // Check if main room is selected
        foreach ($main_room_ids as $main_room_id) {
            if (array_key_exists($main_room_id, $_SESSION['selected_rooms'])) {
                $main_room_selected = true;
                break;
            } else {
                $main_room_selected = false;
            }
        }

        // Check if main room is selected and current room type is extra
        if ($main_room_selected && in_array($room_id, $extra_room_ids)) {
            $room_price = 5;
            $room_type = 'extra';
        } else {
            $room_price = $room['room_price'];
            $room_type = 'main';
        }

        $_SESSION['selected_rooms'][$room_id]['room_price'] = $room_price;
        $_SESSION['selected_rooms'][$room_id]['room_type'] = $room_type;
        $_SESSION['number_of_people'] = empty($_REQUEST['number_of_people']) ? (empty($_SESSION['number_of_people']) ? "" : $_SESSION['number_of_people']) : $_REQUEST['number_of_people'];
        $_SESSION['selected_rooms'][$room_id]['booking_start_time'] = empty($_REQUEST['booking_start_time']) ? (empty($_SESSION['booking_start_time']) ? "08:00" : $_SESSION['booking_start_time']) : $_REQUEST['booking_start_time'];
        $_SESSION['selected_rooms'][$room_id]['booking_end_time'] = empty($_REQUEST['booking_end_time']) ? (empty($_SESSION['booking_end_time']) ? "18:00" : $_SESSION['booking_end_time']) : $_REQUEST['booking_end_time'];
        // Booking duration in hours (for moment.js)
        $_SESSION['selected_rooms'][$room_id]['booking_duration'] = (strtotime($_SESSION['booking_end_time']) - strtotime($_SESSION['booking_start_time'])) / 3600;

        // If is selected "whole studio" don't let the user choose multiple booking times for each room
        if (Cart::specialRoomSelected()) {
            foreach ($_SESSION['selected_rooms'] as &$selected_room) {
                $selected_room['booking_start_time'] = $_SESSION['booking_start_time'];
                $selected_room['booking_end_time'] = $_SESSION['booking_end_time'];
                $selected_room['booking_duration'] = (strtotime($_SESSION['booking_end_time']) - strtotime($_SESSION['booking_start_time'])) / 3600;
            }
            unset($selected_room); // Prevent overwriting the $rooms last member later on
        }

        return $main_room_selected;

    }


    public static function select_whole_studio($room_ids)
    {
        foreach ($room_ids as $room_id) {

            $room = get_first("SELECT room_id, room_name, room_price FROM rooms WHERE room_id = $room_id");
            $main_room_ids = array(1, 6);
            $extra_room_ids = array(2, 4, 5);

            $_SESSION['selected_rooms'][$room_id]['room_id'] = $room['room_id'];
            $_SESSION['selected_rooms'][$room_id]['room_name'] = $room['room_name'];

            // Check if main room is selected
            foreach ($main_room_ids as $main_room_id) {
                if (array_key_exists($main_room_id, $_SESSION['selected_rooms'])) {
                    $main_room_selected = true;
                    break;
                } else {
                    $main_room_selected = false;
                }
            }

            // Check if main room is selected and current room type is extra
            if ($main_room_selected && in_array($room_id, $extra_room_ids)) {
                $room_price = 5;
                $room_type = 'extra';
            } else {
                $room_price = $room['room_price'];
                $room_type = 'main';
            }

            $_SESSION['selected_rooms'][$room_id]['room_price'] = $room_price;
            $_SESSION['selected_rooms'][$room_id]['room_type'] = $room_type;
            $_SESSION['number_of_people'] = empty($_REQUEST['number_of_people']) ? (empty($_SESSION['number_of_people']) ? "" : $_SESSION['number_of_people']) : $_REQUEST['number_of_people'];
            $_SESSION['selected_rooms'][$room_id]['booking_start_time'] = empty($_REQUEST['booking_start_time']) ? (empty($_SESSION['booking_start_time']) ? "08:00" : $_SESSION['booking_start_time']) : $_REQUEST['booking_start_time'];
            $_SESSION['selected_rooms'][$room_id]['booking_end_time'] = empty($_REQUEST['booking_end_time']) ? (empty($_SESSION['booking_end_time']) ? "18:00" : $_SESSION['booking_end_time']) : $_REQUEST['booking_end_time'];
            // Booking duration in hours (for moment.js)
            $_SESSION['selected_rooms'][$room_id]['booking_duration'] = (strtotime($_SESSION['booking_end_time']) - strtotime($_SESSION['booking_start_time'])) / 3600;

            // If is selected "whole studio" don't let the user choose multiple booking times for each room
            if ($_SESSION['whole_studio'] == 1) {
                foreach ($_SESSION['selected_rooms'] as &$selected_room) {
                    $selected_room['booking_start_time'] = $_SESSION['booking_start_time'];
                    $selected_room['booking_end_time'] = $_SESSION['booking_end_time'];
                    $selected_room['booking_duration'] = (strtotime($_SESSION['booking_end_time']) - strtotime($_SESSION['booking_start_time'])) / 3600;
                }
                unset($selected_room); // Prevent overwriting the $rooms last member later on
            }
        }

        return true;
    }

    public static function remove_selected_rooms($room_id)
    {
        $_SESSION['whole_studio'] = 0;
        unset($_SESSION['selected_rooms']);
        unset($_SESSION['selected_rooms'][$room_id]);
    }

    public static function get_selected_rooms()
    {
        $rooms = [];

        $_SESSION['whole_studio'] = 0;
        $_SESSION['whole_studio_exhibition'] = 0;
        unset($_SESSION['exhibition_time_sum']);
        unset($_SESSION['exhibition_sum']);
        unset($_SESSION['selected_exhibition_options']);
        unset($_SESSION['selected_rooms']);
        if (!empty($_SESSION['selected_rooms'])) foreach ((array)@array_keys($_SESSION['selected_rooms']) as $room_id) {
            $rooms[$room_id] = get_all("SELECT room_id,room_name, room_price FROM rooms WHERE room_id=$room_id");
        }
        return json_encode($rooms);

    }

    public static function get_rooms_accordion($room_id)
    {
        $room_name = get_all("SELECT * from rooms WHERE room_id=$room_id");
        return __('Room') . '"' . $room_name[0]['room_name'] . '"';
    }

    public static function check_whole_studio_availability_on_date($info = '', $clicked = 0)
    {
        $date = $_SESSION['booking_date'];
        $booked_rooms = get_all("SELECT
                                  room_name,
                                  TIME(booking_start) AS START,
                                  TIME(booking_end)   AS END
                                FROM bookings
                                  LEFT JOIN rooms USING (room_id)
                                  LEFT JOIN orders USING (order_id)
                                WHERE DATE(booking_start) = '$date'
                                AND room_id != 3 
                                AND bookings.deleted = 0
                                AND orders.deleted = 0;"
        );

        if (empty($booked_rooms)) {

            if ($_SESSION['whole_studio'] == 1) {
                $_SESSION['whole_studio'] = 1;
            } else {
                $_SESSION['whole_studio'] = $clicked;
            }

            $response = 'Ok';
        } else {

            if ($info == 'less') {
                $response = '';
            } else {
                $response = __('Given room(s) are already booked on this date:') . '<br>';


                // Show the user what rooms are booked
                foreach ($booked_rooms as $booked_room) {
                    $response .=
                        'Room "' . $booked_room['room_name'] . '" ' .
                        substr($booked_room['START'], 0, -3) . ' - ' .
                        substr($booked_room['END'], 0, -3) . ' <br>';
                }
            }
            // Get the next available date
            while (!empty(get_all("SELECT
                                  room_name,
                                  TIME(booking_start) AS START,
                                  TIME(booking_end)   AS END
                                FROM bookings
                                  LEFT JOIN rooms USING (room_id)
                                WHERE 
                                DATE(booking_start) = '$date' AND room_id != 3 AND deleted = 0;"))) {
                $date = date('Y-m-d', strtotime($date . ' +1 day'));

                // Check if date is weekend
                while (date('N', strtotime($date)) >= 6) {
                    $date = date('Y-m-d', strtotime($date . ' +1 day'));
                }

                // Final available date
                $available_date = $date;
            }

            $_SESSION['whole_studio'] = 0;
            $_SESSION['whole_studio'] = 0;
            $response .= __('Next available date to book the whole studio is: ') . $available_date;
        }

        return $response;
    }

    public static function get_selected_rooms_max_number_of_people()
    {
        $max = 0;

        foreach ($_SESSION['selected_rooms'] as $selected_room) {
            $max_people = get_first("SELECT room_max_persons FROM rooms WHERE room_id = {$selected_room['room_id']}");
            $max += $max_people['room_max_persons'];
        }

        return $max > MAX_NUM_OF_PEOPLE ? MAX_NUM_OF_PEOPLE : $max;
    }

    public static function getWholeStudio()
    {
        return get_first("SELECT * FROM special_rooms WHERE special_room_id = 1");
    }

    public static function getWholeStudioExhibition()
    {
        return get_first("SELECT * FROM special_rooms WHERE special_room_id = 2");
    }

    public static function calculate_sum($rooms, $whole_studio)
    {
        $rooms_sum = 0;

        // Calculate total price of ordered rooms
        foreach ($rooms as $selected_room) {
            $rooms_sum += $selected_room['room_price'] * $selected_room['booking_duration'];
        }
        return $whole_studio ? self::get_current_prices(true, $_SESSION['booking_duration']) : $rooms_sum;
    }

    public static function get_special_rooms()
    {
        return reindex_array_by_property(get_all("SELECT * FROM special_rooms"), 'special_room_id');

    }

    public static function in_use(): bool
    {
        return !!get_one("SELECT count(*) FROM bookings WHERE room_id = $_POST[room_id]");
    }

    /**
     * @return void
     * @throws \Exception
     */
    public static function remove(int $room_id): void
    {
        delete("rooms", "room_id = $room_id");
    }

    private static function getSessionBookingDate(): string
    {
        if (empty($_REQUEST['booking_date'])) {
            if (empty($_SESSION['booking_date'])) {
                $result = Bookings::get_date_without_bookings();
            } else {
                $result = $_SESSION['booking_date'] < date('Y-m-d') ? Bookings::get_date_without_bookings() : $_SESSION['booking_date'];
            }
        } else {
            $result = $_REQUEST['booking_date'] < date('Y-m-d') ? Bookings::get_date_without_bookings() : $_REQUEST['booking_date'];
        }
        return $result;
    }

}
