<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


use Particle\Validator\Validator;

class Service
{
    private static $services;

    public static function getAll()
    {

        // Skip the trip to the database for subsequent getAll() calls
        if (empty(self::$services)) {
            self::$services = reindex_array_by_property(
                self::get(),
                'service_id');
        }

        return self::$services;
    }

    public static function get($id = null)
    {

        if ($id === null) {
            $services = get_all("SELECT * FROM services");

            // Translate service names
            foreach ($services as &$service) {
                $service['service_name'] = __($service['service_name']);
            }

            return $services;
        }

        if (!is_numeric($id)) {
            throw new \Exception("id is not numeric ($id)");
        }

        return get_first("SELECT * FROM services LEFT JOIN service_groups USING (service_group_id) WHERE service_id = $id");
    }

    public static function getByServiceGroups()
    {
        $result = [];

        $services = get_all("
            SELECT *
            FROM service_groups
            JOIN services USING (service_group_id)
            WHERE service_deleted = 0");

        foreach ($services as $service) {
            $result[$service['service_group_id']]['service_group_id'] = $service['service_group_id'];
            $result[$service['service_group_id']]['service_group_name'] = $service['service_group_name'];
            $result[$service['service_group_id']]['service_group_description'] = $service['service_group_description'];
            $result[$service['service_group_id']]['service_group_image'] = $service['service_group_image'];
            $result[$service['service_group_id']]['service_group_multiselect'] = (int)$service['service_group_multiselect'];
            $result[$service['service_group_id']]['service_group_image_location'] = $service['service_group_image_location'];
            $result[$service['service_group_id']]['services'][] = [
                'service_id' => $service['service_id'],
                'service_description' => $service['service_description'],
                'service_name' => $service['service_name'],
                'service_price' => $service['service_price'],
                'service_quantity_options' => $service['service_quantity_options'],
                'service_unit_singular' => $service['service_unit_singular'],
                'service_unit_plural' => $service['service_unit_plural'],
            ];
        }

        return $result;
    }

    static function validateRequiredFields($data)
    {

        // Example
        $v = new Validator;
        $v->required('service_name')->lengthBetween(2, 255)->alpha();
        $v->required('service_description')->lengthBetween(2, 255)->alpha();
        $v->required('service_price')->lengthBetween(2, 50)->numeric();
        $v->required('service_group_id')->greaterThan(0);
        if (!empty($data['service_quantity_options'])) {
            $v->required('service_unit_singular')->lengthBetween(2, 50)->alpha();

        }
        $result = $v->validate([
            'user' => [
                'first_name' => 'John',
                'last_name' => 'D',
            ],
            'newsletter' => true,
        ]);

        $result->isValid(); // bool(false).
        $result->getMessages();


        if (empty($data['service_name'])) {
            throw new \Exception('Invalid or missing service_name', 400);
        }

        if (empty($data['service_name_in_order_summary'])) {
            throw new \Exception('Invalid or missing service_name_in_order_summary', 400);
        }

        if (!is_numeric($data['service_price'])) {
            throw new \Exception('Invalid or missing service_price', 400);
        }
        if (empty($data['service_group_id'])) {
            throw new \Exception('Invalid or missing service_group_id', 400);
        }

        // If the use quantities checkbox was checked
        if (!empty($data['service_quantity_options'])) {

            if (empty($data['service_unit_singular'])) {
                throw new \Exception('Invalid or missing service_unit_singular', 400);
            }

            if (empty($data['service_unit_plural'])) {
                throw new \Exception('Invalid or missing service_unit_plural', 400);
            }

        }

        return true;

    }

    public static function addEdit($action, $service_id)
    {
        try {
            Service::validateRequiredFields($_POST);
        } catch (\Exception $e) {
            stop($e->getCode(), $e->getMessage());
        }

        $result = [
            'service_name' => $_POST['service_name'],
            'service_name_in_order_summary' => $_POST['service_name_in_order_summary'],
            'service_description' => $_POST['service_description'],
            'service_price' => $_POST['service_price'],
            'service_quantity_options' => @$_POST['service_quantity_options'],
            'service_unit_singular' => @$_POST['service_unit_singular'],
            'service_unit_plural' => @$_POST['service_unit_plural'],
            'service_group_id' => $_POST['service_group_id'],
        ];

        switch ($action) {
            case 'add':
                return insert('services', $result);
                break;
            case 'edit':
                update("services", ["service_price" => $_POST["service_price"]], "service_id = $service_id");
                break;
            default:
                throw new \Exception('unknown action');
        }

    }

    public static function getSelected()
    {
        return isset($_SESSION['selected_services']) ? $_SESSION['selected_services'] : [];
    }

    public static function isPartOfMultiselectServiceGroup($service_id)
    {
        $service_group = Service::get($service_id);

        return !!$service_group['service_group_multilselect'];

    }

    public static function addQuantitySpecificationToservice($services)
    {
        // Specify quantities (in parentheses) in service names which have other quantity than 1
        foreach ($services as $service) {
            if ($service['quantity'] != 1){
                $s = &$services[$service['service_id']];
                $s['service_name_in_order_summary'] = $s['service_name_in_order_summary'] . " ($s[quantity] $s[service_unit_plural])";
            }
               
        }
        unset($s); // Unbind the variable from $order['services']'s last member

        return $services;
    }

    public static function translateNames($services)
    {
        if (empty($services))
            return [];

        foreach ($services as &$service) {
            $service['service_name'] = __($service['service_name']);
            $service['service_name_in_order_summary'] = __($service['service_name_in_order_summary']);
            $service['service_unit_singular'] = __($service['service_unit_singular']);
            $service['service_unit_plural'] = __($service['service_unit_plural']);
        }

        unset($service);

        return $services;
    }

}
