<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class ServiceGroup
{
    public static function getAll()
    {
        $service_groups = [];

        $rows = get_all("
            SELECT
                service_group_id,
                service_group_name,
                service_group_description,
                service_group_image,
                service_group_multiselect,
                service_group_image_location,
                service_name
            FROM service_groups
            LEFT JOIN services USING (service_group_id) WHERE service_deleted = 0");

        foreach ($rows as $row) {

            $service_groups[$row['service_group_id']]['service_group_id'] = $row['service_group_id'];
            $service_groups[$row['service_group_id']]['service_group_name'] = $row['service_group_name'];
            $service_groups[$row['service_group_id']]['service_group_multiselect'] = (int)$row['service_group_multiselect'];
            $service_groups[$row['service_group_id']]['service_group_description'] = $row['service_group_description'];
            $service_groups[$row['service_group_id']]['service_group_image'] = $row['service_group_image'];
            $service_groups[$row['service_group_id']]['service_group_image_location'] = $row['service_group_image_location'];
            $service_groups[$row['service_group_id']]['services'][] = [
                'service_name' => $row['service_name']
            ];
        }

        return $service_groups;
    }

    public static function get($service_group_id)
    {
        // Validate service group
        if (!is_numeric($service_group_id)) {
            throw new \Exception('Invalid service group id', 400);
        }

        // Variable declarations
        $service_group = [];

        // Get data from database
        $rows = get_all("
            SELECT *
            FROM service_groups
            LEFT JOIN services USING (service_group_id)
            WHERE service_group_id = $service_group_id AND service_deleted = 0");

        // Organize data into structured array
        foreach ($rows as $row) {

            // Add service group's own data
            $service_group['service_group_id'] = $row['service_group_id'];
            $service_group['service_group_name'] = $row['service_group_name'];
            $service_group['service_group_description'] = $row['service_group_description'];
            $service_group['service_group_image'] = $row['service_group_image'];
            $service_group['service_group_multiselect'] = (int)$row['service_group_multiselect'];
            $service_group['service_group_image_location'] = $row['service_group_image_location'];

            // Attach services
            if (!empty($row['service_id'])) {
                $service_group['services'][] = [
                    'service_id' => $row['service_id'],
                    'service_name' => $row['service_name'],
                    'service_description' => $row['service_description'],
                    'service_price' => $row['service_price'],
                    'service_quantity_options' => $row['service_quantity_options'],
                    'service_unit_singular' => $row['service_unit_singular'],
                    'service_unit_plural' => $row['service_unit_plural'],
                    'service_group_id' => $row['service_group_id']
                ];
            }
        }

        // Return the result
        return $service_group;
    }

}