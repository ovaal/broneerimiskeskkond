<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 02/06/16
 * Time: 17:45
 */

namespace Broneerimiskeskkond;


class Settings
{
    static $settings = [];

    static function get($setting = false)
    {
        if (empty(self::$settings)) {
            $settings = get_all("SELECT * FROM settings");

            foreach ($settings as $s) {
                self::$settings[$s['setting']] = $s['value'] == 'false' ? false : $s['value'];
            }
        }

        if (!array_key_exists($setting, self::$settings)) {
            return null;
        }

        return $setting ? self::$settings[$setting] : self::$settings;
    }

    public static function set(string $setting, $value)
    {
        insert('settings', ['setting' => $setting, 'value' => $value]);
    }
}