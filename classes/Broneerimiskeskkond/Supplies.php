<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.02.2018
 * Time: 12:42
 */

namespace Broneerimiskeskkond;


class Supplies
{
    public static function get()
    {
        $result = [];

        $supplies = get_all("SELECT * FROM supplies");

        foreach ($supplies as $supply) {
            $result[$supply['supply_id']] = $supply;
        }
        return $result;
    }
}