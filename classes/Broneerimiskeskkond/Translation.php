<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 16.01.2018
 * Time: 15:34
 */

namespace Broneerimiskeskkond;


use Exception;

class Translation
{
    public static function delete($translation_ids)
    {
        $translation_ids = SQL::stringify($translation_ids);
        delete("log", "logged_at < DATE_ADD(NOW(), INTERVAL -6 MONTH)");
    }

    /**
     * @param string $translations_used
     * @return void
     * @throws Exception
     */
    public static function updateLastActive(string $translations_used): void
    {
        update("translations", ["last_active" => date("Y-m-d H:i:s")], "binary phrase in ($translations_used)", do_not_log: true);
    }

    /**
     * @param string $phrase
     * @param string $translation
     * @return void
     * @throws Exception if insert table might be missing.
     */
    public static function add(string $phrase, string $translation = '{untranslated}'): void
    {
        insert('translations', [
            'phrase' => $phrase,
            'translation' => $translation,
            'language' => $_SESSION['language'] ?? null,
        ]);
    }
}