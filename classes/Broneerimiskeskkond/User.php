<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 19.04.2016
 * Time: 13:38
 */

namespace Broneerimiskeskkond;

class User
{

    private static $user;
    private static $user_status_lookup = [
        USER_STATUS_DEFAULT => 'Default',
        USER_STATUS_SILVER => 'Silver',
        USER_STATUS_GOLD => 'Gold',
    ];

    static function register($email, $password, $first_and_last_name, $phone, $company_id, $state_institution)
    {

        if (User::exists($email)) {
            return false;
        }

        // Insert new user
        return insert('users', [
            'email' => $email,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'first_and_last_name' => $first_and_last_name,
            'phone' => $phone,
            'company_id' => $company_id,
            'state_institution' => $state_institution,
            'activation_code' => md5($email . $first_and_last_name . 'activation_code'),
            'user_created_at' => date('Y-m-d H:i:s')
        ]);

    }

    /**
     * Check that user with given email, does not exist
     * @param $email
     * @return bool
     */
    public static function exists($email)
    {
        // Check that user does not exist
        $user = get_first("SELECT user_id
                           FROM users
                           WHERE email = '{$email}'");

        return empty ($user) ? false : true;
    }

    public static function get_user_email($user_id)
    {
        return get_all("SELECT email FROM users WHERE user_id = {$user_id}");
    }

    static function generate_password($length)
    {
        return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode(self::getRandomBytes($length + 1))), 0, $length);
    }

    static function getRandomBytes($nbBytes = 32)
    {
        $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        if (false !== $bytes && true === $strong) {
            return $bytes;
        } else {
            throw new \Exception("Unable to generate secure token from OpenSSL.");
        }
    }

    static function edit($organisation, $registry_number, $address_city, $address_street, $address_zip, $user_id)
    {
        update('users', [
            'organisation' => $organisation,
            'registry_number' => $registry_number,
            'address_city' => $address_city,
            'address_street' => $address_street,
            'address_zip' => $address_zip
        ], "user_id = $user_id");

    }

    public static function get($user_id = null, $exclude_order_id = null)
    {

        $user_id = $user_id ? (int)$user_id : null;

        if (!$user_id) {
            if (!isset($_SESSION['user_id'])) {
                throw new \Exception('Invalid user_id at ' . __FILE__ . ':' . __LINE__);
            }
            $user_id = $_SESSION['user_id'];
        }

        // Check cache
        if (!empty(self::$user[$user_id]))
            return self::$user[$user_id];

        $exclude_sql = '';
        if ($exclude_order_id) {
            $exclude_sql = " AND o.order_id <> " . (int)$exclude_order_id;
        }

        $user = get_first("
            SELECT *,
                   CASE
                       WHEN user_status = 3 THEN 0.10
                       WHEN user_status = 2 THEN 0.05
                       ELSE 0
                       END AS user_discount_percent
            FROM (SELECT u.*,
                         o.order_id      current_discount_qualifying_order_id,
                         b.booking_start last_booking_date,
                         CASE -- customer_level column
                             WHEN u.state_institution = 1 THEN 1 -- state institution users do not get any discounts
                             WHEN b.booking_start BETWEEN DATE_SUB(NOW(), INTERVAL 3 MONTH) AND NOW() THEN 3
                             WHEN b.booking_start BETWEEN DATE_SUB(NOW(), INTERVAL 6 MONTH) AND NOW() THEN 2
                             ELSE 1 -- if the booking was made more than 6 months ago, set customer_level to 1
                             END AS      user_status
                  FROM users u
                           LEFT JOIN orders o
                                     ON (u.user_id = o.user_id -- orders made by the user
                                         OR u.company_id = o.company_id --  orders made by the company that the user belongs to
                                         OR
                                         o.company_id IN ( -- orders made to companies that the user has made orders to in the past
                                             SELECT company_id
                                             FROM orders
                                             WHERE user_id = u.user_id))
                                         AND o.deleted = 0
                                         $exclude_sql
                           LEFT JOIN bookings b
                                     ON o.order_id = b.order_id
                                         AND b.deleted = 0
                                         AND (b.booking_start BETWEEN DATE_SUB(NOW(), INTERVAL 6 MONTH) AND NOW() OR
                                              b.booking_start IS null)
                  WHERE u.user_id = $user_id -- select only data related to given user_id
                  ORDER BY b.booking_start DESC -- order the bookings by booking start date in descending order (most recent first)
                  LIMIT 1) AS users_with_status");

        $user_discount_levels = reindex_array_by_property(
            get_all("SELECT * FROM user_discount_levels"),
            'user_discount_level_id');


        // Add user discount level name
        $user['discount_level'] = $user_discount_levels[$user['user_status']]['user_discount_level_name'];


        // Cache it
        self::$user[$user_id] = $user;

        return $user;
    }

    public static function is_gold_partner($user)
    {
        // Is user gold partner and allowed to get a discount
        return $user['user_status'] == USER_STATUS_GOLD;
    }

    public static function is_silver_partner($user)
    {
        // Is user silver partner and allowed to get a discount
        return $user['user_status'] == USER_STATUS_SILVER;
    }

    public static function is_partner($user)
    {
        return $user['user_status'] == USER_STATUS_DEFAULT;
    }

    public static function getStatus($user)
    {
        // Set user status
        return self::$user_status_lookup[$user['user_status']];
    }

    public static function getStatusName($discount_level)
    {
        if (!is_numeric($discount_level) || $discount_level > 3 || $discount_level < 1) {
            throw new \Exception('Invalid discount level (' . $discount_level . ')');
        }
        return self::$user_status_lookup[$discount_level];
    }

    /**
     * @param $invoice_email
     * @return void
     */
    public static function updateInvoiceAddress($invoice_email): void
    {
        update('users', ['invoice_email' => $invoice_email], "user_id = {$_SESSION['user_id']}");

    }

}

