<?php

namespace SimplBooks;


use Broneerimiskeskkond\Log;
use Exception;

class SimplBooks
{

    static function getInvoiceById($invoice_id)
    {
        return self::curlPost("invoices/get/{$invoice_id}")->data;
    }

    static function createInvoice($simplbooks_client_id, $rows, $days_due, $additional_info = false)
    {
        $today = date('Y-m-d');

        if (!$simplbooks_client_id) {
            throw new Exception('Simplbooks Client id is invalid');
        }

        $invoice = array(
            "Invoice" => array(
                "client_id" => $simplbooks_client_id,
                "language" => $_SESSION['language'],
                "due" => date('Y-m-d', strtotime($today . " + $days_due days")),
                "additional_info" => $additional_info,
                "sent" => $today
            )
        );

        foreach ($rows as $row) {
            $invoice['Tasks'][] = array(
                "Task" => $row
            );
        };

        try {
            $result = self::curlPost("invoices/create", $invoice);
            $result->number = self::getInvoiceById($result->inserted_id)->Invoice->number;
            return $result;
        } catch (Exception $e) {
            $response = (object)unserialize($e->getMessage());
            throw new \Exception('Simplbooks error: ' . $response->status . '  ' . implode(", ", $response->errors));
        }

    }

    static function createClient($client_data)
    {
        $client_id = self::curlPost("clients/create", $client_data);

        return $client_id;
    }

    static function getClients($filter)
    {
        $response = self::curlPost("clients/list", $filter);

        if (empty($response->data)) {
            return array();
        }

        return $response->data;
    }

    static function getAllClients()
    {
        $response = self::curlGet("clients/list");
        return empty($response->data) ? [] : $response->data;

    }


    static function updateClient($simplbooks_client_id, $company_name, $company_city, $company_street, $company_postal_code, $company_reg_no)
    {
        $client = array(
            "Client" => array(
                "id" => $simplbooks_client_id,
                "name" => $company_name,
                "address_city" => $company_city,
                "address_street" => $company_street,
                "address_postal_code" => $company_postal_code,
                "reg_no" => $company_reg_no
            )
        );

        try {
            return self::curlPost("clients/update", $client);
        } catch (Exception $e) {
            $response = (object)unserialize($e->getMessage());
            throw new \Exception('Simplbooks error: ' . $response->status . '  ' . implode(", ", $response->errors));
        }
    }

    static function getClient($filter): object|false
    {

        $clients = self::getClients($filter);


        // Simplbooks does partial match search but we need exact match

        foreach ($clients as $client) {

            foreach ($filter as $search_field => $value) {

                if ($client->Client->$search_field == $value) {

                    return $client->Client;

                }

            }

        }

        return false;

    }

    static function createIncoming($invoice_id, $invoice_number, $income_sum, $client_id)
    {
        $today = date('Y-m-d');

        $incoming_data = array(
            "Incoming" => array(
                "income_account_id" => 2,
                "description" => __("Payment for invoice") . " " . $invoice_number,
                "income_sum" => $income_sum,
                "currency_name" => "EUR",
                "currency_rate" => 1,
                "income_date" => $today,
                "client_id" => $client_id
            ),
            "invoice_id" => $invoice_id
        );

        return self::curlPost("incomings/create", $incoming_data);
    }

    static function setInvoicePaid($invoice_id, $incoming_id)
    {
        $incoming_data = array(
            "incoming_id" => $incoming_id,
            "invoice_id" => $invoice_id
        );
        return self::curlPost("incomings/invoice_paid", $incoming_data);
    }

    static function sendPDFToBrowser($invoice_id, $return = false): string
    {
        $pdf_filename = __('Ovaal invoice') . ' ' . $invoice_id . '.pdf';
        $pdf_content = base64_decode(self::curlPost("invoices/get_pdf/{$invoice_id}")->data);

        if ($return) {
            return $pdf_content;
        } else {
            ob_clean();

            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename*=UTF-8''" . rawurlencode($pdf_filename));
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Description: File Transfer");
            header("Content-Length: " . strlen($pdf_content));

            echo $pdf_content;
            return $pdf_filename;
        }
    }

    static function getXML($invoice_id, $return = false)
    {
        return base64_decode(self::curlPost("invoices/get_xml/{$invoice_id}")->data);
    }

    static function curlPost(string $endpoint, array $post_data = [])
    {
        return self::curlRequest($endpoint, true, $post_data);
    }

    /**
     * @param $endpoint
     * @return object
     * @throws Exception
     */
    static function curlGet($endpoint)
    {
        return self::curlRequest($endpoint, false);
    }

    /**
     * @param string $endpoint SimplBooks API url
     * @param array $post_data Array to be POSTed
     * @param boolean $isPOST Whether the request is POST or GET
     * @return object SimplBooks JSON object converted to PHP object
     * @throws Exception SimplBooks response(JSON, if possible) serialized to PHP array
     */
    static function curlRequest(string $endpoint, bool $isPOST, array $post_data = []): object
    {

        self::checkRequestCount();

        $post_data_encoded = http_build_query($post_data);

        // Curl check
        if (!function_exists("curl_init") &&
            !function_exists("curl_setopt") &&
            !function_exists("curl_exec") &&
            !function_exists("curl_close")
        ) die('Install curl extension first.');

        if (!defined('SIMPLBOOKS_TOKEN') || empty(SIMPLBOOKS_TOKEN)) {
            die('SIMPLBOOKS_TOKEN is not set in config.php');
        }


        if (!defined('SIMPLBOOKS_API_URL') || empty(SIMPLBOOKS_API_URL)) {
            die('SIMPLBOOKS_API_URL is not set in config.php');
        }

        $ch = curl_init();

        if (FALSE === $ch)
            throw new Exception('failed to initialize');

        // set url
        curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
        curl_setopt($ch, CURLOPT_URL, SIMPLBOOKS_API_URL . $endpoint);
        curl_setopt($ch, CURLOPT_POST, $isPOST);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Simplbooks-Token: ' . SIMPLBOOKS_TOKEN));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data_encoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        // $output contains the output string
        $output = curl_exec($ch);
        self::increaseApiCallsCounter($endpoint, $post_data_encoded, $output);
        $information = curl_getinfo($ch);

        // Verify that we have a response
        if (curl_error($ch) || FALSE === $output) {
            throw new Exception(serialize(["status" => 500, "errors" => curl_error($ch)]));
        }

        // Get response code
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = explode("\r\n", substr($output, 0, $header_size));
        $response_code = false;
        foreach ($headers as $header) {
            if (preg_match("/^HTTP\/[0-9\.]+ ([0-9]+).*/", $header, $header_parsed)) {
                $response_code = $header_parsed[1];
            }
        }

        // Process body
        $body = json_decode(substr($output, $header_size));

        Log::log(Log::RPC,
            'simplbooks',
            $body->inserted_id,
            $post_data,
            null,
            $information['request_header']. $post_data_encoded ."\n\n". $output
        );


        // Return output if it's json array
        if (!empty($body->status) && str_starts_with($body->status, "2")) {
            return $body;
        } elseif (!empty($body->status)) {
            throw new Exception(serialize($body), $body->status);
        } else {
            $body = substr($output, $header_size);
            throw new Exception(
                print_r([
                    "status" => 500,
                    //"headers" => $headers,
                    "information" => $information,
                    "errors" => $body
                ], 1)
            );
        }

    }

    /**
     * @throws Exception
     */
    static function rawRequest($url, $data): mixed
    {

        self::checkRequestCount();

        $url = SIMPLBOOKS_API_URL . $url;

        //$url =  "https://1e7a54f281.to.intercept.rest/" . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Set to false if the certificate is not verified
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "X-Simplbooks-Token: " . SIMPLBOOKS_TOKEN,
            "Content-Type: application/json",
            "X-Input-Format:json"
        ]);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($ch);

        self::increaseApiCallsCounter($url, $data, json_encode($response));

        if (curl_errno($ch)) {
            echo 'cURL error: ' . curl_error($ch);
            return false;
        }

        curl_close($ch);

        return json_decode($response, true);
    }


    /**
     * @return void
     * @throws Exception
     */
    public static function checkRequestCount(): void
    {
        // Prune outdated entries (older than 60 seconds)
        q("DELETE FROM simplbooks_requests WHERE request_time < (NOW() - INTERVAL 60 SECOND)");

        // Count requests in the last 60 seconds
        $requestCount = get_one("SELECT COUNT(*) FROM simplbooks_requests WHERE request_time >= (NOW() - INTERVAL 60 SECOND)");

        if ($requestCount >= 50) {
            // Show the user how much time s/he has to wait
            $timeToWait = get_one("SELECT TIMESTAMPDIFF(SECOND, NOW(), DATE_ADD(MAX(request_time), INTERVAL 60 SECOND)) FROM simplbooks_requests");
            throw new Exception("Too many requests. Please wait $timeToWait seconds.");
        }
    }

    public static function getCurrentRequestCount()
    {
        // First delete old
        q("DELETE FROM simplbooks_requests WHERE request_time < (NOW() - INTERVAL 60 SECOND)");
        return get_first("SELECT COUNT(*) as request_count, DATE_ADD(MAX(request_time), INTERVAL 60 SECOND) as zero_at FROM simplbooks_requests");

    }

    /**
     * @param string $url
     * @param $data
     * @return void
     * @throws Exception
     */
    protected static function increaseApiCallsCounter(string $url, $data,$response): void
    {
        insert("simplbooks_requests", [
            'request_time' => date('Y-m-d H:i:s'),
            'url' => $url,
            'data' => $data,
            'response' => $response
        ]);
    }


}
