<?php

// Change this in production:
#define('ENV', ENV_PRODUCTION);

/* HTTP
====================================================================================================*/
define('FORCE_HTTPS', false);

/* PHP
====================================================================================================*/
define('SESSION_MAX_LIFETIME', '21600'); // 6 hours
define('SESSION_SAVE_PATH', __DIR__ . '/.sessions/');

/* Database
====================================================================================================*/
define('DATABASE_HOSTNAME', '127.0.0.1');
define('DATABASE_USERNAME', 'root');
define('DATABASE_PASSWORD', '');
define('DATABASE_DATABASE', 'broneerimiskeskkond');

const STAGING_DATABASE_HOSTNAME = 'test.diarainfra.com';
const STAGING_DATABASE_USERNAME = 'broneerimiskeskk';
const STAGING_DATABASE_PASSWORD = 'xxxxxx';
const STAGING_DATABASE_DATABASE = 'broneerimiskeskkond';

/* Email
====================================================================================================*/
define('SITE_OWNER_EMAIL', 'ovaal@ovaal.ee');
define('SMTP_USE_SENDMAIL', false);
define('SMTP_HOST','localhost');
define('SMTP_AUTH','false');
define('SMTP_AUTH_USERNAME','false');
define('SMTP_AUTH_PASSWORD','false');
define('SMTP_FROM', 'info@ovaal.ee');
define('SMTP_PORT','1025');
define('SMTP_ENCRYPTION', 'none');
define('SMTP_DEBUG', 0);

/* Other constants
====================================================================================================*/
define('MEAL_TYPE_BREAFKAST', 1);
define('MEAL_TYPE_BRUNCH', 2);
define('MEAL_TYPE_LUNCH', 3);
define('MEAL_TYPE_LINNER', 4);
define('MEAL_TYPE_LATE_AFTERNOON', 5);
define('VAT_PERCENT', 0.22);

/* Simplbooks
====================================================================================================*/
/* TEST */
define('SIMPLBOOKS_API_URL', 'https://app.simplbooks.com/e7c6d82a64a51fef292b2ef079568575/api/');
define('SIMPLBOOKS_TOKEN', '7f66aa75d412998e40822db2ea03dac3');

/* LIVE */
//define('SIMPLBOOKS_API_URL', 'https://app.simplbooks.com/aktoseidon/api/');
//define('SIMPLBOOKS_TOKEN', '2c426655e502288ff800258b26cb6a42');

/* LogRocket
====================================================================================================*/
define('LOGROCKET_INIT', ''); // In the form: xxxxxx/name

/* Sentry
====================================================================================================*/
define('SENTRY_DSN', ''); // https://sentry.io/settings/diara-infra-i1/projects/ovaal-broneerimiskeskkond/keys/