<?php

date_default_timezone_set('Europe/Tallinn');
define('PROJECT_VERSION', trim(exec('git log --pretty="%h" -n1 HEAD')));
define('PROJECT_NAME', 'Ovaal');
define('PROJECT_SESSION_ID', 'SESSID_BRON');
define('PROJECT_NATIVE_LANGUAGE', 'xx');
define('DEFAULT_CONTROLLER', 'rooms');
define('DEVELOPER_EMAIL', 'henno.taht@gmail.com');
define('MAX_NUM_OF_PEOPLE', 35);
define('ENV_DEVELOPMENT', 0);
define('ENV_PRODUCTION', 1);
define('SUPPORT_PHONE_NUMBER', '+372 505 0277');
define('USER_STATUS_GOLD', 3);
define('USER_STATUS_SILVER', 2);
define('USER_STATUS_DEFAULT', 1);
define('IS_ID', 1);
define('IS_INT', 2);
define('IS_0OR1', 3);
define('IS_ARRAY', 4);
define('IS_STRING', 5);
define('IS_DATE', 6);
