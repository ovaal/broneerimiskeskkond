<?php namespace Halo;

use Broneerimiskeskkond\AdditionalServices;
use Broneerimiskeskkond\Cart;
use Broneerimiskeskkond\Extras;
use Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\ServiceGroup;
use Broneerimiskeskkond\Service;
use Broneerimiskeskkond\User;

/**
 * @property  cart
 * @property  cart
 */
class additional_services extends Controller
{

    function index()
    {
        // Send the user back to the rooms page if they arrived here by direct URL and hasn't chosen any rooms
        if (empty($_SESSION['selected_rooms'])) {
            header('Location: ' . DEFAULT_CONTROLLER);

            exit();
        }

        $this->order = Cart::get();
        $this->service_groups = Service::getByServiceGroups();
        $this->extras = Room::get_extras();
        $this->supplies = Room::get_supplies();
        $this->user = User::get();
        $this->price = Cart::getRoomsPrice();
        $this->last_viewed_room_link = $_SESSION['last_viewed_room_link'];

    }

    function AJAX_save_extra_selection()
    {
        // Validation
        validate($_POST['room_id']);
        validate($_POST['extra_id']);

        // Sanitization
        $selected = $_POST['selected'] === 'true'  ? true : false;
        $order_id = empty($_POST['order_id']) ? null : (int)$_POST['order_id'];

        /** @var Orders|Cart $OrdersOrCart */
        $OrdersOrCart = '\Broneerimiskeskkond\\' . ($order_id ? 'Orders' : 'Cart');

        $selected ?

            $OrdersOrCart::addExtra(
                $_POST['room_id'],
                $_POST['extra_id'],
                $order_id) :

            $OrdersOrCart::removeExtra(
                $_POST['room_id'],
                $_POST['extra_id'],
                $order_id);

    }


    function AJAX_save_selection()
    {
        if (empty($_POST['service_id']) || !is_numeric($_POST['service_id']) || $_POST['service_id'] <= 0) {
            stop(400, 'Invalid service_id');

        }


        $quantity = empty($_POST['service_quantity']) ? 0 : (int)$_POST['service_quantity'];


        // For radio buttons not to be selected at the same time
        $services = Service::getAll();
        $service_id = $_POST['service_id'];
        $service_group = ServiceGroup::get($services[$_POST['service_id']]['service_group_id']);
        $order_id = empty($_POST['order_id']) ? null : $_POST['order_id'];

        if (!$service_group['service_group_multiselect']) {
            $this->unselectOtherServices($service_group, $order_id);
        }

        /** @var Orders $OrdersOrCart */
        $OrdersOrCart = '\Broneerimiskeskkond\\' . ($order_id ? 'Orders' : 'Cart');

        $quantity ?

            $OrdersOrCart::addService(
                $service_id,
                $quantity,
                $order_id) :

            $OrdersOrCart::removeService(
                $service_id,
                $order_id);

    }


    private function unselectOtherServices(array $service_group, $order_id)
    {
        // Loop over all the services in the same service group
        foreach ($service_group['services'] as $service) {

            $service_id = $service['service_id'];

            if ($order_id) {

                Orders::removeService(
                    $order_id,
                    $service_id);

            } else {

                Cart::removeService(
                    $service_id);
            }
        }

    }
}



