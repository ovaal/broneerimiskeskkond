<?php
/**
 * Created by PhpStorm.
 * User: Rasmus
 * Date: 17/05/2016
 * Time: 11:06
 */

namespace Halo;


use Broneerimiskeskkond\Company;
use Broneerimiskeskkond\HtmlUtils;
use Broneerimiskeskkond\Images;
use Broneerimiskeskkond\Invoices;
use Broneerimiskeskkond\Language;
use Broneerimiskeskkond\Log;
use Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\PaymentStatus;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\Service;
use Broneerimiskeskkond\ServiceGroup;
use Broneerimiskeskkond\Settings;
use Broneerimiskeskkond\Translation;
use Broneerimiskeskkond\User;
use SimplBooks\SimplBooks;


class admin extends Controller
{
    public $template = 'admin';
    public string $errors = '';
    public $requires_admin = true;
    public $companies = [];
    public $invoices = [];

    function index()
    {
        if (isset($_SESSION['real_user_id']) && $_SESSION['real_user_id'] != $this->auth->user_id) {
            $_SESSION['user_id'] = $_SESSION['real_user_id'];
        }

        // Redirect to Bookings page
        header('Location: ' . BASE_URL . 'admin/orders');
    }

    function rooms()
    {
        $this->rooms = get_all("SELECT * FROM rooms ORDER BY room_max_persons DESC");
    }

    function logs()
    {
        $this->logs = Log::get();
    }

    function ajax_add_remark_to_order()
    {
        if ($this->params[0] == 'add_remark') {
            validate($_POST['order_id']);
            validate($_POST['order_remarks'], IS_STRING);
            Orders::addRemark($_POST['order_id'], $_POST['order_remarks']);
        }
    }

    function orders()
    {

        $this->orders_js = json_encode(get_all("SELECT
                                                      companies.company_name  AS  title,
                                                      booking_start AS  start,
                                                      booking_end   AS  end
                                                    FROM orders
                                                      LEFT JOIN bookings USING (order_id)
                                                      LEFT JOIN users USING (user_id)
                                                      LEFT JOIN companies ON (orders.company_id = companies.company_id)
                                                    WHERE orders.deleted = 0
                                                      GROUP BY order_id"));
        $this->orders = Orders::get_structured(
                $_GET['showAllOrders'] ?? null || !empty($_GET['search']) ? [] : ['booking_start > DATE_SUB(NOW(), INTERVAL 30 DAY)'],
            $_GET['search'] ?? null,
            $_GET['order_by'] ?? 'orders.order_id',
            $_GET['order_direction'] ?? 'DESC');

        $this->bookings = \Broneerimiskeskkond\Bookings::get_all();
        $this->statuses = get_all("SELECT * FROM payment_statuses");

    }

    /**
     * @throws \Exception when order id is missing or invalid
     */
    function order_view()
    {
        if (empty($this->params[0])) {
            throw new \Exception('Missing/invalid order id');
        }

        $order_id = $this->params[0];
        validate($order_id);

        $this->logs = Log::get(['object_type' => 'orders', 'object_id' => $order_id]);

        $this->bookings = \Broneerimiskeskkond\Bookings::get_all_by_order_id($order_id);

        $this->order = get_first("SELECT *
                                    FROM orders
                                      LEFT JOIN bookings USING (order_id)
                                      LEFT JOIN payment_statuses USING (payment_status_id)
                                      WHERE orders.deleted = 0 AND bookings.deleted = 0 AND order_id = $order_id
                                    GROUP BY order_id");

        $this->invoice_data = \Broneerimiskeskkond\Orders::get($order_id);
    }

    function users()
    {
        $this->users = get_all("SELECT
                                       *,
                                       (SELECT GROUP_CONCAT(DISTINCT company_name SEPARATOR ', ')
                                        FROM users u2
                                          LEFT JOIN orders USING (user_id)
                                          LEFT JOIN companies ON (orders.company_id = companies.company_id)
                                        WHERE u1.user_id = u2.user_id) company_names
                                     FROM users u1
                                     WHERE u1.deleted = 0");
    }

    function info_boxes()
    {
        $this->info_boxes = get_all("SELECT * FROM info_boxes WHERE deleted = 0");

    }

    function services()
    {
        $this->service_groups = ServiceGroup::getAll();
    }

    function translations()
    {
        global $supported_languages;

        // Redirect to admin/translations/estonian when no language is specified
        if (empty($this->params[0]) || !in_array($this->params[0], ['english', 'estonian'])) {
            header('Location: ' . BASE_URL . 'admin/translations/estonian/untranslated');
            exit();
        }


        // Redirect to admin/translations/LANGUAGE/untranslated when no translatedness is specified
        if (empty($this->params[1]) || !in_array($this->params[1], ['untranslated', 'translated', 'all'])) {
            header('Location: ' . BASE_URL . 'admin/translations/' . $this->params[0] . '/untranslated');
            exit();
        }


        $lng_lookup = ['english' => 'en_GB', 'estonian' => 'et_EE'];
        $language = $lng_lookup[$this->params[0]];
        if ($this->params[1] === 'translated') {
            $translatedness = "AND translation != '{untranslated}'";
        } elseif ($this->params[1] === 'untranslated') {
            $translatedness = "AND translation = '{untranslated}'";
        } else {
            $translatedness = "";
        }


        $this->translations = get_all("SELECT * FROM translations WHERE language='$language' $translatedness ORDER BY appeared DESC, phrase");
    }

    function offers()
    {
        $this->offers = get_all("SELECT * FROM offers LEFT JOIN users ON (offer_requested_by=user_id)");
    }

    function special_rooms()
    {
        $this->special_rooms = get_all("SELECT * FROM special_rooms");
    }

    function invoices()
    {
        $this->simplbooks_api_call_count = 0;
        // If user didn't select event start and end dates, use the current month
        if (empty($_GET['event_start']) || empty($_GET['event_end'])) {
            $this->event_start = date('Y-m-01');
            $this->event_end = date('Y-m-t');
        } else {
            $this->event_start = $_GET['event_start'];
            $this->event_end = $_GET['event_end'];
        }
        // validate that the dates are in the correct format
        validate($this->event_start, IS_DATE);
        validate($this->event_end, IS_DATE);
        // do not allow more than 32 days to be selected
        if (strtotime($this->event_end) - strtotime($this->event_start) > 62 * 24 * 60 * 60) {
            $this->error = 'You can only select 62 days at a time';
            return;
        }

        $ordersWithoutPrices = get_all("
            SELECT *
            FROM orders
                     LEFT JOIN bookings USING (order_id)
                     LEFT JOIN users USING (user_id)
                     LEFT JOIN companies ON companies.company_id = orders.company_id
            WHERE date(booking_start) > '$this->event_start' AND date(booking_start) < '$this->event_end'
              AND dont_send_invoice = 0
              AND orders.deleted = 0
                AND bookings.deleted = 0
            GROUP BY order_id");

        // Iterate over the orders
        $result = [];
        foreach ($ordersWithoutPrices as $order) {

            // Get order details
            $orderWithPrices = Orders::get($order['order_id']);

            // Write company_name
            $result[$order['simplbooks_client_id']]['company_name'] = $order['company_name'];

            // Initialize orders_total
            if (!isset($result[$order['simplbooks_client_id']]['orders_total']))
                $result[$order['simplbooks_client_id']]['orders_total'] = 0;

            // Initialize order count
            if (!isset($result[$order['simplbooks_client_id']]['order_count']))
                $result[$order['simplbooks_client_id']]['order_count'] = 0;


            // Add current order's sum to orders_total
            $result[$order['simplbooks_client_id']]['orders_total'] = +$orderWithPrices['sum'];

            // Increment order count
            $result[$order['simplbooks_client_id']]['order_count']++;

            // Add order data
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['order_id'] = $order['order_id'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['order_made'] = $order['order_made'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['discount'] = $orderWithPrices['discount'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['booking_start'] = $order['booking_start'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['sum'] = $orderWithPrices['sum'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['payment_status_name'] = $orderWithPrices['payment_status_name'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['total_number_of_people'] = $orderWithPrices['total_number_of_people'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['order_billed_rent_percentage'] = $orderWithPrices['order_billed_rent_percentage'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['order_remarks'] = $orderWithPrices['order_remarks'];

            // Add order rooms and services
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['rooms'] = $orderWithPrices['rooms'];
            $result[$order['simplbooks_client_id']]['orders'][$order['order_id']]['services'] = $orderWithPrices['services'];

            // Find the earliest order_made date
            if (!isset($result[$order['simplbooks_client_id']]['earliest_order_made']) || $result[$order['simplbooks_client_id']]['earliest_order_made'] > $order['order_made'])
                $result[$order['simplbooks_client_id']]['earliest_order_made'] = $order['order_made'];
        }

        foreach ($result as $simplbooks_client_id => $company) {

            // Get order invoices from Simplbooks
            $simplbooksInvoices = SimplBooks::rawRequest('invoices/list', json_encode([
                "created_from" => $company['earliest_order_made'],
                "client_id" => $simplbooks_client_id,
                "per_page" => 1000,
            ]));

            // Increment Simplbooks API call count
            $this->simplbooks_api_call_count++;


            // Initialize invoices_total
            if (!isset($company['invoices_total']))
                $result[$simplbooks_client_id]['invoices_total'] = 0;

            // Initialize invoice count
            if (!isset($company['invoice_count']))
                $result[$simplbooks_client_id]['invoice_count'] = 0;


            // Iterate over the invoices
            $this->error = $this->validateResponse($simplbooksInvoices);

            if (!$this->error && isset($simplbooksInvoices['data'])) foreach ($simplbooksInvoices['data'] as $invoice) {

                // Point to the actual invoice
                $invoice = $invoice['invoices'];

                // Get invoice details from Simplbooks
                $invoiceDetails = Simplbooks::rawRequest('invoices/get/' . $invoice['id'], null);

                // Increment Simplbooks API call count
                $this->simplbooks_api_call_count++;

                $this->error = $this->validateResponse($invoiceDetails);
                if ($this->error) break;
                $invoiceDetails = $invoiceDetails['data'];

                // Add current invoice's sum to invoices_total
                $result[$simplbooks_client_id]['invoices_total'] += $invoice['sum'];

                // Increment invoice count
                $result[$simplbooks_client_id]['invoice_count']++;

                // Add invoice metadata
                $result[$simplbooks_client_id]['invoices'][$invoice['id']]['number'] = $invoice['number'];
                $result[$simplbooks_client_id]['invoices'][$invoice['id']]['sum'] = $invoice['sum'];
                $result[$simplbooks_client_id]['invoices'][$invoice['id']]['created_time'] = $invoiceDetails['Invoice']['created_time'];
                $result[$simplbooks_client_id]['invoices'][$invoice['id']]['modified_time'] = $invoiceDetails['Invoice']['modified_time'];
                $result[$simplbooks_client_id]['invoices'][$invoice['id']]['additional_info'] = $invoiceDetails['Invoice']['additional_info'];

                // Add invoice rows
                foreach ($invoiceDetails['Task'] as $invoiceRow) {
                    $result[$simplbooks_client_id]['invoices'][$invoice['id']]['rows'][] = [
                        'name' => $invoiceRow['name'],
                        'price_per_unit' => $invoiceRow['price_per_unit'],
                        'amount' => $invoiceRow['amount'],
                        'sum' => $invoiceRow['price_per_unit'] * $invoiceRow['amount']
                    ];
                }
            }
        }

        // Generate months array (from current month to 12 months ago)
        $this->months = [];
        for ($i = 0; $i < 12; $i++) {
            $this->months[] = date('Y-m', strtotime("-$i months"));
        }

        $this->result = $result;
    }

    /**
     * Fetches and organizes invoice data grouped by company
     *
     * This function:
     * 1. Retrieves companies with unbilled rent (less than 100%)
     * 2. Gets all orders for these companies (except excluded ones)
     * 3. Gets all invoices for these companies
     * 4. Organizes data into class properties for view rendering
     *
     * Class properties populated:
     * - $this->invoices: Array of all invoices indexed by invoice ID
     * - $this->invoices_by_company: Invoices grouped by company ID
     * - $this->orders_by_company: Orders grouped by company ID
     * - $this->companies: Company details indexed by company ID
     */
    function invoices_by_company()
    {
        // Get list of companies that have partially billed orders (rent < 100%)
        $company_ids = get_col("SELECT DISTINCT company_id FROM orders WHERE order_billed_rent_percentage < 100 AND deleted = 0");
        $company_ids = array_filter($company_ids); // Remove empty/null values
        $company_ids = implode(',', $company_ids); // Convert to comma-separated string for SQL

        // Fetch all relevant orders
        // Excludes deleted orders and specific companies (Diara, Aktoseidon)
        $orders = get_all("
            SELECT * FROM orders 
                LEFT JOIN users USING (user_id)
                LEFT JOIN payment_statuses USING (payment_status_id)
            WHERE orders.deleted = 0 
                AND orders.company_id IN ($company_ids)
                AND orders.company_id != 32926 -- Diara
                AND orders.company_id != 6300 -- Aktoseidon
            ORDER BY order_id DESC ");

        // Fetch all invoices for these companies
        $invoices = get_all("
            SELECT * 
            FROM simplbooks_invoices 
                LEFT JOIN companies ON simplbooks_invoices.client_id = companies.simplbooks_client_id 
            WHERE company_id IN ($company_ids)
            ORDER BY simplbooks_invoices.created DESC");

        // Organize invoices into lookup arrays
        foreach ($invoices as $invoice) {
            $this->invoices[$invoice['id']] = $invoice;
            $this->invoices_by_company[$invoice['company_id']][$invoice['id']] = $invoice;

            // Commented out code for future implementation of detailed invoice fetching
//            $invoiceDetails = SimplBooks::rawRequest('invoices/get/' . $invoice['id'], json_encode([
//                "page" => $page,
//                "per_page" => $per_page
//            ]));
//            var_dump($invoiceDetails);
//            die();
        }

        var_dump(count($invoices));

        // Group orders by company
        foreach ($orders as $order) {
            $this->orders_by_company[$order['company_id']][] = $order;
        }

        // Fetch and organize company details
        $company_ids = array_column($orders, 'company_id');
        $companies = get_all("SELECT * FROM companies WHERE company_id IN (" . implode(',', $company_ids) . ")");

        // Create company lookup array
        foreach ($companies as $company) {
            $this->companies[$company['company_id']] = $company;
        }
    }

    function ajax_edit_translation()
    {
        $translation_id = (int)$_POST['translation_id'];
        $key = $_POST['key'];
        $value = HtmlUtils::purify($_POST['value']);

        if (!empty($value)) {
            exit(update('translations', [$key => $value], 'translation_id = ' . $translation_id) ? 'Ok' : 'Fail');
        }
    }

    function ajax_delete_info_box()
    {
        $info_box_id = (int)$_POST['info_box_id'];

        exit(update('info_boxes', ['deleted' => 1],
            'info_box_id = ' . $info_box_id) ? 'Ok' : 'Fail');
    }

    function ajax_edit_info_box()
    {

        $info_box_id = (int)$_POST['info_box_id'];

        $edit = get_first("SELECT * FROM info_boxes WHERE info_box_id = $info_box_id");;
        include('views/admin/info_box_edit.php');

        exit();
    }

    function ajax_info_box_edit()
    {
        $toEdit = (int)$_POST['info_box_id'];

        update('info_boxes', $_POST['info_box'], "info_box_id=$toEdit");
    }

    function ajax_info_box_add()
    {
        $info_box_id = insert('info_boxes', $_POST);

        exit(json_encode(['result' => $info_box_id ? 'OK' : 'FAIL', 'info_box_id' => $info_box_id]));
    }

    function ajax_delete_service()
    {
        $service_id = (int)$_POST['service_id'];

        // If service has been used in order, it cannot be deleted (FK constraint)
        update("services", ['service_deleted' => 1], "service_id = $service_id");
    }

    function ajax_get_service()
    {

        $service_id = (int)$_POST['service_id'];

        $service = get_first("SELECT * FROM services WHERE service_id = $service_id AND service_deleted = 0");
        stop(200, $service);

    }

    function ajax_edit_service()
    {
        $service_id = (int)$_POST['service_id'];

        $data = [
            'service_name' => $_POST['service_name'],
            'service_description' => $_POST['service_description'],
            'service_unit_singular' => $_POST['service_unit_singular'],
            'service_unit_plural' => $_POST['service_unit_plural'],
            'service_quantity_options' => $_POST['service_quantity_options'],
            'service_group_id' => $_POST['service_group_id'],
            'service_price' => $_POST['service_price']];

        update('services', $data, "service_id=$service_id");

        stop(200, Service::get($service_id));
    }

    function add_service()
    {
        $service_id = Service::addEdit('add', $_POST);
        stop(200, Service::get($service_id));

    }

    function edit_service()
    {
        Service::addEdit('edit', $_POST);
    }

    function ajax_delete_service_group()
    {
        $service_group_id = (int)$_POST['service_group_id'];

        q("DELETE FROM service_groups WHERE service_group_id = $service_group_id");

        // Delete service group image
        $image_file = dirname(__DIR__) . "/uploads/ServiceGroup$service_group_id.jpg";
        if (file_exists($image_file)) {
            unlink($image_file);
        }
    }

    function ajax_get_service_group()
    {
        try {
            stop(200, ServiceGroup::get($_POST['service_group_id']));
        } catch (\Exception $e) {
            stop($e->getCode(), $e->getMessage());
        }
    }

    function ajax_edit_service_group()
    {
        $service_group_id = (int)$_POST['service_group_id'];

        update('service_groups', [
            'service_group_name' => $_POST['service_group_name'],
            'service_group_description' => $_POST['service_group_description'],
            'service_group_multiselect' => empty($_POST['service_group_multiselect']) ? 0 : 1,
            'service_group_image_location' => $_POST['service_group_image_location']
        ], "service_group_id=$service_group_id");
    }

    function ajax_upload_service_group_image()
    {

        if (empty($_GET['service_group_id']) || !($_GET['service_group_id'] > 0)) {
            stop(400, "Invalid service_group_id");
        }

        $project_root = dirname(__DIR__);
        $relative_path_to_file = "uploads/ServiceGroup" . $_GET['service_group_id'] . ".jpg";
        $full_path_to_file = "$project_root/$relative_path_to_file";

        // Convert file contents to optimized JPG string
        $jpeg_data = Images::getContentAsString($_FILES['file']['tmp_name'], 'jpg', 688, 478);

        // Check that conversion succeeded
        if (empty($jpeg_data)) {
            throw new \Exception('Converting image to JPG failed. Check that image is not too large!');
        }

        // Write file to disk
        file_put_contents($full_path_to_file, $jpeg_data);


        // Write image file name to database
        update('service_groups', ['service_group_image' => $relative_path_to_file], "service_group_id = $_GET[service_group_id]");
    }

    function ajax_add_service_group()
    {

        if (empty($_POST['service_group_name'])) {
            stop(400, 'Invalid or missing name');
        }
        if (empty($_POST['service_group_image_location'])) {
            stop(400, 'Invalid or missing image location');
        }

        $service_group_id = insert('service_groups', [
            'service_group_name' => $_POST['service_group_name'],
            'service_group_description' => $_POST['service_group_description'],
            'service_group_multiselect' => empty($_POST['service_group_multiselect']) ? 0 : 1,
            'service_group_image_location' => $_POST['service_group_image_location']
        ]);

        stop(200, $service_group_id);
    }


    function ajax_delete_booking()
    {
        $booking_id = (int)$_POST['booking_id'];
        update('bookings', ['deleted' => 1], "booking_id = $booking_id");
        exit("Ok");
    }

    function ajax_edit_booking()
    {
        $booking_id = (int)$_POST['booking_id'];
        $key = $_POST['key'];
        $value = $_POST['value'];

        if (strtotime($value)) {
            // If value is date, change its format from d-m-Y H:i to Y-m-d H:i:s
            $new_date = date('Y-m-d H:i:s', strtotime($value));
            update('bookings', [$key => $new_date], "booking_id = $booking_id");
            exit("Ok");

        } else {
            update('bookings', [$key => $value], "booking_id = $booking_id");
            exit("Ok");

        }
    }

    function ajax_edit_user()
    {

        $user_id = (int)$_POST['user_id'];

        update('users', [$_POST['key'] => $_POST['value']], "user_id = $user_id");

        exit("Ok");
    }

    function ajax_delete_user()
    {
        $user_id = (int)$_POST['user_id'];

        update('users', ['deleted' => 1], "user_id = $user_id");

        exit("Ok");
    }

    function ajax_switch_user()
    {
        $user_id = (int)$_POST['user_id'];

        if (User::get()) {
            if ($user_id != $this->auth->user_id) {
                $_SESSION['real_user_id'] = $this->auth->user_id;
                $_SESSION['user_id'] = $user_id;
            }
            exit("Ok");
        }

        exit("Fail");
    }

    function ajax_edit_order()
    {
        $order_id = (int)$_POST['order_id'];

        update('orders', [$_POST['key'] => $_POST['value']], "order_id = $order_id");

        exit("Ok");
    }

    function ajax_delete_order()
    {
        $order_id = (int)$_POST['order_id'];

        update('orders', ['deleted' => 1], "order_id = $order_id");

        exit("Ok");
    }

    /**
     * Changes given order's status to given value
     * @throws \Exception when parameter is not valid
     */
    function ajax_edit_order_status()
    {
        validate($_POST['order_id']);
        validate($_POST['payment_status_id']);

        update(
            'orders',
            ['payment_status_id' => (int)$_POST['payment_status_id']],
            'order_id = ' . (int)$_POST['order_id']);
    }

    function ajax_edit_room()
    {

        $room_id = (int)$_POST['room_id'];
        $room_type = $_POST['room_type'];
        $room_type == 'usual' ? $special = false : $special = true;

        $images = \Broneerimiskeskkond\Room::get_all_images($room_id, $special);

        if ($room_type == 'usual') {
            $extras = \Broneerimiskeskkond\Room::get_extras();
            $room_extras = get_all("SELECT * FROM room_extras WHERE room_id=$room_id");

            foreach ($room_extras as $extra) {
                $roomExtra[$extra['extra_id']] = 1;
            }

            $designs = \Broneerimiskeskkond\Room::get_designs();
            $room_designs = get_all("SELECT * FROM room_designs WHERE room_id=$room_id");

            foreach ($room_designs as $design) {
                $roomDesign[$design['design_id']] = 1;
            }
            $edit = \Broneerimiskeskkond\Room::get($room_id);
            include('views/admin/room_edit.php');
        } else {
            $edit = \Broneerimiskeskkond\Room::get_special($room_id);
            include('views/admin/special_room_edit.php');
        }


        exit();

    }

    function edit_extras()
    {

        $extras = \Broneerimiskeskkond\Room::get_extras();

        include('views/admin/extras_edit.php');

        exit();

    }

    function edit_supplies()
    {

        $supplies = \Broneerimiskeskkond\Room::get_supplies();

        include('views/admin/supplies_edit.php');

        exit();

    }

    function edit_designs()
    {

        $designs = \Broneerimiskeskkond\Room::get_designs();

        include('views/admin/designs_edit.php');

        exit();

    }

    function remove_extra()
    {
        $extra_id = (int)$_POST['id'];
        update("extras", ['extra_in_use' => 0], "extra_id=$extra_id");
        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function remove_supply()
    {
        $supply_id = (int)$_POST['id'];
        update("supplies", ['supply_in_use' => 0], "supply_id=$supply_id");
        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function remove_design()
    {
        $design_id = (int)$_POST['id'];
        update("designs", ['design_in_use' => 0], "design_id=$design_id");
        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function post_add_extra()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $newExtra) {
                $data['extra_name'] = $newExtra['name'];
                $data['extra_price'] = $newExtra['price'];
                $data['extra_unit'] = $newExtra['unit'];
                insert("extras", $data);
            }

        }
        header("Location:" . BASE_URL . 'admin/rooms');

    }

    function post_add_supply()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $newSupply) {
                $data['supply_name'] = $newSupply['name'];
                $data['supply_price'] = $newSupply['price'];
                insert("supplies", $data);
            }

        }
        header("Location:" . BASE_URL . 'admin/rooms');

    }

    function post_add_design()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $newDesign) {
                $data['design_name'] = $newDesign['name'];
                $data['design_price'] = $newDesign['price'];
                insert("designs", $data);
            }

        }
        header("Location:" . BASE_URL . 'admin/rooms');

    }

    function post_edit_extra()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $extra_id => $extra) {
                $extra_id = (int)$extra_id;
                $data['extra_name'] = $extra['name'];
                $data['extra_price'] = $extra['price'];
                $data['extra_unit'] = $extra['unit'];
                $data['extra_in_use'] = 1;
                update("extras", $data, "extra_id=$extra_id");
            }

        }

        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function post_edit_supply()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $supply_id => $supply) {
                $supply_id = (int)$supply_id;
                $data['supply_name'] = $supply['name'];
                $data['supply_price'] = $supply['price'];
                $data['supply_in_use'] = 1;
                update("supplies", $data, "supply_id=$supply_id");
            }

        }

        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function post_edit_design()
    {
        if (isset($_POST['edit']) && $_POST['edit']) {

            foreach ($_POST['edit'] as $design_id => $design) {
                $design_id = (int)$design_id;
                $data['design_name'] = $design['name'];
                $data['design_price'] = $design['price'];
                $data['design_in_use'] = 1;
                update("designs", $data, "design_id=$design_id");
            }

        }

        header("Location:" . BASE_URL . 'admin/rooms');
    }

    function ajax_generate_and_send_final_invoice()
    {
        $order_id = (int)$_POST['order_id'];
        $order = Orders::get($order_id);

        if ($invoice = Invoices::create_from_order(
            $order_id,
            1,
            true,
            3
        )) {
            Invoices::send(
                $invoice->inserted_id,
                $invoice->number,
                $order_id,
                $order['invoice_email'],
                'FINAL'
            );

            exit(json_encode($invoice));
        }

    }

    function ajax_generate_deposit_credit()
    {
        $order_id = (int)$_POST['order_id'];
        $deposit_invoice_id = get_one("SELECT deposit_invoice_id FROM orders WHERE order_id=$order_id");
        $invoice = Invoices::create_credit_invoice($deposit_invoice_id);

        exit(json_encode($invoice));
    }

    function ajax_set_item_changeability()
    {
        $menu_item_id = (int)$_POST['item_id'];
        $value = (int)$_POST['value'];
        exit(update('menu_items', ['is_changeable' => $value], 'menu_item_id = ' . $menu_item_id) ? 'Ok' : 'Fail');
    }

    function ajax_update_invoice_to_send()
    {
        $order_id = $_POST['order_id'];
        $order['dont_send_invoice'] = $_POST['value'];
        exit(update('orders', $order, 'order_id = ' . $order_id) ? 'OK' : 'FAIL');
    }

    function ajax_update_order_billed_rent_percentage()
    {
        $order_id = $_POST['order_id'];
        $order['order_billed_rent_percentage'] = $_POST['value'];
        exit(update('orders', $order, 'order_id = ' . $order_id) ? 'OK' : 'FAIL');
    }

    function ajax_update_order_reminder()
    {
        update('orders', ['send_reminder' => $_POST['value']], "order_id = $_POST[order_id]");
    }

    function settings()
    {
        $this->settings = Settings::get();
    }

    function ajax_update_setting()
    {
        $setting = addslashes($_POST['setting']);
        update("settings", ['value' => $_POST['value']], "setting = '$setting'");
    }

    function ajax_add_invoice_row()
    {
        update("orders", [
            "invoice_has_extra_row" => $_POST['value']
        ], "order_id = {$_POST['order_id']}");

    }

    function ajax_delete_translations()
    {
        $translation_ids = $_POST['translation_ids'];

        Translation::delete($translation_ids);
    }

    function ajax_toggle_language()
    {
        Language::toggle($_POST['language_id'], $_POST['state']);

    }

    function ajax_get_language()
    {
        return Language::get();
    }

    function ajax_delete_room()
    {
        validate($_POST['room_id']);
        if (Room::in_use()) {
            stop(400) . ("Room in use");
        }
        Room::remove($_POST['room_id']);
    }

    /**
     * @param mixed $simplbooksResponse
     * @throws \Exception
     */
    public function validateResponse(mixed $simplbooksResponse)
    {
        if ($simplbooksResponse['status'] != 200) {
            $e = new \Exception(json_encode($simplbooksResponse));
            send_error_report($e);
            return $this->formatErrorMessage($simplbooksResponse);
        }
        return '';
    }

    private function formatErrorMessage($arr)
    {
        $status = $arr['status'];
        $duration = $arr['duration'];
        $inserted_id = $arr['inserted_id'];
        // Try to read errors from $arr
        $errors = [];
        if (isset($arr['errors'])) {
            $errors = $arr['errors'];
        } elseif (isset($arr['data']['errors'])) {
            $errors = $arr['data']['errors'];
        } elseif (isset($arr['error'])) {
            $error[] = $arr['error'];
        }

        $output = "<h1>Simplbooks Error $status</h1>";
        $output .= "<p>Duration: $duration</p>";
        $output .= "<p>Inserted ID: $inserted_id</p>";

        if (count($errors) === 1) {
            $output .= "<p>Error message: {$errors[0]}</p>";
        } else {
            $output .= "<ul>";
            foreach ($errors as $error) {
                $output .= "<li>$error</li>";
            }
            $output .= "</ul>";
        }

        return $output;
    }

}
