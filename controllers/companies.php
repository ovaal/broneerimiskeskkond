<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 15.01.2018
 * Time: 13:43
 */

namespace Halo;


use Broneerimiskeskkond\Company;

class companies extends Controller
{

    // For auth_template company name autocomplete
    public $requires_auth = false;

    function ajax_get_for_select()
    {
        $query = addslashes($_GET['query']);
        $companies = Company::getAll("company_name LIKE '%$query%'");

        $result = [];
        foreach ($companies as $company) {
            $result[] = [
                "id" => $company['company_id'],
                "text" => $company['company_name']
            ];
        }

        stop(200, $result);
    }

    function ajax_get()
    {
        // Prevent SQLi
        $company_id = (int)$_POST['company_id'];

        stop(200, Company::get($company_id));
    }

    function ajax_add()
    {
        if (fieldsAreEmpty([
            $_POST['company_registry_nr'],
            $_POST['company_name'],
            $_POST['company_city'],
            $_POST['company_street'],
            $_POST['company_postal_code']
        ])) {
            stop(400, __("Please fill in all the company data."));
        }

        $company_registry_nr = (int)$_POST['company_registry_nr'];

        // Check that the new company doesn't already exist
        if (Company::get("company_registry_nr = $company_registry_nr")) {
            stop(400, __("Company with given registry number already exists."));
        }
        $company_name = addslashes($_POST['company_name']);
        if ($company = Company::get("company_name = '$company_name'")) {
            stop(400, __("Company with given name already exists. It's registry number is: " . $company['company_registry_nr']));
        }

        // Add the new company
        $company_id = insert("companies", $_POST);

        stop(200, Company::get($company_id));
    }

}