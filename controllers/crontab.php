<?php namespace Halo;

use Broneerimiskeskkond\Company;
use Broneerimiskeskkond\PaymentStatus;
use ZipArchive;
use SimplBooks\SimplBooks;
use Prewk\XmlStringStreamer;
use Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\Invoices;
use Prewk\XmlStringStreamer\Stream;
use Prewk\XmlStringStreamer\Parser;
use Broneerimiskeskkond\Email as Mail;

class CronTab extends Controller
{
    public $requires_auth = false;

    function index()
    {
        die();
    }

    function send_invoices()
    {
        $orders = Orders::getAll([
            'date(booking_start)' => date('Y-m-d'),
            'dont_send_invoice' => 0,
        ]);

        foreach ($orders as $order) {

            if ($invoice = Invoices::create_from_order(
                $order['order_id'],
                1,
                true,
                $order['state_institution'] ? 21 : 3
            )) {
                Invoices::send(
                    $invoice->inserted_id,
                    $invoice->number,
                    $order['order_id'],
                    $order['invoice_email'] ?: $order['email'],
                    'FINAL'
                );
            }
        }

        die();
    }

    // Reminder email to order additional services
    function additional_services_reminder_email()
    {
        // Helper function to add working days (Monday to Friday) to a given timestamp.
        function addWorkingDays($timestamp, $days)
        {
            while ($days > 0) {
                $timestamp = strtotime('+1 day', $timestamp);
                // date('N') returns 1 (Monday) to 7 (Sunday); 1-5 are working days.
                if (date('N', $timestamp) < 6) {
                    $days--;
                }
            }
            return $timestamp;
        }

        // Calculate the target date: 5 working days from today.
        $targetTimestamp = addWorkingDays(time(), 5);
        $targetDate = date('Y-m-d', $targetTimestamp);

        // Retrieve orders scheduled for the target date.
        $ordersForTargetDate = Orders::get_orders_by_date($targetDate);

        foreach ($ordersForTargetDate as $order) {
            // Only send the reminder if the order's reminder flag is enabled.
            if ($order['send_reminder'] == 1) {
                $order_id   = $order['order_id'];
                $user_email = $order['email'];

                $body = __("Dear Customer,") . "<br><br>" .
                    sprintf(
                        __("We hope you are doing well. This is a friendly reminder that your upcoming event is scheduled for %s and you still have the option to add additional services to enhance your experience."),
                        $targetDate
                    ) . "<br><br>" .
                    __("Please note that you have until 16:00 on the day of your booking to update your order. To review or modify your order, simply click the link below:") . "<br>" .
                    "<a href='" . BASE_URL . "myorders/$order_id" . "'>" . __("View My Order") . "</a>" . "<br><br>" .
                    __("If you have any questions or need further assistance, please don't hesitate to contact us.") . "<br><br>" .
                    __("Thank you for choosing Ovaalstuudio!") . "<br>" .
                    "Ovaalstuudio";

                Mail::send(
                    $user_email,
                    __('Reminder from ' . PROJECT_NAME),
                    $body,
                    false,
                    [],
                    SITE_OWNER_EMAIL
                );
            }
        }
        die();
    }

    // Delete inactivated users that are older than 24 hours
    function delete_inactivated_users()
    {
        $inactive_users = get_all("SELECT * FROM users 
                                         WHERE is_active = 0
                                         AND deleted = 0
                                         AND user_created_at < DATE_SUB(NOW(), INTERVAL 24 HOUR)");

        foreach ($inactive_users as $inactive_user) {
            $user_id = $inactive_user['user_id'];
            update("users", ["deleted" => 1], "user_id = $user_id");
        }
        die();
    }

    /**
     * Update the Äriregister data
     */
    function update_ariregister_data()
    {
        set_time_limit(1200);

        $zip_file_name = '.temp/ariregister_xml.zip';

        // Download the zip file
        $fh = fopen($zip_file_name, 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://avaandmed.rik.ee/andmed/ARIREGISTER/ariregister_xml.zip');
        curl_setopt($ch, CURLOPT_FILE, $fh);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // this will follow redirects
        curl_exec($ch);
        curl_close($ch);
        fclose($fh);

        // Unzip the file
        $zip = new ZipArchive;
        $res = $zip->open($zip_file_name);
        if ($res === TRUE) {
            $zip->extractTo('.temp');
            $zip->close();
        } else {
            die('error');
        }

        // Get the unzipped file name
        foreach (glob(".temp/*.xml") as $filename) {
            $xml_file_name = $filename;
        }

        // Prepare our stream to be read with a 1kb buffer
        $stream = new Stream\File($xml_file_name, 1024);

        // Construct the default parser (StringWalker)
        $parser = new Parser\StringWalker();

        // Create the streamer
        $streamer = new XmlStringStreamer($parser, $stream);

        // Iterate through the nodes
        while ($node = $streamer->getNode()) {
            $simpleXmlNode = simplexml_load_string($node);

            // Insert the company (Updates old entries, company_registry_nr is the unique key)
            $company_id = insert("companies", [
                'company_name' => $simpleXmlNode->nimi,
                'company_registry_nr' => $simpleXmlNode->ariregistri_kood,
                'company_kmkr_nr' => $simpleXmlNode->kmkr_nr,
                'company_status' => $simpleXmlNode->ettevotja_staatus_tekstina,
                'company_street' => $simpleXmlNode->ettevotja_aadress->asukoht_ettevotja_aadressis,
                'company_city' => $simpleXmlNode->ettevotja_aadress->asukoha_ehak_tekstina,
                'company_postal_code' => $simpleXmlNode->ettevotja_aadress->indeks_ettevotja_aadressis,
                'company_full_address' => $simpleXmlNode->ettevotja_aadress->ads_normaliseeritud_taisaadress,
                'company_ariregister_link' => $simpleXmlNode->teabesysteemi_link
            ]);

            // Check that we have the company_id
            if (!$company_id) {
                $company_id = Company::get("company_registry_nr = $simpleXmlNode->ariregistri_kood")['company_id'];
            }

            // If client exists in SimplBooks, update its data
            if ($simplbooks_client = Company::inSimplbooks($company_id)) {
                // Set the companies simplbooks_client_id if the client exists
                update('companies', ['simplbooks_client_id' => $simplbooks_client->id], "company_id = $company_id");

                // Get the updated company
                $company = Company::get($company_id);

                // Update the client in SimplBooks
                SimplBooks::updateClient(
                    $company['simplbooks_client_id'],
                    $company['company_name'],
                    $company['company_city'],
                    $company['company_street'],
                    $company['company_postal_code'],
                    $company['company_registry_nr']
                );
            }
        }

        // Remove the files
        unlink($zip_file_name);
        unlink($xml_file_name);

        die();
    }

    function update_invoice_cache()
    {

        $page = 1;
        $per_page = 1000;

        try {
            do {
                // Get order invoices from Simplbooks
                $simplbooksInvoices = SimplBooks::rawRequest('invoices/list', json_encode([
                    "page" => $page,
                    "per_page" => $per_page
                ]));

                foreach ($simplbooksInvoices['data'] as $invoice) {
                    $invoice = $invoice['invoices'];
                    $invoice_id = insert('simplbooks_invoices', [
                        'id' => $invoice['id'],
                        'client_id' => $invoice['client_id'],
                        'client_name' => $invoice['client_name'],
                        'client_reg_no' => $invoice['client_reg_no'],
                        'client_vat_no' => $invoice['client_vat_no'],
                        'created' => $invoice['created'],
                        'transaction_date' => $invoice['transaction_date'],
                        'due' => $invoice['due'],
                        'number' => $invoice['number'],
                        'sum' => $invoice['sum'],
                        'vat' => $invoice['vat'],
                        'rounding' => $invoice['rounding'],
                        'currency_name' => $invoice['currency_name'],
                        'currency_rate' => $invoice['currency_rate'],
                        'sent' => $invoice['sent'],
                        'paid' => $invoice['paid'],
                        'credit_invoice_for' => $invoice['credit_invoice_for'],
                        'proforma' => empty($invoice['proforma']) ? 0 : 1,  // Assuming boolean values as 1 or 0
                        'proforma_percent' => $invoice['proforma_percent'],
                        'total_sum' => $invoice['total_sum'],
                        'client_kmknr' => $invoice['client_kmknr']
                    ]);
                }

                // Increment the page number
                $page++;

            } while (count($simplbooksInvoices['data']) == $per_page);
        } catch (\Exception $e) {
            // Print the stack trace
            print_r($e->getTraceAsString());
            exit("Error: " . $e->getMessage());
        }

        // Get all orders
        $orders = get_all("SELECT * FROM orders WHERE deleted = 0");

        // Structure orders by company
        $orders_by_company = [];

        foreach ($orders as $order) {
            $companyId = $order['company_id'];

            // Check if this company has already been added; if not, initialize its keys.
            if (!isset($orders_by_company[$companyId])) {
                $orders_by_company[$companyId] = [
                    'orders' => [],
                    'order_total' => 0,  // Initialize with 0 to avoid undefined array key errors.
                ];
            }

            // Add the order to the company's orders list.
            $orders_by_company[$companyId]['orders'][] = $order;

            // Safely add the order's sum, using 0 if 'sum' isn't set.
            $orders_by_company[$companyId]['order_total'] += $order['sum'] ?? 0;
        }

        exit();
    }

    function update_invoice_row_cache()
    {
        // Get all Simplbooks invoice ids that don't have invoice rows
        $simplbooks_invoice_ids = get_col("
            SELECT i.id 
            FROM orders 
                JOIN companies c USING(company_id)
                JOIN simplbooks_invoices i ON c.simplbooks_client_id = i.client_id
                LEFT JOIN simplbooks_invoice_rows ir ON i.id = ir.invoice_id
            WHERE order_billed_rent_percentage < 100 
                AND orders.deleted = 0
                AND orders.company_id != 32926 -- Diara
                AND orders.company_id != 6300 -- Aktoseidon
                AND ir.id IS NULL
            GROUP BY i.id
            ORDER BY i.id");

        // Get invoice details from Simplbooks
        try {
            foreach ($simplbooks_invoice_ids as $simplbooks_invoice_id) {

                $invoice = SimplBooks::rawRequest("invoices/get/$simplbooks_invoice_id", null);

                // Write additional info to the invoice
                update('simplbooks_invoices', [
                    'additional_info' => $invoice['data']['Invoice']['additional_info']
                ], "id = $simplbooks_invoice_id");

                // Write the invoice rows
                foreach ($invoice['data']['Task'] as $row) {

                    insert('simplbooks_invoice_rows', [
                        'id' => $row['id'],
                        'invoice_id' => $simplbooks_invoice_id,
                        'warehouse_id' => $row['warehouse_id'],
                        'article_id' => $row['article_id'],
                        'code' => $row['code'],
                        'name' => $row['name'],
                        'contents' => $row['contents'],
                        'unit' => $row['unit'],
                        'amount' => $row['amount'],
                        'price_per_unit' => $row['price_per_unit'],
                        'worker' => $row['worker'],
                        'vat' => $row['vat'],
                        'vat_type_id' => $row['vat_type_id'],
                        'discount' => $row['discount'],
                    ]);
                }

            }
        } catch (\Exception $e) {
            exit("Error2: " . $e->getMessage());
        }
    }

}