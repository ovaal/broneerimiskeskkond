<?php namespace Halo;

class info_boxes extends Controller
{
    public $requires_admin = true;

    function index()
    {
        $this->info_boxes = get_all("SELECT * FROM info_boxes");
    }

    function view()
    {
        $info_box_id = addslashes($this->params[0]);
        $this->info_box = get_first("SELECT * FROM info_boxes WHERE info_box_id = $info_box_id");
    }

    function info_box_pdf()
    {

        $info_box_id = (int)$this->params[0];
        $info_box = get_first("SELECT * FROM info_boxes WHERE info_box_id = $info_box_id");

        if (!$info_box) {
            $errors[] = __('Info box data not found!');
            require 'templates/error_template.php';
            exit();
        }

        ob_start();
        include 'views/info_boxes/info_box_pdf.php';
        $html = ob_get_clean();

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');

        $mpdf->Output($info_box['info_box_name'] . '.pdf', 'D');
        exit("OK");

    }

}