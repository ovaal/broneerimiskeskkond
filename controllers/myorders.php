<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 02/06/16
 * Time: 17:29
 */

namespace Halo;


use Broneerimiskeskkond\AdditionalServices;
use Broneerimiskeskkond\Cart;
use Broneerimiskeskkond\Date;
use Broneerimiskeskkond\Extras;
use Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\Service;

class myorders extends Controller
{
    function index()
    {
        $user_id = $this->auth->user_id;
        $this->orders = \Broneerimiskeskkond\Orders::find_by_user($user_id);
        $this->bookings = \Broneerimiskeskkond\Orders::get_bookings_data($user_id);
        unset($_SESSION['myorders_view']);
    }

    function view()
    {
        $order_id = $this->params[0];
        $this->booking_info = \Broneerimiskeskkond\Bookings::serialize_by_order_id($order_id);


        // Check if user is allowed on this page
        if (!$this->auth->is_admin && $this->auth->user_id !== $this->booking_info['booking']['user_id']) {
            error_out(__("No rights to view this order (This order isn't created by you)"));
        }

        try {
            $this->order = Orders::get($order_id);
        } catch (\Exception $e) {
            error_out($e->getMessage());
        }

        Extras::processForOrderSummary($this->order['rooms']);


        $this->order['available_services'] = Service::translateNames(Service::getAll());
        $this->service_groups = Service::getByServiceGroups();
        $this->read_only = $this->booking_info['booking']['read_only'];
        $this->booking_date = strtotime($this->booking_info['booking']['booking_start']);
        $this->five_days_from_now = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + 5, date("Y")));
        $this->five_days = strtotime($this->five_days_from_now);
        $this->place = $this->booking_info['booking']['booking_start'];
        $this->coordinator_name = get_one("SELECT coordinator_name FROM orders WHERE order_id = $order_id");
        $this->total_number_of_people = get_one("SELECT total_number_of_people FROM orders WHERE order_id = $order_id");
        $this->humanReadableDate = Date::toHumanReadable($this->booking_info['booking']['booking_start']);

        $this->room_id = isset($_SESSION['extras_for_room']) ? $_SESSION['extras_for_room'] : $this->booking_info['room'][0]['room_id'];
        $this->extras = Extras::translateNames(Room::get_extras($this->room_id));

        // Unset previous catering data if exists (might be another orders data)
        if (isset($_SESSION['myorders_view'])) {
            unset($_SESSION['myorders_view']);
        }

        $_SESSION['myorders_view'] = [];
        $_SESSION['myorders_view']['order_id'] = $order_id;

    }

    function ajax_set_extras_for_room()
    {
        exit($_SESSION['extras_for_room'] = $_POST['room_id']);
    }

    function ajax_update_extras_for_room()
    {
        $order_id = $_POST['order_id'];
        $room_id = $_POST['room_id'];
        $extra_id = $_POST['extra_id'];
        $extra_price = $_POST['extra_price'];
        $booking_id = get_one("SELECT booking_id FROM bookings WHERE room_id = $room_id AND order_id = $order_id");
        $extra_is_checked = get_all("SELECT * FROM booking_extras WHERE booking_id = $booking_id AND extra_id = $extra_id");

        if (!empty($extra_is_checked)) {
            q("DELETE FROM booking_extras WHERE booking_id = $booking_id AND extra_id = $extra_id");
        } else {
            insert('booking_extras', ['booking_id' => $booking_id, 'extra_id' => $extra_id, 'booking_extra_price' => $extra_price]);
        }
    }

    function ajax_update_coordinator_name()
    {
        $order_id = $_POST['order_id'];
        $coordinator_name = $_POST['coordinator_name'];
        update('orders', ['coordinator_name' => $coordinator_name], "order_id = $order_id");
    }

    function ajax_change_password()
    {
        $new_pw = $_POST['new_pw'];
        $user = get_first("SELECT * FROM users WHERE user_id = {$_SESSION['user_id']}");

        $password_check = password_verify($_POST['old_pw'], $user['password']);

        if (!$password_check) {
            $return = __('Current password incorrect!');
        } else {
            $hashed_pw = password_hash($new_pw, PASSWORD_DEFAULT);
            update('users', ['password' => $hashed_pw], "user_id = " . $user['user_id']);
            $return = __('Your password has been updated!');
        }

        exit($return);
    }

}
