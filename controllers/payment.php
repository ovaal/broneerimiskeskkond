<?php namespace Halo;

use Broneerimiskeskkond\additional_services;
use Broneerimiskeskkond\Bookings;
use Broneerimiskeskkond\Cart;
use Broneerimiskeskkond\Company;
use Broneerimiskeskkond\Date;
use Broneerimiskeskkond\Email;
use Broneerimiskeskkond\Invoices;
use Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\PaymentStatus;
use Broneerimiskeskkond\PDF;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\Settings;
use Broneerimiskeskkond\User;
use SimplBooks\SimplBooks;

class payment extends Controller
{

    function index()
    {
        // Check that room, booking duration and number of people are set
        if (empty($_SESSION['selected_rooms']) || empty($_SESSION['booking_duration']) || empty($_SESSION['number_of_people'])) {

            // Redirect user to my orders page (hopefully the order was successfully submitted)
            header('Location: ' . BASE_URL . 'myorders');
            exit();
        }
        $this->user = User::get();
        $this->order = Cart::get();
        $this->rent = Cart::getRoomsSum();
        $this->room_names = Cart::getRoomNames();
        $this->bookingDate = Date::toHumanReadable($_SESSION['booking_date']);
        $this->display_100_percent_invoice_opt = Settings::get('100_PERCENT_INVOICE_OPTION');
        $this->discount = Cart::getDiscountAmount();

    }


    function ajax_send_email_quote()
    {
        // Generate a filename pointing to a system's /tmp folder
        $file_name = sys_get_temp_dir() . '/' . PROJECT_NAME . '_room_rent.pdf';

        // Get content of the quote in PDF format

        $company_name = isset($_POST['company_name']) ? $_POST['company_name'] : Company::get($this->auth->company_id)['company_name'];

        $data = Cart::get(false);
        $data['user_id'] = $this->auth->user_id; // PDF::create checks this to prevent other people from seeing user's orders
        $data['user_name'] = $this->auth->first_and_last_name;
        $data['user_organisation'] = $company_name;

        PDF::create(
            __('Quote for') . " $company_name",
            $data,
            $this->auth,
            $file_name,
            PDF::FILE
        );

        // Send this file to the user as an email attachment
        Email::send(
            $this->auth->email,
            __('A quote from Ovaal.ee'),
            __("Quote for rent in attachment"),
            $file_name,
            [],
            SITE_OWNER_EMAIL
        );

    }

    function ajax_check_date_availability()
    {
        if (empty($_SESSION['booking_date'])) {
            stop(400, "No rooms selected");
        }

        $booking_date = $_SESSION['booking_date'];

        $already_booked = array();
        foreach ($_SESSION['selected_rooms'] as $selected_room) {
            $room_id = $selected_room['room_id'];
            $start = $booking_date . ' ' . $selected_room['booking_start_time'];
            $end = $booking_date . ' ' . $selected_room['booking_end_time'];

            $check = get_all("SELECT
                                    room_id,
                                    room_name
                                  FROM
                                    bookings
                                    LEFT JOIN rooms USING (room_id)
                                    LEFT JOIN orders USING (order_id)
                                  WHERE
                                    orders.deleted = 0
                                    AND room_id = $room_id
                                    AND DATE(booking_start) = '$booking_date'
                                    AND (
                                      ('$start' < booking_end)
                                      AND ('$end' > booking_start)
                                      AND bookings.deleted = 0)");

            if ($check) {
                @$already_booked[] = $check[0]['room_name'];
            }

        }

        if (!empty($already_booked)) {
            stop(400, implode(", ", $already_booked));
        }
    }

    function ajax_coordinator_to_session()
    {
        $_SESSION['coordinator'] = $_POST['coordinator'];
    }

    function ajax_confirm()
    {
        $button = $_POST['button'];
        $attachments = [];

        $status_id = match ($button) {
            'btnGuaranteeLetter' => PaymentStatus::NO_INVOICE_SENT,
            'btnPaymentByInvoice', 'btnPaymentByInvoice100PercentNow' => PaymentStatus::DEPOSIT_INVOICE_SENT,
        };
        if (empty($_SESSION['selected_rooms'])) {
            stop(400, __("No rooms selected") . '. <a href="'. BASE_URL .'">' . __("Start over?") . '</a>');
        }

        if (empty($_POST['invoice_email']) || !filter_var($_POST['invoice_email'], FILTER_VALIDATE_EMAIL)) {
            stop(400, __("Invalid email address"));
        }

        $order_id = Orders::create(
            $_POST['company_id'],
            $_POST['coordinator'],
            $_POST['invoice_additional_info']
        );

        if ($button === 'btnGuaranteeLetter') {

            $body =
                __("Hello!") . "<br><br>" .
                __("As a confirmation of your booking, please send the letter of guarantee/purchase order to: ovaal@ovaal.ee.") . "<br><br>" .
                __("Thank You.") . "<br><br>" .
                __("Best regards,") . "<br>" .
                "Ovaalstuudio";

            // Invoice to the client
            Email::send(
                $_POST['invoice_email'],
                __('Invoice from ' . PROJECT_NAME),
                $body,
                null,
                $attachments,
                SITE_OWNER_EMAIL
            );

            Orders::updateStatus($order_id, $status_id);


        } else {

            $invoice = Invoices::create_from_order(
                $order_id,
                match ($button) {
                    'btnPaymentByInvoice' => 0.25,
                    'btnPaymentByInvoice100PercentNow' => 1
                },
                false,
                match ($button) {
                    'btnPaymentByInvoice' => 4,
                    'btnPaymentByInvoice100PercentNow' => 3
                }
            );

            // Store Simplbooks invoice id and number locally and set order as sent
            $updated_order['deposit_invoice_number'] = $invoice->number;
            $updated_order['deposit_invoice_id'] = $invoice->inserted_id;
            $updated_order['payment_status_id'] = PaymentStatus::DEPOSIT_INVOICE_SENT;
            update('orders', $updated_order, "order_id = $order_id");

            $pdf_invoice = SimplBooks::sendPDFToBrowser($invoice->inserted_id, true);
            $e_invoice = SimplBooks::getXML($invoice->inserted_id, true);

            $attachments = [
                "Invoice_nr_{$invoice->number}{$order_id}.pdf" => $pdf_invoice,
                "Invoice_nr_{$invoice->number}{$order_id}.xml" => $e_invoice
            ];

            $body =
                __("Hello!") . "<br><br>" .
                __('An invoice is attached to this message.') . "<br><br>" .
                __("Thank You.") . "<br><br>" .
                __("Best regards,") . "<br>" .
                "Ovaalstuudio";

            Invoices::send(
                $invoice->inserted_id,
                $invoice->number,
                $order_id,
                $_POST['invoice_email'],
                'DEPOSIT'
            );
        }

        User::updateInvoiceAddress($_POST['invoice_email']);

        // Clear session
        Bookings::clear();
    }

}
