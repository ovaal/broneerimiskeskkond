<?php namespace Halo;

use \Broneerimiskeskkond\Orders;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\Service;
use Broneerimiskeskkond\User;
use Broneerimiskeskkond\PDF as pdfClass;


/**
 * Created by PhpStorm.
 * User: henno
 * Date: 01/12/16
 * Time: 10:45
 */
class pdf extends Controller
{

    public $requires_auth = true;


    function order_summary()
    {

        // All these variables are used by templates/pdf/order_summary.php which is included later as a separate file

        $order_id = $this->getId();

        try {
            $order = Orders::get($order_id);
        } catch (\Exception $e) {
            error_out($e->getMessage());
        }

        if (empty($order)) {
            error_out(__('No such order'));
        }


        pdfClass::create(
            __('Order') . ' #' . $order['order_id'],
            $order,
            $this->auth,
            'Order_nr_' . $order_id . '.pdf',
            pdfClass::DOWNLOAD
        );

        exit();


    }
}
