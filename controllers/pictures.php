<?php
namespace Halo;

use Broneerimiskeskkond\Images;

class pictures extends Controller
{
    function ajax_save()
    {

        if (empty($_POST['room_id'])) {
            ini_set('html_errors', 0);
            throw new \Exception('Missing room ID');
        }

        // Set filename
        if ($_POST['room_type'] == 'usual') {
            $filename = "assets/img/rooms/" . $_POST['room_id'] . "/" . microtime(1) . '.jpg';
        } else {
            $filename = "assets/img/special_rooms/" . $_POST['room_id'] . "/" . microtime(1) . '.jpg';
        }


        // Decode image data
        $data = Images::decode_image_data($_POST['image_data']);


        // Save image
        Images::save($filename, $data);


    }

    function ajax_delete()
    {
        $_POST['room_type'] == 'usual' ?
            $filepath = dirname(__DIR__) . "/assets/img/rooms/" . $_POST['filepath'] :
            $filepath = dirname(__DIR__) . "/assets/img/special_rooms/" . $_POST['filepath'];

        Images::delete($filepath);
    }
}