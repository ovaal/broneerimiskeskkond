<?php namespace Halo;


use Broneerimiskeskkond\Cart;
use Broneerimiskeskkond\Service;
use Broneerimiskeskkond\Room;
use Broneerimiskeskkond\User;

class rooms extends Controller
{
    function __construct()
    {
        Room::save_search_parameters_to_session();

        $this->booking_date = $_SESSION['booking_date'];
        $this->booking_start_time = $_SESSION['booking_start_time'];
        $this->booking_end_time = $_SESSION['booking_end_time'];
        $this->number_of_people = $_SESSION['number_of_people'];
    }

    function index()
    {
        Cart::clear();
        $this->rooms = Room::get_available_rooms($this->booking_date, $this->booking_start_time, $this->booking_end_time, $this->number_of_people);
        $this->booking_start_time_options = Room::get_booking_time_options(7, 0, 17, 30);
        $this->booking_end_time_options = Room::get_booking_time_options(7, 30, 18, 00);
        $this->selected_room = Room::get_selected_rooms();
        $this->whole_studio = Room::get_available_rooms($this->booking_date, $this->booking_start_time, $this->booking_end_time, $this->number_of_people, 1)[0];
        $this->whole_studio_exhibition = Room::get_available_rooms($this->booking_date, $this->booking_start_time, $this->booking_end_time, $this->number_of_people, 2)[0];
    }


    function view()
    {
        $room_id = $this->params[0];
        $this->special_room_view = !empty($this->params[1]) && $this->params[1] == 'special' ? true : false;
        $_SESSION['last_viewed_room_link'] = 'rooms/' . $room_id . ($this->special_room_view ? '/special' : '');


        $this->room = $this->special_room_view ? Room::get_special($room_id) : Room::get($room_id);

        if ($this->special_room_view) {

            // Remember which special room the user selected
            $_SESSION['selected_special_room'] = $this->room;

            // Select all rooms when whole studio
            Room::select_whole_studio([1, 6, 2, 5, 4]);

        }

        // Get previously selected special room
        $this->special_room = empty($_SESSION['selected_special_room']) ?
            null : $_SESSION['selected_special_room'];

        // Redirect user to first selected room, if user has deleted this room from selected rooms
        $room_ids = array_keys($_SESSION['selected_rooms']);
        if (!in_array($room_id, $room_ids)) {
            $new_room_id = $room_ids[0];
            header('Location: ' . BASE_URL . "rooms/$new_room_id");
            exit();
        }

        if (!$this->special_room_view) {
            $this->extras = Room::get_extras($this->params[0]);
            $this->designs = Room::get_designs($this->params[0]);
            $this->supplies = Room::get_supplies();
            $this->rooms = Room::get_available_rooms($this->booking_date, $this->booking_start_time, $this->booking_end_time, $this->number_of_people);
        }

        $this->images = Room::get_all_images($room_id, $this->special_room_view);
        $this->booking_start_time_options = Room::get_booking_time_options(7, 0, 23, 00);
        $this->booking_end_time_options = Room::get_booking_time_options(11, 00, 24, 00);
        $this->user = User::get();
        $this->bookings_js = json_encode(Room::get_all_bookings_js($this->room['room_id']));
        $this->booking_start = $_SESSION['selected_rooms'][$this->params[0]]['booking_start_time'];
        $this->booking_end = $_SESSION['selected_rooms'][$this->params[0]]['booking_end_time'];
        $this->max_people = Room::get_selected_rooms_max_number_of_people();
    }

    /**
     * Workaround for #159991371
     */
    function additional_services()
    {
        header('Location: ' . BASE_URL . "additional_services");
        exit();
    }

    /**
     * @throws \Exception
     */
    function ajax_get_booking()
    {
        stop(200, json_encode(Room::get_all_bookings_js((int)$_POST['room_id'])));
    }

    function ajax_add()
    {
        $_POST['room_class'] = \URLify::filter($_POST['room_name']);
        $room_id = insert('rooms', $_POST);

        exit(json_encode(['result' => $room_id ? 'OK' : 'FAIL', 'room_id' => $room_id]));
    }

    function ajax_edit()
    {
        $toEdit = $_POST['id'];
        $_POST['room']['room_visible'] = empty($_POST['room']['room_visible']) ? 0 : 1;
        update('rooms', $_POST['room'], "room_id=$toEdit");

        if (!empty($_POST['extras'])) {
            delete("room_extras", "room_id=$toEdit");
            foreach ($_POST['extras'] as $extra) {
                $extra_data['room_id'] = $toEdit;
                $extra_data['extra_id'] = $extra;
                insert('room_extras', $extra_data);
            }
        } else {
            q("DELETE FROM room_extras WHERE room_id=$toEdit");
        }

        if (!empty($_POST['designs'])) {
            delete("room_designs", "room_id=$toEdit");
            foreach ($_POST['designs'] as $design) {
                $design_data['room_id'] = $toEdit;
                $design_data['design_id'] = $design;
                insert('room_designs', $design_data);
            }
        } else {
            delete("room_designs", "room_id=$toEdit");
        }

    }

    function ajax_edit_special()
    {
        validate($_POST['id']);
        validate($_POST['room'], IS_ARRAY, true);
        $r = $_POST['room'];

        update('special_rooms', [
            'special_room_name' => $r['room_name'],
            'special_room_max_persons' => $r['room_max_persons'],
            'special_room_description' => $r['room_description'],
            'special_room_slogan' => $r['room_slogan'],
            'special_room_message' => $r['room_message'],
            'special_room_price' => $r['room_price']
        ], "special_room_id=" . (int)$_POST['id']);
    }

    function ajax_save_booking()
    {
        Room::save_selected_rooms($_POST['room_id']);
    }

    function ajax_save_extras()
    {
        Room::save_extras($_POST['extra_id'], $_POST['value'], $_POST['room_id']);
    }

    function ajax_save_designs()
    {
        Room::save_designs($_POST['design_id'], $_POST['value'], $_POST['room_id']);
    }

    function ajax_save_supplies()
    {
        Room::save_supplies($_POST['supply_id'], $_POST['quantity'], $_POST['room_id']);
    }

    function ajax_save_selected_rooms()
    {
        exit(json_encode(Room::save_selected_rooms($_POST['id'])));
    }

    function ajax_get_selected_rooms()
    {
        json_encode(Room::get_selected_rooms());
    }

    function ajax_hide_modals()
    {
        $name = $_POST['name'];
        $value = $_POST['value'];
        $_SESSION[$name] = $value;
    }

    function ajax_check_date()
    {
        $info = $_POST['info'];
        $clicked = $_POST['clicked'];
        exit(Room::check_whole_studio_availability_on_date($info, $clicked));
    }

    function ajax_whole_studio()
    {
        $_SESSION['whole_studio'] = $_POST['value'];
    }
}
