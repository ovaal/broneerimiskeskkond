<?php namespace Halo;

use Broneerimiskeskkond\User;
use SimplBooks\SimplBooks;
use Broneerimiskeskkond\Email;

/**
 * Created by PhpStorm.
 * User: hennotaht
 * Date: 7/29/13
 * Time: 21:48
 */
class signup extends Controller
{
    public $requires_auth = false;

    function ajax_register()
    {

        if (!isset($_POST['email'],
            $_POST['password']
        )) exit(__('Please fill in all the fields'));

        $email = $_POST['email'];
        $password = $_POST['password'];
        $first_and_last_name = "";
        $phone = "";
        $company_id = empty($_POST['company_id']) ? null : $_POST['company_id'];
        $state_institution = empty($_POST['state_institution']) ? 0 : 1;

        // User already exists
        if (User::exists($email)) {
            exit (__('User with given email already exists'));
        }

        if ($user_id = User::register(
            $email,
            $password,
            $first_and_last_name,
            $phone,
            $company_id,
            $state_institution
        )
        ) {

            $activation_code = get_one("SELECT activation_code FROM users WHERE user_id = $user_id");

            $body = __('Thank you for choosing Ovaalstuudio.') . '<br><br>' .
                __('Please click the link below to activate your account.') . '<br>' .
                BASE_URL . 'signup/activate_user/' . $activation_code . '<br><br>' .
                __('Please note that if you do not activate the account within 24 hours, it will be deleted.');

            Email::send(
                $email,
                __('Ovaalstuudio registration.'),
                $body,
                false,
                [],
                SITE_OWNER_EMAIL
            );

            exit (__('Please check your email to activate your account.'));
        } else {
            exit (__('There was an error'));
        }

    }

    function ajax_send_new_password()
    {

        $email = addslashes($_POST['email']);

        if (Email::hasValidDomain($email)) {

            $user = get_first("SELECT email FROM users WHERE email = '$email'");

            if (empty($user))
                exit(json_encode(['status' => 404, 'message' => __('Unknown email address')]));

            // Password reset
            $password = User::generate_password(8);
            $hash = password_hash($password, PASSWORD_DEFAULT);
            update('users', ['password' => $hash], "email = '$email'");

            // Email password

            $url = BASE_URL;
            $body = nl2br(__("Hello!") . "\n\n" .
                __("You recently wanted us to send you a new password. Your new password is") . " " . $password . "\n\n" .
                __("Take good care of it!") . "\n\n" . "<a href='$url'>$url</a>");

            Email::send(
                $email,
                __('Your new password'),
                $body,
                false,
                []

            );

            exit(json_encode(['status' => 200, 'message' => __("Your new password has been sent to you")]));
        }

        exit(json_encode(['status' => 400, 'message' => __("Invalid email address")]));

    }

    function activate_user()
    {
        $activation_code = $this->params[0];
        $user = get_first("SELECT * 
                             FROM users 
                             WHERE activation_code = '$activation_code' 
                             AND deleted = 0 
                             AND is_active = 0");
        // Log the user in
        if (!empty($user)) {
            update("users", ["is_active" => 1], "user_id= $user[user_id]");
            $_SESSION['user_id'] = $user['user_id'];

            header('Location: ' . BASE_URL);
        } else {
            $errors[] = __('The account has been deleted or already activated.');
            require 'templates/error_template.php';
            exit();
        }
    }

}