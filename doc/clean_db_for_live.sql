SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE booking_designs;
TRUNCATE booking_extras;
TRUNCATE booking_supplies;
TRUNCATE offers;
TRUNCATE bookings;
TRUNCATE order_meal_items;
TRUNCATE order_meals;
TRUNCATE orders;
TRUNCATE translations;
TRUNCATE users;
INSERT INTO users (user_id, is_admin, password, email, deleted, first_and_last_name, phone, state_institution, activation_code, is_active, user_created_at)
VALUES
  (1, 1, '$2y$10$f4OTZETUajddcZ.BGyDi1ecgvMiqiRpMXCUIC1UJHnNSlOMZCIZ3G', 'demo@demo.ee', 0, 'Demo Demo', '55555555', 1,
      NULL, 1, NOW());

SET FOREIGN_KEY_CHECKS = 1;