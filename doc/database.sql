-- MariaDB dump 10.19  Distrib 10.4.28-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: broneerimiskeskkond
-- ------------------------------------------------------
-- Server version	10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking_designs`
--

DROP TABLE IF EXISTS `booking_designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_designs` (
  `booking_id` int(5) unsigned NOT NULL,
  `design_id` smallint(5) unsigned NOT NULL,
  `booking_design_price` decimal(6,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`booking_id`,`design_id`),
  KEY `design_id` (`design_id`,`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_designs`
--

LOCK TABLES `booking_designs` WRITE;
/*!40000 ALTER TABLE `booking_designs` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_extras`
--

DROP TABLE IF EXISTS `booking_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_extras` (
  `booking_id` int(5) unsigned NOT NULL,
  `extra_id` smallint(5) unsigned NOT NULL,
  `booking_extra_name` varchar(191) DEFAULT NULL,
  `booking_extra_price` decimal(6,2) NOT NULL DEFAULT 0.00,
  `booking_extra_unit` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`booking_id`,`extra_id`),
  KEY `extra_id` (`extra_id`,`booking_id`),
  CONSTRAINT `booking_extras_bookings_booking_id_fk` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`) ON DELETE CASCADE,
  CONSTRAINT `booking_extras_extras_booking_id_fk` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`extra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_extras`
--

LOCK TABLES `booking_extras` WRITE;
/*!40000 ALTER TABLE `booking_extras` DISABLE KEYS */;
INSERT INTO `booking_extras` VALUES (1,12,'Kõllid',5.00,'kord'),(2,12,'Lällarid',5.00,'pc'),(5,4,'Pabertahvel markeritega',5.00,'kord'),(6,3,'Projector, screen',15.00,'pc'),(6,4,'Paper whiteboard, markers',5.00,'set'),(6,12,'Kõlarid',5.00,'pc'),(6,20,'A4, post-it, pastakad',5.00,'pc'),(7,3,'Projector, screen',15.00,'pc'),(7,4,'Paper whiteboard, markers',5.00,'set'),(7,12,'Kõlarid',5.00,'pc'),(7,20,'A4, post-it, pastakad',5.00,'pc'),(8,4,'Paper whiteboard, markers',5.00,'set'),(8,20,'A4, post-it, pastakad',5.00,'pc'),(9,4,'Paper whiteboard, markers',5.00,'set'),(10,4,'Paper whiteboard, markers',5.00,'set');
/*!40000 ALTER TABLE `booking_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_supplies`
--

DROP TABLE IF EXISTS `booking_supplies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_supplies` (
  `booking_id` int(11) unsigned NOT NULL,
  `supply_id` tinyint(3) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `booking_supply_price` decimal(6,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`booking_id`,`supply_id`),
  KEY `supply_id` (`supply_id`,`booking_id`),
  CONSTRAINT `booking_supplies_bookings_booking_id_fk` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`) ON DELETE CASCADE,
  CONSTRAINT `booking_supplies_supplies_supply_id_fk` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`supply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_supplies`
--

LOCK TABLES `booking_supplies` WRITE;
/*!40000 ALTER TABLE `booking_supplies` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_supplies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `booking_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned DEFAULT NULL,
  `booking_start` datetime DEFAULT NULL,
  `booking_end` datetime DEFAULT NULL,
  `order_id` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `room_type` varchar(191) DEFAULT NULL,
  `booking_room_price` decimal(6,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`booking_id`),
  KEY `bookings_orders_order_id_fk` (`order_id`),
  KEY `bookings_rooms_room_id_fk` (`room_id`),
  CONSTRAINT `bookings_orders_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `bookings_rooms_room_id_fk` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,1,'2019-12-18 08:00:00','2019-12-18 18:00:00',1,0,'main',54.00),(2,6,'2019-12-18 08:00:00','2019-12-18 18:00:00',1,0,'main',54.00),(3,2,'2019-12-18 08:00:00','2019-12-18 18:00:00',1,0,'extra',5.00),(4,5,'2019-12-18 08:00:00','2019-12-18 18:00:00',1,0,'extra',5.00),(5,4,'2019-12-18 08:00:00','2019-12-18 18:00:00',1,0,'extra',5.00),(6,1,'2019-12-19 08:00:00','2019-12-19 18:00:00',2,0,'main',54.00),(7,6,'2019-12-19 08:00:00','2019-12-19 18:00:00',2,0,'main',54.00),(8,2,'2019-12-19 08:00:00','2019-12-19 18:00:00',2,0,'extra',5.00),(9,5,'2019-12-19 08:00:00','2019-12-19 18:00:00',2,0,'extra',5.00),(10,4,'2019-12-19 08:00:00','2019-12-19 18:00:00',2,0,'extra',5.00);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(400) NOT NULL,
  `company_registry_nr` varchar(191) NOT NULL,
  `company_kmkr_nr` varchar(191) DEFAULT NULL,
  `company_status` varchar(191) DEFAULT NULL,
  `company_street` varchar(191) DEFAULT NULL,
  `company_city` varchar(191) DEFAULT NULL,
  `company_postal_code` int(11) DEFAULT NULL,
  `simplbooks_client_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`company_id`),
  UNIQUE KEY `companies_company_registry_nr_uindex` (`company_registry_nr`),
  KEY `companies_simplbooks_client_id_index` (`simplbooks_client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'001 group OÜ','12754230','','Registrisse kantud','Õismäe tee 78-9','Tallinn',13513,NULL),(2,'001 Kinnisvara OÜ','12652512','EE101721589','Registrisse kantud','Õismäe tee 78-9','Tallinn',13513,NULL),(3,'007 Autohaus osaühing','11694365','EE101335276','Registrisse kantud','Fortuuna tn 1','Tartu, Tartu, Tartumaa',50603,447),(4,'013 Graphics OÜ','12624154','EE101701716','Registrisse kantud','Sõstra tn 4-36','Tallinn',10616,NULL),(5,'013 investment OÜ','12937781','','Registrisse kantud','Tartu mnt 18','Tallinn',10115,NULL),(6,'01Arvutiabi OÜ','14112620','','Registrisse kantud','Aru tn 19-5','Tallinn',10318,NULL),(7,'01 Creations Osaühing','10818150','EE100725654','Registrisse kantud','Ahtri tn 12','Tallinn',10151,NULL),(8,'0dav riie OÜ','14288602','','Registrisse kantud','Vanaaseme talu','Ingliste küla, Kehtna vald, Raplamaa',79004,NULL),(9,'0 rent OÜ','14289174','','Registrisse kantud','Väike tn 14a-12','Võhma, Põhja-Sakala, Viljandimaa',70602,NULL),(10,'0-Takso OÜ','12816557','','Registrisse kantud','Liivalaia tn 7-29','Tallinn',10118,NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designs`
--

DROP TABLE IF EXISTS `designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designs` (
  `design_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `design_name` varchar(255) NOT NULL,
  `design_price` decimal(7,2) DEFAULT 0.00,
  `design_in_use` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`design_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designs`
--

LOCK TABLES `designs` WRITE;
/*!40000 ALTER TABLE `designs` DISABLE KEYS */;
INSERT INTO `designs` VALUES (1,'INNOVAATILINE I kuni 20 in<br> poolkaares, kergelt liigutatavad ühised<br> ümarad lauad tassi/materjali toetamiseks',0.00,1),(2,'Klassikaline laud (ümber ühe laua) kuni 10 in',0.00,0),(3,'ringis (kuni 22 in)',0.00,0),(4,'1-25 in I Kaks poolkaart, ühised väiksed lauad tassi/materjali toetamiseks',0.00,0),(6,'TEATRISTIILIS I kuni 25 in / kuni 30 in <br> kahes poolkaares, kergelt liigutatavad ühised ümarad lauad tassi/materjali toetamiseks',0.00,1),(7,'KLASSIKALINE I kuni 10 in <br> ümber ühise suure laua',0.00,1);
/*!40000 ALTER TABLE `designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extras`
--

DROP TABLE IF EXISTS `extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extras` (
  `extra_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `extra_name` varchar(255) NOT NULL,
  `extra_price` decimal(7,2) DEFAULT 0.00,
  `extra_unit` varchar(255) DEFAULT NULL,
  `extra_in_use` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`extra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extras`
--

LOCK TABLES `extras` WRITE;
/*!40000 ALTER TABLE `extras` DISABLE KEYS */;
INSERT INTO `extras` VALUES (1,'A4 papers',3.00,NULL,0),(2,'Glue pads',5.00,NULL,0),(3,'Projector, screen',15.00,'pc',1),(4,'Paper whiteboard, markers',5.00,'set',1),(6,'Presentation remote',5.00,'pc',0),(10,'Canon 60D camera',40.00,'pc',0),(11,'Pabertahvel, markerid',5.00,'pc',0),(12,'Kõlarid',5.00,'pc',1),(13,'Teenindaja kohalolu kohtumise ajal',50.00,'pc',0),(14,'Kontoritarvete esmaabi (A4 paberid, post-it, paberinäts, pastakad) ',10.00,'pc',0),(15,'Wifi (allalaadimise kiirus kuni 150 Mbit/s)',0.00,'pc',0),(16,'LCDTV',10.00,'pc',0),(18,'Kirjutamisalused A3',0.00,'pc',0),(19,'Paigutus: ümber suure laua ruumi keskel',0.00,'pc',0),(20,'A4, post-it, pastakad',5.00,'pc',1),(21,'Vastuvõtu teenus: külaliste vastuvõtmine, ruumi juhatamine, ärasaatmine terve kohtumise vältel',49.00,'pc',0),(24,'Lisateenus: tasside/klaaside koristamine koolitusruumist (alternatiiv on ise nuti-nõudepesumasinasse asetada) ',25.00,'pc',0),(25,'Esitluspult ',5.00,'pc',0),(26,'Eripuhastusteenus: ruumi jäetud tasside/klaaside koristus',25.00,'pc',0),(27,'Lisapabertahvel, markerid',5.00,'pc',0),(28,'Ruumid Maa, Tuli, Vesi rent',15.00,'pc',0),(29,'LCDTV',5.00,'pc',0);
/*!40000 ALTER TABLE `extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info_boxes`
--

DROP TABLE IF EXISTS `info_boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info_boxes` (
  `info_box_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `info_box_text` longtext NOT NULL,
  `info_box_name` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`info_box_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info_boxes`
--

LOCK TABLES `info_boxes` WRITE;
/*!40000 ALTER TABLE `info_boxes` DISABLE KEYS */;
INSERT INTO `info_boxes` VALUES (1,'<p>Tere! <br /><br />Suurep&auml;rane, et planeerite k&uuml;lastada&nbsp;Ovaalstuudiot.</p>\r\n<p>&mdash;&nbsp; stuudios on 6 unikaalset ruumi<br />&mdash;&nbsp; innovaatilised&nbsp;disain- ja IT lahendused<br />&mdash;&nbsp; k&otilde;rged laed avaraks m&otilde;ttelennuks</p>\r\n<p>Palume valida&nbsp;aeg ja ruum, vajadusel tehnika/kohvipausid ning&nbsp;kokkuv&otilde;tte lehel kalkuleerub interaktiivselt pakkumine. <br />Broneerimine kl&otilde;psates nupule \'Kinnita broneering\'.<span style=\"font-size: 12pt;\"><br /></span><br />M&otilde;nusat avastamist!<br />Teie Ovaalstuudio<em><span style=\"font-size: 12pt;\"><br /></span></em></p>','broneerimistingimused',0),(2,'<p>E-BRONEERIMISKESKKONNA&nbsp;PRIVAATSUSE JA TURVALISUSE P&Otilde;HIM&Otilde;TTED&nbsp;</p>\r\n<p>- Ovaalstuudio Rentniku andmete privaatsus ning turvalisus on meile oluline.<br />- T&ouml;&ouml;tleme vaid neid juriidilise v&otilde;i f&uuml;&uuml;silise isiku andmeid, mis on vajalikud Ovaalstuudio ruumi(de) rentimiseks ja e-broneerimiskeskkonna kaudu Ovaalstuudio cateringipartnerilt cateringi tellimiseks. <br />- Rentniku andmeid ei edastata kolmandatele osapooltele, v.a. juhul kui seadus seda kohustab.<br />- Teil on &otilde;igus oma kliendikonto E-broneerimiss&uuml;steemi andmebaasist kustutada kontakteerudes e-posti teel: <a href=\"mailto:ovaal@ovaal.ee\">ovaal@ovaal.ee</a>.<br />- Pangalingiga maksmisel ei saa Rendileandja ega ka kolmandad isikud teada Rentniku internetipanga privaatseid andmeid.<br />- Rentniku konto infot E-broneerimiskeskkonnas kaitseb ainult Rentnikule endale teada olev parool. Rentnik kohustub mitte avaldama oma E-broneerimiskeskkonna Kasutajanime ja parooli k&otilde;rvalistele isikutele, samuti mitte kasutama Kasutajanime ja parooli nii, et m&otilde;ni k&otilde;rvaline isik v&otilde;iks neid tuvastada.<br />- Rentnik kinnitab registreerumisega sisestatud andmete &otilde;igsuse.<br />- Ovaalstuudio E-broneerimiskeskkonda kasutama asudes kinnitate, et olete eelkirjeldatud p&otilde;him&otilde;tetega tutvunud ning n&otilde;us.</p>\r\n<p>Lisainfo vajadusel tel. 5050277 v&otilde;i e-post: ovaal@ovaal.ee &nbsp;</p>','privacy',0),(3,'<p>E-BRONEERIMISKESKKONNA JA OVAALSTUUDIO <br />RUUMIDE &Uuml;LD- JA RENDITINGIMUSED.</p>\r\n<p><br />(1) Aktoseidon O&Uuml;, registrikoodiga 11307397, asukohaga Toompuiestee 4, 10142, Tallinn, &nbsp;esindades br&auml;ndi Ovaalstuudio (edaspidi: Rendileandja) on seadnud allj&auml;rgnevad Ovaalstuudio ruumi(de) &uuml;ld-ja renditingimused.<br />(2) Rentnik sisestab E-broneerimiskeskkonda sisenemisel j&auml;rgmised andmed*:<br />[Organisatsiooni esindaja e-posti aadress], [Parool], [Organisatsiooni esindaja ees- ja perekonannimi],&nbsp;[Organisatsiooni esindaja telefon], [Organisatsiooni nimi], [Organisatsiooni registrikood], [Organisatsiooni aadress] (edaspidi: Rentnik).</p>\r\n<p>*E-broneerimiskeskkonda sisestatud andmed on turvaliselt kaitstud. Andmeid kasutatakse vaid Ovaalstuudio ruumi(de) rendileandmiseks. Cateringi tellimisel E-broneerimiskeskkonna kaudu edastatakse stuudio cateringipartnerile ainult tellimuse t&auml;itmiseks vajalik info (ruumi nimi, kellaajad, inimeste arv, tellimus). Privaatsustingimuste lisainfo on E-broneerimiss&uuml;steemi lehe allosas.<br /><br />RENDITAV(AD) RUUM(ID)</p>\r\n<p>Rentnikule saab kasutamiseks rendiobjekti ruum(id) (edaspidi: Ovaalstuudio) vastavalt Rentniku valikule. Ovaalstuudio asub Toompuiestee 4, Tallinn, 10142.&nbsp;<br />Ovaalstuudio ruumi(d) valib Rentnik E-broneerimiskeskkonnas aadressil www.ovaal.ee ja bron.ovaal.ee.<br /><br />RENDIHIND</p>\r\n<p>&mdash; Rendihind kehtib vastavalt E-broneerimiskeskkonnas toodud hinnakirjale.<br />&mdash;&nbsp;Rendileandjal on &otilde;igus hinnakirja muuta etteteatamata.&nbsp;<br />Rendihind sisaldab:<br />&mdash;&nbsp;Renditud ruumi(e) ja ruumi inventari: ruumi m&ouml;&ouml;bel (toolid ja lauad); WIFI; kirjutamisalused; pikendusjuhe; pleedid; sussid.<br />&mdash;&nbsp;Kommunaalteenuseid (elektrienergia, vee- ja kanalisatsiooni, k&uuml;tte ja pr&uuml;giveo kulusid), valves&uuml;steemi.<br />Lisaks on v&otilde;imalik vastavalt hinnakirjale&nbsp;rentida e-broneerimiskeskkonna kaudu:<br />&mdash;&nbsp;Projektor-ekraan, kaks k&otilde;larit ja heliv&otilde;imendi (ruumides &ldquo;P&auml;ike&rdquo; ja &ldquo;Eeter&rdquo;), pabertahvel-markerid (ruumides \"P&auml;ike\", \"Eeter\", \"Maa\", \"Tuli\", \"Vesi\").<br />&mdash;&nbsp;Kontoritarvete pakett (A4, post-it, kleepmass, pastakad).<br /><br />CATERING</p>\r\n<p>&mdash;&nbsp;E-broneerimiskeskkonna kaudu on v&otilde;imalik ainult ette tellida Ovaalstuudio cateringipartneri tooteid.<br />&mdash;&nbsp;Ovaalstuudio cateringipartner toob tellimuse vastavalt E-broneerimiskeskkonda sisestatud infole (tellimus, inimeste arv, kellaaeg) Ovaalstuudios kokkulepitud kohta.<br />&mdash;&nbsp;Cateringi saab tellida ainult E-broneerimiskeskkonna men&uuml;&uuml;st. <br />&mdash; Toidu/joogi/pakendite kaasav&otilde;tmine stuudiosse pole lubatud.<br />&mdash;&nbsp;Cateringipartneri teenindaja on Ovaalstuudios kohapeal l&otilde;unapausi ajal kl 12:00-12:45 ja/v&otilde;i 13:15-14:00.<br />&mdash;&nbsp;Cateringi tellimise korral on kasutada Ovaalstuudio kohvimasin, veekeetmismasin, tassid ja klaasid. <br />&mdash;&nbsp;P&auml;rast broneeringu kinnitamist saab cateringi infot muuta 4 t&ouml;&ouml;p&auml;eva enne broneeritud kuup&auml;eva hiljemalt kell 18:00.<br />&mdash;&nbsp;Cateringi annuleerimisel v&auml;hem kui 3 t&ouml;&ouml;p&auml;eva enne broneeritud kuup&auml;eva kuulub catering v&auml;ljaostmisele.<br /><br />RENDIAEG JA RENDIMAHT</p>\r\n<p>&mdash;&nbsp;Ruume saab kasutada E &mdash; R ajavahemikus kl 08.00 &mdash; 18.00. <br />&mdash;&nbsp;Rendiaeg on Rentniku Ovaaalstuudiosse saabumisest&nbsp;lahkumiseni, sh ettevalmistus ja asjade kokkupaneku aeg.<br />&mdash;&nbsp;Rendimaht arvestub 1/2 tunni alusel. Minimaalne ruumi(de) renditav aeg on 4 j&auml;rjestikust tundi.<br />&mdash;&nbsp;&Uuml;letades rendiaega, esitatakse&nbsp;automaatselt arvelisa, alustatud pooltund 30 eur +km.<br /><br />RENTNIK JA&nbsp;V&Otilde;TMEISIK</p>\r\n<p>&mdash;&nbsp;Ovaalstuudio kasutamise ajal viibib kohapeal Rentnik v&otilde;i tema poolt volitatud vastutav isik (edaspidi: V&otilde;tmeisik). V&otilde;tmeisiku&nbsp;nime m&auml;rgib Ovaalstuudio broneerija e-broneerimiskeskkonnas kokkuv&otilde;tte&nbsp;lehele.<br />&mdash;&nbsp;Rentnik/V&otilde;tmeisik saab Ovaalstuudio broneeritud ruumi(id) enda kasutusse broneeringus ﬁkseeritud algusajal. Rentnik/V&otilde;tmeisik veendub ruumide seisukorras broneeritud aja alguses.<br />&mdash;&nbsp;Ovaalstuudio v&auml;lisuks 5. korrusel lukustub automaatselt.<br />&mdash;&nbsp;Rentnik/v&otilde;tmeisik lahkub Ovaalstuudiost koos meeskonna/k&uuml;lalistega, kuid mitte varem.<br />&mdash;&nbsp;Rendiajal Ovaalstuudiost ajutiselt v&auml;lja/sisse liikumiseks on Rentnikule/V&otilde;tmeisik kasutamiseks v&auml;lisuksekaart. Stuudio loovutamisel j&auml;tab Rentnik/V&otilde;tmeisik&nbsp;v&auml;lisuksekaardi Rendileandjaga kokkulepitud kohta.&nbsp;<br />&mdash;&nbsp;Ovaalstuudios liigutakse sokkis, oma kaasav&otilde;etud v&otilde;i kohapeal pakutavates lambavillastes sussides.<br />&mdash;&nbsp;Ovaalstuudios on iseteeninduskeskkond &ndash; osaleja poolt kasutusse v&otilde;etud inventar tagastatakse samas seisundis samasse kohta kui v&otilde;eti. Tassid/klaasi asetatakse n&otilde;udepesumasinasse.<br /><br />BRONEERINGU KINNITAMINE JA RUUMI GARANTEERIMINE RENTNIKULE</p>\r\n<p>&mdash; Rentnik tasub Rendileandjale esimese osamakse arve (edaspidi: Broneeringutasuarve) alusel. Broneeringutasuarve on 25% broneeritud ruumi(de) rendi kogusummast.<br />&mdash; Riigiasutuse korral v&otilde;tame vastu garantiikirja.<br />&mdash; Broneeringutasu/garantiikiri garanteerib rentnikule Ovaalstuudio ruumi(de) saadavuse broneeritud ajal ja Rendileandjale broneerimisega kaasnenud ruumi kasutuseta hoidmise kulude h&uuml;vitamise (25% broneeritud ruumi(de) ja inventari rendi kogusummast) juhul  kui Rentnik loobub ruumide kasutamisest.<br /><br />BRONEERINGU ANNULEERIMINE</p>\r\n<p>&mdash;&nbsp;Kui Rentnik loobub broneeringust 1 kuu enne broneeritud aja saabumist, siis Broneeringutasu 25% tagastatakse 7 t&ouml;&ouml;p&auml;eva jooksul.<br />&mdash;&nbsp;Kui Rentnik loobub broneeringust v&auml;hem kui &uuml;ks (1) kuu enne broneeritud aja saabumist, siis broneerimistasu enam ei tagastata, kuid Rentnik saab Broneeringutasu kasutada j&auml;rgneva broneeringu tegemiseks 3 kuu jooksul alates loobumisest teatamisest, kuid mitte varem kui Rendileandjalt kinnituse saamise p&auml;evast.<br />&mdash;&nbsp;Broneeringust loobumiseks/edasi l&uuml;kkamiseks tuleb kontakteeruda ovaal@ovaal.ee aadressil.<br />&mdash; Broneeringutasu arve&nbsp;mitte tasumisel vastavalt makset&auml;htajale broneering&nbsp;annuleeritakse automaatselt.<br /><br />MAKSEVIIS</p>\r\n<p>&mdash;&nbsp;Rentniku poolt E-broneerimiskeskkonnas klahvile \"Olen lugenud ning n&otilde;ustun tingimustega\" ja seej&auml;rel &ldquo;Kinnita broneering&rdquo; vajutamist edastatakse E-broneerimiskeskkonna kaudu automaatselt Broneeringutasuarve.<br />&mdash;&nbsp;Rentnik tasub Broneeringutasuarve vastavalt makset&auml;htajale Maksekeskuse kaudu v&otilde;i panga&uuml;lekandega arve alusel.<br />&mdash;&nbsp;Maksekeskuse kaudu on valikus j&auml;rgmised pangad: Swedbank, SEB, LHVpank, Nordea, DanskeBank, Krediidipank.<br />&mdash;&nbsp;K&otilde;ik E-broneerimiskeskkonnas kuvatavad hinnad on eurodes, millele lisandub k&auml;ibemaks.<br />&mdash;&nbsp;Arveid ja Maksekeskuses tehtud tehinguid saab tasuda ainult eurodes.&nbsp;<br />&mdash;&nbsp;Maksekorraldusele tuleb m&auml;rkida Broneeringutasuarve number.<br />&mdash;&nbsp;Maksekeskuse kaudu tasumisel n&auml;htub broneeringutasuarve number automaatselt (mitte kustutada).<br />&mdash;&nbsp;Broneering j&otilde;ustub kui Rentnik on tasunud Broneeringutasuarve Rendileandja arveldusarvele SWEDBANK EE302200221033472853.<br /><br />TURVALISUS JA HEAPEREMEHELIKKUS</p>\r\n<p>&mdash;&nbsp;Rentnik, V&otilde;tmeisik&nbsp;ja iga k&uuml;laline vastutab Ovaalstuudios viibides oma tervise ja kaasav&otilde;etud materiaalsete v&auml;&auml;rtuste eest ise. Rentnik/V&otilde;tmeisik teavitab k&uuml;lalisi nimetatud vastutusest.<br />&mdash; Rentnik teavitab Rendileandjat viivitamatult Ovaalstuudios tekkinud ohust, avariist, tulekahjust ja v&otilde;tab koheselt tarvitusele abin&otilde;ud p&otilde;hjuste likvideerimiseks. Ovaalstuudio avaruumis ja koridoris asub kaks tulekustutit ja esmaabikarp.<br />&mdash;&nbsp;Rentnik korraldab oma tegevusi nii, et arvestab teiste rentnike tegevustega Ovaalstuudios.<br />&mdash;&nbsp;Stuudio pildistamine&nbsp;ja/v&otilde;i filmimine on lubatud ainult kirjaliku loa alusel.<br />&mdash;&nbsp;Rentnik teatab Rendileandjale enne Ovaalstuudiost lahkumist tekitatud kahjudest. Rentnik on kohustatud h&uuml;vitama v&otilde;i asendama samav&auml;&auml;rsega Ovaalstuudiole h&auml;vimisest v&otilde;i kahjustumisest tuleneva kahju, mis tekkis ajal, kui Ovaalstuudio ruum(id) oli Rentniku valduses. Taastamisega v&otilde;i asendamisega seotud kulude kohta esitab Rendileandja arve.<br />&mdash;&nbsp;Rentnik kasutab Ovaalstuudio ruumi(e) heaperemehelikult ja vastavalt sihtotstarbele. <br /><br />Ovaalstuudio E-broneerimiskeskkonnas &ldquo;KINNITA BRONEERING&rdquo; klahvile vajutamisega on Rentnik kinnitanud, et saab aru ning kohustub t&auml;itma Ovaalstuudio &uuml;ld- ja renditingimusi.<br /><br />Lisainfo vajadusel on kontakt tel. 5050277 v&otilde;i e-post: <a href=\"mailto:ovaal@ovaal.ee\">ovaal@ovaal.ee</a></p>\r\n<p><em><br />Selle veebilehe informatsiooni&nbsp;autori&otilde;igused kuuluvad Aktoseidon O&Uuml;-le.&nbsp;Antud</em><em>&nbsp;dokument sisaldab konfidentsiaalset infot&nbsp;ja on m&otilde;eldud kasutamiseks ainult Ovaalstuudio e-broneerimiskeskkonda&nbsp;registreerunud rentnikule. Antud&nbsp;dokumendis sidalduvat&nbsp;infot ei ole&nbsp;lubatud ilma autori kirjaliku loata kopeerida, avaldada, levitada v&otilde;i muul moel kasutada. Aktoseidon O&Uuml; Copyright 2017, k&otilde;ik &otilde;igused kaitstud.</em></p>\r\n<p><br />T&auml;nu ja lugupidamisega</p>','general conditions',0),(4,'<p><br />TELLIMINE<br />E-broneerimiskeskkonna kaudu saab tellida stuudio&nbsp;cateringipartneri tooteid.</p>\r\n<p>&mdash;&nbsp;P&auml;rast broneeringu kinnitamist saab<strong>&nbsp;</strong>cateringi tellida/muuta&nbsp;hiljemalt 4 t&ouml;&ouml;p&auml;eva&nbsp;enne s&uuml;ndmust&nbsp;kl&nbsp;18:00-ni.&nbsp;<br />&mdash; L&otilde;unaks saab valida paketi; praeks on liharoog, taimetoiduks taimne h&otilde;rgutis. Praevalikul usaldame kokkasid.<br />&mdash; Toitude/jookide/pakendite stuudiosse&nbsp;kaasav&otilde;tmine pole lubatud.<br />L&otilde;unatada&nbsp;saab&nbsp;samuti l&auml;hedalasuvates toidukohtades - nt Viktus (samas majas), P&ouml;&ouml;bel, Kuldmokk, L\'Ermitage jpt.</p>\r\n<p>T&auml;name.</p>','catering',0),(5,'<p>TURVALISUS JA HEAPEREMEHELIKKUS<br /><br />&mdash; Rentnik, V&otilde;tmeisik&nbsp;ja iga k&uuml;laline vastutab Ovaalstuudios viibides oma tervise ja kaasav&otilde;etud <br />materiaalsete v&auml;&auml;rtuste eest ise. Rentnik/V&otilde;tmeisik teavitab k&uuml;lalisi nimetatud vastutusest.<br />&mdash;&nbsp;Rentnik teavitab Rendileandjat viivitamatult Ovaalstuudios tekkinud ohust, avariist, tulekahjust ja v&otilde;tab koheselt tarvitusele abin&otilde;ud p&otilde;hjuste likvideerimiseks. Stuudio&nbsp;avaruumis ja koridoris asub kaks tulekustutit&nbsp;ning esmaabikarp.<br />&mdash;&nbsp;Rentnik korraldab oma tegevusi nii, et arvestab teiste rentnike tegevustega Ovaalstuudios.<br />&mdash;&nbsp;Rentnik kasutab Ovaalstuudio ruumi(e) heaperemehelikult ja vastavalt sihtotstarbele.<br /><br />T&auml;name</p>','turvalisus, heaperemehelikkus',0),(6,'<p><br /><em>Ovaalstuudio keskkonna eesm&auml;rk on luua meeskonda </em><br /><em>&uuml;hendavaid&nbsp;ja inimesi koost&ouml;&ouml;sse toovaid&nbsp;hetki.</em><br /><br />V&Otilde;TMEISIK<br /><br />&mdash; Ovaalstuudio kasutamise ajal viibib kohapeal Rentnik v&otilde;i volitatud vastutav isik (edaspidi: V&otilde;tmeisik). <br />&mdash; V&otilde;tmeisik saab broneeritud ruumi(d) enda kasutusse broneeringus fikseeritud ajal.&nbsp;<br />&mdash;&nbsp;V&otilde;tmeisikul&nbsp;palume saabuda&nbsp;esimesena -&nbsp;v&otilde;tmeisik saab vajadusel stuudiost v&auml;lja-sisse liikumiseks stuudio poolt uksekaardi. V&otilde;tmeisikul palume lahkuda koos v&otilde;i p&auml;rast k&uuml;lalisi/meeskonda, mitte varem.&nbsp;<br /><br /><em>Stuudios on loodud disain ja IT lahendused, privaatsus ning&nbsp;&otilde;dusus.</em><br /><em>Ovaalstuudios liigutakse sokkis, oma kaasav&otilde;etud v&otilde;i kohapeal pakutavates lambavillastes sussides.</em><br /><br />T&auml;name</p>','Renter / Keyperson',0);
/*!40000 ALTER TABLE `info_boxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(191) NOT NULL,
  `language_short_name` char(5) NOT NULL,
  `language_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `language_is_default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `languages_language_short_name_uindex` (`language_short_name`),
  UNIQUE KEY `languages_language_is_default_uindex` (`language_is_default`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'estonian','et_EE',1,1),(2,'english','en_GB',1,NULL);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logged_at` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  `user_id` int(10) unsigned DEFAULT NULL,
  `log_event_id` tinyint(3) unsigned NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `changed_fields` varchar(1000) DEFAULT NULL,
  `object_id` int(10) unsigned DEFAULT NULL,
  `additional_info` varchar(3000) DEFAULT NULL,
  `backtrace` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `error` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`log_id`),
  KEY `log_users_user_id_fk` (`user_id`),
  CONSTRAINT `log_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_events`
--

DROP TABLE IF EXISTS `log_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_events` (
  `log_event_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `log_event_name` varchar(191) NOT NULL,
  PRIMARY KEY (`log_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_events`
--

LOCK TABLES `log_events` WRITE;
/*!40000 ALTER TABLE `log_events` DISABLE KEYS */;
INSERT INTO `log_events` VALUES (1,'ADD'),(2,'EDIT'),(3,'DELETE'),(4,'RPC'),(5,'EMAIL');
/*!40000 ALTER TABLE `log_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `offer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `offer_requested_by` int(10) unsigned NOT NULL,
  `offer_requested_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,1,'2018-05-21 11:41:42'),(2,1,'2018-05-21 11:51:30');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_services`
--

DROP TABLE IF EXISTS `order_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_services` (
  `order_id` int(11) unsigned NOT NULL,
  `service_id` int(11) unsigned NOT NULL COMMENT 'to bind the service to a checkbox in myorders/x page',
  `order_service_name` varchar(191) DEFAULT NULL,
  `order_service_price` decimal(6,2) DEFAULT 0.00,
  `order_service_quantity` int(6) DEFAULT 1,
  `order_service_unit` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`order_id`,`service_id`),
  KEY `order_services_services_service_id_fk` (`service_id`),
  CONSTRAINT `order_services_orders_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_services`
--

LOCK TABLES `order_services` WRITE;
/*!40000 ALTER TABLE `order_services` DISABLE KEYS */;
INSERT INTO `order_services` VALUES (1,2,'3D paintings exhibition for 1 hour',250.00,1,''),(1,3,'3D paintings exhibition for ½ hour',150.00,1,''),(1,4,'Coffee machine usage',5.90,3,'persons'),(1,6,'Lunch break 11:30 - 13:00',0.00,1,''),(2,1,'3D paintings exhibition for 4 hours',690.00,1,''),(2,2,'3D paintings exhibition for 1 hour',250.00,1,''),(2,3,'3D paintings exhibition for ½ hour',150.00,1,''),(2,4,'Coffee machine usage',5.90,4,'persons'),(2,5,'Not having lunch in the studio',0.00,1,'');
/*!40000 ALTER TABLE `order_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `coordinator_name` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `dont_send_invoice` int(11) NOT NULL DEFAULT 0,
  `invoice_additional_info` text DEFAULT NULL,
  `invoice_has_extra_row` tinyint(4) NOT NULL DEFAULT 0,
  `special_room_selected` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `order_made` datetime DEFAULT NULL,
  `payment_status_id` tinyint(4) NOT NULL DEFAULT 4,
  `deposit_invoice_id` smallint(5) unsigned DEFAULT NULL,
  `deposit_invoice_number` varchar(255) DEFAULT NULL,
  `send_reminder` tinyint(4) NOT NULL DEFAULT 1,
  `total_number_of_people` int(11) DEFAULT 0,
  `user_id` int(11) unsigned NOT NULL,
  `special_room_name` varchar(191) DEFAULT NULL,
  `special_room_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount_percent` decimal(5,2) NOT NULL DEFAULT 0.00,
  `discount_level` varchar(191) NOT NULL DEFAULT '',
  `order_billed_rent_percentage` tinyint(4) NOT NULL DEFAULT 0,
  `order_remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `orders_users_user_id_fk` (`user_id`),
  KEY `orders_payment_statuses_payment_status_id_fk` (`payment_status_id`),
  KEY `orders_companies_company_id_fk` (`company_id`),
  CONSTRAINT `orders_companies_company_id_fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`company_id`),
  CONSTRAINT `orders_payment_statuses_payment_status_id_fk` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_statuses` (`payment_status_id`),
  CONSTRAINT `orders_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,3,'asd',0,0,'',0,1,'2019-12-18 20:42:15',2,10742,'20191218204215',1,1,1,'Whole studio with 3D exhibition',79.00,0.00,'Default',0,NULL),(2,3,'asd',0,0,'',0,1,'2019-12-18 20:56:29',5,10750,'20191218205630',1,1,1,'Whole studio with 3D exhibition',79.00,0.10,'Gold',0,NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `payment_method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `payment_method` varchar(25) NOT NULL,
  PRIMARY KEY (`payment_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (1,'Pangalink (Maksekeskus)'),(2,'Arve alusel');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_statuses`
--

DROP TABLE IF EXISTS `payment_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_statuses` (
  `payment_status_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `payment_status_name` varchar(255) DEFAULT NULL,
  `payment_status_info` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`payment_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_statuses`
--

LOCK TABLES `payment_statuses` WRITE;
/*!40000 ALTER TABLE `payment_statuses` DISABLE KEYS */;
INSERT INTO `payment_statuses` VALUES (1,'No invoice sent','Used for state institutions when letter of guarantee request email has been sent.'),(2,'Deposit invoice sent','When initial booking payment invoice has been sent (25%-100% rent)'),(3,'Final invoice sent','When final invoice has been sent(remaining rent and extras)'),(4,'Cancelled','Currently not in use'),(5,'In Maksekeskus','Currently not in use'),(6,'Deposit invoice not sent','When system is about to send deposit invoice'),(7,'Final invoice not sent','When system is about to send final invoice');
/*!40000 ALTER TABLE `payment_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_designs`
--

DROP TABLE IF EXISTS `room_designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_designs` (
  `room_id` int(10) unsigned NOT NULL,
  `design_id` smallint(5) unsigned NOT NULL,
  KEY `design_id` (`design_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `room_designs_rooms_room_id_fk` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_designs`
--

LOCK TABLES `room_designs` WRITE;
/*!40000 ALTER TABLE `room_designs` DISABLE KEYS */;
INSERT INTO `room_designs` VALUES (1,1),(1,6),(1,7);
/*!40000 ALTER TABLE `room_designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_extras`
--

DROP TABLE IF EXISTS `room_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_extras` (
  `room_id` int(10) unsigned NOT NULL,
  `extra_id` smallint(5) unsigned NOT NULL,
  KEY `room_id` (`room_id`),
  KEY `extra_id` (`extra_id`),
  CONSTRAINT `room_extras_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`),
  CONSTRAINT `room_extras_ibfk_2` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`extra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_extras`
--

LOCK TABLES `room_extras` WRITE;
/*!40000 ALTER TABLE `room_extras` DISABLE KEYS */;
INSERT INTO `room_extras` VALUES (3,2),(1,3),(1,4),(1,11),(1,12),(1,20),(6,3),(6,4),(6,11),(6,12),(6,20),(2,4),(2,20),(4,4),(5,4);
/*!40000 ALTER TABLE `room_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `room_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) DEFAULT NULL,
  `room_max_persons` tinyint(3) unsigned DEFAULT NULL,
  `room_description` text DEFAULT NULL,
  `room_message` varchar(255) NOT NULL,
  `room_slogan` varchar(255) DEFAULT NULL,
  `room_price` decimal(10,2) DEFAULT 0.00,
  `room_size` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `room_seats` tinyint(3) unsigned DEFAULT NULL,
  `room_seat_type` varchar(255) DEFAULT NULL,
  `room_class` varchar(255) NOT NULL,
  `room_visible` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'PÕHJASÄRA',25,'<p>&mdash; Mugavad klassikalised seljatoega toolid ja patjadega tammepuidust istumispaneel&nbsp;<br />&mdash; Kergelt liigutatavad&nbsp;&uuml;hised &uuml;marad lauad tassi/materjali toetamiseks.&nbsp;<br />&mdash; Avar, k&otilde;rge lagi, loomulik valgus ja reguleeritav valgustus<br />&mdash;&nbsp; &Otilde;dus mahutavus 5-25 in / broneerides terve stuudio kuni 30 in<br /><br /></p>','JUHTIMISKESKUS','Koosolekuks, strateegiakohtumiseks, sündmuseks, mõttetalguks, esitluseks, seminariks, treeninguks, koolituseks',54.00,62,25,'klassikalised seljatoega toolid','paike',1),(2,'Maa',5,'<p>&mdash; Mugavad toolid ja diivan<br />&mdash; Kergelt liigutatav &uuml;hine &uuml;mar laud tassi/materjali toetamiseks<br />&mdash; Avarus, k&otilde;rge lagi ja loomulik p&auml;ikesevalgus<br />&mdash; &Otilde;dus mahutavus kuni 5 inimesele</p>','INTELLIGENTSUS','Koosolekuks, strateegiapäevaks, konsultatsiooniks, grupitöödeks ruum Eeter/Põhjasära juurde.',15.00,13,5,'toolid, diivan','maa',1),(3,'Õhk - Ovaali kontor ',0,'','Õhk on Ovaali kontor.','',0.00,0,0,'','ohk',1),(4,'Vesi',5,'<p>&mdash; Mugavad toolid ja diivan<br />&mdash; Kergelt liigutatav &uuml;hine &uuml;mar laud tassi/materjali toetamiseks<br />&mdash; Avarus, k&otilde;rge lagi ja loomulik p&auml;ikesevalgus<br />&mdash; &Otilde;dus mahutavus kuni 5 inimesele</p>','VÄRSKUS','Koosolekuks, strateegiapäevaks, konsultatsiooniks, grupitöödeks ruum Eeter/Põhjasära juurde.',15.00,13,5,'toolid, diivan','vesi',1),(5,'Tuli',5,'<p>&mdash; Mugavad toolid ja diivan<br />&mdash; Kergelt liigutatav &uuml;hine &uuml;mar laud tassi/materjali toetamiseks<br />&mdash; Avarus, k&otilde;rge lagi ja loomulik p&auml;ikesevalgus<br />&mdash; &Otilde;dus mahutavus kuni 5 inimesele</p>','KÄIVITAJA','Koosolekuks, strateegiapäevaks, konsultatsiooniks, grupitöödeks ruum Eeter/Põhjasära juurde.',15.00,13,5,'','tuli',1),(6,'EETER',15,'<p><span style=\"font-size: 11px;\">&mdash;&nbsp;</span>Kott-toolid ja istumisala<br />Kott-toolid vormuvad&nbsp;vastavalt inimese kehakujule ja soovile, pakkudes mugavust, vaheldust ja avatud &otilde;hkkonda<br />&mdash; V&auml;iksed kergelt liigutatavad &uuml;hised&nbsp;lauad tassi/materjali toetamiseks<br />&mdash; Avar, k&otilde;rge&nbsp;lagi, loomulik valgus, reguleeritav valgustus<br />&mdash; &Otilde;dus mahutavus: kuni 15 inimesele<br /><br /></p>','PROGRESSIOON','Koosolekuks, strateegiakohtumiseks, mõttetalguks, esitluseks, treeninguks, koolituseks',54.00,40,15,'kott-toolid','eeter',1);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_groups`
--

DROP TABLE IF EXISTS `service_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_groups` (
  `service_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Autocreated',
  `service_group_name` varchar(255) NOT NULL COMMENT 'Autocreated',
  `service_group_description` text DEFAULT NULL,
  `service_group_image` longblob DEFAULT NULL,
  `service_group_image_location` varchar(5) DEFAULT NULL,
  `service_group_multiselect` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`service_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_groups`
--

LOCK TABLES `service_groups` WRITE;
/*!40000 ALTER TABLE `service_groups` DISABLE KEYS */;
INSERT INTO `service_groups` VALUES (1,'Want some inspiration? 3D Painting experience for the team.','<p> <b> How do you create and maintain creativity in your team? How to operate it? How do creative forces suddenly grab us? How do we create (create) tension? </b> </p>\r\n<ul>\r\n<li> · In a joint conversation, the current situation of the team and everyone is drawn. </li>\r\n<li> · During the chat, rhythms come into play, where we experience the instant state of the team and sharing it through art. </li>\r\n<li> · Let\'s see how art as a tool can help you find creative freedom. </li>\r\n</ul>\r\n<p> We recommend taking the 3D painting experience in the morning or first half of the day. </p> <br>\r\n<p> <b> Founder of the Oval Studio, Terje Krupp </b> </p>\r\n<br>\r\n<p> <b> New Reservations for 3D Painting Experience Available November 4, 2019 </b> </p>','uploads/ServiceGroup1.jpg','left',1),(2,'Want tea, coffee, water?','The kitchenette in the studio has tea and coffee for you. Up to three large cups (300ml) of coffee for people and tea in any amount. Rich selection of teas: 11 different crumbs and 6 different packets. In addition, brown sugar, honey, milk, flavor syrup, tea strainers, napkins, mugs and glasses.\r\nPackage: € 5.90 + VAT / person.\r\n\r\nYou can change the number of people at the latest\r\n72 hours before visit (by logging in)','uploads/ServiceGroup2.jpg','left',1),(3,'Want to dine outside or in the studio?','1) CARMEN CATERING\r\n\r\nby e-mail: info@carmen.ee by\r\nphone: 655 6698\r\n\r\n2) VILLA THAI CATERING\r\n\r\nby email: villathai@villathai.ee by\r\nphone: 6419348\r\nVilla Thai catering also offers more classic conference catering.\r\n\r\n3) 4SISTERS CATERING\r\n\r\nby e-mail: info@4sisterskitchen.com by\r\nphone: 5103954 for\r\ngroups of less than 20 people.\r\n\r\n4) BOLT / WOLT\r\n\r\nYou can pick up the courier yourself, order your cutlery and leave a clean trail.','uploads/ServiceGroup3.jpg','left',0);
/*!40000 ALTER TABLE `service_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_name` varchar(191) NOT NULL,
  `service_name_in_order_summary` varchar(255) DEFAULT NULL,
  `service_description` text DEFAULT NULL,
  `service_price` decimal(7,2) NOT NULL DEFAULT 0.00,
  `service_group_id` smallint(5) unsigned NOT NULL DEFAULT 1,
  `service_quantity_options` varchar(255) DEFAULT NULL,
  `service_unit_singular` varchar(255) DEFAULT NULL,
  `service_unit_plural` varchar(255) DEFAULT NULL,
  `service_deleted` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`service_id`),
  KEY `services_service_groups_service_group_id_fk` (`service_group_id`),
  CONSTRAINT `services_service_groups_service_group_id_fk` FOREIGN KEY (`service_group_id`) REFERENCES `service_groups` (`service_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'4 hours','3D paintings exhibition for 4 hours','',690.00,1,NULL,'','',0),(2,'60 minutes','3D paintings exhibition for 1 hour','',250.00,1,NULL,'','',0),(3,'30 minutes','3D paintings exhibition for ½ hour','',150.00,1,NULL,'','',0),(4,'Yes, we want','Coffee machine usage','',5.90,2,'1,2,3,4','person','persons',0),(5,'OUT OF STUDIO / DECIDE LATER','Not having lunch in the studio','',0.00,3,'','','',0),(6,'Lunch break between 11:30 - 13:00','Lunch break 11:30 - 13:00','',0.00,3,'','','',0),(7,'Lunch break between 13:00 - 14:30','Lunch break between 13:00 - 14:30','',0.00,3,'','','',0);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `setting` varchar(255) NOT NULL,
  `value` varchar(191) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('100_PERCENT_INVOICE_OPTION','true'),('SHOW_ADDITIONAL_PRODUCTS','false'),('WEEKENDS_CAN_BE_BOOKED','true');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `special_rooms`
--

DROP TABLE IF EXISTS `special_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `special_rooms` (
  `special_room_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `special_room_name` varchar(255) DEFAULT NULL,
  `special_room_max_persons` tinyint(3) unsigned DEFAULT NULL,
  `special_room_description` text DEFAULT NULL,
  `special_room_message` varchar(255) NOT NULL,
  `special_room_slogan` varchar(255) DEFAULT NULL,
  `special_room_price` decimal(10,2) DEFAULT 0.00,
  `special_room_size` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `special_room_seats` tinyint(3) unsigned DEFAULT NULL,
  `special_room_seat_type` varchar(255) DEFAULT NULL,
  `special_room_class` varchar(255) NOT NULL,
  PRIMARY KEY (`special_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `special_rooms`
--

LOCK TABLES `special_rooms` WRITE;
/*!40000 ALTER TABLE `special_rooms` DISABLE KEYS */;
INSERT INTO `special_rooms` VALUES (1,'Whole studio',35,'','Message','Slogan',64.00,0,0,'','kogu_stuudio'),(2,'Whole studio with 3D exhibition',35,'','Message','Slogan',79.00,0,0,'','3d_maalikunst');
/*!40000 ALTER TABLE `special_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplies`
--

DROP TABLE IF EXISTS `supplies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplies` (
  `supply_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `supply_name` varchar(255) NOT NULL,
  `supply_price` decimal(7,2) DEFAULT 0.00,
  `supply_in_use` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`supply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplies`
--

LOCK TABLES `supplies` WRITE;
/*!40000 ALTER TABLE `supplies` DISABLE KEYS */;
INSERT INTO `supplies` VALUES (1,'A4 papers',3.00,0),(2,'Glue pads',0.00,0),(5,'Post-it ',0.00,0),(6,'Pens 30 tk',0.00,0),(8,'Teenindaja stuudios kohapeal',12.00,0),(10,'Pliiatsid',2.00,0);
/*!40000 ALTER TABLE `supplies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `translation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phrase` varbinary(765) NOT NULL,
  `language` char(5) NOT NULL,
  `translation` text DEFAULT NULL,
  `appeared` datetime NOT NULL DEFAULT current_timestamp(),
  `last_active` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`translation_id`),
  UNIQUE KEY `language_phrase_controller_action_index` (`language`,`phrase`)
) ENGINE=InnoDB AUTO_INCREMENT=1204 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (724,'Whole studio with 3D exhibition','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:31'),(725,'Kõllid','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(726,'PÕHJASÄRA','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(727,'kord','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(728,'Lällarid','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(729,'EETER','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:29'),(730,'pc','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:31'),(731,'Pabertahvel markeritega','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(732,'Vesi','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:33'),(734,'3D paintings exhibition for 1 hour','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:44'),(735,'pcs','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:55:22'),(736,'3D paintings exhibition for ½ hour','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:44'),(738,'Coffee machine usage','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:44'),(739,'person','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:44'),(740,'Lunch break 11:30 - 13:00','et_EE','{untranslated}','2019-12-18 20:55:20','2019-12-18 20:56:44'),(742,'Order ID for this invoice is ','et_EE','{untranslated}','2019-12-18 20:55:21','2019-12-18 20:56:31'),(743,'Ovaal invoice','et_EE','{untranslated}','2019-12-18 20:55:21','2019-12-18 20:56:31'),(744,'Dear Customer,','et_EE','{untranslated}','2019-12-18 20:55:22','2019-12-18 20:55:22'),(745,'Please see the attached file for a quote for the room','et_EE','{untranslated}','2019-12-18 20:55:22','2019-12-18 20:55:22'),(746,'Thank You,','et_EE','{untranslated}','2019-12-18 20:55:22','2019-12-18 20:55:22'),(747,'This email is automatically generated.','et_EE','{untranslated}','2019-12-18 20:55:22','2019-12-18 20:55:22'),(748,'Invoice nr','et_EE','{untranslated}','2019-12-18 20:55:22','2019-12-18 20:55:22'),(749,'Toggle navigation','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(750,'Home','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(751,'Admin','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(752,'Conditions','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(753,'english','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(754,'My Bookings','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(755,'Contact: tel. 50 50 277','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(756,'Choose time / room','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(757,'Extra possibilities / catering','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(758,'Get offer / book now','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(759,'My orders','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(760,'Instructional video','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(761,'Number of people','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(762,'Search for a room','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(763,'Whole studio info','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(764,'Capacity','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(765,'people','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(766,'Room','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:47'),(767,'Room info','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(770,'Vacant time to book:','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(799,'Other rooms','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(801,'Close','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:57:26'),(802,'Ok','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(803,'Maximum number of people is ','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(804,'Vacant time to book','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(805,'CHOOSE STUDIO','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(808,'Starting time must be lower than the ending time!','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:33'),(809,'All Rights Reserved','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(810,'Privacy policy','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(811,'OK','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(812,'Accept','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(813,'Loading','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:56:45'),(814,'Server returned response in an unexpected format','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:57:26'),(815,'Forbidden','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:57:26'),(816,'Server returned an error','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:57:26'),(817,'Oops...','et_EE','{untranslated}','2019-12-18 20:55:24','2019-12-18 20:57:26'),(819,'Room search','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(820,'Search result ','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(821,'Message','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:57:11'),(822,'Whole studio','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:35'),(823,'Slogan','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:57:11'),(824,'Space:','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(825,'Capacity:','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(826,'About whole studio','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(827,'Gold Partner. If your company has visited us in the last 3 months then you will get a -10% discount','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(828,'Silver Partner. If your company has visited us in the last 6 months then you will get a -5% discount','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(829,'Your Golden Partner status is valid for','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(830,'status ends','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(831,'Studio (5 rooms)','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(833,'Today','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(834,'Day with booking(s)','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(835,'Available','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(836,'Selected','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(837,'Your booking','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(838,'The rental time is the time from arrival to departure','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(839,'Date','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(840,'Check-in','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(841,'Check-out','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(842,'People','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(843,'Rent time includes time arriving, preparing and time concluding.','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(844,'Room rent discount','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(845,'Price','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:57:26'),(846,'VAT (20%)','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(847,'Total','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:56:47'),(848,'Additional services','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:57'),(849,'Previous step','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(850,'Maximum number of people for selected room(s) is ','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(851,'Please select the correct time to book (minimum 4 hours)','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:35'),(852,'Selected time overlaps with booked time(s)! Please select an available date to continue. At least 1 hour must be in between the bookings.','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(853,'Selected time overlaps with existing booking.','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(854,'The number of people has not been entered','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(855,'Whole Studio','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(856,'hour(s)','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(857,'month,months','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(858,'day,days','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(859,'hour,hours','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(860,'minute,minutes','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(861,'second,seconds','et_EE','{untranslated}','2019-12-18 20:55:26','2019-12-18 20:55:40'),(862,'PROGRESSIOON','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:55:29'),(863,'Koosolekuks, strateegiakohtumiseks, mõttetalguks, esitluseks, treeninguks, koolituseks','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:55:29'),(864,'About room','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:55:33'),(865,'<p><span style=\"font-size: 11px;\">&mdash;&nbsp;</span>Kott-toolid ja istumisala<br />Kott-toolid vormuvad&nbsp;vastavalt inimese kehakujule ja soovile, pakkudes mugavust, vaheldust ja avatud &otilde;hkkonda<br />&mdash; V&auml;iksed kergelt liigutatavad &uuml;hised&nbsp;lauad tassi/materjali toetamiseks<br />&mdash; Avar, k&otilde;rge&nbsp;lagi, loomulik valgus, reguleeritav valgustus<br />&mdash; &Otilde;dus mahutavus: kuni 15 inimesele<br /><br /></p>','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:55:29'),(866,'Extra possibilities','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:55:33'),(867,'Projector, screen','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:56:47'),(868,'Paper whiteboard, markers','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:56:47'),(869,'Kõlarid','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:56:47'),(870,'A4, post-it, pastakad','et_EE','{untranslated}','2019-12-18 20:55:29','2019-12-18 20:56:47'),(871,'INTELLIGENTSUS','et_EE','{untranslated}','2019-12-18 20:55:31','2019-12-18 20:55:31'),(872,'Maa','et_EE','{untranslated}','2019-12-18 20:55:31','2019-12-18 20:55:31'),(873,'Koosolekuks, strateegiapäevaks, konsultatsiooniks, grupitöödeks ruum Eeter/Põhjasära juurde.','et_EE','{untranslated}','2019-12-18 20:55:31','2019-12-18 20:55:33'),(874,'<p>&mdash; Mugavad toolid ja diivan<br />&mdash; Kergelt liigutatav &uuml;hine &uuml;mar laud tassi/materjali toetamiseks<br />&mdash; Avarus, k&otilde;rge lagi ja loomulik p&auml;ikesevalgus<br />&mdash; &Otilde;dus mahutavus kuni 5 inimesele</p>','et_EE','{untranslated}','2019-12-18 20:55:31','2019-12-18 20:55:33'),(876,'Please select the correct time to book (minimum 4 hours for main room and 1 hour for groupwork room)','et_EE','{untranslated}','2019-12-18 20:55:31','2019-12-18 20:55:40'),(878,'KÄIVITAJA','et_EE','{untranslated}','2019-12-18 20:55:32','2019-12-18 20:55:32'),(879,'Tuli','et_EE','{untranslated}','2019-12-18 20:55:32','2019-12-18 20:55:32'),(881,'VÄRSKUS','et_EE','{untranslated}','2019-12-18 20:55:33','2019-12-18 20:55:33'),(882,'A4 papers','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(883,'Glue pads','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(884,'set','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(885,'Presentation remote','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(886,'Canon 60D camera','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(887,'Pabertahvel, markerid','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(888,'Teenindaja kohalolu kohtumise ajal','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(889,'Kontoritarvete esmaabi (A4 paberid, post-it, paberinäts, pastakad) ','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(890,'Wifi (allalaadimise kiirus kuni 150 Mbit/s)','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(891,'LCDTV','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(892,'Kirjutamisalused A3','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(893,'Paigutus: ümber suure laua ruumi keskel','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(894,'Vastuvõtu teenus: külaliste vastuvõtmine, ruumi juhatamine, ärasaatmine terve kohtumise vältel','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(895,'Lisateenus: tasside/klaaside koristamine koolitusruumist (alternatiiv on ise nuti-nõudepesumasinasse asetada) ','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(896,'Esitluspult ','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(897,'Eripuhastusteenus: ruumi jäetud tasside/klaaside koristus','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(898,'Lisapabertahvel, markerid','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(899,'Ruumid Maa, Tuli, Vesi rent','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:31'),(901,'4 hours','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(902,'60 minutes','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(903,'30 minutes','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(904,'Yes, we want','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(905,'OUT OF STUDIO / DECIDE LATER','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(906,'Lunch break between 11:30 - 13:00','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(907,'Lunch break between 13:00 - 14:30','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(909,'3D paintings exhibition for 4 hours','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(910,'','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(919,'persons','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(921,'Not having lunch in the studio','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(936,'Refreshments can be ordered 3 days before the event. You can first confirm the room booking, and then add the catering (Just log in). No outside food or drinks. Thank you. You can have lunch also in the cafe/pub/restaurant nearby to the studio, e.g. Viktus (in the same building), Pööbel, L\'Ermitage, Kuldmokk, etc. Enjoy!','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(937,'Want some inspiration? 3D Painting experience for the team.','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(941,'Explore more','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(942,'<p> <b> How do you create and maintain creativity in your team? How to operate it? How do creative forces suddenly grab us? How do we create (create) tension? </b> </p>\r\n<ul>\r\n<li> · In a joint conversation, the current situation of the team and everyone is drawn. </li>\r\n<li> · During the chat, rhythms come into play, where we experience the instant state of the team and sharing it through art. </li>\r\n<li> · Let\'s see how art as a tool can help you find creative freedom. </li>\r\n</ul>\r\n<p> We recommend taking the 3D painting experience in the morning or first half of the day. </p> <br>\r\n<p> <b> Founder of the Oval Studio, Terje Krupp </b> </p>\r\n<br>\r\n<p> <b> New Reservations for 3D Painting Experience Available November 4, 2019 </b> </p>','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(943,'Want tea, coffee, water?','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(945,'1','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(946,'2','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(948,'3','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(950,'4','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(953,'The kitchenette in the studio has tea and coffee for you. Up to three large cups (300ml) of coffee for people and tea in any amount. Rich selection of teas: 11 different crumbs and 6 different packets. In addition, brown sugar, honey, milk, flavor syrup, tea strainers, napkins, mugs and glasses.\r\nPackage: € 5.90 + VAT / person.\r\n\r\nYou can change the number of people at the latest\r\n72 hours before visit (by logging in)','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(954,'Want to dine outside or in the studio?','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(959,'1) CARMEN CATERING\r\n\r\nby e-mail: info@carmen.ee by\r\nphone: 655 6698\r\n\r\n2) VILLA THAI CATERING\r\n\r\nby email: villathai@villathai.ee by\r\nphone: 6419348\r\nVilla Thai catering also offers more classic conference catering.\r\n\r\n3) 4SISTERS CATERING\r\n\r\nby e-mail: info@4sisterskitchen.com by\r\nphone: 5103954 for\r\ngroups of less than 20 people.\r\n\r\n4) BOLT / WOLT\r\n\r\nYou can pick up the courier yourself, order your cutlery and leave a clean trail.','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(960,'Gold partner discount','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:09'),(961,'Services','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:57:26'),(962,'VAT','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:47'),(963,'Rooms and extras','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:56:44'),(964,'Rooms','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:57:26'),(965,'Summary','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:55:57'),(966,'Previous','et_EE','{untranslated}','2019-12-18 20:55:43','2019-12-18 20:55:57'),(967,'December','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:44'),(968,'Company name','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(969,'Cancel and go back','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(970,'SERVICE PROVIDER','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(971,'BRAND','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(972,'CUSTOMER','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(973,'READ','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(974,'Booking','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(975,'cancellation','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(977,'Renter','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(978,'Keyperson','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(979,'Excellent, almost there. Let\'s summarize.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(980,'Order summary','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(981,'To confirm the booking, 25% of the room rent must be paid or by letter of guarantee.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(982,'By confirming, you agree to send the letter of guarantee or pay the sum based on the following calculation:','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(983,'Room(s)','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(984,'rent','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(985,'EUR, excluding VAT','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(986,'25% of the sum','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(988,'Rent time includes arriving and leaving time.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(989,'We kindly ask the key person to arrive first to introduce the studio, the key person will be given studio\'s door card.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(990,'Coordinators name ','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(991,'By clicking \"Receive order summary (PDF) to your email\", you get an offer, but it\'s <strong>not</strong> a confirmation.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(992,'By clicking \'Confirm booking\', you confirm your order.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(993,'Coordinator enters Ovaalstuudio first and get a doorcard.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(994,'I have read and agree to','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(995,'purchase conditions','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(996,'Receive order summary (PDF) to your email','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(997,'Confirm booking','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(998,'Contact: (+372) 505 0277 or ovaal@ovaal.ee','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(999,'Your info','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1001,'Registry number','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1002,'Address','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1003,'Invoice email','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1004,'Additional info for invoice','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1005,'Can\'t find your company?','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1006,'Add it here!','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1007,'Payment method','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1008,'Send me an invoice','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1009,'as PDF/Print','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1010,'To confirm the order, I will send a letter of guarantee to ovaal@ovaal.ee','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1011,'The invoice will be sent to Your email 1 day before the booking.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1013,'Perfect, all done!','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1014,'Thanks for choosing Ovaalstuudio.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1015,'CLOSE','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1016,'Back to main page','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1017,'Stay here','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1018,'<p>TURVALISUS JA HEAPEREMEHELIKKUS<br /><br />&mdash; Rentnik, V&otilde;tmeisik&nbsp;ja iga k&uuml;laline vastutab Ovaalstuudios viibides oma tervise ja kaasav&otilde;etud <br />materiaalsete v&auml;&auml;rtuste eest ise. Rentnik/V&otilde;tmeisik teavitab k&uuml;lalisi nimetatud vastutusest.<br />&mdash;&nbsp;Rentnik teavitab Rendileandjat viivitamatult Ovaalstuudios tekkinud ohust, avariist, tulekahjust ja v&otilde;tab koheselt tarvitusele abin&otilde;ud p&otilde;hjuste likvideerimiseks. Stuudio&nbsp;avaruumis ja koridoris asub kaks tulekustutit&nbsp;ning esmaabikarp.<br />&mdash;&nbsp;Rentnik korraldab oma tegevusi nii, et arvestab teiste rentnike tegevustega Ovaalstuudios.<br />&mdash;&nbsp;Rentnik kasutab Ovaalstuudio ruumi(e) heaperemehelikult ja vas','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:55:54'),(1019,'<p><br /><em>Ovaalstuudio keskkonna eesm&auml;rk on luua meeskonda </em><br /><em>&uuml;hendavaid&nbsp;ja inimesi koost&ouml;&ouml;sse toovaid&nbsp;hetki.</em><br /><br />V&Otilde;TMEISIK<br /><br />&mdash; Ovaalstuudio kasutamise ajal viibib kohapeal Rentnik v&otilde;i volitatud vastutav isik (edaspidi: V&otilde;tmeisik). <br />&mdash; V&otilde;tmeisik saab broneeritud ruumi(d) enda kasutusse broneeringus fikseeritud ajal.&nbsp;<br />&mdash;&nbsp;V&otilde;tmeisikul&nbsp;palume saabuda&nbsp;esimesena -&nbsp;v&otilde;tmeisik saab vajadusel stuudiost v&auml;lja-sisse liikumiseks stuudio poolt uksekaardi. V&otilde;tmeisikul palume lahkuda koos v&otilde;i p&auml;rast k&uuml;lalisi/meeskonda, mitte varem.&nbsp;<br /><br /><em>Stuudios on loodud disain ja IT la','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:55:54'),(1020,'Go to main page.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1021,'We couldn\'t find a company with that registry number.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1022,'Please check that the entered registry number is correct and try again or add a new company below.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1023,'Registry Number','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1025,'Name','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:57:26'),(1027,'City','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1028,'Company city','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1029,'Street','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1030,'Company street address','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1031,'Postal code','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1032,'Comapny postal code','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1033,'Add new company','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1034,'Please enter your company name','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1035,'Send quote','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1036,'Given room(s) is already booked on selected date and time','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1037,'Read and accept our purchase conditions to continue.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1038,'Please fill out the coordinators name!','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1039,'Please enter the correct company name','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1040,'Thanks for the inquiry!','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1041,'Booking offer was sent to your email.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1042,'Please select or create a new company first.','et_EE','{untranslated}','2019-12-18 20:55:54','2019-12-18 20:56:09'),(1043,'partial payment','et_EE','{untranslated}','2019-12-18 20:56:29','2019-12-18 20:56:31'),(1044,'Hello!','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1045,'An invoice is attached to this message.','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1046,'Thank You.','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1047,'Best regards,','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1048,'Invoice from Ovaal','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1049,'A booking has been made!','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1050,'A copy of the invoice sent to the customer has been attached to this message.','et_EE','{untranslated}','2019-12-18 20:56:31','2019-12-18 20:56:31'),(1051,'My Orders','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1052,'Order ID','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:57:13'),(1053,'Room Name','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1054,'Date and time','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1055,'Manage','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1056,'Summary PDF','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:57:13'),(1057,'Key person','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1058,'General conditions','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1059,'Change catering options','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1060,'View conditions','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1063,'Coordinator name','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:57:13'),(1064,'Old password','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1065,'New password','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1066,'Change password','et_EE','{untranslated}','2019-12-18 20:56:35','2019-12-18 20:56:45'),(1067,' partner discount','et_EE','{untranslated}','2019-12-18 20:56:44','2019-12-18 20:56:44'),(1068,'Order','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1069,'Rooms, extras and services','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1070,'partner discount','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1071,'Total including VAT','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1072,'Tel: (+372) 50 50 277','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1073,'E-mail:','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1074,'Website:','et_EE','{untranslated}','2019-12-18 20:56:46','2019-12-18 20:56:47'),(1075,'Ovaal admin','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1076,'Users','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1077,'Special rooms','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1078,'Bookings','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1079,'Offers','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1080,'Info boxes','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1081,'Translations','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1082,'Settings','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:26'),(1083,'Edit order','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1084,'Orders','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1085,'To edit a order, click on a cell and insert a value. To save the value, click on any other cells.','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1086,'Order created','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1087,'Booking(s) date','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1088,'Total people','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1089,'Status','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1090,'Client','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1091,'Additional info','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1092,'Extras/Supplies/Designs','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1093,'Bookings for this order','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1094,'View/Edit catering','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1095,'Send reminder','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1096,'SimplBooks','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1097,'Don\'t send 75%','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1098,'Add invoice row','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1099,'Delete Order','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1100,'View Extras/Supplies/Designs','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1101,'View bookings','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1102,'No','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1103,'Yes','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1104,'Credit proforma invoice','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1111,'Generate invoice minus proforma','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1114,'Something went wrong!','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:13'),(1115,'Start typing','et_EE','{untranslated}','2019-12-18 20:57:08','2019-12-18 20:57:15'),(1116,'Edit user','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1117,'To edit a user, click on a cell and insert a value. To save the value, click on any other cells.','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1118,'User ID','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1119,'Is admin','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1120,'Email','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1121,'Full name','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1122,'Phone','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1123,'Company','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1124,'State institution','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1125,'Actions','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1126,'Action','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1127,'Switch user','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1128,'Delete user','et_EE','{untranslated}','2019-12-18 20:57:10','2019-12-18 20:57:10'),(1129,'Room ID','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1130,'Room name','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1131,'Room max persons','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1132,'Room description','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1133,'Room slogan','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1134,'Room message','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1135,'Room price','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1136,'Room size','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1137,'Room seats','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1138,'Room seat type','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1139,'Room class','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1140,'Delete Room','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1141,'Edit Room','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:12'),(1142,'Add room','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1143,'Edit extras','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1144,'Edit supplies','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1145,'Edit designs','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1147,'Max persons','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1149,'Area size','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1151,'Description','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:26'),(1152,'Seat count','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1154,'Seat type','et_EE','{untranslated}','2019-12-18 20:57:11','2019-12-18 20:57:11'),(1157,'Offer requested by','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1158,'Offer requested at','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1159,'Offer PDF','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1160,'Info box ID','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1161,'Info box name','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1162,'Info box text','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1163,'Add info box','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1168,'Save changes','et_EE','{untranslated}','2019-12-18 20:57:14','2019-12-18 20:57:14'),(1169,'Add translation','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1170,'To edit a translation, click on a cell and insert a value. To save the value, click on any other cells. Only the first 255 characters of phrases are shown. Translations can be up to 32768 characters long.','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1171,'Untranslated','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1172,'Translated','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1173,'All','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1174,'Appeared','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1175,'Language','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1176,'Phrase','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1177,'Translation','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1178,'Last active','et_EE','{untranslated}','2019-12-18 20:57:15','2019-12-18 20:57:15'),(1179,'Show 100% invoice option','et_EE','{untranslated}','2019-12-18 20:57:16','2019-12-18 20:57:16'),(1180,'Weekends can be booked','et_EE','{untranslated}','2019-12-18 20:57:16','2019-12-18 20:57:16'),(1181,'Service groups','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1182,'Add service group','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1184,'Multiselect?','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1185,'Image','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1186,'Image location','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1187,'Save service group','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1188,'Add service','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1189,'Unit','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1191,'Service name','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1192,'Name in ordersummary','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1193,'Service name in ordersummary','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1194,'Service description','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1195,'Quantity selection','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1196,'Enable','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1197,'Options','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1199,'separate by commas','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1200,'Unit price','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1201,'Service price','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26'),(1202,'Save service','et_EE','{untranslated}','2019-12-18 20:57:17','2019-12-18 20:57:26');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_discount_levels`
--

DROP TABLE IF EXISTS `user_discount_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_discount_levels` (
  `user_discount_level_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_discount_level_name` varchar(191) NOT NULL,
  PRIMARY KEY (`user_discount_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_discount_levels`
--

LOCK TABLES `user_discount_levels` WRITE;
/*!40000 ALTER TABLE `user_discount_levels` DISABLE KEYS */;
INSERT INTO `user_discount_levels` VALUES (1,'Default'),(2,'Silver'),(3,'Gold');
/*!40000 ALTER TABLE `user_discount_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `first_and_last_name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `state_institution` int(11) DEFAULT 0,
  `activation_code` text DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `user_created_at` datetime NOT NULL,
  `invoice_email` varchar(191) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_company___fk` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'$2y$10$f4OTZETUajddcZ.BGyDi1ecgvMiqiRpMXCUIC1UJHnNSlOMZCIZ3G','demo@demo.ee',0,'Demo Demo','55555555',0,NULL,1,'2018-04-03 13:43:51','demo@demo.ee',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-21 18:56:07
