<?php

require __DIR__.'/../config.php';
$database = DATABASE_DATABASE;
// Andmebaasi ühendused
$dbTest = mysqli_connect(DATABASE_HOSTNAME, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_DATABASE) or die(mysqli_error($dbTest));

mysqli_select_db($dbTest, 'information_schema') or die(mysqli_error($dbTest));

mysqli_query($dbTest, "SET NAMES 'utf8'") or die (mysqli_error($dbTest));

mysqli_query($dbTest, "SET CHARACTER SET utf8") or die (mysqli_error($dbTest));

$q = mysqli_query($dbTest, "SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA = '$database' AND COLUMN_NAME LIKE '%_id'");
while ($row = mysqli_fetch_assoc($q)) {
    $test_tables[$row['TABLE_NAME']][$row['COLUMN_NAME']] = $row;
}

foreach ($test_tables as $table_name => $columns) {

    foreach ($columns as $column_name => $column) {
        $q = mysqli_query($dbTest, "SELECT * FROM `KEY_COLUMN_USAGE` kcu JOIN `TABLE_CONSTRAINTS` tc ON kcu.TABLE_NAME = tc.TABLE_NAME AND kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME AND kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA
                                    WHERE kcu.TABLE_SCHEMA = '$database' AND kcu.TABLE_NAME = '$table_name' AND kcu.COLUMN_NAME = '$column_name'");
        if (mysqli_num_rows($q) == 0) {
            $results[] = '<span class="bad">'.$table_name . '.' . $column_name . ' does not reference other columns'.'</span>';
        } else {
            while ($row = mysqli_fetch_assoc($q)) {
                if ($row['CONSTRAINT_TYPE'] == 'FOREIGN KEY') {
                    $results[] = '<span class="good">'.$row['TABLE_NAME'] . '.' . $row['COLUMN_NAME'] . ' has a ' . $row['CONSTRAINT_TYPE'] . ' constraint with ' . $row['REFERENCED_TABLE_NAME'] . '.' . $row['REFERENCED_COLUMN_NAME'].'</span>';
                }
            }
        }
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Andmebaaside võrdlus</title>
    <link rel="stylesheet" href="../assets/components/bootstrap/3.3.0/css/bootstrap.min.css">
    <style>
        .good{
            background-color: springgreen;
        }
        .bad{
            background-color: red;
        }
    </style>
</head>
<body>
<div class="container">

    <h1>Constraints:</h1>
    <ul>
        <?php if (!empty($results)) foreach ($results as $r): ?>
            <li><?= $r ?></li>
        <?php endforeach ?>
    </ul>

</div>

</body>
</html>