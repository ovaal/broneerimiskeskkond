Feature: As a user i want to search for available rooms on a given date

  Scenario:
    Given the following bookings exist:
      | booking_id | room_id | booking_start       | booking_end         |
      | 1          | 1       | 2016-12-11 13:00:00 | 2016-12-11 14:00:00 |
      | 2          | 2       | 2016-12-11 12:00:00 | 2016-12-11 13:00:00 |
      | 3          | 2       | 2016-12-11 11:00:00 | 2016-12-11 12:00:00 |
      | 4          | 3       | 2016-12-12 13:00:00 | 2016-12-12 14:00:00 |
      | 5          | 4       | 2016-12-12 13:01:00 | 2016-12-12 14:01:00 |
      | 6          | 5       | 2016-12-11 12:30:00 | 2016-12-11 13:30:00 |
    And I enter "2016-12-11" as a date
    And I enter "13:00" as a start time
    And I enter "14:00" as an end time
    When I press Search
    Then I see rooms with IDs "[2,3,4]"
