Feature: Booking a room
  In order to use a room
  As a customer
  I must be able to book it and pay for it

  @javascript
  Scenario: Logging in
    Given I am logged in
    When I select number of people
    And I pick a room
    And I select extras
    And I select catering options
    And I have given coordinator's name
    And I have confirmed that I agree to terms and conditions
    And I have confirmed my booking details
    And I have selected a payment method
    Then I should receive an email with an invoice
