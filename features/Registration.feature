Feature: As a non registered user i want to create a new account

  Scenario:
    Given the following users exist:
      | user_id | email         | password |
      | 1       | demo@demo.ee  | demo     |
      | 2       | kalkun@hot.ee | demo     |

    When I register as "Ants" using email "lammas123@hot.ee" and password "demo"
    Then it succeeds
    When I register as "Robert" using email "lammas123@hot.ee" and password "demo"
    Then it fails
