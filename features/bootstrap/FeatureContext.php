<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\MinkExtension\Context\RawMinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context, SnippetAcceptingContext
{
    private $rooms, $result;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->rooms = new \Broneerimiskeskkond\Room();
    }

    /**
     * @When I wait :sec
     */
    public function iWait($sec)
    {
        sleep($sec);
    }

    public function spin($lambda)
    {
        while (true) {
            try {
                if ($lambda($this)) {
                    return true;
                }
            } catch (Exception $e) {
                // do nothing
            }

            sleep(1);
        }
    }

    /**
     * @When I wait for :text to appear
     * @Then I should see :text appear
     * @param $text
     * @throws \Exception
     */
    public function iWaitForTextToAppear($text)
    {
        $this->spin(function (FeatureContext $context) use ($text) {
            try {
                $context->assertPageContainsText($text);
                return true;
            } catch (\Behat\Mink\Exception\ResponseTextException $e) {
                // NOOP
            }
            return false;
        });
    }

    /**
     * @Then /^I click on "([^"]*)"$/
     */
    public function iClickOn($element)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $element);
        if (!$findName) {
            throw new Exception($element . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * @Given I am logged in
     */
    public function iAmLoggedIn()
    {
        $this->visit("http://localhost/ovaal/");
        $this->fillField("login-email", "demo@demo.ee");
        $this->fillField("login-pw", "demo");
        $this->pressButton("signin-btn");
        $this->iWaitForTextToAppear('Tere!');
        $this->iClickOn("a#accept-conditions");
    }

    /**
     * @When I select number of people
     */
    public function iSelectNumberOfPeople()
    {
        $this->fillField("number_of_people", "5");
        $this->pressButton("search-for-room");
        $this->iWaitForTextToAppear('Other rooms');
    }

    /**
     * @When I pick a room
     */
    public function iPickARoom()
    {
        $this->iWait(1); // Wait until the page scrolls down for the element to be visible
        $this->iClickOn("span#1");
        $this->iClickOn("a.nextstep");
        $this->iWaitForTextToAppear('Põhjasära on juhtimiskeskus.');
    }

    /**
     * @When I select extras
     */
    public function iSelectExtras()
    {
        $this->checkOption("extra_checbox");
        $this->pressButton("next-step");
        $this->iWaitForTextToAppear('Choose products');
    }

    /**
     * @When I have given coordinator's name
     */
    public function iHaveGivenCoordinatorsName()
    {
        $this->fillField("coordinator", "5");

    }

    /**
     * @When I have confirmed that I agree to terms and conditions
     */
    public function iHaveConfirmedThatIAgreeToTermsAndConditions()
    {
        $this->checkOption("conditions-checkbox");

    }

    /**
     * @When I have confirmed my booking details
     */
    public function iHaveConfirmedMyBookingDetails()
    {
        $this->iClickOn("a#confirm-order");
        $this->iWaitForTextToAppear('Payment method');
    }

    /**
     * @When I have selected a payment method
     */
    public function iHaveSelectedAPaymentMethod()
    {
        $this->iClickOn("a#btnPaymentByInvoice");
        $this->iWaitForTextToAppear('Payment method');
    }

    /**
     * @Then I should receive an email with an invoice
     */
    public function iShouldReceiveAnEmailWithAnInvoice()
    {
        $this->visit("http://localhost:8025/");
        $this->iWaitForTextToAppear('a few seconds ago');
    }
}
