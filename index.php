<?php namespace Halo;
use Broneerimiskeskkond\Response;

ob_start();

// Init composer auto-loading
require 'vendor/autoload.php';

// Load constants
require 'constants.php';

date_default_timezone_set('Europe/Tallinn');

// Convert warnings and notices to exceptions


set_error_handler(function ($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new \ErrorException($message, 0, $severity, $file, $line);
});


if (file_exists('config.php')) {

    include 'config.php';

    // Default env is development
    if (!defined('ENV')) define('ENV', ENV_DEVELOPMENT);

}

// Load sentry
require 'templates/_partials/sentry.php';

// Load app
require 'system/classes/Application.php';

try {
    new Application;
} catch (\Exception $e) {

    // To see the error message in dev
    if (ENV == ENV_PRODUCTION) {
        send_error_report($e);
        Response::showHtmlErrorPage('There was an error while processing your request. Please try again later.');
    }

    throw $e;
}