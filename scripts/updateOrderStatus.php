<?php namespace Halo;

if (version_compare(PHP_VERSION, '8.0.0', '<')) {
    exit('Vähemalt php versioon 8 on nõutav');
}

// Migrates orders table from old to new by PaymentStatus

use Broneerimiskeskkond\PaymentStatus;
use Broneerimiskeskkond\Settings;
use SimplBooks\SimplBooks;

dlog('Starting ' . __FILE__);

// Show all errors
error_reporting(E_ALL);

// Crontab in Zone sets current working directory to /data01/virt24050/tmp
chdir(dirname(__DIR__, 1));


define('BASE_URL', '');

// Start output buffering
ob_start();

require './vendor/autoload.php';
require './system/functions.php';
require './constants.php';
require './config.php';
require './system/database.php';
//require 'classes/broneerimiskeskkond';

if (Settings::get('UPDATE_ORDER_STATUSES_SCRIPT_HAS_ALREADY_BEEN_RUN')) {
    exit('Update script has already been done');
}

// Set the timezone to Tallinn (for DB)
date_default_timezone_set('Europe/Tallinn');

// Set time limit for the script
set_time_limit(1200);

start();
dlog('Finished ' . __FILE__);
die();

function dlog($msg)
{
    $myfile = fopen(dirname(__DIR__, 1) . "/scripts/updateOrderStatus.txt", "a") or die("Unable to write to file!");
    fwrite($myfile, print_r(date('Y-m-d H:i:s') . ' ' . $msg, 1) . "\n");
    fclose($myfile);
}

function start()
{
    $lookup = [
        PaymentStatus::INVOICE_COMPLETED => PaymentStatus::FINAL_INVOICE_SENT,
        PaymentStatus::INVOICE_WAITING_FOR_EVENT => PaymentStatus::FINAL_INVOICE_SENT,
        PaymentStatus::INVOICE_CANCELLED_HISTORICAL => PaymentStatus::ORDER_CANCELLED,
        PaymentStatus::INVOICE_TO_SEND => PaymentStatus::NO_INVOICE_SENT,
        PaymentStatus::INVOICE_SENT => null,
        PaymentStatus::INVOICE_IN_MAKSEKESKUS_HISTORICAL => PaymentStatus::INVOICE_IN_MAKSEKESKUS,
        PaymentStatus::INVOICE_NEW => PaymentStatus::NO_INVOICE_SENT,
    ];

    q( "SELECT * FROM orders JOIN bookings USING (order_id)", $q);
    while (($order = mysqli_fetch_assoc($q))) {
        $orders[$order['order_id']] = $order;
        if ($order['payment_status_id'] == PaymentStatus::INVOICE_SENT) {
            $newPaymentStatusId = $order['booking_start'] >= date('Y-m-d H:i:s')
                ? PaymentStatus::DEPOSIT_INVOICE_SENT : PaymentStatus::FINAL_INVOICE_SENT;
        } else {
            $newPaymentStatusId = $lookup[$order['payment_status_id']];
        }
        $orders[$order['order_id']] ['payment_status_id'] = $newPaymentStatusId;
    }

    q( 'set FOREIGN_KEY_CHECKS=0');
    q( 'TRUNCATE table orders');
    foreach ($orders as $order) {
        insert('orders', [
            'order_id' => $order['order_id'],
            'deposit_invoice_id' => $order['deposit_invoice_id'],
            'invoice_has_extra_row' => $order['invoice_has_extra_row'],
            'user_id' => $order['user_id'],
            'payment_status_id' => $order['payment_status_id'],
            'deposit_invoice_number' => $order['deposit_invoice_number'],
            'invoice_additional_info' => $order['invoice_additional_info'],
            'order_made' => $order['order_made'],
            'total_number_of_people' => $order['total_number_of_people'],
            'company_id' => $order['company_id'],
            'special_room_price' => $order['special_room_price'],
            'discount_percent' => $order['discount_percent'],
            'send_reminder' => $order['send_reminder'],
            'discount_level' => $order['discount_level'],
            'coordinator_name' => $order['coordinator_name'],
            'deleted' => $order['deleted'],
            'order_billed_rent_percentage' => $order['order_billed_rent_percentage'],
            'special_room_name' => $order['special_room_name'],
            'dont_send_invoice' => $order['dont_send_invoice'],
            'special_room_selected' => $order['special_room_selected']
        ]);
    }
    q('set FOREIGN_KEY_CHECKS=1');
    q('DROP TABLE IF EXISTS orders2');
    q("UPDATE payment_statuses SET payment_status_name = 'No invoice sent' WHERE payment_status_id=1");
    q("UPDATE payment_statuses SET payment_status_name = 'Deposit invoice sent' WHERE payment_status_id=2");
    q("UPDATE payment_statuses SET payment_status_name = 'Final invoice sent' WHERE payment_status_id=3");
    q("UPDATE payment_statuses SET payment_status_name = 'Cancelled' WHERE payment_status_id=4");
    q("UPDATE payment_statuses SET payment_status_name = 'In Maksekeskus' WHERE payment_status_id=5");
    delete("payment_statuses", "payment_status_id>5");
    q("ALTER TABLE orders CHANGE deposit_invoice_id deposit_invoice_id SMALLINT UNSIGNED NULL");
    q("ALTER TABLE orders CHANGE deposit_invoice_number deposit_invoice_number VARCHAR(255) NULL");

    // Update billed rent percentage
    // Set all old orders to 100% billed and final invoice sent
    q( "UPDATE orders JOIN bookings USING (order_id) SET order_billed_rent_percentage = 25 WHERE payment_status_id = 2");
    q( "UPDATE orders JOIN bookings USING (order_id) SET order_billed_rent_percentage = 100 WHERE payment_status_id = 3");

    Settings::set('UPDATE_ORDER_STATUSES_SCRIPT_HAS_ALREADY_BEEN_RUN', 'true');
}