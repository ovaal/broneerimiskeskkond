<?php namespace Halo;

use Broneerimiskeskkond\Language;
use Broneerimiskeskkond\SQL;
use Broneerimiskeskkond\Translation;
use Halo;

/**
 * Created by PhpStorm.
 * User: henno
 * Date: 9/16/13
 * Time: 11:17 PM
 */
class Application
{
    public $auth = null;
    public $params = null;
    public $action = 'index';
    public $controller = DEFAULT_CONTROLLER;

    function __construct()
    {
        global $controller;

        $this->load_common_functions();
        $this->set_session_save_path();

        session_name(PROJECT_SESSION_ID);
        session_start();

        $_SESSION['language'] = empty($_SESSION['language']) ? 'et_EE' : $_SESSION['language'];

        $this->set_base_url();

        // Redirect to HTTPS
        if (defined('FORCE_HTTPS') && FORCE_HTTPS && $this->https_is_off()) {
            $this->redirect_to_https();
        }

        $this->init_db();
        $this->set_language();
        $this->process_uri();
        $this->register_shutdown_function();
        $this->handle_routing();

        $this->auth = new Auth;

        // Instantiate controller
        $controller_fqn = '\Halo\\' . $this->controller;

        if (!file_exists("controllers/$this->controller.php")) {
            error_out("<b>Error:</b> File <i>controllers/{$this->controller}.php</i> does not exist.");
        }
        require "controllers/$this->controller.php";

        if (!class_exists($controller_fqn, 1)) {
            error_out("<b>Error:</b>
				File  <i>controllers/{$this->controller}.php</i> exists but class <i>{$this->controller}</i> does not. You probably copied the file but forgot to rename the class in the copy.");
        }
        $controller = new $controller_fqn();

        // Make request and auth properties available to controller
        $controller->controller = $this->controller;
        $controller->action = $this->action;
        $controller->params = $this->params;
        $controller->auth = $this->auth;

        // Check if the user has extended Controller
        if (!isset($controller->requires_auth)) {
            $errors[] = 'You forgot the "<i>extends Controller</i>" part for the class <i>' . $controller->controller . '</i> in controllers/' . $controller->controller . '.php</i>. Fix it.';
            require 'templates/error_template.php';
            exit();
        }

        // Authenticate user, if controller requires it
        if ($controller->requires_auth && !$controller->auth->logged_in) {
            $controller->auth->require_auth();
        }

        $controller->user_has_orders = empty($controller->auth->user_id) ? false : \Broneerimiskeskkond\Orders::find_by_user($controller->auth->user_id);

        // Check if user is admin, if controller requires it
        if ($controller->requires_admin && !$controller->auth->is_admin && !isset($_SESSION['real_user_id'])) {
            $errors[] = __('You\'re not admin!');
            require 'templates/error_template.php';
            exit();
        }

        // Run the action
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && method_exists($controller,
                'AJAX_' . $controller->action)) {

            $action_name = 'AJAX_' . $controller->action;

            $controller->$action_name();

            stop(200);

        } else {
            // Check for and process POST ( executes $action_post() )
            if (isset($_POST) && !empty($_POST) && method_exists($controller, 'POST_' . $controller->action)) {
                $action_name = 'POST_' . $controller->action;
                $controller->$action_name();
            }

            // Check for and process FILES ( executes $action_upload() )
            if (isset($_FILES) && !empty($_FILES) && method_exists($controller, 'UPLOAD_' . $controller->action)) {
                $action_name = 'UPLOAD_' . $controller->action;
                $controller->$action_name();
            }

            // Proceed with regular action processing ( executes $action() )
            if (!method_exists($controller, $controller->action)) {
                error_out("<b>Error:</b>
				The action <i>{$controller->controller}::{$controller->action}()</i> does not exist.
				Open <i>controllers/{$controller->controller}.php</i> and add method <i>{$controller->action}()</i>");
            }
            $controller->{$controller->action}();
            $controller->render($controller->template);
        }

    }

    private function load_common_functions()
    {
        require dirname(__FILE__) . '/../functions.php';

    }

    private function set_base_url()
    {
        $s = &$_SERVER;
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME']);
        $uri = $protocol . '://' . $host . $port . dirname($_SERVER['SCRIPT_NAME']);
        define('BASE_URL', rtrim($uri, '/') . '/');
    }

    private function process_uri()
    {
        if (isset($_SERVER['REQUEST_URI'])) {

            // Get path from REQUEST_URI
            $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);


            // Strip directory from $path
            $project_directory = dirname($_SERVER['SCRIPT_NAME']);
            $path = substr($path, strlen($project_directory));


            // Split path parts into an array
            $path = explode('/', $path);


            // Remove empty values, due to leading or trailing or double slash, and renumber array from 0
            $path = array_values(array_filter($path));


            // Set controller, action and params
            $this->controller = isset($path[0]) ? array_shift($path) : DEFAULT_CONTROLLER;
            $this->action = isset($path[0]) && !empty($path[0]) ? array_shift($path) : 'index';
            $this->params = isset($path[0]) ? $path : array();
        } else {
            trigger_error('$_SERVER[REQUEST_URI] is undefined. Cannot continue. Make sure .htaccess is read by Apache or that NginX is configured properly.');
        }
    }

    private function handle_routing()
    {
        //TODO: write here your own code if you want to manipulate controller, action

        // Allow shorter URLs (users/view/3 becomes users/3)
        if (is_numeric($this->action)) {

            // Prepend the number in action to params array
            array_unshift($this->params, $this->action);

            // Overwrite action to view
            $this->action = 'view';
        }

        // Admin orders
        if ($this->controller == 'admin' && $this->action == 'orders' && isset($this->params[0]) && is_numeric($this->params[0])) {
            $this->action = 'order_view';
        }

        // admin/invoices/by-company
        if ($this->controller == 'admin' && $this->action == 'invoices' && isset($this->params[0]) && $this->params[0] == 'by-company') {
            $this->action = 'invoices_by_company';
        }


    }

    private function init_db()
    {
        require dirname(__FILE__) . '/../database.php';
    }

    private function set_language()
    {

        // Extract supported languages
        $enabled_languages = Language::getEnabled();

        // Set default language (defaults to 'et', if no supported languages are given
        $default_language = isset($enabled_languages[0]) ? $enabled_languages[0]['language_short_name'] : 'et_EE';


        // Check GET
        if (isset($_GET['language']) && Language::isEnabled($_GET['language'])) {

            if (is_array($_GET['language'])) {
                trigger_error('Possible hacking attempt');
            }

            $_SESSION['language'] = $_GET['language'];
            setcookie("language", $_SESSION['language'], time() + 3600 * 24 * 30);

            // Else check COOKIE
        } elseif (!empty($_COOKIE["language"]) && Language::isEnabled($_COOKIE['language'])) {

            $_SESSION['language'] = $_COOKIE['language'];

        } // First visit, set default langauge
        else {
            if (!isset($_SESSION['language']) || !Language::isEnabled($_SESSION['language'])) {
                $_SESSION['language'] = $default_language;
            }
        }

        // Else leave $_SESSION['language'] unchanged
    }

    private function set_session_save_path()
    {
        if (defined('SESSION_SAVE_PATH') && defined('SESSION_MAX_LIFETIME')) {
            ini_set("session.gc_maxlifetime", SESSION_MAX_LIFETIME);
            ini_set("session.save_path", SESSION_SAVE_PATH);
        }
    }

    private function register_shutdown_function()
    {
        $dir = dirname(__DIR__, 2);
        register_shutdown_function(function () use ($dir) {

            global $translations_used;
            global $new_translations;

            if (!empty($translations_used)) {
                chdir($dir);

                $translations_used = SQL::stringify($translations_used);

                Translation::updateLastActive($translations_used);
            }

            if (!empty($new_translations)) {
                foreach ($new_translations as $phrase => $translation) {
                    Translation::add($phrase);
                }
            }
        });
    }

    private function https_is_off()
    {
        return empty($_SERVER['HTTPS']);
    }


    private function redirect_to_https()
    {

        header('Location: ' . str_replace('http://', 'https://', BASE_URL));
        exit();
    }

}
