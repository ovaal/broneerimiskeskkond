<?php namespace Halo;

/**
 * Class auth authenticates user and permits to check if the user has been logged in
 * Automatically loaded when the controller has $requires_auth property.
 */
class Auth
{

    public $logged_in = FALSE;
    public $is_admin = FALSE;

    function __construct()
    {
        if (isset($_SESSION['user_id'])) {
            $this->logged_in = TRUE;
            $user = get_first("SELECT *
                                       FROM users
                                       WHERE user_id = '{$_SESSION['user_id']}'");

            // Fix page filled with notices after switching from live to dev DB
            // It's because session has a user_id which does not exist in the new DB
            if(empty($user)){
                header('Location: '. BASE_URL . 'logout');
            }

            $this->load_user_data($user);

        }
    }

    /**
     * Verifies if the user is logged in and authenticates if not and POST contains email, else displays the login form
     * @return bool Returns true when the user has been logged in
     */
    function require_auth()
    {
        global $errors;

        // If user has already logged in...
        if ($this->logged_in) {
            return TRUE;
        }

        // Authenticate by POST data
        if (isset($_POST['email'])) {

            $email = addslashes($_POST['email']);
            $user = get_first("SELECT * FROM users WHERE email = '$email' AND deleted = 0");

            if (@$user['is_active'] == 1) {
                if (!$user) {
                    $errors[] = __('Wrong email or password');
                    require 'templates/auth_template.php';
                    exit();
                }
            } else {
                $errors[] = __('User has not been activated');
                require 'templates/auth_template.php';
                exit();
            }

            $password_check = password_verify($_POST['password'], $user['password']);

            if (!$password_check) {
                $errors[] = __('Wrong email or password');
            } else {
                $_SESSION['user_id'] = $user['user_id'];
                if ($user['is_admin']) {
                    $_SESSION['real_user_id'] = $user['user_id'];
                }
                $_SESSION['logged_in_at'] = date('Y-m-d H:i:s');
                $this->load_user_data($user);
                return true;
            }

        }

        // Display the login form
        require 'templates/auth_template.php';

        // Prevent loading the requested controller (not authenticated)
        exit();
    }

    /**
     * Dynamically add all user table fields as object properties to auth object
     * @param $user
     */
    public function load_user_data($user)
    {
        foreach ($user as $user_attr => $value) {
            $this->$user_attr = $value;
        }
        $this->logged_in = TRUE;
    }
}
