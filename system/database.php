<?php
/**
 * Database functions
 * Not included in class to shorten typing effort.
 */

use Broneerimiskeskkond\Convert;
use Broneerimiskeskkond\Exceptions\DatabaseException;
use Broneerimiskeskkond\Log;
use Broneerimiskeskkond\Request;
use Broneerimiskeskkond\Response;

const ID_REGEX = '/(?P<object_type>[a-z_]+_id)\s*=\s*(?P<object_id>\d+)/';
connect_db();
function create_mysqli_connection($hostname, $username, $password)
{
    $socket = null;

    if (strpos($hostname, ':') !== false) {
        list($hostname, $socket) = explode(':', $hostname, 2);
    }

    return ($socket)
        ? new mysqli($hostname, $username, $password, null, null, $socket)
        : new mysqli($hostname, $username, $password);
}

function connect_db()
{
    global $db;

    $db = @create_mysqli_connection(DATABASE_HOSTNAME, DATABASE_USERNAME, DATABASE_PASSWORD);

    if ($connection_error = mysqli_connect_error()) {
        $errors[] = 'There was an error trying to connect to database at ' . DATABASE_HOSTNAME . ':<br><b>' . $connection_error . '</b>';
        require 'templates/error_template.php';
        die();
    }

    mysqli_select_db($db, DATABASE_DATABASE) or error_out('<b>Error:</b><i> ' . mysqli_error($db) . '</i><br>
                This usually means that MySQL does not have a database called <b>' . DATABASE_DATABASE . '</b>.<br><br>
                Create that database and import some structure into it from <b>doc/database.sql</b> file:<br>
                <ol>
                <li>Open database.sql</li>
                <li>Copy all the SQL code</li>
                <li>Go to phpMyAdmin</li>
                <li>Create a database called <b>' . DATABASE_DATABASE . '</b></li>
                <li>Open it and go to <b>SQL</b> tab</li>
                <li>Paste the copied SQL code</li>
                <li>Hit <b>Go</b></li>
                </ol>', 500);

    // Switch to utf8
    if (!$db->set_charset("utf8")) {
        trigger_error(sprintf("Error loading character set utf8: %s\n", $db->error));
        exit();
    }

    // MySQL 5.7 compatibility
    q("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
    q("SET sql_mode=(SELECT REPLACE(@@sql_mode,'STRICT_TRANS_TABLES',''))");
}


function q($sql, &$query_pointer = NULL, $debug = FALSE)
{
    global $db;
    if ($debug) {
        print "<pre>$sql</pre>";
    }
    $query_pointer = mysqli_query($db, $sql) or db_error_out($sql);

    return mysqli_affected_rows($db);
}

function get_one($sql, $debug = FALSE)
{
    global $db;

    $sql = trim($sql);

    if ($debug) { // kui debug on TRUE
        print "<pre>$sql</pre>";
    }
    switch (substr($sql, 0, 6)) {
        case 'SELECT':
            $q = mysqli_query($db, $sql) or db_error_out($sql);
            $result = mysqli_fetch_array($q);
            return empty($result) ? NULL : $result[0];
        default:
            exit('get_one("' . $sql . '") failed because get_one expects SELECT statement.');
    }
}

function get_all($sql)
{
    global $db;
    $q = mysqli_query($db, $sql) or db_error_out($sql);
    while (($result[] = mysqli_fetch_assoc($q)) || array_pop($result)) {
        ;
    }
    return $result;
}

function get_first($sql)
{
    global $db;
    $q = mysqli_query($db, $sql) or db_error_out($sql);
    $first_row = mysqli_fetch_assoc($q);
    return empty($first_row) ? array() : $first_row;
}

/**
 * Returns single dimensional array of values from the first column of the result set.
 * @param $sql
 * @return array
 */
function get_col($sql)
{
    global $db;
    $q = mysqli_query($db, $sql) or db_error_out($sql);
    while ($row = mysqli_fetch_array($q)) {
        $result[] = $row[0];
    }
    return empty($result) ? array() : $result;
}

function db_error_out($sql = null)
{
    global $db;

    if (!empty($_SERVER["SERVER_PROTOCOL"])) {
        header($_SERVER["SERVER_PROTOCOL"] . " 500 Internal server error", true, 500);
    }

    $PREG_DELIMITER = '/';

    $db_error = mysqli_error($db);

    if (strpos($db_error, 'You have an error in SQL syntax') !== FALSE) {
        $db_error = '<b>Syntax error in</b><pre> ' . substr($db_error, 135) . '</pre>';

    }


    $backtrace = debug_backtrace();

    $file = $backtrace[1]['file'];
    $file = str_replace(dirname(__DIR__), '', $file);

    $line = $backtrace[1]['line'];
    $function = isset($backtrace[1]['function']) ? $backtrace[1]['function'] : NULL;
    $args = isset($backtrace[1]['args']) ? $backtrace[1]['args'] : NULL;
    if (!empty($args)) {
        foreach ($args as $arg) {
            if (is_array($arg)) {
                $args2[] = implode(',', $arg);
            } else {
                $args2[] = $arg;
            }
        }
    }

    $args = empty($args2) ? '' : addslashes('"' . implode('", "', $args2) . '"');

    // Fault highlight
    preg_match(
        "/check the manual that corresponds to your MySQL server version for the right syntax to use near '([^']+)'./",
        $db_error,
        $output_array);

    if (!empty($output_array[1])) {
        $fault = $output_array[1];
        $fault_quoted = preg_quote($fault);


        $args = preg_replace($PREG_DELIMITER . "(\w*\s*)$fault_quoted" . $PREG_DELIMITER, "<span class='fault'>\\1$fault</span>", $args);

        $args = stripslashes($args);
    }

    $location = "<b>$file</b><br><b>$line</b>: ";
    if (!empty($function)) {

        $args = str_replace("SELECT", '<br>SELECT', $args);
        $args = str_replace("\n", '<br>', $args);
        $args = str_replace("\t", '&nbsp;', $args);

        $code = "$function(<span style=\" font-family: monospace; ;padding:0; margin:0\">$args</span>)";
        $location .= "<span class=\"line-number-position\">&#x200b;<span class=\"line-number\">$code";
    }

    // Generate stack trace
    $e = new Exception();
    $trace = print_r(preg_replace('/#(\d+) \//', '#$1 ', str_replace(dirname(dirname(__FILE__)), '', $e->getTraceAsString())), 1);
    $trace = nl2br(preg_replace('/(#1.*)\n/', "<b>$1</b>\n", $trace));

    //$sql = SqlFormatter::format($sql);

    $htmlError = '<h1>Database error</h1>' .
        '<p>' . $db_error . '</p>' .
        '<p><h3>Location</h3> ' . $location . '<br>' .
        ($sql ? '<p><h3>SQL</h3> ' . $sql . '<br>' : '') .
        '<p><h3>Stack trace</h3>' . $trace . '</p>';


    send_error_report(new DatabaseException(Convert::toPlainText($htmlError), $line, $file));

    if (Request::isCli() || Request::isAjax()) {
        Response::outputAsPlainText($htmlError);
    } else {
        Response::showHtmlErrorPage($htmlError);
    }
}

/**
 * @param $table string The name of the table to be inserted into.
 * @param $data array Array of data. For example: array('field1' => 'mystring', 'field2' => 3);
 * @return bool|int Returns the ID of the inserted row or FALSE when fails.
 * @throws Exception when parameters are invalid
 */
function insert($table, $data): bool|int
{
    global $db;
    if (!$table || !is_array($data) || empty($data)) {
        throw new Exception("Invalid parameter(s)");
    }
    $values = implode(',', escape($data));
    $sql = "INSERT INTO `{$table}` SET {$values} ON DUPLICATE KEY UPDATE {$values}";
    $q = mysqli_query($db, $sql) or db_error_out($sql);
    $id = mysqli_insert_id($db);
    if ($table !== 'log') {
        Log::log(
            Log::ADD,
            $table,
            $id,
            $data
        );
    }
    return ($id > 0) ? $id : FALSE;
}

/**
 * @return int|string The number of affected rows as an int, or as a string if the number of affected rows is greater than the maximum int value (PHP_INT_MAX)
 * @throws Exception when inserting a log record about the update gets invalid parameters
 */
function update(string $table, array $data, string $where, bool $do_not_log = false): int|string
{
    global $db;
    if (!$table || empty($data)) {
        throw new Exception("Invalid parameter(s)");
    }
    $values = implode(',', escape($data));

    if (isset($where)) {
        $sql = "UPDATE `{$table}` SET {$values} WHERE {$where}";
    } else {
        $sql = "UPDATE `{$table}` SET {$values}";
    }
    mysqli_query($db, $sql) or db_error_out($sql);

    if ($table !== 'log' && !$do_not_log) {
        if (preg_match(ID_REGEX, $where, $matches)) {
            $object_id = str_contains($table, $matches['object_type']) ? $matches['object_id'] : null;
        } else {
            $object_id = null;
        }

        Log::log(
            Log::EDIT,
            $table,
            $object_id,
            $data,
            null,
            $where
        );
    }

    return mysqli_affected_rows($db);
}

function escape(array $data): array
{
    global $db;
    $values = array();


    if (!empty($data)) {
        foreach ($data as $field => $value) {
            if ($value !== null && str_starts_with($value, '!')) {
                $operator = '!=';
                $value = substr($value, 1);
            } else {
                $operator = '=';
            }

            if (!str_contains($field, '(') && !str_contains($field, '.')) {
                $field = "`$field`";
            }
            if ($value === NULL) {
                $values[] = "$field $operator NULL";
            } elseif (is_numeric($value)) {
                $values[] = "$field $operator " . mysqli_real_escape_string($db, $value);
            } elseif (is_array($value) && isset($value['no_escape'])) {
                $values[] = "$field $operator " . mysqli_real_escape_string($db, $value['no_escape']);
            } else {
                $values[] = "$field $operator '" . mysqli_real_escape_string($db, $value) . "'";
            }
        }
    }
    return $values;
}

function delete(string $table, string $where): int|string
{
    global $db;
    if (!$table) {
        throw new Exception("Invalid parameter(s)");
    }

    if (!empty($where)) {
        $deleted_data = get_all("SELECT * FROM $table WHERE $where");
        $sql = "DELETE FROM `{$table}` WHERE {$where}";
    } else {
        $deleted_data = get_all("SELECT * FROM $table");
        $sql = "DELETE FROM `{$table}`";
    }

    mysqli_query($db, $sql) or db_error_out($sql);

    if ($table !== 'log') {
        if (preg_match(ID_REGEX, $where, $matches)) {
            $object_id = $matches['object_id'];
        } else {
            $object_id = null;
        }

        Log::log(
            Log::DELETE,
            $table,
            $object_id,
            $deleted_data,
            null,
            $where
        );
    }

    return mysqli_affected_rows($db);
}