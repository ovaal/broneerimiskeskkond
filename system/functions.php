<?php

use Broneerimiskeskkond\Backtrace;
use Broneerimiskeskkond\Exceptions\InternalException;
use Broneerimiskeskkond\Request;
use Broneerimiskeskkond\Response;
use JetBrains\PhpStorm\NoReturn;
use Sentry\State\Scope;
use function Sentry\captureException;
use function Sentry\captureLastError;
use function Sentry\configureScope;

/**
 * Display a fancy error page and quit.
 * @param $error_msg string
 */
#[NoReturn] function error_out(string $error_msg)
{

    // Delete previous output
    ob_clean();

    // Show error template
    $errors[] = $error_msg;
    require dirname(__FILE__) . '/../templates/error_template.php';
    exit();
}

function get_translation_strings($lang)
{
    global $translations;


    $translations_raw = get_all("SELECT DISTINCT phrase,translation 
                                      FROM translations 
                                      WHERE language='$lang'");

    foreach ($translations_raw as $item) {
        $translations[$item['phrase']] = $item['translation'];

    }
}

/**
 * @param $text
 * @return string
 */
function __($text, $global = true)
{
    global $translations;
    global $translations_used;

    $active_language = $_SESSION['language'];

    // Database phrase field length is 255 due to unique index
    $first_765_bytes_of_text = substr($text, 0, 765);

    // Load translations only the first time (per request)
    if (empty($translations) && $active_language) {
        get_translation_strings($active_language);
    }

    // Safe way to query translation
    $translation = isset($translations[$first_765_bytes_of_text]) ? $translations[$first_765_bytes_of_text] : null;

    // Insert new translation stub into DB when text wasn't empty but a matching translation didn't exist in the DB
    if ($text !== null && $translation == null) {

        global $new_translations;
        $new_translations[$first_765_bytes_of_text] = $text;

        // Set translation to input text when stub didn't exist
        $translation = $text;

    } else {
        if ($translation == '{untranslated}') {

            // Set translation to input text when stub existed but was not yet translated
            $translation = $text;
        }
    }

    // Store this translation to update its last_active at the end of the script
    $translations_used[] = $text;

    return nl2br($translation);

}

function debug($msg)
{
    if (defined('DEBUG') and DEBUG == true) {
        echo "<br>\n";
        $file = debug_backtrace()[0]['file'];
        $line = debug_backtrace()[0]['line'];
        echo "[" . $file . ":" . $line . "] <b>" . $msg . "</b>";
    }
}

/**
 * @throws InternalException
 */
function stop($code, $messageOrException = false)
{
    if ($messageOrException instanceof \Exception) {
        $exception = $messageOrException;
        $message = $exception->getMessage();
    } else {
        $message = $messageOrException;
    }
    $response['status'] = $code;
    if ($code == 500) {
        if (!isset($exception)) {
            list($file, $line) = getCaller();
            $exception = new InternalException(!is_string($message)
                ? json_encode($message, JSON_PRETTY_PRINT)
                : $message, $line, $file);
        }
        if (ENV == ENV_PRODUCTION) {
            send_error_report($exception);
            // Hide actual error in production in case of system errors
            $message = __('There was an error');
        } else {
            throw $exception;
        }
    }


    if ($message) {
        $response['data'] = $message;
    }
    if (Request::isAjax()) {
        Response::outputAsJson($response);
    } else {
        Response::showHtmlErrorPage($message);
    }
}

/**
 * Returns file and line of the calling function
 * @return array
 */
function getCaller(): array
{
    $backtrace = debug_backtrace();
    $file = $backtrace[1]['file'];
    $file = str_replace(dirname(__DIR__), '', $file);
    $line = $backtrace[1]['line'];
    return array($file, $line);
}

function fieldsAreEmpty($array)
{
    foreach ($array as $item) {
        if (empty($item)) {
            return true;
        }
    }

    return false;
}

function send_error_report($exception)
{
    // Get user data from session
    $auth = empty($_SESSION['user_id']) ? null : get_first("select * from users where user_id = $_SESSION[user_id]");

    // Add user data to Sentry
    configureScope(function (Scope $scope) use ($auth): void {

        if (!empty($_SESSION['user_id'])) {
            $scope->setUser([
                'auth' => $auth ?? null,
                'session' => $_SESSION ?? null,
                'email' => $auth['email'],
            ]);
        }
    });
    // Send error data to Sentry
    captureException($exception);

    captureLastError();
}

/**
 * Return an array indexed by the given property of the members of the input array
 * @param $input_array array input array
 * @param $key_name string the name of the property to index the new array by
 */
function reindex_array_by_property($input_array, $key_name)
{

    $output_array = [];

    // Input validation
    if (!is_array($input_array)) {
        throw new \Exception('Invalid input_array');
    }
    if (!is_string($key_name)) {
        throw new \Exception('Invalid key_name');
    }

    // Transform the array
    foreach ($input_array as $item) {
        $output_array[$item[$key_name]] = $item;
    }

    return $output_array;
}

function toJson($array)
{
    return empty($array) ? '[]' : json_encode(array_values($array), JSON_NUMERIC_CHECK);
}

/**
 * Validates that the parameter is of specified type
 * @throws Exception if given parameter does not have a valid value
 */
function validate($var, $type = IS_ID, $must_not_be_empty = false): bool
{
    if (($must_not_be_empty && empty($var)) || ($type == IS_ID && !isValidID($var)
            || $type == IS_ARRAY && !is_array($var)
            || $type == IS_STRING && !is_string($var)
            || $type == IS_DATE && !preg_match('/^\d{4}-\d{2}-\d{2}$/', $var)
            || $type == IS_0OR1 && !($var === 0 || $var === 1))) {
        throw new \Exception('Invalid parameter value');
    }
    return true;
}

function isValidID($id): bool
{
    return !!filter_var($id, FILTER_VALIDATE_INT) && $id > 0;
}

function isIE()
{
    if (empty($_SERVER['HTTP_USER_AGENT'])) {
        return false;
    }
    $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
    return preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false) ? true : false;
}

function array2csv(array $data, string $delimiter = ',', string $enclosure = '"', string $escape_char = "\\"): bool|string
{
    $f = fopen('php://memory', 'r+');
    foreach ($data as $item) {
        foreach ($item as &$i) {
            $i = Backtrace::convertToJsonIfArrayOrObject($i);
        }
        fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
    }
    rewind($f);
    return trim(stream_get_contents($f));
}

function getArgumentList(string $funcName, ?string $className): array
{

    $attribute_names = [];

    if ($className && method_exists($className, $funcName)) {
        $fx = new ReflectionMethod($className, $funcName);
    } elseif (function_exists($funcName)) {
        $fx = new ReflectionFunction($funcName);
    } elseif(in_array($funcName,['require','require_once','include','include_once'])) {
        return ['file' => null];
    } else {
        return $attribute_names;
    }

    foreach ($fx->getParameters() as $param) {

        $attribute_names[$param->name] = NULL;

        if ($param->isOptional()) {

            $attribute_names[$param->name] = $param->getDefaultValue();
        }
    }

    return $attribute_names;
}

function platformSlashes(string $path): string
{
    return str_replace('/', DIRECTORY_SEPARATOR, $path);
}