<style>
    .the-header {
        background: url('assets/img/additional-services-back.jpg') !important;
        background-position: center center !important;
    }

    .notes {
        color: #606060;
        font-weight: 300;
        font-size: 16px;
    }

    .save-additional-services-btn {
        margin-bottom: 20px !important;
    }

    .save-additional-services-btn:disabled {
        background: darkgrey;
        color: lightgrey;
    }

    a:hover {
        text-decoration: none;
    }

    .the-room-view-sidebar {
        padding-top: 34px;
    }

    .the-room-view input[type="time"] {
        padding-top: 10px;
        padding-bottom: 10px;
        -webkit-appearance: none !important;
    }

    .chosen_booking_time {
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 20px;
        -webkit-appearance: none !important;
        display: inline-block;
        width: 100%;
        font-family: 'Lato', Sans-Serif, Arial;
        font-size: 16px;
        color: #555;
        border: 1px solid #bdbdbd;
        outline: none;
    }

    .h2-restyled {
        margin-top: -15px !important;
        font-size: 16px !important;
    }

    .step-number {
        font-weight: bold !important;
        color: #56a534 !important;
        display: inline-block;
        font-size: 24px !important;
    }

    /* Custom checkboxes */
    #checked_checkbox, #unchecked_checkbox {
        top: -1px;
        width: 16px;
        height: 16px;
        display: inline-block;
        margin-right: 10px;
    }

    /* Color the checkboxes gray when their input is disabled */
    input[type=checkbox]:disabled + img.checkbox_image {
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        opacity: 0.5;
    }

    .myorders-steps {
        color: #606060;
        font-size: 16px;
        font-weight: 300;
    }

    .myorders-steps > p {
        margin: 0;
    }

    .extras {
        text-align: left;
        margin-left: -27px;
        margin-right: -27px;
    }

    .selected-room {
        background: #56a534;
        color: white;
        display: block;
        width: 100%;
        height: 50px;
        line-height: 50px;
        text-transform: uppercase;
        cursor: pointer;
        border: 0;
        text-align: center;
        margin-bottom: 0px;
        position: relative;
    }

    .unselected-room {
        display: block;
        width: 100%;
        height: 50px;
        line-height: 50px;
        color: #fff;
        text-transform: uppercase;
        cursor: pointer;
        background: #bebebe;
        border: 0;
        text-align: center;
        margin-top: 5px;
        margin-bottom: 5px;
        position: relative;
    }

    @media (min-width: 992px) {
        span.start-to-end-times-arrow {
            margin-left: 0 !important;
            z-index: 1;
        }
    }

    @media (max-width: 991px) {
        span.start-to-end-times-arrow {
            display: none;
        }
    }

    .word-read {
        margin-bottom: 0;
    }

    .save-additional-services-btn {
        display: block;
        width: calc(100% + 80px);
        height: 50px;
        line-height: 50px;
        color: #fff;
        text-transform: uppercase;
        cursor: pointer;
        background: #e5cf88;
        border: 0;
        margin-top: 40px;
        margin-left: -40px;
        margin-bottom: -40px;
    }

    .service-div {
        padding: 50px;
        min-height: 240px;
        margin-bottom: 20px;
    }

    .service-div div span, .service-div h3 {
        max-width: 500px;
    }

    .service-description {
        margin-top: 20px
    }

    .whitebox label {
        margin: 0;
    }

    .service-group-heading {
        font-size: 1.4em;
        font-weight: bold;
        margin-bottom: 1em;
    }

    .service-group-description {
        margin: 40px;
    }
</style>

<div class="the-header-room-bottom-bar">
    <div class="the-wrap">
        <div class="the-left">
            <a href="#"><?= __('Home'); ?></a>
            - <?= $controller == 'additional_services' ? __('Additional services') : __('My Orders') . ' - #' . $params[0] ?>
        </div>
    </div>
</div>

<div class="the-room-view">
    <div class="the-wrap">
        <div class="the-room-view-content">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12" style="height: 152px;">
                    <div class="notice notice-left"
                         style="/* Center this div in its parent div */
                                top: 50%;
                                left: 50%;
                                transform: translate(-50%,-50%);
                                margin:0;">
                        <?= __('Refreshments can be ordered 3 days before the event. You can first confirm the room booking, and then add the catering (Just log in). No outside food or drinks. Thank you. You can have lunch also in the cafe/pub/restaurant nearby to the studio, e.g. Viktus (in the same building), Pööbel, L\'Ermitage, Kuldmokk, etc. Enjoy!'); ?>

                    </div>
                </div>
            </div>


            <!-- SERVICES -->
            <?php foreach ($service_groups as $service_group_id => $service_group): ?>

                <div class="whitebox additional-service-options service-div">

                    <!-- Image -->
                    <img width="344" height="240"
                         style="margin-left: -50px; margin-top: -50px; margin-right: 50px; margin-bottom: 50px ;float: left"
                         src="uploads/ServiceGroup<?= $service_group['service_group_id'] ?>.jpg" alt="img"/>

                    <!-- Heading-->
                    <div class="service-group-heading"><?= __($service_group['service_group_name']) ?></div>

                    <!-- Services-->
                    <?php foreach ($service_group['services'] as $service): ?>


                        <label class="label-service">

                            <!-- The checkbox -->
                            <input type="<?= $service_group['service_group_multiselect'] ? 'checkbox' : 'radio' ?>"
                                   class="checkboxOrRadio-service"
                                   data-service_id="<?= $service['service_id'] ?>"
                                   name="<?= $service_group['service_group_multiselect'] ? 'multiservice' . 'service' . $service['service_id'] : $service_group['service_group_id'] ?>"
                                <?= empty($order['services'][$service['service_id']]['quantity']) ? '' : 'checked' ?>
                            />

                            <!-- The service name -->
                            <span class=""><?= __($service['service_name']) ?></span>

                        </label>

                        <!-- The service content area -->
                        <div class="service-block" <?= $order['services'][$service['service_id']]['quantity'] ? '' : 'style="display:none"' ?>>

                            <!-- Description -->
                            <?php if ($service['service_description']): ?>
                                <?= __($service['service_description']) ?><br>
                            <?php endif; ?>

                            <!-- Quantity -->
                            <?php if ($service['service_quantity_options']): ?>
                                <div style="display: block">

                                    <select class="dropdown-service-quantity"
                                            name="service[<?= $service['service_id'] ?>][quantity]"
                                            data-service_id="<?= $service['service_id'] ?>">

                                        <?php foreach (explode(',', $service['service_quantity_options']) as $service_quantity_option): ?>
                                            <option value="<?= $service_quantity_option ?>" <?= $order['services'][$service['service_id']]['quantity'] == $service_quantity_option ? 'selected' : '' ?>
                                            ><?= __($service_quantity_option) ?>
                                                <?= $service_quantity_option == 1 ? __($service['service_unit_singular']) : __($service['service_unit_plural']) ?>
                                            </option>
                                        <?php endforeach ?>

                                    </select>

                                </div>
                            <?php endif; ?>
                        </div>


                    <?php endforeach; ?>

                    <!-- Service group description -->
                    <?php if ($service_group['service_group_description']): ?>
                        <div class="service-group-description">
                            <a href="#" class="link-explore-more"><?= __('Explore more') ?></a>
                            <div style="display: none"><br><?= nl2br(__($service_group['service_group_description'])) ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>

        <?php require 'templates/_partials/order_summary.php'; ?>
        <?php require 'views/additional_services/additional_services_index_order_summary_css.php' ?>
        <?php if ($controller == 'additional_services' || $controller == 'myorders'): ?>
            <?php require 'views/additional_services/additional_services_index_order_summary_css.php' ?>
        <?php endif; ?>

        <?php if ($controller == 'payment'): ?> 
            <?php require 'views/additional_services/payment_index_order_summary_css.php' ?>
        <?php endif; ?>

        <script>

            <?php if ($controller == 'additional_services'): ?>
                
            orderSummary.buttons = [
                {
                    text: '<?= __('Summary') ?>',
                    href: 'payment',
                    rightIconClass: 'glyphicon glyphicon-chevron-right',
                    class: 'the-room-booking-next-btn'
                },
                {
                    text: '<?= __('Previous')?>',
                    href: '<?= $last_viewed_room_link?>',
                    class: 'the-room-booking-previous-btn'
                }];

            <?php endif; ?>

        </script>
    </div>
</div>

<script>

</script>
<!-- Controller specific JS -->
<?php $controller === 'additional_services' ? require 'templates/_partials/additional_services_page_js.php' : null ?>
<?php $controller === 'myorders' ? require 'templates/_partials/additional_services_page_myorders_js.php' : null ?>
<?php require 'templates/_partials/additional_services_page_functions_js.php' ?>
<script>

    $('.checkboxOrRadio-service').change(showOrHideService);
    $('.dropdown-service-quantity').change(changeServiceQuantity);
    $('.link-explore-more').click(toggleServiceGroupDescription);
    $('.label-service').click(showServiceDetails);

</script>
