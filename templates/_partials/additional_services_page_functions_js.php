<script>

    // This adds .grep function to all arrays:
    // Example: myArray.grep('id', 3).name
    Array.prototype.grep = function (key, value) {

        var that = this, ret = [];

        this.forEach(function (elem, index) {
            if (elem[key].toString() === value.toString()) {
                ret.push(that[index]);
            }
        });

        // DEBUG:
        if (ret.length > 1) {
            alert('warning: result is not exactly one member');

            console.log(ret);
            console.log(ret.length);
            console.log(key);
            console.log(value);
            console.log('this:');
            console.log(this);

        }

        // Return undefined if nothing matched
        if (ret.length === 0) {
            return undefined;
        }

        return ret.length < 2 ? ret[0] : ret;
    };

    var showOrHideService = function () {

        var service_id = $(this).data('service_id');
        var quantity;

        // Test if the element was a checkbox
        if ($(this).prop('type') === 'checkbox') {

            debug("Checkbox pressed");
            // it was checked, quantity is 1, if unchecked, quantity is 0
            quantity = this.checked ? 1 : 0;
            debug('Quantity is ' + quantity);

        } else if ($(this).prop('type') === 'radio') { // If the element was a radio button

            debug("Radio button pressed");

            // In case of radio buttons, the quantity is always one
            quantity = 1;

            // Get the service group of the selected service
            var service_group_id = orderSummary.availableServices.grep('service_id', service_id).service_group_id;

            // Remove all other services belonging to the same service group from order summary
            for (var i = 0; i < orderSummary.selectedServices.length; i++) {

                var service = orderSummary.selectedServices[i];

                if (service.service_group_id === service_group_id && service.service_id !== service_id) {

                    // Remove this service from order summary
                    orderSummary.setService(service.service_id, 0);

                }
            }
        }

        // Add the service to selected services array
        _changeServiceQuantity(service_id, quantity);

    };

    var _changeServiceQuantity = function (service_id, quantity) {

        // Update server
        ajax('additional_services/save_selection', {
            service_id: service_id,
            service_quantity: quantity,
            order_id: <?= $controller == 'myorders' ? $params[0] : 'null'?>
        }, function (json) {

            orderSummary.setService(service_id, quantity);

        });

    };

    var changeServiceQuantity = function () {
        _changeServiceQuantity($(this).data('service_id'), this.value);
    };

    var toggleServiceGroupDescription = function (e) {
        e.preventDefault();
        $(this).next().slideToggle();
    };

    var showServiceDetails = function (e) {
        var label = $(this);
        var checkboxOrRadio = label.find('input.checkboxOrRadio-service');

        // First close everything
        $('.service-block').slideUp();

        // If the user unchecked
        if (!checkboxOrRadio.prop("checked")) {

            // Set the service dropdown to the first element
            $(this).next('.service-block').find('.dropdown-service-quantity').prop("selectedIndex", 0);

            return;
        }

        // Show the current service's details
        $(this).next('.service-block').slideDown();

    };

</script>