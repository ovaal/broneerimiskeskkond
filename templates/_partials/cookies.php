<?php if (!isset($_COOKIE['cookie_agreement'])): ?>
    <script>
        function setCookie(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 90000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        $(function () {
            $(".cookiesAgreementClose").click(function () {
                setCookie("cookie_agreement", 1);
                $('.cookiesAgreement').remove();
            });
        });

    </script>

    <div class="cookiesAgreement">
        <div class="cookiesAgreementClose"><i class="fa fa-times-circle-o"></i></div>
        <div class="cookiesAgreementWrapper"><?= __('Lehekülg kasutab küpsiseid, et pakkuda Teile parimat kogemust meie lehel.') ?>
            <a href="cookies"><?= __('Rohkem infot küpsiste kohta') ?></a></div>
    </div>
<?php endif; ?>