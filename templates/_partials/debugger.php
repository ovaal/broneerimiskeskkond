<script>

    function setDebug(isDebug) {
        if (isDebug) {
            window.debug = window.console.log.bind(window.console,  '%o');
        } else {
            window.debug = function() {};
        }
    }

    setDebug(<?= defined('DEBUG') ? 'true' : 'false'?>);

</script>