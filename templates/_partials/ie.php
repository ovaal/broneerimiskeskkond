<style>
    .browsers {
        display: inline-block;
    }

    .browsers div {
        text-align: center;
        line-height: 0.5;
    }

    .browsers i {
        font-size: 2.5em;

    }

    .modal.modal-narrow .modal-dialog {
        max-width: 600px;
        text-align: center;

    }

    .modal-narrow .modal-body {
        overflow-y: auto;
        padding: 30px;
    }

    #more-information {
        margin: 20px;
        background-color: #f5f5f5;
        border: 1px solid lightgray;
        padding:10px;
        border-radius: 3px;
    }

    .modal-header h2 {
        margin-top: 20px;
    }

    a[data-toggle="collapse"]{
        cursor: pointer;
    }
</style>
<div class="modal modal-narrow fade" id="modal-ie">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Your web browser is too old :(</h2>
            </div>
            <div class="modal-body">
                <div style="text-align: left">
                    <p><b>Problem</b>: This site is built on modern Javascript (ES2015) but your browser does not
                        support it. Click <a data-toggle="collapse" data-target="#more-information">here</a> for more
                        information.</p>

                    <div id="more-information" class="collapse">
                        <p>In 2015, Microsoft introduced a new browser - Microsoft Edge - and stopped updating Internet Explorer.</p>
                        <p>As a
                            result, IE does not work with technologies introduced after 2015.</p>
                        <p>While it is still installed in
                            Windows 10 for maintaining compatibility with older websites and intranet sites that require
                            ActiveX and other Microsoft legacy web technologies, it should not be used for everyday
                            browsing.</p>
                    </div>


                    <p style="margin-top: 30px"><b>Solution:</b> Use one of the modern browsers: <br></p>
                </div>
                <div class="row browsers" style="margin-top: 20px">
                    <div class="col-xs-3">
                        <img src="assets/img/browsers/chrome.png"><br>
                        <a href="https://www.google.com/chrome/">Chrome</a>

                    </div>
                    <div class="col-xs-3">
                        <img src="assets/img/browsers/firefox.png"><br>
                        <a href="https://getfirefox.com">Firefox</a>

                    </div>
                    <div class="col-xs-3">
                        <img src="assets/img/browsers/edge.png"><br>
                        Edge

                    </div>
                    <div class="col-xs-3">
                        <img src="assets/img/browsers/opera.png"><br>
                        <a href="https://www.opera.com/download">Opera</a>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $('#modal-ie').modal('show');
</script>