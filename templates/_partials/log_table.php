<?php

use Broneerimiskeskkond\Backtrace;
use Broneerimiskeskkond\Log;

?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300;700&display=swap" rel="stylesheet">
<style>

    #log-table td:nth-child(5), #log-table td:nth-child(6) {
        font-family: "Roboto Mono", monospace;
        word-break: normal;
        overflow-wrap: anywhere
    }

    #log-table td:nth-child(6) {
        word-break: normal;
        overflow-wrap: anywhere
    }

    .extras {
        margin-left: 5px;
    }

    #log-table td:nth-child(6) span {
        padding: 1px;

    }

    /* Name */
    #log-table td:nth-child(6) span:nth-of-type(odd) {
        border: 1px solid #cacaca;
        color: #898989;
        margin-right: 3px;
        border-radius: 7px;
        background-color: #e9e9e9;
        margin-left: 50px;
        font-size: 0.7em;
    }


    #log-table td:nth-child(6) span:last-of-type {
        margin-right: 0 !important;
    }

    #log-table .backtrace-call {
        line-height: 1.6
    }

    #log-table td {
        width: 1%;
    }

    #log-table td:nth-child(4) {
        width: 5%;

    }

    #log-table td:nth-child(n+5) {
        width: 35%;

    }

    #log-table td:nth-child(5) {
        width: 35%;

    }
</style>

<table id="log-table" class="table table-hover">
    <thead>
    <tr>
        <th><?= __('Aeg') ?></th>
        <th><?= __('Kasutaja') ?></th>
        <th><?= __('Sündmus') ?></th>
        <th><?= __('Objekti tüüp ja ID') ?></th>
        <th><?= __('Täiendav info') ?></th>
        <th><?= __('Backtrace') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($logs as $log): ?>
        <tr>
            <td><?= date('d-m-Y H:i:s', strtotime($log['logged_at'])) ?></td>
            <td><?= $log['creator_name'] ?></td>
            <td>
                <span class="label label-<?= Log::Labels[$log['log_event_id']] ?>"><?= $log['log_event_name'] ?></span>
            </td>
            <td><?= $log['object_type'] ?>=<?= $log['object_id'] ?></td>
            <td>
                <?= nl2br(Log::makeKeysBold(Log::getChangedFields($log['changed_fields']))) ?>

                <?php if ($log['changed_fields'] && $log['additional_info']): ?>
                    <br>
                    <br>
                <?php endif; ?>

                <?= nl2br(Log::makeKeysBold($log['additional_info'])) ?></td>
            <td>
                <?= Backtrace::asHTML($log['backtrace']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>