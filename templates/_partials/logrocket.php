<?php if (!empty($_SERVER['HTTP_HOST']) && defined('LOGROCKET_INIT') && LOGROCKET_INIT != ''): ?>

    <script src="https://cdn.lr-ingest.io/LogRocket.min.js" crossorigin="anonymous"></script>
    <script>window.LogRocket && window.LogRocket.init('<?=LOGROCKET_INIT?>');</script>

    <?php if ($user_logrocket = empty($_SESSION['user_id']) ? null : (empty($user['email']) ? \Broneerimiskeskkond\User::get() : $user)): ?>
        <script>
            LogRocket.identify('<?=$_SESSION['user_id']?>', {
                name: '<?= addcslashes($user_logrocket['first_and_last_name'], "'")?>',
                email: '<?= addcslashes($user_logrocket['email'], "'")?>',
                phone: '<?= addcslashes($user_logrocket['phone'], "'")?>',
            });
        </script>
    <?php endif ?>

<?php endif ?>