<style>
    .order-summary > div > div:nth-child(1) {
        padding-right: 0 !important;
        text-align: left;
    }

    .order-summary > div > div:nth-child(2) {
        padding-left: 0 !important;
        padding-right: 15px !important;
        text-align: right;
        white-space: nowrap;
    }

    .order-summary .group-title {
        margin-top: 20px;
        font-size: 24px;
        font-weight: 500;
    }

    .order-summary {
        max-width: 361px;
    }

</style>

<div class="order-summary">

    <!-- DATE -->
    <div class="row date">
        <div class="col-xs-6">{{ date }}</div>
        <div class="col-xs-6">{{ start }} → {{ end }}</div>
    </div>

    <!-- TITLE (ROOMS) -->
    <div class="row group-title">
        <div class="col-xs-12">{{ rooms_title }}</div>
    </div>

    <!-- ROOMS -->
    <template v-for="room in rooms">

        <!-- A ROOM -->
        <div class="row">
            <div class="col-xs-10"><i class="fa fa-check-circle-o"></i> {{ room.room_name }} × {{ duration }}h</div>
            <div class="col-xs-2" style="padding-right: 20px;">{{ room.room_price == 0 ? '' : room.room_price * duration
                +
                ' €'}}
            </div>
        </div>
        <div class="row order-summary-extra" v-for="(extra, i) in filterExtras(room.extras)">
            <div class="col-xs-9 col-xs-offset-1">
                <span style="margin-left: -55px" v-if="showOnlySelectedExtras">
                        ✓ {{ extra.name}}
                </span>
                <span v-else>
                        <input type="checkbox" style="margin-left: 10px" v-model="room.extras[i].selected"
                               @change="extraCheckboxChanged(room, extra)"></i> {{ extra.name }}
                    </span>
            </div>
            <div class="col-xs-2">{{ extra.selected ? extra.price + '€': '' }}</div>
        </div>

        <div class="separator" v-if="filterExtras(room.extras).length">&nbsp;</div>

    </template>

    <!-- DISCOUNT -->
    <div class="row" v-if="discountSum != 0">
        <div class="col-xs-10">
            <i class="fa fa-check-circle-o"></i>
            <?= __($order['discount_level'] . ' partner discount') ?>
        </div>
        <div class="col-xs-2">{{ discountSum }} €</div>
    </div>

    <!-- SERVICES -->
    <template v-if="selectedServices.length">

        <!-- TITLE (SERVICE) -->
        <div class="row group-title">
            <div class="col-xs-12"><?= __('Services') ?></div>
        </div>

        <!-- SERVICES -->
        <div class="row" :key="'service-'+service.service_id" v-for="service in selectedServices">
            <div class="col-xs-10">
                <i class="fa fa-check-circle-o"></i> {{ service | formatServiceName }}
            </div>
            <div class="col-xs-2">{{ serviceSum(service) | formatPrice }}</div>
        </div>

    </template>

    <hr>

    <!-- TOTALS -->
    <div class="row">
        <div class="col-xs-8"><?= __('Price') ?></div>
        <div class="col-xs-4">{{ sum | formatPrice }}</div>
    </div>
    <div class="row">
        <div class="col-xs-8"><?= __('VAT') ?></div>
        <div class="col-xs-4">{{ vat | formatPrice }}</div>
    </div>
    <div class="row">
        <div class="col-xs-8"><?= __('Total') ?></div>
        <div class="col-xs-4">{{ total | formatPrice }}</div>
    </div>

    <!-- BUTTONS -->
    <a :href="button.href" v-for="button in buttons">
        <button :class="button.class" :id="button.id">
            {{ button.text }}
            <span v-if="button.rightIconClass" :class="button.rightIconClass"></span>
        </button>
    </a>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.min.js"></script>
<script>
    var orderSummary = new Vue({
        el: '.order-summary',
        filters: {
            'formatPrice': function (value) {
                return value + ' €';
            },
            'formatServiceName': function (s) {

                var quantityString = '';

                // Only specify quantity if it's not 1
                if (s.service_unit_plural.length) {
                    var unit = parseInt(s.quantity) === 1 ? s.service_unit_singular : s.service_unit_plural;
                    quantityString = ' (' + s.quantity + ' ' + unit + ')';
                }
                return s.service_name_in_order_summary + quantityString;
            }
        },
        data: {
            rooms: <?= toJson($order['rooms']) ?>,
            availableServices: <?= toJson($order['available_services']) ?>,
            selectedServices: <?= toJson($order['services']) ?>,
            date: '<?= $order['booking_start_date'] ?>',
            start: '<?= $order['booking_start_time'] ?>',
            end: '<?= $order['booking_end_time'] ?>',
            duration: <?= (float)$order['booking_duration'] ?>,
            discountSum: <?= $order['discount'] ?>,
            showOnlySelectedExtras: <?= $controller == 'payment' ? 'true' : 'false' ?>,
            buttons: []
        },
        methods: {

            serviceSum: function (service) {
                var q = service.quantity ? service.quantity : 1;
                return parseFloat((service.service_price * q).toFixed(13));
            },
            currency: function (num) {
                return Math.round((num + Number.EPSILON) * 100) / 100;
            },
            extraCheckboxChanged: function (room, extra) {
                // Update server
                ajax('additional_services/save_extra_selection', {
                    room_id: room.room_id,
                    extra_id: extra.id,
                    selected: extra.selected,
                    order_id: <?= $controller == 'myorders' ? $params[0] : 'null' ?>
                }, function (json) {

                });
            },
            setService: function (service_id, quantity) {

                service_id = parseInt(service_id);

                var service = this.selectedServices.grep('service_id', service_id);

                // Add service to selected services, if it's not there
                if (typeof service === "undefined" && quantity !== 0) {

                    // Get this service from available services
                    service = this.availableServices.grep('service_id', service_id);

                    // Error check
                    if (typeof service === "undefined") {
                        alert('orderSummary.setService: unable to get the required service (' + service_id + ')');
                        return;
                    }

                    // Add the service to selected services
                    orderSummary.selectedServices.push(service);

                }

                if (quantity) {

                    service.quantity = quantity;

                } else {

                    debug("Removing service " + service_id + "from sidebar");
                    // Remove this service from services array
                    orderSummary.selectedServices = orderSummary.selectedServices.filter(function (service) {
                        return service.service_id !== service_id
                    })
                }

                // Reorder services
                orderSummary.selectedServices.sort(function (a, b) {
                    return a.service_name_in_order_summary > b.service_name_in_order_summary ? 1 : -1
                });

            },
            filterExtras: function (extras) {

                // Sometimes the extras might not exist
                if (typeof extras !== 'object') return [];

                return this.showOnlySelectedExtras ? extras.filter(function (el) {
                    return el.selected;
                }) : extras;
            }
        },

        computed: {
            sum: function () {

                orderSummary = this;

                var servicesSum = this.selectedServices.reduce(function (sum, service) {
                    return sum + orderSummary.serviceSum(service);
                }, 0);


                var roomsSum = this.rooms.reduce(function (sum, room) {

                    var roomSum = room.room_price * orderSummary.duration;
                    var extrasSum = 0;


                    // Add room extras
                    if (typeof room.extras !== 'undefined' && room.extras.length) {

                        var extrasSum = room.extras.reduce(function (extrasSum, extra) {

                            return extrasSum + (extra.selected ? extra.price : 0);

                        }, 0)

                    }

                    return sum + roomSum + extrasSum;

                }, 0);

                return parseFloat((servicesSum + roomsSum + this.discountSum).toFixed(4));
            },
            vat: function () {
                return this.currency(this.sum * <?= VAT_PERCENT ?>)

            },
            total: function () {
                return this.currency(this.sum + this.vat);
            },
            rooms_title: function () {

                var extras_exist = this.rooms.reduce(function (roomsSum, room) {

                    return roomsSum + (typeof room.extras === 'undefined' ? 0 : room.extras.reduce(function (extrasSum, extra) {
                        return extrasSum + extra.price;
                    }, 0));

                }, 0);

                return extras_exist ? '<?= __('Rooms and extras') ?>' : '<?= __('Rooms') ?>';
            }

        }
    });
</script>