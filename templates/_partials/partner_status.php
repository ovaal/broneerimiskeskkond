<div class="other-offers">
    <div class="gold-partner content-col-12">
        <input type="checkbox" class="goldentext">
        <p class="goldentext" style="display: inline">
            <?= __('Gold Partner. If your company has visited us in the last 3 months then you will get a -10% discount') ?>
        </p>
    </div>

    <div class="silver-partner content-col-12">
        <input type="checkbox" disabled>
        <p class="silvertext" style="display: inline">
            <?= __('Silver Partner. If your company has visited us in the last 6 months then you will get a -5% discount') ?>
        </p>
    </div>

    <div class="-partner-discount-offer-countdown content-col-12">
        <p style="display: inline">
            <?php if ($user['user_status'] === '3'): ?>
                <i class="fa fa-clock-o"
                   style="font-size: 19px; margin-right: 9px;"></i>
                <?= __('Your Golden Partner status is valid for') . ' '; ?>
                <span class="timer"></span>&nbsp;<?= ' (' . __('status ends') . ' ' . $user['user_status_ends'] . ')' ?>

            <?php elseif ($user['user_status'] === '2'): ?>
                <i class="fa fa-clock-o"
                   style="font-size: 19px; margin-right: 9px;"></i>
                <?= __('Your Silver Partner status is valid for') . ' '; ?>
                <span class="timer"></span>
                <span class="indent"><?= ' (' . __('status ends') . ' ' . $user['user_status_ends'] . ')' ?></span>

            <?php elseif ($user['user_status'] === '1'): ?>
                <span class="indent"><?= __('In the past few months there has not been any visits.'); ?></span>

            <?php else: ?>
                <span class="indent"><?= __('There has been an error'); ?></span>

            <?php endif; ?>
        </p>
    </div>
</div>