<?php use Broneerimiskeskkond\User;

if (!empty($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] === 'bron.ovaal.ee'): ?>
    <script type="text/javascript">
        window.smartlook || (function (d) {
            var o = smartlook = function () {
                o.api.push(arguments)
            }, h = d.getElementsByTagName('head')[0];
            var c = d.createElement('script');
            o.api = new Array();
            c.async = true;
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.src = 'https://rec.smartlook.com/recorder.js';
            h.appendChild(c);
        })(document);
        smartlook('init', 'a3dbf27c841d0564e57a1ebbea4cd910a479c09e');

        <?php if($user_smartlook = empty($_SESSION['user_id']) ? null : (empty($user['email']) ? User::get() : $user)): ?>

        smartlook('consentForms', false);
        smartlook('consentIP', false);
        smartlook('consentAPI', false);

        smartlook('tag', 'name', '<?=addcslashes($user_smartlook['first_and_last_name'], "'")?>');
        smartlook('tag', 'phone', '<?=addcslashes($user_smartlook['phone'], "'")?>');
        smartlook('tag', 'email', '<?=addcslashes($user_smartlook['email'], "'")?>');
        smartlook('tag', 'session', '<?=session_id()?>');
        <?php endif?>

    </script>
<?php endif ?>