<!DOCTYPE html>
<html>
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <link href="assets/components/adminto/Horizontal/assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="assets/components/adminto/Horizontal/assets/images/favicon.ico">
    <title><?= __('Ovaal admin', 1) ?></title>
    <!--Morris Chart CSS -->
    <!--<link rel="stylesheet" href="assets/components/adminto/Horizontal/assets/plugins/morris/morris.css">-->
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css">
    <link href="assets/components/adminto/Horizontal/assets/css/core.css" rel="stylesheet" type="text/css"/>
    <link href="assets/components/adminto/Horizontal/assets/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="assets/components/adminto/Horizontal/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="assets/components/adminto/Horizontal/assets/css/pages.css" rel="stylesheet" type="text/css"/>
    <link href="assets/components/adminto/Horizontal/assets/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="assets/components/adminto/Horizontal/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// --><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script><![endif]-->
    <!-- jQuery  -->
    <script src="vendor/components/jquery/jquery.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="assets/js/custom.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="assets/js/jquery-ui.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/modernizr.min.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/bootstrap.min.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/detect.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/fastclick.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/jquery.slimscroll.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/jquery.blockUI.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/waves.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/js/wow.min.js"></script>
    <!--<script src="assets/components/adminto/Horizontal/assets/js/jquery.nicescroll.js"></script>-->
    <script src="assets/components/adminto/Horizontal/assets/js/jquery.scrollTo.min.js"></script>
    <?php require 'templates/_partials/smartlook.php' ?>
    <?php require 'templates/_partials/logrocket.php' ?>
</head>
<body>
<style>
    ul.navigation-menu {
        text-align: center;
        font-size: 0.8em;
    }
</style>
<!-- Navigation Bar-->
<header id="topnav">
    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <!-- Users -->
                    <li <?= $action == 'users' ? 'class="active"' : '' ?>>
                        <a href="admin/users"><i class="fa fa-users" aria-hidden="true"></i>
                            <br><span><?= __('Users', 1) ?></span></a>
                    </li>
                    <!-- Rooms -->
                    <li <?= $action == 'rooms' ? 'class="active"' : '' ?>>
                        <a href="admin/rooms"><i class="zmdi zmdi-home"></i><br><span><?= __('Rooms') ?></span></a>
                    </li>
                    <!-- Special Rooms -->
                    <li <?= $action == 'special_rooms' ? 'class="active"' : '' ?>>
                        <a href="admin/special_rooms"><i class="zmdi zmdi-home"></i>
                            <br><span><?= __('Special rooms', 1) ?></span></a>
                    </li>
                    <!-- Orders -->
                    <li <?= $action == 'orders' ? 'class="active"' : '' ?>>
                        <a href="admin/orders"><i class="zmdi zmdi-calendar-check"></i>
                            <br><span><?= __('Orders', 1) ?></span></a>
                    </li>
                    <!-- Offers -->
                    <li <?= $action == 'offers' ? 'class="active"' : '' ?>>
                        <a href="admin/offers"><i class="fa fa-file-pdf-o"></i><br><span><?= __('Offers') ?></span></a>
                    </li>
                    <!-- Info Boxes -->
                    <li <?= $action == 'info_boxes' ? 'class="active"' : '' ?>>
                        <a href="admin/info_boxes"><i class="fa fa-file-text" aria-hidden="true"></i>
                            <br><span><?= __('Info boxes', 1) ?></span></a>
                    </li>
                    <!-- Translations -->
                    <li <?= $action == 'translations' ? 'class="active"' : '' ?>>
                        <a href="admin/translations"><i class="fa fa-globe" aria-hidden="true"></i>
                            <br><span><?= __('Translations', 1) ?></span></a>
                    </li>
                    <!-- Settings -->
                    <li <?= $action == 'settings' ? 'class="active"' : '' ?>>
                        <a href="admin/settings"><i class="fa fa-wrench" aria-hidden="true"></i>
                            <br><span><?= __('Settings') ?></span></a>
                    </li>
                    <!-- Services -->
                    <li <?= $action == 'services' ? 'class="active"' : '' ?>>
                        <a href="admin/services"><i class="fa fa-coffee" aria-hidden="true"></i>
                            <br><span><?= __('Services') ?></span></a>
                    </li>
                    <!-- Logs -->
                    <li <?= $action == 'logs' ? 'class="active"' : '' ?>>
                        <a href="admin/logs"><i class="fa fa-book" aria-hidden="true"></i>
                            <br><span><?= __('Logs') ?></span></a>
                    </li>
                    <!-- Invoices -->
                    <li <?= $action == 'invoices' ? 'class="active"' : '' ?>>
                        <a href="admin/invoices"><i class="fa fa-book" aria-hidden="true"></i>
                            <br><span><?= __('Invoices') ?></span></a>
                    </li>
                    <!-- Invoices by Company -->
                    <li <?= $action == 'invoices_by_company' ? 'class="active"' : '' ?>>
                        <a href="admin/invoices_by_company"><i class="fa fa-book" aria-hidden="true"></i>
                            <br><span><?= __('Invoices by company') ?></span></a>
                    </li>
                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>
</header>

<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="container">
        <!-- Main component for a primary marketing message or call to action -->
        <?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
        <?php @require "views/$controller/{$controller}_$action.php"; ?>

        <!-- End Footer -->
    </div>
    <!-- end container -->
    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle"><i class="zmdi zmdi-close-circle-o"></i></a>
        <h4 class="">Notifications</h4>
        <div class="notification-list nicescroll">
            <ul class="list-group list-no-border user-list">
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="assets/components/adminto/Horizontal/assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">Michael Zenaty</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-info">
                            <i class="zmdi zmdi-account"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Signup</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">5 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-pink">
                            <i class="zmdi zmdi-comment"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Message received</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="assets/components/adminto/Horizontal/assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">James Anderson</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 days ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-warning">
                            <i class="zmdi zmdi-settings"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Settings</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /Right-bar -->
</div>
<?php require 'templates/_partials/error_modal.php' ?>
<!-- KNOB JS --><!--[if IE]>
<script type="text/javascript" src="assets/components/adminto/Horizontal/assets/plugins/jquery-knob/excanvas.js"></script><![endif]-->
<script src="assets/components/adminto/Horizontal/assets/plugins/jquery-knob/jquery.knob.js"></script>
<!--Morris Chart-->
<!--<script src="assets/components/adminto/Horizontal/assets/plugins/morris/morris.min.js"></script>-->
<!--<script src="assets/components/adminto/Horizontal/assets/plugins/raphael/raphael-min.js"></script>-->
<!-- Dashboard init -->
<!--<script src="assets/components/adminto/Horizontal/assets/pages/jquery.dashboard.js"></script>-->
<!-- Sweet Alert js -->
<script src="assets/components/adminto/Horizontal/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/pages/jquery.sweet-alert.init.js"></script>
<!-- App js -->
<script src="assets/components/adminto/Horizontal/assets/js/jquery.core.js"></script>
<script src="assets/components/adminto/Horizontal/assets/js/jquery.app.js"></script>
<?php require 'system/error_translations.php' ?>
</body>
</html>

