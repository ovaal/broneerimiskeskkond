<?php namespace Broneerimiskeskkond; ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title><?= PROJECT_NAME ?></title>

    <!-- Custom styles for this template -->

    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css">
    <link href="assets/css/style.css?v<?= PROJECT_VERSION ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="vendor/components/bootstrap/js/bootstrap.min.js"></script>

    <style>
        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .the-userbox-form textarea {
            width: 100%;
            line-height: 25px;
            font-family: 'Lato', Sans-Serif, Arial;
            font-size: 14px;
            color: #fff;
            border: 2px solid #717c95;
            background: transparent;
            outline: none;
            padding-left: 17px;
            padding-right: 17px;
            margin-bottom: 20px;
        }

        /* Make Select2 boxes match Bootstrap3 heights: */
        .select2-selection__rendered {
            line-height: 44px !important;
        }

        .select2-selection {
            height: 46px !important;
        }

        .select2-selection__arrow {
            height: 46px !important;
        }

        #signup > form > div:nth-child(1) > div > span > span.selection > span {
            color: #fff;
            border: 2px solid #b4b7ba;
            background: transparent !important;
            border-radius: 0;
        }

        #select2-registration-organisation-container {
            color: #fff;
        }

        #select2-registration-organisation-container > span {
            color: #757575 !important;
        }

    </style>


    <?php require 'templates/_partials/smartlook.php' ?>
    <?php require 'templates/_partials/logrocket.php' ?>

</head>
<body class="is-home">

<!-- Cookies Agreement -->
<?php require '_partials/cookies.php' ?>

<div class="the-header">
    <div class="the-navbar">

        <div class="the-wrap">
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="the-navbar-logo">
                            <a href="http://ovaal.ee/"></a>
                        </div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse the-navbar-menu" id="navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="http://ovaal.ee/#kontakt"><?= __('Contact'); ?></a></li>
                            <?= Language::renderLanguageOptions() ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.navbar -->
        </div>
    </div>


</div> <!-- .the-header -->

<div class="the-home-view">

    <div class="the-wrap">

        <div class="content-row">

            <div class="content-col-7">

                <h2><?= __('Ovaal - innovative way'); ?><br><?= __('for productive work'); ?></h2>

                <p>
                    <?= __('Find a suitable meeting and work room for your company.') ?>
                    <br>
                    <?= __('Just register and book a room in seconds.') ?>
                </p>

                <p style="margin-left: -5px">
                    <img src="assets/img/vlinnuke.png" width="20px" height="20px" style="display: inline-block;">
                    <?= __('24/7 in real time.') ?>
                    <img src="assets/img/vlinnuke.png" width="20px" height="20px" style="display: inline-block;">
                    <?= __('Interactive Planning.') ?>
                    <img src="assets/img/vlinnuke.png" width="20px" height="20px" style="display: inline-block;">
                    <?= __('Comfortable review.') ?>
                </p>

                <ul class="rooms">

                    <li class="room-paike">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Põhjasära"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>

                    <li class="room-eeter">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Eeter"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>

                    <li class="room-ohk">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Õhk"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>
                    <li class="room-tuli">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Tuli"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>
                    <li class="room-maa">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Maa"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>
                    <li class="room-vesi">

                        <div class="room-layer">

                            <div class="content-row">

                                <div class="content-col-5">

                                    <div class="room-layer-icon"></div>

                                </div>

                                <div class="content-col-5">

                                    <div class="room-layer-info">
                                        <?= __('Room'); ?>
                                        <span>"Vesi"</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </li>

                </ul>

            </div>

            <div class="content-col-3">

                <div class="the-home-view-userbox">

                    <div class="the-userbox-tabs">

                        <ul>

                            <li id="forgot" style="display: none"><?= __('Forgot Password'); ?></li>

                            <li id="login" class="active"><?= __('Sign In'); ?></li>

                            <li id="signup"><?= __('Sign Up'); ?></li>

                        </ul>

                    </div> <!-- .the-userbox-tabs -->

                    <div class="the-userbox-form" id="signup">

                        <form action="signup/register" method="post">
                            <div class="row">

                                <div class="col-md-10">
                                    <input name="email" required type="email" placeholder="e-mail">
                                    <input name="password" required type="password" placeholder="Parool">
                                    <input type="password" required placeholder="Parool uuesti">

                                    <br><br><br>

                                    <select id="registration-organisation" name="company_id"
                                            class="form-control"></select>

                                    <div class="text-center"
                                         style="color: white; margin-bottom: 50px; margin-top: 15px; display: inline-block">
                                        <input type="checkbox"
                                               style="width: auto; height: auto; margin-left: 5px; margin-bottom: 0"
                                               name="state_institution">
                                        <?= __("State Institution") ?>
                                    </div>
                                </div>

                            </div>

                            <p><?= __('Your information is 100% protected and is used only to successfully process your booking'); ?></p>
                            <input type="submit" value="<?= __('Sign Up'); ?>">


                        </form>

                    </div> <!-- .the-userbox-form #login -->

                    <div class="the-userbox-form" id="login">

                        <form method="post">

                            <?php if (isset($errors)): foreach ($errors as $error): ?>

                                <div class="the-error"><?= $error ?></div>

                            <?php endforeach;
                            endif; ?>

                            <input type="text" id="login-email" name="email" placeholder="<?= __('email') ?>">
                            <input type="password" id="login-pw" name="password" value="<?= __('Password') ?>">
                            <a id="linkForgotPassword" href="#"><?= __('Forgot your password?') ?></a>
                            <input type="submit" id="signin-btn" value="<?= __('Sign In') ?>">

                        </form>

                    </div>

                    <div class="the-userbox-form" id="forgot">

                        <form method="post">

                            <input id="email_for_password_retrieval" required type="email" placeholder="user@gmail.com">

                            <!-- Back to Sign In link -->
                            <a href="#" id="linkBackToLogin">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <?= __('Back to Sign In') ?></a>


                            <input id="btnSendMeMyPassword" type="submit" value="<?= __('Send me my password'); ?>">

                        </form>
                    </div>

                </div>

            </div>

        </div>

    </div> <!-- .the-home-view -->

    <?php require 'system/js_constants.php' ?>

    <script src="assets/js/jquery.form.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="assets/js/custom.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="assets/js/company-organisation-select2.js?v<?= PROJECT_VERSION ?>"></script>


    <script>

        $(document).ready(function () {
            $('.show-privacy-modal').on('click', function () {
                $('#privacy-modal').modal('show');
            });
            $('.show-conditions-modal').on('click', function () {
                $('#conditions-modal').modal('show');
            });
        });

        // Sign up ajax
        $('#signup > form').ajaxForm(function (response) {
            alert(response);
        });

        // Forgot password link
        $('#linkForgotPassword').on('click', function () {

            $('#login').hide();
            $('#forgot').show();

            $('#forgot').trigger('click');

        });

        // Forgot's back link
        $('#linkBackToLogin').on('click', function () {

            $('#forgot').hide();
            $('#login').show();

            $('#login').trigger('click');

        });

        // Send me my password button
        $('#btnSendMeMyPassword').on('click', function () {


            $.post('signup/send_new_password', {email: $('#email_for_password_retrieval').val()}, function (response) {

                var json = tryToParseJSON(response);

                if (json === false) {
                    console.log(response);
                    alert('<?=__("an error occurred")?>');
                    return false;
                }

                alert(json.message);

                if (json.status == 200) {

                    $('#forgot').hide();
                    $('#login').show().trigger('click');

                }

            });

            return false;

        });

    </script>
    <footer>Ovaalstuudio Copyright © 2017 <?= __('All Rights Reserved') ?> | <a href="#"
                                                                                class="show-privacy-modal"><?= __('Privacy policy') ?></a>
    </footer>
    <?php require 'system/error_translations.php' ?>

</body>
</html>

<div id="privacy-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <?= __(get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=2")) ?>
            </div>

            <div class="termsbutton"><a href="#" data-dismiss="modal"><?= __('OK') ?></a></div>
        </div>
    </div>
</div>

<div id="conditions-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" unselectable="on">
                <?= __(get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=3")) ?>
            </div>

            <div class="termsbutton"><a href="#" data-dismiss="modal"><?= __('Accept') ?></a></div>
        </div>
    </div>
</div>