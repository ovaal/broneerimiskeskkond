<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= PROJECT_NAME ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .fault {
            background-color: yellow;
        }
    </style>

    <?php if (!empty($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] === 'bron.ovaal.ee'): ?>
        <?php require 'templates/_partials/smartlook.php'; ?>
        <?php require 'templates/_partials/logrocket.php'; ?>
    <?php endif; ?>
</head>
<body>
<div class="container">
    <br/><br/>
    <?php if (isset($errors)): ?>
        <?php foreach ($errors as $error): ?>
            <?php
            // Determine the error message.
            if (is_object($error) && method_exists($error, 'getMessage')) {
                $errorMessage = $error->getMessage();
            } else {
                $errorMessage = is_array($error) ? json_encode($error, JSON_PRETTY_PRINT) : $error;
            }

            // Only show the stack trace if xdebug is available.
            if (extension_loaded('xdebug')) {
                if (is_object($error) && method_exists($error, 'getTraceAsString')) {
                    $stackTrace = $error->getTraceAsString();
                } elseif (function_exists('xdebug_print_function_stack')) {
                    ob_start();
                    xdebug_print_function_stack();
                    $stackTrace = ob_get_clean();
                } else {
                    $stackTrace = null;
                }
            } else {
                $stackTrace = null;
            }
            ?>

            <?php if (!empty($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] === 'bron.ovaal.ee'): ?>
            <script>
                smartlook('tag', 'error', '<?= addcslashes(preg_replace('/\s+/', '_', $errorMessage), "'") ?>');
            </script>
        <?php endif; ?>

            <div class="alert alert-danger">
                <?= htmlspecialchars($errorMessage) ?>
            </div>

        <?php if ($stackTrace): ?>
            <div class="alert alert-warning">
                <h4>Stack Trace:</h4>
                <!-- Render the raw HTML stack trace (xdebug provides HTML formatting) -->
                <pre><?= $stackTrace ?></pre>
            </div>
        <?php endif; ?>

            <div class="text-center">
                <a href="<?= BASE_URL ?>" onclick="window.history.go(-1); return false;" class="btn btn-success">
                    <span class="glyphicon glyphicon-chevron-left"></span> Go back
                </a>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        Tundmatu viga!
    <?php endif; ?>
</div>
</body>
</html>
