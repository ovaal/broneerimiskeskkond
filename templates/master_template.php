<?php namespace Broneerimiskeskkond; ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title><?= PROJECT_NAME ?></title>

    <!-- Custom styles for this template -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css">
    <link href="assets/css/style.css?v<?= PROJECT_VERSION ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="assets/js/custom.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="assets/js/jquery-ui.js?v<?= PROJECT_VERSION ?>"></script>
    <script src="vendor/components/bootstrap/js/bootstrap.js?v<?= PROJECT_VERSION ?>"></script>

    <!-- Sweet Alert -->
    <link href="assets/components/adminto/Horizontal/assets/plugins/bootstrap-sweetalert/sweet-alert.css"
          rel="stylesheet" type="text/css"/>
    <script src="assets/components/adminto/Horizontal/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
    <script src="assets/components/adminto/Horizontal/assets/pages/jquery.sweet-alert.init.js"></script>
    <?php require 'templates/_partials/smartlook.php' ?>
    <?php require 'templates/_partials/logrocket.php' ?>
    <?php require 'templates/_partials/debugger.php' ?>
</head>
<body>

<style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .master-info-box {
        font-family: 'Lato', Sans-Serif, Arial;
        font-size: 16px;
        color: #606060;
        text-transform: none;
    }

    .room-images {
        display: none;
    }

</style>

<!-- Cookies Agreement -->
<?php require '_partials/cookies.php' ?>

<?php
if (isIE()) {
    require '_partials/ie.php';
}

?>


<div class="the-header header-image"
     style="background:url('<?= BASE_URL ?>assets/img/ovaal-back.jpg') no-repeat;background-size:cover; background-position:top center;">

    <!-- Header image carousel arrows-->
    <span class="nvgt" id="prev"></span>
    <span class="nvgt" id="next"></span>

    <div class="the-navbar">

        <div class="the-wrap">
            <nav class="navbar navbar-default">
                <div class="container" style="padding-left:90px;">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse-1">
                            <span class="sr-only"><?= __('Toggle navigation') ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="the-navbar-logo">
                            <a href="<?= BASE_URL ?>"></a>
                        </div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse the-navbar-menu" id="navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-left">
                            <li><a href="<?= BASE_URL ?>"><?= __('Home'); ?></a></li>
                            <?php if ($auth->is_admin): ?>
                                <li><a href="<?= BASE_URL ?>admin"><?= __('Admin'); ?></a></li>
                                <li><a href="<?= BASE_URL ?>info_boxes"><?= __('Conditions'); ?></a></li>
                            <?php elseif (isset($_SESSION['real_user_id']) && $_SESSION['real_user_id'] != $auth->user_id): ?>
                                <li><a href="<?= BASE_URL ?>admin"><?= __('Switch user back'); ?></a></li>
                            <?php endif; ?>

                            <?= Language::renderLanguageOptions() ?>

                            <?php if (!($controller === 'rooms' && $action === 'view') && !($controller === 'additional_services')): ?>
                                <?php if ($user_has_orders): ?>
                                    <li><a href="<?= BASE_URL ?>myorders"><?= __('My Bookings'); ?></a></li>
                                <?php endif; ?>
                                <li><a><?= __('Contact: tel. 50 50 277'); ?></a></li>

                                <li class="<?= $auth->logged_in == true ? 'the-navbar-user' : 'the-navbar-signin-btn' ?>">
                                    <?= $auth->logged_in == true ?
                                        '<i class="fa fa-user"></i>' . $auth->email . '<a href="logout"><i
                                class="fa fa-times"></i></a>' :
                                        '<a href="' . BASE_URL . 'signin">' . __('Enter') . '</a>' ?>
                                </li>
                            <?php else: ?>

                            <li class="<?= $auth->logged_in == true ? 'the-navbar-user' : 'the-navbar-signin-btn' ?>">
                                <?= $auth->logged_in == true ?
                                    '<a href="logout"><i class="fa fa-times"></i></a>' :
                                    '<a href="' . BASE_URL . 'signin">' . __('Enter') . '</a>' ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.navbar -->

            <?php if ($controller == 'rooms' && $action == 'index'): ?>
                <div class="steps-container">
                    <div class="steps">
                        <span><?= __('Choose time / room') ?></span>
                    </div>
                    <div class="steps">
                        <span><?= __('Extra possibilities / catering') ?></span>
                    </div>
                    <div class="steps">
                        <span><?= __('Get offer / book now') ?></span>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div> <!-- .the-header -->
<!-- Main component for a primary marketing message or call to action -->
<?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
<?php @require "views/$controller/{$controller}_$action.php"; ?>

<script type="text/javascript">

    $(document).ready(function () {
        $('.show-privacy-modal').on('click', function () {
            $('#privacy-modal').modal('show');
            //Prevent from redirecting
            return false;
        });

        $('.show-conditions-modal').on('click', function () {
            $('#conditions-modal').modal('show');
            //Prevent from redirecting
            return false;
        });
    });

    $(function () {
        $.datepicker.setDefaults({
            dateFormat: "yy-mm-dd"
        });
        $("#datepicker").datepicker();
    });



</script>

<footer>Ovaalstuudio Copyright © <?= date('Y') ?>. <?= __('All Rights Reserved') ?>
    | <a href="#" class="show-privacy-modal"><?= __('Privacy policy') ?></a>
</footer>

<div id="privacy-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body master-info-box">
                <?= get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=2") ?>
            </div>

            <div class="termsbutton">
                <a href="#" data-dismiss="modal"><?= __('OK') ?></a>
            </div>
        </div>
    </div>
</div>

<div id="conditions-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content master-info-box">
            <div class="modal-body" unselectable="on">
                <?= get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=3") ?>
            </div>

            <div class="termsbutton">
                <a href="#" data-dismiss="modal"><?= __('Accept') ?></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loading-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title"><?= __('Loading') ?>...</h4>
            </div>
            <div class="modal-body">
                <img src="assets/img/loader.gif" alt="loading" style="margin: auto;">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require 'system/error_translations.php' ?>
<?php require 'templates/_partials/error_modal.php' ?>
</body>
</html>
