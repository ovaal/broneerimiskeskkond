<style>

    body {
        font-family: "Lato", sans-serif;
        font-size: 11px;
    }

    h2 {
        font-family: "Lato-Light", sans-serif;
        font-size: 28px;
    }

    #ovaal {
        width: 35%;
        background-color: #374054;
        color: #fff;
        text-align: center;
        height: 240px;
        padding-top: 25px;
        float: left;
    }

    #ovaal p, #ovaal h2 {
        margin: 0;
    }


    .righttext, .prices {
        text-align: right;
    }

    tr#title td {
        font-size: 11px;
        text-transform: uppercase;

        border-bottom: 1px solid #8a8a8a;
    }

    table {
        border-collapse: collapse;
        line-height: 1.5;
    }

    .prices {
        vertical-align: bottom;
    }

    th, td {
        padding: 0;
    }

    .gray {
        color: #8a8a8a;
        text-transform: uppercase;
        padding-top: 5px;
        padding-bottom: 8px;
        font-family: "AvenirLT-Light", sans-serif;
    }

    .secondary {
        color: #8a8a8a;
    }


    hr {
        color: #929292;
    }

    #tableTitle {
        color: #000;
    }

    table, table p, a {
        font-size: 11px;

    }

    table, table p, #heading, #tableTitle {
        font-family: "AvenirLT-Light", sans-serif;
        font-weight: normal;
    }

    #tableTitle {
        font-size: 15px;
    }

    .prices {
        color: #000;
        font-size: 11px;
    }

    .sectionTitle {
        color: #000;
        font-family: "AvenirLT-Medium", sans-serif;
        /* font-weight: bold;*/
    }

    tr.sectionRow td {
        padding-top: 10px;
    }

    tr.sectionRow-first td {
        padding-top: 10px
    }

    tr#contactRow td {
        font-family: "AvenirLT-Book", sans-serif;
    }


</style>

<h1 id="tableTitle"><?= $title ?>
    (<?= $data['user_name'] ?>, <?= $data['user_organisation'] ?>)</h1>

<img id="pdfImg" src="assets/img/pdf_offer_header.jpg" alt="Rooms">

<table width="100%">

    <tr>
        <td class="col-md-10 lefttext gray"><?= __('Rooms, extras and services') ?></td>
        <td class="col-md-2 righttext gray"><?= __('Price') ?></td>
    </tr>

    <?php if (!empty($data['rooms'])): ?>
        <?php $n = 0; ?>
        <?php foreach ($data['rooms'] as $room): ++$n ?>

            <tr>
                <td class="<?= $room['room_price'] ? '' : 'secondary' ?>">
                    <?= $room['room_price'] ?
                        ($data['special_room_selected'] ? '' : __('Rent for room')) . ' ' . $room['room_name'] .
                        " ($data[booking_start_date], $data[booking_start_time], " . round($data['booking_duration'], 2) . 'h)' : __('Room') . " $room[room_name]" ?>
                </td>

                <td class="prices <?= $room['room_price'] ? '' : 'secondary' ?>">
                    <p><?= $room['room_price'] ? $room['room_price'] * $data['booking_duration'] . ' €' : '' ?></p>
                </td>
            </tr>

            <?php if (!empty($room['extras']) && isset($room['extras'])): ?>
                <?php foreach ($room['extras'] as $extra): ?>
                    <tr>
                        <td class="lefttext">
                            <p>‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎‏‏‎ ‎ <?= __($extra['name']) ?></p>
                        </td>
                        <td class="prices">
                            <p><?= $extra['price'] ?> €</p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>


            <?php if (!empty($room['supplies']) && isset($room['supplies'])): ?>
                <?php foreach ($room['supplies'] as $supply): ?>
                    <tr>
                        <td class="lefttext">
                            <p><?= __($supply['name']) ?> (<?= $supply['amount'] ?>)</p>
                        </td>
                        <td class="prices">
                            <p><?= $supply['price'] ?> €</p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
        <?php endforeach; ?>

    <?php endif; ?>


    <!-- DISCOUNT
     ==============================================================================================-->
    <?php if ($data['discount'] != 0): ?>
        <tr>
            <td>‏‏‎ ‎</td>
        </tr>
        <tr>
            <td class="lefttext"><?= $data['discount_level'] ?> <?= __('partner discount') ?></td>
            <td class="prices"><?= $data['discount'] ?> €</td>
        </tr>
    <?php endif ?>


    <!-- SERVICES
    ==============================================================================================-->
    <?php if (!empty($data['services'])): ?>
        <?php foreach ($data['services'] as $service): ?>
            <tr>
                <td><?= $service['service_name_in_order_summary'] ?></td>
                <td class="prices"><?= $service['service_price'] * $service['quantity'] ?> €</td>
            </tr>
        <?php endforeach ?>
    <?php endif; // services ?>

    <tr class="sectionRow">
        <td class="sectionTitle"><?= __('Total'); ?></td>
        <td class="prices"><?= $data['sum'] ?> €</td>
    </tr>

    <tr>
        <td class="sectionTitle"><?= __('VAT'); ?></td>
        <td class="prices"><?= ($data['sum']) * VAT_PERCENT ?> €</td>
    </tr>

    <tr>
        <td class="sectionTitle"><?= __('Total including VAT'); ?></td>
        <td class="prices"><?= ($data['sum']) * (1 + VAT_PERCENT) ?> €</td>
    </tr>

    <tr>
        <td>‏‏‎ ‎</td>
    </tr>
    <tr>
        <td>‏‏‎ ‎</td>
    </tr>
    <tr>
        <td>‏‏‎ ‎</td>
    </tr>
    <tr>
        <td colspan="2">
            <hr>
        </td>
    </tr>

    <tr id="contactRow">
        <td>
            <?= __('Tel: (+372) 50 50 277') ?><br/>
            <?= __('E-mail:') ?> <a href="mailto:ovaal@ovaal.ee?Subject=Ovaal.ee" target="_top">ovaal@ovaal.ee</a><br/>
            <?= __('Website:') ?> <a href="http://ovaal.ee"> www.ovaal.ee </a>
        </td>
        <td class="righttext">
            <img width="60px" src="assets/img/logo_purple.png" alt="Ovaal.ee">
        </td>
    </tr>
</table>

