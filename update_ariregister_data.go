package main

import (
	"archive/zip"
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/thedevsaddam/gojsonq"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"sort"
	"strings"
	"sync"
	"time"
)

// For holding xml and db companies data
type Company struct {
	ID                 string
	Name               string
	Street             string
	City               string
	PostalCode         string
	RegistryNr         string
	SimplbooksClientID string
	KMKR               string
}

// xmlEttevotjad struct XML
type xmlEttevotjad struct {
	XMLName    xml.Name       `xml:"ettevotjad"`
	Ettevotjad []xmlEttevotja `xml:"ettevotja"`
}

// xmlEttevotja struct XML
type xmlEttevotja struct {
	XMLName         xml.Name   `xml:"ettevotja"`
	Name            string     `xml:"nimi"`
	AriregistriKood string     `xml:"ariregistri_kood"`
	Kmkr_nr         string     `xml:"kmkr_nr"`
	Aadress         xmlAadress `xml:"ettevotja_aadress"`
}

// xmlAadress struct XML
type xmlAadress struct {
	XMLName                   xml.Name `xml:"ettevotja_aadress"`
	AsukohtEttevotjaAadressis string   `xml:"asukoht_ettevotja_aadressis"`
	AsukohaEhakTekstina       string   `xml:"asukoha_ehak_tekstina"`
	IndeksEttevotjaAadressis  string   `xml:"indeks_ettevotja_aadressis"`
}

// Counter variables
var InsertedCompaniesCount = 0
var SimplbooksUpdatedCompaniesCount = 0
var SimplbooksErrorCount = 0
var UpdatedCompaniesCount = 0

// Print Console Messages variables
var QuietMode = flag.Bool("q", false, "QuietMode")
var FoundError = false
var LogMsgesSlice = []string{}

// Read database conf from config.php
var conf = getConfiguration("config.php")

var mapDB = map[string]Company{}
var mapXML = map[string]Company{}
var mapCompaniesToUpdateInSimplbooks = map[string]Company{}

func main() {

	startTime := time.Now()

	var xmlDirectory = ".temp/"
	var zipFileName = "ariregister_xml.zip"

	// Parse command line arguments
	percentageOfMaxConnectionsToUse := flag.Int("percentageOfMaxConnectionsToUse", 75, "How many connections to use out of the maximum the server can support")
	flag.Parse()

	if *QuietMode {
		LogMsgesSlice = append(LogMsgesSlice, "\n------> START TIME: "+startTime.Format("15:04:05 (2006-01-02)")+" <------\n")
	}

	//fmt.Println("QuietMode: ", *QuietMode)

	// Make a wait group for loading data
	var wgLoadData sync.WaitGroup

	// Add 2 for wait group
	wgLoadData.Add(2)

	// Get XML file name
	xmlFileName := GetXMLFileName(xmlDirectory)
	if xmlFileName == "" {
		DownloadFile("http://avaandmed.rik.ee/andmed/ARIREGISTER/"+zipFileName, xmlDirectory+zipFileName)
		Unzip(xmlDirectory+zipFileName, xmlDirectory)
		os.Remove(xmlDirectory + zipFileName)
		AppendOrPrintLogs("REMOVING: " + zipFileName)
		xmlFileName = GetXMLFileName(xmlDirectory)
	}

	// Launch loading XML data
	go LoadXML(&mapXML, xmlDirectory, xmlFileName, &wgLoadData)

	// Init DB
	db := ConnectToDB(*percentageOfMaxConnectionsToUse)

	// Launch loading DB data
	go LoadDB(&mapDB, db, &wgLoadData)

	// Wait until both XML and DB data are loaded
	wgLoadData.Wait()

	DeleteCompanies(&mapDB, &mapXML, db)
	UpdateCompanies(&mapDB, &mapXML, db)
	InsertCompanies(&mapDB, &mapXML, db)
	UpdateSimpleBooks(&mapCompaniesToUpdateInSimplbooks)
	RemoveFile(xmlDirectory + xmlFileName)
	AppendOrPrintLogs("Simplebooks errors: " + fmt.Sprintf("%v", SimplbooksErrorCount))
	AppendOrPrintLogs("Simplebooks companies updated: " + fmt.Sprintf("%v", SimplbooksUpdatedCompaniesCount) + " of " + fmt.Sprintf("%v", len(mapCompaniesToUpdateInSimplbooks)))
	AppendOrPrintfmt("\n<------ Overall Time: " + fmt.Sprintf("%v", time.Since(startTime)) + " ------>")

	// Print logs if error
	if FoundError {
		PrintAppendedLogs(LogMsgesSlice)
	}

}

// AppendOrPrintLogs function
func AppendOrPrintLogs(msg string) {
	if *QuietMode {
		LogMsgesSlice = append(LogMsgesSlice, msg)
	} else {
		log.Println(msg)
	}

}

// AppendOrPrintfmt function
func AppendOrPrintfmt(msg string) {
	if *QuietMode {
		LogMsgesSlice = append(LogMsgesSlice, msg)
	} else {
		fmt.Println(msg)
	}

}

// PrintAppendedLogs function
func PrintAppendedLogs(logmsg []string) {

	for _, value := range logmsg {
		fmt.Println(value)
	}
}

// RemoveFile function
func RemoveFile(ZipFileName string) {

	err := os.Remove(ZipFileName)

	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ RemoveFile function (os.Remove() err) ] <------")
		fmt.Println()
		log.Fatal(err)
	}

	AppendOrPrintLogs("REMOVING: " + ZipFileName)
}

// LoadXML function
func LoadXML(mapXML *map[string]Company, xmlDirectory string, xmlFileName string, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	var ettevotjad xmlEttevotjad

	AppendOrPrintLogs("LoadXML: Starting to load companies from XML")

	xmlReader, _ := os.Open(xmlDirectory + xmlFileName)
	xmlFileContents, _ := ioutil.ReadAll(xmlReader)
	errReader := xmlReader.Close()

	if errReader != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ LoadXML function (errReader) ] <------")
		fmt.Println()
		log.Fatal(errReader)
	}

	err := xml.Unmarshal(xmlFileContents, &ettevotjad)

	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ LoadXML function (xml.Unmarshal() err) ] <------")
		fmt.Println()
		log.Fatal(err)
	}

	xmlLength := len(ettevotjad.Ettevotjad)

	for i := 0; i < xmlLength; i++ {
		(*mapXML)[ettevotjad.Ettevotjad[i].AriregistriKood] = Company{
			Name:       ettevotjad.Ettevotjad[i].Name,
			Street:     ettevotjad.Ettevotjad[i].Aadress.AsukohtEttevotjaAadressis,
			City:       ettevotjad.Ettevotjad[i].Aadress.AsukohaEhakTekstina,
			PostalCode: ettevotjad.Ettevotjad[i].Aadress.IndeksEttevotjaAadressis,
			RegistryNr: ettevotjad.Ettevotjad[i].AriregistriKood,
		}
	}

	AppendOrPrintLogs("LoadXML: DONE. Loaded " + fmt.Sprintf("%v", len(*mapXML)) + " companies")
}

// DeleteCompanies function
func DeleteCompanies(mapDB *map[string]Company, mapXML *map[string]Company, db *sql.DB) {

	AppendOrPrintLogs("Gathering compaines to delete...")

	var slCompanyIDs []string

	for regNr := range *mapDB {
		if len((*mapXML)[regNr].RegistryNr) == 0 {
			slCompanyIDs = append(slCompanyIDs, regNr)
		}
	}

	DeleteCompaniesUnlessItHasOrders(&slCompanyIDs, db)
}

// UpdateCompanies function
func UpdateCompanies(mapDB *map[string]Company, mapXML *map[string]Company, db *sql.DB) {

	AppendOrPrintLogs("UpdateCompanies: Starting to update companies")

	var wgUpdate sync.WaitGroup
	var xmlCompany Company

	for regNr, dbCompany := range *mapDB {

		xmlCompany = (*mapXML)[regNr]
		xmlNameAddressRegNo := xmlCompany.Name + xmlCompany.Street + xmlCompany.RegistryNr

		if dbCompany.Name+dbCompany.Street+dbCompany.RegistryNr !=
			xmlNameAddressRegNo && len(xmlNameAddressRegNo) != 0 {

			wgUpdate.Add(1)

			if len(dbCompany.SimplbooksClientID) != 0 {
				mapCompaniesToUpdateInSimplbooks[regNr] = Company{
					ID:                 dbCompany.ID,
					Name:               xmlCompany.Name,
					Street:             xmlCompany.Street,
					City:               xmlCompany.City,
					PostalCode:         xmlCompany.PostalCode,
					RegistryNr:         regNr,
					SimplbooksClientID: dbCompany.SimplbooksClientID,
					KMKR:               xmlCompany.KMKR,
				}
			}

			go UpdateExistingCompanies(xmlCompany, db, &wgUpdate)
		}
	}

	wgUpdate.Wait()

	AppendOrPrintLogs("Updated companies count: " + fmt.Sprintf("%v", UpdatedCompaniesCount))
}

// printCompany function
func printCompany(company Company) {

	v := reflect.ValueOf(company)
	typeOfS := v.Type()

	for i := 0; i < v.NumField(); i++ {
		AppendOrPrintfmt(fmt.Sprintf("%s: %v", typeOfS.Field(i).Name, v.Field(i).Interface()))
	}
}

// UpdateSimpleBooks function
func UpdateSimpleBooks(UpdatedSimplbooksCompanies *map[string]Company) {

	AppendOrPrintLogs("Updating Simplebooks")

	for _, company := range *UpdatedSimplbooksCompanies {

		// Street name and house number is required by SimpelBooks
		if len(company.Street) == 0 {
			FoundError = true
			AppendOrPrintfmt("\n<------ Found a company without street address ------>\n")
			printCompany(company)
			AppendOrPrintfmt("\n<---------------------------------------------------->\n")
			continue
		}

		formMap := make(map[string]interface{})

		form := url.Values{}
		form.Add("Client[id]", company.SimplbooksClientID)
		form.Add("Client[name]", company.Name)
		form.Add("Client[address_city]", company.City)
		form.Add("Client[address_street]", company.Street)
		form.Add("Client[address_postal_code]", company.PostalCode)
		form.Add("Client[reg_no]", company.RegistryNr)

		formMap["id"] = company.SimplbooksClientID
		formMap["name"] = company.Name
		formMap["address_city"] = company.City
		formMap["address_street"] = company.Street
		formMap["address_postal_code"] = company.PostalCode
		formMap["reg_no"] = company.RegistryNr
		formMap["company_id"] = company.ID

		body := SimplebooksPOSTConnection("clients/update", form)
		CheckBodyForErrors(body, formMap, company.Name, company.RegistryNr, company.SimplbooksClientID)

		time.Sleep(1 * time.Second)

	}

}

// CheckBodyForErrors function
func CheckBodyForErrors(body []byte, formMap map[string]interface{}, CompanyName string, CompanyRegNr string, CompanyId string) {

	bodyMap := MakeBodyMapInterface(body)

	for key, value := range bodyMap {

		if key == "errors" {
			FoundError = true
			AppendOrPrintfmt("\n------------------------------------------------------------")
			AppendOrPrintfmt("ERROR: " + fmt.Sprintf("%v", value))

			SimplbooksErrorCount++

			// CONVERT interface{} TO string
			ValueToString := fmt.Sprintf("%v", value)

			if ValueToString == "[Sisestatud registrikood või isikukood on andmebaasis juba olemas.]" {

				AppendOrPrintfmt("\n<------ UUED ANDMED ------> \n")
				PrintMap(formMap)

				AppendOrPrintfmt("\n<------ SIMPLEBOOKSI ANDMED ------> ")

				company := GetSimplebooksCompanyMap("reg_no", CompanyRegNr)
				company2 := GetSimplebooksCompanyMap("id", CompanyId)

				AppendOrPrintfmt("\n<-- Company with the same reg_no --> \n")
				PrintMap(company)

				AppendOrPrintfmt("\n<-- Company that did not get updated --> \n")
				PrintMap(company2)

			} else if ValueToString == "[Sisestatud ettevõtte või isiku nimi on andmebaasis juba olemas.]" {

				AppendOrPrintfmt("\n<------ UUED ANDMED ------> \n")
				PrintMap(formMap)

				AppendOrPrintfmt("\n<------ SIMPLEBOOKSI ANDMED ------> ")

				company := GetSimplebooksCompanyMap("name", CompanyName)
				company2 := GetSimplebooksCompanyMap("id", CompanyId)

				AppendOrPrintfmt("\n<-- Company with the same name --> \n")
				PrintMap(company)

				AppendOrPrintfmt("\n<-- Company that did not get updated --> \n")
				PrintMap(company2)

			} else if ValueToString == "[Sisestatud ettevõtte või isiku nimi on andmebaasis juba olemas. Sisestatud registrikood või isikukood on andmebaasis juba olemas.]" {

				AppendOrPrintfmt("\n<------ UUED ANDMED ------> \n")
				PrintMap(formMap)

				AppendOrPrintfmt("\n<------ SIMPLEBOOKSI ANDMED ------> ")

				company1 := GetSimplebooksCompanyMap("reg_no", CompanyRegNr)
				company2 := GetSimplebooksCompanyMap("name", CompanyName)
				company3 := GetSimplebooksCompanyMap("id", CompanyId)

				CheckIfCompaniesAreTheSameAndPrint(company1, company2)

				AppendOrPrintfmt("\n<-- Company that did not get updated --> \n")
				PrintMap(company3)

			} else {

				PrintMap(formMap)

				if SimplbooksErrorCount > 10 {
					PrintAppendedLogs(LogMsgesSlice)
					fmt.Println("\n------> [ CheckBodyForErrors function (SimplebooksErrorCount > 10) ] <------")
					fmt.Println()
					panic("Too many errors")
				}
			}

			AppendOrPrintfmt("\n------------------------------------------------------------\n")
		}

		if key == "response" {
			SimplbooksUpdatedCompaniesCount++
		}
	}
}

// PrintMap function
func PrintMap(formMap map[string]interface{}) {

	for key, value := range formMap {
		AppendOrPrintfmt(fmt.Sprintf("%v", key) + ": " + fmt.Sprintf("%v", value))
	}

}

// CheckIfCompaniesAreTheSameAndPrint function
func CheckIfCompaniesAreTheSameAndPrint(MapData1 map[string]interface{}, MapData2 map[string]interface{}) {

	if MapData1["reg_no"] == MapData2["reg_no"] && MapData1["name"] == MapData2["name"] {
		fmt.Println("\n<-- Company with the same reg_no and name -->\n")
		PrintMap(MapData1)
		fmt.Println()
	} else {
		fmt.Println("\n<-- Conflicting Company 1 (same name or reg_no) -->\n")
		PrintMap(MapData1)
		fmt.Println("\n<-- Conflicting Company 2 (same name or reg_no) -->\n")
		PrintMap(MapData2)
		fmt.Println()

	}

}

// GetSimplebooksCompanyMap function
func GetSimplebooksCompanyMap(parameterName string, parameterValue string) map[string]interface{} {

	form := url.Values{}
	form.Add(parameterName, parameterValue)

	GetCompanyFromSimplebooks := SimplebooksPOSTConnection("clients/list", form)

	jsonString := string(GetCompanyFromSimplebooks)

	MapData := FindSimplebooksCompany(jsonString, "Client."+parameterName, parameterValue)

	return MapData
}

// FindSimplebooksCompany from jsonString and return map
func FindSimplebooksCompany(jsonString string, parameterName string, parameterValue string) map[string]interface{} {

	// Find from json string where keys equal
	GetCompanyByRegNrOrName := gojsonq.New().FromString(jsonString).
		From("data").
		Where(parameterName, "=", parameterValue).
		Only("Client.id", "Client.name", "Client.address_city", "Client.address_street", "Client.address_postal_code", "Client.reg_no")

	CompanyToString := fmt.Sprintf("%q", GetCompanyByRegNrOrName)

	nameReplacer := strings.NewReplacer("[map[", "{", "]]", "}", `" "`, `" , "`)
	NewStringOutput := nameReplacer.Replace(CompanyToString)

	CompanyDataMap := MakeBodyMapInterface([]byte(NewStringOutput))

	return CompanyDataMap
}

// MakeBodyMapInterface function
func MakeBodyMapInterface(body []byte) map[string]interface{} {

	bodyMapInterface := make(map[string]interface{})

	err := json.Unmarshal(body, &bodyMapInterface)

	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ MakeBodyMapInterface function (json.Unmarshal() err) ] <------")
		fmt.Println()
		panic(err)
	}

	return bodyMapInterface

}

// SimplebooksPOSTConnection function
func SimplebooksPOSTConnection(endpoint string, form url.Values) []byte {

	payload := strings.NewReader(form.Encode())

	req, _ := http.NewRequest("POST", conf["SIMPLBOOKS_API_URL"]+endpoint, payload)

	req.PostForm = form

	req.Header.Add("X-Simplbooks-Token", conf["SIMPLBOOKS_TOKEN"])
	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	return body
}

// InsertCompanies function
func InsertCompanies(mapDB *map[string]Company, mapXML *map[string]Company, db *sql.DB) {

	AppendOrPrintLogs("Inserting companies")
	// Create new wait group wgInsert
	var wgInsert sync.WaitGroup
	batchCounter := 0
	var valueStrings []string
	var valueArgs []interface{}
	// Loop over XML companies
	for regNr, xmlCompany := range *mapXML {

		// Check if the company is missing from DB
		if _, exists := (*mapDB)[regNr]; !exists {

			valueStrings = append(valueStrings, "(?,?,?,?,?,?)")
			valueArgs = append(valueArgs, xmlCompany.Name)
			valueArgs = append(valueArgs, xmlCompany.RegistryNr)
			valueArgs = append(valueArgs, xmlCompany.KMKR)
			valueArgs = append(valueArgs, xmlCompany.Street)
			valueArgs = append(valueArgs, xmlCompany.City)
			valueArgs = append(valueArgs, xmlCompany.PostalCode)
			InsertedCompaniesCount++
			batchCounter++
			if batchCounter%10000 == 0 {
				// Increment waitgroup counter
				wgInsert.Add(1)

				go Insert10kCompanies(db, valueStrings, valueArgs, &wgInsert)
				valueStrings = nil
				valueArgs = nil
			}

		}

	}
	if valueArgs != nil && valueStrings != nil {
		// Increment waitgroup counter
		wgInsert.Add(1)
		go Insert10kCompanies(db, valueStrings, valueArgs, &wgInsert)
	}
	// Wait until all companies have been inserted
	wgInsert.Wait()

	AppendOrPrintLogs("Inserted: " + fmt.Sprintf("%v", InsertedCompaniesCount))
}

// Insert10kCompanies function
func Insert10kCompanies(db *sql.DB, valueStrings []string, valueArgs []interface{}, wgInsert *sync.WaitGroup) {

	defer wgInsert.Done()

	stmt := fmt.Sprintf("INSERT INTO companies (company_name, company_registry_nr, company_kmkr_nr, company_street, company_city, company_postal_code) VALUES %s", strings.Join(valueStrings, ","))
	_, err := db.Exec(stmt, valueArgs...)

	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ InsertNewCompanies function (db.Exec() err) ] <------")
		fmt.Println()
		log.Print("Insert:", stmt)
		log.Fatal("Insert:", err.Error())
	}
}

// UpdateExistingCompanies function
func UpdateExistingCompanies(xmlCompany Company, db *sql.DB, wgUpdate *sync.WaitGroup) {

	defer wgUpdate.Done()

	_, err := db.Exec(
		"UPDATE companies SET "+
			"company_name = ?, "+
			"company_kmkr_nr = ?, "+
			"company_street = ?, "+
			"company_city = ?, "+
			"company_postal_code = ? "+
			"WHERE company_registry_nr = ?",
		xmlCompany.Name,
		xmlCompany.KMKR,
		xmlCompany.Street,
		xmlCompany.City,
		xmlCompany.PostalCode,
		xmlCompany.RegistryNr)

	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n------> [ UpdateExistingCompanies function (db.Exec() err) ] <------")
		fmt.Println()
		log.Print("Update:xmlCompany.Name=", xmlCompany.Name)
		log.Print("Update:xmlCompany.RegistryNr=", xmlCompany.RegistryNr)
		log.Print("Update:xmlCompany.KMKR=", xmlCompany.KMKR)
		log.Print("Update:xmlCompany.Street=", xmlCompany.Street)
		log.Print("Update:xmlCompany.City=", xmlCompany.City)
		log.Print("Update:xmlCompany.PostalCode=", xmlCompany.PostalCode)
		panic(err.Error())
	}

	UpdatedCompaniesCount++

}

// DeleteCompaniesUnlessItHasOrders function
func DeleteCompaniesUnlessItHasOrders(slCompanyIDs *[]string, db *sql.DB) {

	strCompanyIDs := strings.Join(*slCompanyIDs, ",")

	if len(strCompanyIDs) == 0 {
		return
	}

	result, err := db.Exec(`DELETE companies FROM companies LEFT JOIN orders USING (company_id) WHERE orders.company_id IS NULL AND company_registry_nr IN (` + strCompanyIDs + `)`)

	if err != nil {
		if reflect.TypeOf(err).String() == "*errors.errorString" {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ DeleteCompaniesUnlessItHasOrders function (db.Exec() err) ] <------")
			fmt.Println()

			log.Fatal(err)
		}

		AppendOrPrintLogs(err.Error())
		FoundError = true

	} else {
		count, err2 := result.RowsAffected()

		if err2 != nil {

			AppendOrPrintLogs(err2.Error())
			FoundError = true

		} else {
			AppendOrPrintLogs("Companies deleted: " + fmt.Sprintf("%v", count))
		}
	}

	// Show not deleted companies
	notDeletedCompanies, err1 := db.Query(`SELECT company_name,company_registry_nr FROM companies LEFT JOIN orders USING (company_id) WHERE orders.company_id IS NOT NULL AND company_registry_nr IN (` + strCompanyIDs + `)`)

	if err1 != nil {
		if reflect.TypeOf(err1).String() == "*errors.errorString" {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ DeleteCompaniesUnlessItHasOrders function (notDeletedCompanies db.Query() err1) ] <------")
			fmt.Println()

			log.Fatal(err1)
		}

		AppendOrPrintLogs(err1.Error())
		FoundError = true
	}

	notDeletedCompaniesMap := map[string]string{}

	for notDeletedCompanies.Next() {

		var company Company
		err = notDeletedCompanies.Scan(&company.Name, &company.RegistryNr)

		if err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ DeleteCompaniesUnlessItHasOrders function (notDeletedCompanies.Next() err) ] <------")
			fmt.Println()

			panic(err.Error())
		}

		notDeletedCompaniesMap[company.RegistryNr] = company.Name
	}

	AppendOrPrintLogs("Not deleted companies: " + fmt.Sprintf("%v", len(notDeletedCompaniesMap)))

	// Printing not deleted companies if they exist
	if len(notDeletedCompaniesMap) != 0 {

		i := 1
		AppendOrPrintfmt("\n------> [ Printing Not Deleted Companies ] <------\n")

		for key, value := range notDeletedCompaniesMap {
			AppendOrPrintfmt(fmt.Sprintf("%v)", i) + " Company name: " + value + " [RegNR: " + key + "]")
			i++
		}

		AppendOrPrintfmt("\n-------------------> [ END ] <--------------------\n")
	}
}

// GetXMLFileName from .temp
func GetXMLFileName(path string) string {

	OpenFolder, _ := os.Open(path)
	Files, _ := OpenFolder.Readdir(-1)

	// Sort by date
	sort.Slice(Files, func(i, j int) bool {
		return Files[i].ModTime().Unix() > Files[j].ModTime().Unix()
	})

	for _, file := range Files {
		if file.Mode().IsRegular() {
			if filepath.Ext(file.Name()) == ".xml" {
				return file.Name()
			}
		}
	}

	return ""
}

// DownloadFile from Äriregister
func DownloadFile(url string, location string) {

	AppendOrPrintLogs("---> DOWNLOADING: " + url)

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		PrintAppendedLogs(LogMsgesSlice)
		fmt.Println("\n-------> [ DownloadFile function (http.Get() err) ] <------")
		fmt.Println()
		log.Fatal(err)
	}

	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(location)
	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ DownloadFile function (os.Create() err) ] <------")
		fmt.Println()

		log.Fatal(err)
	}

	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)

	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ DownloadFile function (io.Copy() err) ] <------")
		fmt.Println()

		log.Fatal(err)
	}

}

// ConnectToDB function
func ConnectToDB(percentageOfMaxConnectionsToUse int) *sql.DB {

	var serverSupportedMaxConnections int

	db, err := sql.Open("mysql", conf["DATABASE_USERNAME"]+
		":"+conf["DATABASE_PASSWORD"]+
		"@tcp("+conf["DATABASE_HOSTNAME"]+
		")/"+conf["DATABASE_DATABASE"])

	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ ConnectToDB function (sql.Open() err) ] <------")
		fmt.Println()

		panic(err.Error())
	}

	db.QueryRow("SET sql_mode=(SELECT REPLACE(@@sql_mode,'STRICT_TRANS_TABLES',''))")

	row := db.QueryRow("SELECT IF(@@max_user_connections = 0, @@max_connections, @@max_user_connections)")

	if err := row.Scan(&serverSupportedMaxConnections); err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ ConnectToDB function (row.Scan() err) ] <------")
		fmt.Println()

		log.Fatal(err)
	}

	maxConnections := serverSupportedMaxConnections * percentageOfMaxConnectionsToUse / 100
	db.SetMaxOpenConns(maxConnections)

	AppendOrPrintLogs("Connecting to database ")

	return db
}

// getConfiguration function
func getConfiguration(filename string) map[string]string {

	fileContent, err := ioutil.ReadFile(filename)

	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ getConfiguration function (ioutil.ReadFile() err) ] <------")
		fmt.Println()

		panic(err)
	}

	regex := regexp.MustCompilePOSIX(`^[ \t]*define\(['"]([A-Z_]+)['"],[ \t]*['"]*([^'"]*)['"]*\)`)
	var match = regex.FindAllSubmatch(fileContent, -1)

	conf := map[string]string{}

	for n := range match {
		var parameterName = string(match[n][1])
		var parameterValue = string(match[n][2])
		conf[parameterName] = parameterValue
	}

	return conf
}

// Get companies form db
func LoadDB(mapDB *map[string]Company, db *sql.DB, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	AppendOrPrintLogs("LoadDB: Starting to load companies from DB")

	// Getting companie info
	results, err := db.Query("SELECT company_id,company_name,company_street,company_registry_nr,simplbooks_client_id FROM companies")

	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ LoadDB function (db.Query() err) ] <------")
		fmt.Println()

		panic(err.Error())
	}

	companiesCount := 0

	// Appending to Mbd
	for results.Next() {

		var company Company
		companiesCount++

		// simplebooks_client_id could be NULL and Scan refuses to write NULL to a string field
		var simplbooks_client_id sql.NullString

		err = results.Scan(
			&company.ID,
			&company.Name,
			&company.Street,
			&company.RegistryNr,
			&simplbooks_client_id)

		if err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ LoadDB function (results.Next() err) ] <------")
			fmt.Println()

			panic(err.Error())
		}

		// Assign simplebooks_client_id stringified value to company
		company.SimplbooksClientID = simplbooks_client_id.String

		// Add company data to mapDB
		(*mapDB)[company.RegistryNr] = company
	}

	AppendOrPrintLogs("LoadDB: DONE. Loaded " + fmt.Sprintf("%v", companiesCount) + " companies")
}

// Unzip files
func Unzip(src string, dest string) {

	AppendOrPrintLogs("UNZIPPING: " + src)
	var filenames []string
	r, err := zip.OpenReader(src)

	if err != nil {

		PrintAppendedLogs(LogMsgesSlice)

		fmt.Println("\n------> [ Unzip function (zip.OpenReader() err) ] <------")
		fmt.Println()

		log.Fatal(err)
	}

	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ Unzip function (if !strings.HasPrefix()) ] <------")
			fmt.Println()

			log.Fatalf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			_ = os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ Unzip function (os.MkdirAll() err) ] <------")
			fmt.Println()

			log.Fatal(err)
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ Unzip function (os.OpenFile() err) ] <------")
			fmt.Println()

			log.Fatal(err)
		}

		rc, err := f.Open()

		if err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ Unzip function (f.Open() err) ] <------")
			fmt.Println()

			log.Fatal(err)
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		_ = outFile.Close()
		_ = rc.Close()

		if err != nil {

			PrintAppendedLogs(LogMsgesSlice)

			fmt.Println("\n------> [ Unzip function (io.Copy() err) ] <------")
			fmt.Println()

			log.Fatal(err)
		}
	}

}
