<?php namespace Halo;

use Prewk\XmlStringStreamer;
use SimplBooks\SimplBooks;
use ZipArchive;
use Prewk\XmlStringStreamer\Stream;
use Prewk\XmlStringStreamer\Parser;

dlog('Starting ' . __FILE__);

// Show all errors
error_reporting(E_ALL);

// Crontab in Zone sets current working directory to /data01/virt24050/tmp
chdir(__DIR__);

define('BASE_URL', '');

// Start output buffering
ob_start();

require 'vendor/autoload.php';
require 'system/functions.php';
require 'constants.php';
require 'config.php';
require 'system/database.php';

// Set the timezone to Tallinn (for DB)
date_default_timezone_set('Europe/Tallinn');

// Get all companies from DB
$companies = getCompanies();

// Set time limit for the script
set_time_limit(1200);

// Download new companies' data zip file
$zip_file_name = '.temp/ariregister_xml.zip';
if (!file_exists($zip_file_name)) {
    downloadFileToDisk('http://avaandmed.rik.ee/andmed/ARIREGISTER/ariregister_xml.zip', $zip_file_name);
}

// Unzip it
$xml_file_name = unzipFile($zip_file_name);


$streamer = createStreamer($xml_file_name);


// Iterate through the nodes
$n = 0;
while ($node = $streamer->getNode()) {

    $simpleXmlNode = simplexml_load_string($node);
    $ariregistri_kood = (string)$simpleXmlNode->ariregistri_kood;
    $oldCompany = isset($companies[$ariregistri_kood]) ? $companies[$ariregistri_kood] : null;

    // Log after every 99th company
    $n++;
    if ($n > 9999) {
        dlog($simpleXmlNode->nimi);
        $n = 0;
    }

    $data = [
        'company_name' => $simpleXmlNode->nimi,
        'company_registry_nr' => $simpleXmlNode->ariregistri_kood,
        'company_kmkr_nr' => $simpleXmlNode->kmkr_nr,
        'company_status' => $simpleXmlNode->ettevotja_staatus_tekstina,
        'company_street' => $simpleXmlNode->ettevotja_aadress->asukoht_ettevotja_aadressis,
        'company_city' => $simpleXmlNode->ettevotja_aadress->asukoha_ehak_tekstina,
        'company_postal_code' => $simpleXmlNode->ettevotja_aadress->indeks_ettevotja_aadressis,
        'company_full_address' => $simpleXmlNode->ettevotja_aadress->ads_normaliseeritud_taisaadress,
        'company_ariregister_link' => $simpleXmlNode->teabesysteemi_link
    ];

    // Insert new company
    if (!$oldCompany) {

        continue; // While
    }

    // Detect if something is new
    if ($simpleXmlNode->ariregistri_kood . $simpleXmlNode->nimi . $simpleXmlNode->ettevotja_aadress->ads_normaliseeritud_taisaadress !== $oldCompany["company_registry_nr"] . $oldCompany["company_name"] . $oldCompany["company_full_address"]) {

    // Update local database

        dlog("Updated in local database: $oldCompany[company_name]");

        // Update SimplBooks
        if (!empty($companies[$ariregistri_kood]['simplbooks_client_id'])) {

            SimplBooks::updateClient(
                $data['simplbooks_client_id'],
                $data['company_name'],
                empty($data['company_city']) ? $simplbooks_client->address_city : $data['company_city'],
                empty($data['company_street']) ? $simplbooks_client->address_street : $data['company_street'],
                empty($data['company_postal_code']) ? $simplbooks_client->address_postal_code : $data['company_postal_code'],
                $data['company_registry_nr']
            );

            dlog("Updated in SimplBooks: $simpleXmlNode->nimi");
        }
    }
}

// Remove the files
unlink($zip_file_name);
unlink($xml_file_name);

dlog('Finished ' . __FILE__);
die();

// Log script start
function dlog($msg)
{
    $myfile = fopen("crontab.txt", "a") or die("Unable to write to file!");
    fwrite($myfile, print_r(date('Y-m-d H:i:s') . ' ' . $msg, 1) . "\n");
    fclose($myfile);
}

function getCompanies()
{
    global $db;
    $q = mysqli_query($db, "SELECT company_full_address,company_name,company_registry_nr FROM companies") or db_error_out($sql);
    while (($company = mysqli_fetch_assoc($q))) {
        $companies[$company['company_registry_nr']] = $company;
    }
    return $companies;
}

/**
 * @return string
 */
function downloadFileToDisk($url, $fileName)
{

    dlog("Downloading ");
    $fh = fopen($fileName, 'w');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FILE, $fh);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // this will follow redirects
    curl_exec($ch);
    if ($errno = curl_errno($ch)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
        die();
    }
    curl_close($ch);
    fclose($fh);
    return $fileName;
}

// Unzip the file
/**
 * @param string $zip_file_name
 */
function unzipFile($zip_file_name)
{
    $zipArchive = new ZipArchive();
    if ($zipArchive->open($zip_file_name) === TRUE) {
        dlog("Extracting $zip_file_name");
        $zipArchive->extractTo('.temp');
        $zipArchive->close();
    } else {
        dlog("Failed extracting $zip_file_name");
        die('error');
    }

    // Get the unzipped file name
    foreach (glob(".temp/*.xml") as $filename) {
        $xml_file_name = $filename;
    }

    return $xml_file_name;
}

function createStreamer($xml_file_name)
{
    // Prepare our stream to be read with a 1kb buffer
    $stream = new Stream\File($xml_file_name, 1024);

    // Construct the default parser (StringWalker)
    $parser = new Parser\StringWalker();

    // Create the streamer
    $streamer = new XmlStringStreamer($parser, $stream);

    return $streamer;
}
