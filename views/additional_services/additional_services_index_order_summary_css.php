<style>
    .order-summary {
        display: block;
        float: right;
        background: #fff;
        border: 1px solid #e1e1e1;
        padding: 0 40px 40px 40px;
        font-family: Lato, Sans-Serif, serif;
        max-width: 396px;
    }

    .order-summary i {
        font-size: 28px;
        color: #e5cf88;
        vertical-align: middle;
        margin-right: 10px;
        top: -1px;
    }

    .order-summary .date {
        border: 1px solid rgb(189, 189, 189);
        margin: 30px 0 0 0;
        text-align: center;

        padding: 20px;
        background: #e5cf88;
    }

</style>
