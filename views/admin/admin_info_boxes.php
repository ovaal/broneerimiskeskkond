<div class="row">
    <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Info boxes') ?></h4>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Info box ID') ?></th>
                        <th><?= __('Info box name') ?></th>
                        <th><?= __('Info box text') ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($info_boxes as $info_box): ?>
                        <tr>
                            <td class="info_box_id"><?= $info_box['info_box_id'] ?></td>
                            <td><?= $info_box['info_box_name'] ?></td>
                            <td><?= $info_box['info_box_text'] ?></td>
                            <td>
                                <button id="<?= $info_box['info_box_id'] ?>"
                                        class="delete-info-box btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                            <td>
                                <button id="<?= $info_box['info_box_id'] ?>"
                                        class="info-box-edit-modal btn btn-icon waves-effect waves-light btn-danger m-b-5"
                                        data-toggle="modal">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

</div>
<button class="pull-right btn btn-success waves-effect w-md waves-light m-b-5" data-toggle="modal"
        href="#add-info-box"><?= __('Add info box'); ?></button>

<div class="modal fade" id="add-info-box">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add info box'); ?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" role="form" id="formInfoBoxAdd">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Info box name') ?></label>
                                <div class="col-md-10">
                                    <input name="info_box_name" class="form-control" type="text" id="info_box_name"
                                           placeholder="<?= __('Info box name') ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Info box text') ?></label>
                                <div class="col-md-10">
                                    <textarea name="info_box_text" class="form-control" id="info_box_text"
                                              rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" id="add-info-box"
                    class="btn btn-primary addInfoBox"><?= __('Save changes'); ?></button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<!-- Edit modal -->

<div class="modal fade" id="edit-info-box-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div id="edit-modal-content" class="modal-content">
        </div>
    </div>
</div>
<script src="assets/components/tinymce 2/js/tinymce/tinymce.min.js"></script>

<script>

    var info_box_id;

    function save_info_box(action) {

        var formData = $('#formInfoBox' + action).serialize();

        if (action == 'Edit') {
            formData = formData + '&info_box_id=' + info_box_id;
        }

        $.post("admin/info_box_" + action.toLowerCase(),
            formData
            , function (responseText) {

                if (action == 'Add') {
                    // Parse response
                    try {
                        data = JSON.parse(responseText);
                    }
                    catch (e) {
                        console.log("error: " + e);
                        return;
                    }

                    if (data.result == 'FAIL') {
                        alert('There was an error saving your changes');
                        console.log('data', data);
                        return;
                    }
                    info_box_id = data.info_box_id;
                }
                window.location.href = 'admin/info_boxes';

            }
        );

    }


    $(document).ready(function () {

        $(".addInfoBox").on('click', function () {
            save_info_box('Add');
            window.location.href = 'admin/info_boxes';
        });

        $(document).on('click', ".saveEdit", function () {
            save_info_box('Edit');
        });

        $(".delete-info-box").click(function () {
            var that = this;
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this info box!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                var info_box_id = $(that).attr("id");
                $.post("admin/delete_info_box", {
                        info_box_id: info_box_id
                    }, function (response) {
                        if (response !== "Ok") {
                            console.log(response.result);
                            swal("Problem!", "There was an error saving your changes", "error");
                        } else {
                            swal("Deleted!", "Selected info box has been deleted.", "success");
                            window.location.href = 'admin/info_boxes';
                        }
                    }
                );
            });
        });

        $(window).on('hidden.bs.modal', function () {
            $('textarea').each(function () {
                id = $(this).attr('id');
                tinymce.EditorManager.execCommand('mceRemoveEditor', true, id);

            });
        });

        $(window).on('shown.bs.modal', function () {

            tinymce.init({
                selector: "textarea",
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                font_formats: 'Lato=lato,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
                fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt',
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontselect | fontsizeselect",
                content_css: '<?= BASE_URL?>assets/css/content.css?v<?= PROJECT_VERSION ?>',
                block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3;Header 4=h4',
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                }
            });

        });

        $(".info-box-edit-modal").click(function () {
            info_box_id = $(this).attr("id");
            $.ajax({
                url: 'admin/edit_info_box',
                type: "post",
                data: {info_box_id: info_box_id},
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-info-box-modal').modal('show');

                }
            });
        });

    });

</script>