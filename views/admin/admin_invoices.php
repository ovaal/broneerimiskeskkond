<style>
    .card-box .form-control {
        display: inline;
        width: inherit;
    }

    #pretty-table > tbody > tr > td:first-child, #pretty-table > thead > tr > th:first-child {
        border-left: 1px solid #666 !important;
    }

    #pretty-table > tbody > tr > td:last-child, #pretty-table > thead > tr > th:last-child {
        border-right: 1px solid #666 !important;
    }

    #pretty-table > tbody > tr > td, #pretty-table > thead > tr > th {
        border-top: 1px solid #666 !important;
        border-bottom: 1px solid #666 !important;
    }

    #pretty-table > tbody > tr > td:nth-child(2) {
        padding: 0;
    }

    #pretty-table table > thead {
        background: #eee;
    }

    .add_remark:hover {
        text-decoration: underline;
    }

    .remark {
        color: red;
        font-weight: bold;
    }

    .info-table td, .info-table th {
        border: 1px solid #ddd;
        padding: 2px;

    }

    .info-table {
        width: 100%;
    }

    .info-table th {
        background-color: #eee;

    }

    .remark-link-container {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 5px;
        vertical-align: bottom;
    }

    .table > tbody > tr > td {
        position: relative;
    }
</style>
<div class="card-box">
    <!-- Input for date filter -->
    <h1>Orders and their (possible) invoices</h1>
    <div class="col-md-12">
        <div class="form-group">
            <form id="date-range-selector-form">
                <label>Search orders and associated invoices for all events occurring in this date
                    range:</label><br>
                <select class="form-control" id="month" name="month">
                    <option value="0">Period</option>
                    <?php foreach ($months as $month): ?>
                        <option value="<?= $month ?>"><?= $month ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="text" class="form-control" id="event_start" name="event_start" value="<?= $event_start ?>">
                <input type="text" class="form-control" id="event_end" name="event_end" value="<?= $event_end ?>">


                <!-- Input submit -->
                <button id="btn-filter" type="submit" class="btn btn-primary">Filter</button>
            </form>
        </div>
    </div>
    <?php
    // If error is positive, show error
    if ($error) {
        echo $error;
        return;
    }
    ?>

    <table class="table table-bordered table-condensed" style="background-color:white;" id="maintable">
        <thead>
        <tr>
            <th>Company</th>
            <th>Orders</th>
            <th>Invoices</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row): ?>
            <tr style="background-color: <?= $row['orders_total'] === $row['invoices_total'] ? 'lightGreen' : 'pink' ?>">
                <td><h3><?= htmlspecialchars($row['company_name']) ?></h3><br>
                    <strong>Orders total: </strong><?= $row['orders_total'] ?><br>
                    <strong>Invoices total: </strong><?= $row['invoices_total'] ?><br>
                    <strong>Earliest Order Made: </strong><br><?= $row['earliest_order_made'] ?></td>
                <td>
                    <!-- Loop through orders -->
                    <table id="pretty-table" class="table table-bordered">

                        <tbody>
                        <?php if (!empty($row['orders'])) foreach ($row['orders'] as $order): ?>
                            <tr>
                                <td style="width: 184px">
                                    <span style="white-space: nowrap"><h3
                                                style="margin-top:0; display: inline-block"><span
                                                    class="label label-primary"><?= $order['order_id'] ?></span></h3>&nbsp;<span
                                                class="label label-<?= $order['payment_status_name'] === 'Final invoice sent'
                                                    ? 'success'
                                                    : ($order['payment_status_name'] === 'Deposit invoice sent'
                                                        ? 'warning'
                                                        : 'danger') ?>"><?= $order['payment_status_name'] ?></span></span>
                                    <table class="info-table">
                                        <tr>
                                            <th>Order</th>
                                            <td><?= substr($order['order_made'], 0, 10) ?></td>
                                        </tr>
                                        <tr>
                                            <th>Booking</th>
                                            <td><?= substr($order['booking_start'], 0, 10) ?></td>
                                        </tr>
                                    </table>

                                    <div class="remark remark<?= $order['order_id'] ?>">
                                        <?= $order['order_remarks'] ?>
                                    </div>
                                    <div class="remark-link-container">
                                        <a href="#" class="add_remark" data-id="<?= $order['order_id'] ?>">
                                            Comment</a>
                                    </div>
                                </td>
                                <td>
                                    <!-- Order rooms -->
                                    <?php if (!empty($order['rooms'])): ?>

                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th colspan="2"></th>
                                                <th style="text-align: right">Price</th>
                                                <th style="text-align: right">Amount</th>
                                                <th style="text-align: right">Sum</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($order['rooms'] as $orderRoom): ?>
                                                <tr>
                                                    <td colspan="2">
                                                        <?= "<b>$orderRoom[room_name]</b><br>$orderRoom[booking_start_date] $orderRoom[booking_start_time] - $orderRoom[booking_end_date] $orderRoom[booking_end_time]" ?>
                                                    </td>
                                                    <td style="text-align: right"><?= $orderRoom['room_price'] ?></td>
                                                    <td style="text-align: right"><?= round($orderRoom['booking_duration']) ?></td>
                                                    <td style="text-align: right"><?= $orderRoom['booking_duration'] * $orderRoom['room_price'] ?></td>
                                                </tr>

                                                <?php foreach ($order['services'] as $service): ?>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td><?= $service['service_name_in_order_summary'] ?></td>
                                                        <td style="text-align: right"><?= $service['service_price'] ?></td>
                                                        <td style="text-align: right"><?= $service['quantity'] ?></td>
                                                        <td style="text-align: right"><?= $service['quantity'] * $service['service_price'] ?></td>

                                                    </tr>
                                                <?php endforeach; ?>
                                                <?php foreach ($orderRoom['extras'] as $extra): ?>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3"><?= $extra['name'] ?></td>
                                                        <td style="text-align: right"><?= $extra['price'] ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                <?php if ($order['discount']): ?>
                                                    <tr>
                                                        <td colspan="4">Discount</td>
                                                        <td style="text-align: right"><?= $order['discount'] ?></td>
                                                    </tr>
                                                <?php endif ?>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td colspan="4">Total</td>
                                                <td style="text-align: right"><?= $order['sum'] ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>

                <td>
                    <?php if (!empty($row['invoices'])): ?>
                        <table id="pretty-table" class="table table-striped table-bordered">
                            <tbody>
                            <?php foreach ($row['invoices'] as $invoice): ?>
                                <tr>
                                    <td>
                                        <h3><?= $invoice['number'] ?></h3>
                                        <table class="info-table">
                                            <tr>
                                                <th>Info</th>
                                                <td><?= preg_replace('/(ID)(\d+)/', '$1<span class="label label-primary">$2</span>', $invoice['additional_info']) ?></td>
                                            </tr>
                                            <tr>
                                                <th>Created</th>
                                                <td><?= substr($invoice['created_time'], 0, 16) ?></td>
                                            </tr>
                                            <?php if ($invoice['modified_time']): ?>
                                                <tr>
                                                    <th>Modified</th>
                                                    <td style="color: red; font-weight: bold"><?= substr($invoice['modified_time'], 0, 16); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th style="text-align: right">Price</th>
                                                <th style="text-align: right">Amount</th>
                                                <th style="text-align: right">Sum</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($invoice['rows'] as $invoiceRow): ?>
                                                <tr>
                                                    <td><?= preg_replace('/(\d+%)/', '<span class="label label-success">$1</span>', $invoiceRow['name']) ?></td>
                                                    <td style="text-align: right"><?= $invoiceRow['price_per_unit'] ?></td>
                                                    <td style="text-align: right"><?= $invoiceRow['amount'] ?></td>
                                                    <td style="text-align: right"><?= $invoiceRow['sum'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td colspan="3">Total</td>
                                                <td style="text-align: right"><?= $invoice['sum'] ?></td>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <label>Simplbooks API call count:</label> <?= $simplbooks_api_call_count ?>
</div>

<script>
    function pad(number) {
        return number < 10 ? '0' + number : number;
    }

    function formatDate(date) {
        return `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
    }

    // Add event listener to month select
    $('#month').on('change', function () {

        // Get the selected month
        var [year, month] = $(this).val().split('-');

        // Get the first day of the selected month considering timezone
        var firstDay = new Date(year, month - 1, 1);

        // Get the last day of the selected month considering timezone
        var lastDay = new Date(year, month, 0);

        // Fill event_start and event_end with the first and last day of the selected month considering timezone
        $('#event_start').val(formatDate(firstDay));
        $('#event_end').val(formatDate(lastDay));
        $('#date-range-selector-form').submit();

    });
    // add event listener to all remark buttons
    $('.add_remark').on('click', function (e) {
        e.preventDefault()
        var order_id = $(this).data('id');
        var order_remarks = prompt('Add, edit or delete a comment for order ' + order_id + ':');
        ajax("admin/add_remark_to_order", {
                order_id: order_id,
                order_remarks: order_remarks
            },
            function (data) {
                if (data.status === 200) {
                    $('.remark' + order_id).html('<br>' + order_remarks);
                }
            });
    });


</script>