<style>
    table {
        width: 100%;
        border-collapse: collapse;
        background-color: white;
    }

    th, td {
        padding: 10px;
        border: 1px solid black;
        vertical-align: top;
    }

    th {
        background-color: #cccccc;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    tr:hover {
        background-color: #f5f5f5;
    }

    #orders-vs-invoices-table > tbody > tr > td:last-child:not(:first-child) {
        border-left: 2px solid #f5f5f5;
    }

    .nowrap {
        white-space: nowrap;
    }

    .wrap {
        white-space: wrap;
    }

    h1, h2 {
        font-weight: bold;
    }

    h1 {
        margin-bottom: 50px;
    }

    /* Align center <TD>s which have colspan=2 */
    #orders-vs-invoices-table > tbody > tr > td[colspan="2"] {
        text-align: center;
    }
</style>
<h1>Tellimused vs arved</h1>
<p>Näitab nende klientide tellimusi ja arveid, kellel vähemalt ühe tellimuse rent pole täielikult ära arveldatud.</p>
<table border="1" id="orders-vs-invoices-table">
    <thead>
    <tr>
        <th><h2>Tellimused</h2></th>
        <th><h2>Arved</h2></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orders_by_company as $company_id => $orders): ?>
        <tr>
            <td colspan="2"><h2><strong><?= $companies[$company_id]['company_name']; ?>
                        (<?= $companies[$company_id]['company_id']; ?>)</strong></h2></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <th>Tellimus</th>
                        <th>Makse staatus<br>Tagatisarve & %</th>
                        <th>Koordinaator & E-kirjad</th>
                        <th>Soodustus</th>
                        <th>Lisainfo & Märkused</th>
                        <th>Genereeri ja saada lõplik arve</th>
                    </tr>
                    <?php foreach ($orders as $order): ?>
                        <tr>
                            <td>
                                <span class="badge badge-info"><?= $order['order_id']; ?></span><br><?= $order['order_made']; ?>
                            </td>
                            <td>
                                <span class="badge badge-<?= strtoupper($order['payment_status_name']) == 'FINAL INVOICE SENT' ? 'success' : 'danger' ?>"><?= $order['payment_status_name']; ?></span><br><?= $order['deposit_invoice_number']; ?>
                                (<?= $order['deposit_invoice_id']; ?>) <span
                                        class="badge badge-<?= $order['order_billed_rent_percentage'] == 100 ? 'success' : 'danger' ?>">Makstud: <?= $order['order_billed_rent_percentage']; ?>%</span>
                            </td>
                            <td class="wrap"><?= $order['coordinator_name']; ?> (<?= $order['email']; ?>
                                , <?= $order['invoice_email']; ?>)
                            </td>
                            <td><?= $order['discount_percent']; ?>
                                (<?= $order['discount_qualifying_order_id']; ?>)
                            </td>
                            <td><?= !empty($order['invoice_additional_info']) ? $order['invoice_additional_info'] : ''; ?> <?= $order['order_remarks']; ?></td>
                            <td>
                                <?php if ($order['order_billed_rent_percentage'] < 100): ?>
                                    <button title="Genereeri ja saada lõplik arve"
                                            onclick="generateAndSendFinalInvoice(<?= $order['order_id']; ?>)">Genereeri ja saada lõplik arve
                                    </button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <th>Arve</th>
                        <th>Kuupäev<br>Saadetud</th>
                        <th>Summa</th>
                        <th>Krediidi arve</th>
                    </tr>
                    <?php foreach ($invoices_by_company[$company_id] as $invoice): ?>
                        <tr>
                            <td class="nowrap">
                                <?= $invoice['number']; ?> (<?= $invoice['id']; ?>)
                            </td>
                            <td class="nowrap">
                                <?= $invoice['transaction_date']; ?><br>
                                <span style="<?php if ($invoice['transaction_date'] != $invoice['sent']) echo 'color: red'; ?>">
                    <?= $invoice['sent']; ?>
                </span></td>
                            <td style="text-align: right; white-space: nowrap"><?= $invoice['sum']; ?>
                                + <?= $invoice['vat']; ?>
                                =
                                <strong><?= $invoice['total_sum'] ?></strong>
                            </td>
                            <td>
                                <?php if (!empty($invoice['credit_invoice_for'])): ?>

                                    <?= $invoices[$invoice['credit_invoice_for']]['number']; ?>
                                    (<?= $invoice['credit_invoice_for']; ?>)
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script>
    function generateAndSendFinalInvoice(orderId) {
        if (confirm('Kas soovite kindlasti lõpliku arve genereerida ja kliendile saata?')) {
            fetch('<?= BASE_URL; ?>admin/ajax_generate_and_send_final_invoice', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'order_id=' + orderId
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        alert('Lõplik arve on edukalt genereeritud ja saadetud.');
                    } else {
                        alert('Arve genereerimine ebaõnnestus.');
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    alert('Tekkis tõrge arve genereerimisel.');
                });
        }
    }
</script>
