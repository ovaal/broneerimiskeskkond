<?php

use Broneerimiskeskkond\Backtrace;
use Broneerimiskeskkond\Log;

?>

<!-- DataTables -->
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/fixedHeader.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="table-responsive">
                <?php require 'templates/_partials/log_table.php';?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#datatable').dataTable();
    });
</script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>