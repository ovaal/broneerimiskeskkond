<div class="row">

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Offers') ?></h4>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Offer requested by') ?></th>
                        <th><?= __('Offer requested at') ?></th>
                        <th><?= __('Offer PDF') ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($offers as $offer): ?>
                        <tr>
                            <td><?= $offer['first_and_last_name'] ?></td>
                            <td><?= $offer['offer_requested_at'] ?></td>
                            <td>
                                <a class="btn btn-success" href="uploads/offers/<?= $offer['offer_id'] ?>.pdf" download>
                                    <i class="fa fa-file-pdf-o"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>