<?php

?>
<style>
    .extras {
        margin-left: 5px;
    }
</style>
<!-- DataTables -->
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/fixedHeader.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>


<div class="row">
    <!-- end col -->

    <div class="col-lg-12">

        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Bookings for order') . ' ' . $this->params[0] ?>
                <span
                        class="pull-right"><?= __('To edit a booking, click on a cell and insert a value. To save the value, click on any other cells.') ?></span>
            </h4>

            <div class="table-responsive">
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Booking ID') ?></th>
                        <th><?= __('Room ID') ?></th>
                        <th><?= __('Room Name') ?></th>
                        <th><?= __('Booking Start') ?></th>
                        <th><?= __('Booking End') ?></th>
                        <th><?= __('Delete Booking') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($bookings as $booking): ?>
                        <tr>
                            <td><?= $booking['booking_id'] ?></td>
                            <td class="room_id"><span class="loadNum"
                                                      data-id="<?= $booking['booking_id'] ?>"><?= $booking['room_id'] ?></span>
                            </td>
                            <td><?= $booking['room_name'] ?></td>
                            <td class="booking_start"><span class="loadNum"
                                                            data-id="<?= $booking['booking_id'] ?>"><?= date("d-m-Y H:i", strtotime($booking['booking_start'])) ?></span>
                            </td>
                            <td class="booking_end"><span class="loadNum"
                                                          data-id="<?= $booking['booking_id'] ?>"><?= date("d-m-Y H:i", strtotime($booking['booking_end'])) ?></span>
                            </td>
                            <td>
                                <button id="<?= $booking['booking_id'] ?>"
                                        class="delete-booking btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

    <!-- logs -->

    <div class="col-lg-12">

        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Logs for order') . ' ' . $this->params[0] ?></h4>

            <div class="table-responsive">
                <?php require 'templates/_partials/log_table.php'; ?>
            </div>
        </div>
    </div>

    <!-- end logs -->

    <a href="admin/orders">
        <button class="edit-extra-modal btn btn-primary pull-left"><?= __('Back to orders'); ?></button>
    </a>

</div>

<script>
    $(document).ready(function () {
        $('#datatable').dataTable();
    });


    // Database update
    var switchToInput = function () {
        var that = $(this).text();

        var $input = $("<input>", {
            val: that,
            type: "text",
            placeholder: 'test'
        });
        $input.addClass("loadNum");
        $input.data('id', $(this).data('id'));
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };

    var switchToSpan = function () {

        $.post('admin/edit_booking', {
                booking_id: $(this).data('id'),
                key: $(this).parent().attr('class'),
                value: $(this).val()
            }, function (response) {
                if (response !== "Ok") {
                    console.log(response.result);
                } else {
                    //swal("Edited!", "Selected booking has been edited.", "success");
                    //window.location.href = 'admin/bookings';
                }
            }
        );

        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $span.data('id', $(this).data('id'));
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
    };

    $(".loadNum").on("click", switchToInput);

    $(".delete-booking").click(function () {
        var that = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this booking!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            var id = $(that).attr("id");
            $.post("admin/delete_booking", {
                    booking_id: id
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                        swal("Problem!", "There was an error saving your changes", "error");
                    } else {
                        swal("Deleted!", "Selected booking has been deleted.", "success");
                        window.location.href = '<?= BASE_URL . 'admin/order_view/' . $this->params[0] ?>';
                    }
                }
            );
        });
    });

</script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>