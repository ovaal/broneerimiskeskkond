<?php

use Broneerimiskeskkond\PaymentStatus;

?>
<style>
    span[contenteditable] {
        display: inline-block;
    }

    span[contenteditable]:empty::before {
        content: '<?= __('Edit order') ?>';
        display: inline-block;
        color: lightgrey;
    }

    .pending-order {
        color: black;
    }
</style>

<!-- DataTables -->
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/fixedHeader.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>

<div id='calendar' style="width: 800px;"></div>
<br>

<div class="row">
    <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Orders') ?>
                <span
                    class="pull-right"><?= __('To edit a order, click on a cell and insert a value. To save the value, click on any other cells.') ?></span>
            </h4>
            <div class="table-responsive">
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Order ID') ?></th>
                        <th><?= __('Order created') ?></th>
                        <th><?= __('Booking(s) date') ?></th>
                        <th><?= __('Rooms') ?></th>
                        <th><?= __('Total people') ?></th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('Client') ?></th>
                        <th><?= __('Coordinator name') ?></th>
                        <th><?= __('Additional info') ?></th>
                        <th><?= __('Summary PDF') ?></th>
                        <th><?= __('Bookings for this order') ?></th>
                        <th><?= __('View/Edit catering') ?></th>
                        <th><?= __('Send reminder') ?></th>
                        <th><?= __('SimplBooks') ?></th>
                        <th><?= __("Don't send 75%") ?></th>
                        <th><?= __('Add invoice row') ?></th>
                        <th><?= __('Delete Order') ?></th>
                        <th><?= __('Order Billed Rent %') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order): ?>
                        <tr class="<?= $order['booking_start'] >= date('Y-m-d H:i:s') ? 'pending-order' : 'historical-order' ?>"
                            data-order_id="<?= $order['order_id'] ?>">
                            <td><?= $order['order_id'] ?></td>

                            <td><?= substr($order['order_made'], 0, -3) ?></td>

                            <td><?= substr($order['booking_start'], 0, -8) ?></td>

                            <td>
                                <span class="lable" title="<?php foreach ($order['rooms'] as $room_name) {
                                    echo $room_name['room_name'] . ' ';
                                } ?>">
                                <?php
                                $resultstr = array();
                                foreach ($order['rooms'] as $room_name) {
                                    $resultstr[] = $room_name['room_name'][0];
                                }
                                echo implode(", ", $resultstr);
                                ?>
                                </span>
                            </td>

                            <td>
                                <?= $order['total_number_of_people'] ?>
                            </td>

                            <td class="payment_status_id">
                                <?php if ($order['payment_status_id'] !== NULL): ?>

                                    <select data-id="<?= $order['order_id'] ?>"
                                            title="<?= $statuses[$order['payment_status_id']]['payment_status_info'] ?>"
                                            class="selectStatus">

                                        <option value="0" selected>--</option>

                                        <?php foreach ($statuses as $status): ?>
                                            <option data-id="<?= $order['order_id'] ?>"
                                                    value="<?= $status['payment_status_id'] ?>"
                                                    title="<?= $status['payment_status_info'] ?>"
                                                <?= ($order['payment_status_name'] == $status['payment_status_name'])
                                                    ? 'selected' : '' ?>>
                                                <?= $status['payment_status_name'] ?></option>
                                        <?php endforeach; ?>

                                    </select>

                                <?php else: ?>
                                    <p><?= _('Payment doesn\'t exist!') ?></p>
                                <?php endif; ?>
                            </td>

                            <td data-container="body" data-toggle="tooltip" title="<?= $order['user_id'] ?>">
                                <?= $order['first_and_last_name'] ?> <?= empty($order['organisation']) ? '' : '(' . $order['organisation'] . ')' ?>
                            </td>

                            <td class="coordinator_name"><span class="loadNum"
                                ><?= $order['coordinator_name'] ?></span>
                            </td>

                            <td class="invoice_additional_info">
                                <span class="loadNum"><?= $order['invoice_additional_info'] ?></span>
                            </td>

                            <td>
                                <a title="View order as PDF" class="btn btn-success"
                                   href="pdf/order_summary/<?= $order['order_id'] ?>" download>
                                    <i class="fa fa-file-pdf-o"></i>
                                </a>
                            </td>

                            <td>
                                <a href="<?= BASE_URL . 'admin/order_view/' . $order['order_id'] ?>">
                                    <button class="btn btn-icon waves-effect waves-light btn-info m-b-5">
                                        <?= __('View bookings') ?>
                                    </button>
                                </a>
                            </td>

                            <td>
                                <a href="<?= BASE_URL . 'myorders/' . $order['order_id'] ?>">
                                    <button class="btn btn-icon waves-effect waves-light btn-success m-b-5">
                                        <i class="zmdi zmdi-assignment"></i>
                                    </button>
                                </a>
                            </td>

                            <td>
                                <select data-id="<?= $order['order_id'] ?>" class="send-reminder">
                                    <option value="0" <?= ($order['send_reminder'] == 0) ? 'selected' : '' ?>><?= __('No') ?></option>
                                    <option value="1" <?= ($order['send_reminder'] == 1) ? 'selected' : '' ?>><?= __('Yes') ?></option>
                                </select>
                            </td>

                            <td class="invoices">
                                <?php if (!in_array($order['payment_status_id'], [
                                    PaymentStatus::FINAL_INVOICE_SENT,
                                    PaymentStatus::ORDER_CANCELLED])): ?>
                                    <button class="generate-and-send-final-invoice btn btn-success btn-sm">
                                        <?= __("Generate final invoice") ?>
                                    </button>
                                    <button class="deposit-credit btn btn-success btn-sm">
                                        <?= __("Credit invoice") ?>
                                    </button>
                                <?php endif; ?>
                            </td>

                            <td class="dont_send_invoice">
                                <select data-id="<?= $order['order_id'] ?>" class="send-75percent-invoice">
                                    <option value="0" <?= ($order['dont_send_invoice'] == 0) ? 'selected' : '' ?>><?= __('No') ?></option>
                                    <option value="1" <?= ($order['dont_send_invoice'] == 1) ? 'selected' : '' ?>><?= __('Yes') ?></option>
                                </select>
                            </td>

                            <td>
                                <input class="add-invoice-row"
                                       type="checkbox" <?= $order['invoice_has_extra_row'] == 1 ? 'checked disabled' : '' ?>>
                            </td>

                            <td>
                                <button
                                    class="delete-order btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>

                            <td class="order_billed_rent_percentage">
                                <select data-id="<?= $order['order_id'] ?>" class="edit_rent_percentage">
                                    <option value="0" <?= ($order['order_billed_rent_percentage'] == 0) ? 'selected' : '' ?>><?= __('0%') ?></option>
                                    <option value="25" <?= ($order['order_billed_rent_percentage'] == 25) ? 'selected' : '' ?>><?= __('25%') ?></option>
                                    <option value="100" <?= ($order['order_billed_rent_percentage'] == 100) ? 'selected' : '' ?>><?= __('100%') ?></option>
                                </select>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

</div>

<!-- Fullcalendar -->
<script src="assets/components/adminto/Horizontal/assets/plugins/moment/min//moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.8.0/fullcalendar.min.js"></script>
<link rel="stylesheet"
      href="assets/components/adminto/Horizontal/assets/plugins/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" media="print"
      href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.8.0/fullcalendar.print.css">

<!-- Date.js -->
<script src="assets/components/datejs/date.js"></script>


<script>
    $(document).ready(function () {

        // Bootstrap tooltips
        $('[data-toggle="tooltip"]').tooltip();

        $('#calendar').fullCalendar({
            firstDay: 1, // Monday
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            timeFormat: 'H.mm',
            eventRender: function (event, element) {
                var dataToFind = moment(event.start).format('YYYY-MM-DD');
            },
            events: <?= $orders_js ?>,
            weekends: true
        });

        $('.send-75percent-invoice').on('change', function () {
            $.post("admin/update_invoice_to_send", {
                value: $(this).val(),
                order_id: getOrderId(this)
            }, function (response) {
                if (response != 'OK') {
                    alert('<?= __('Something went wrong!')  ?>');
                }
            });
        });

        $('.edit_rent_percentage').on('change', function () {
            $.post("admin/update_order_billed_rent_percentage", {
                value: $(this).val(),
                order_id: getOrderId(this)
            }, function (response) {
                if (response != 'OK') {
                    alert('<?= __('Something went wrong!')  ?>');
                }
            });
        });

        $('.send-reminder').on('change', function () {
            ajax("admin/update_order_reminder", {
                value: $(this).val(),
                order_id: getOrderId(this)
            });
        });

        $('.add-invoice-row').on('change', function () {
            ajax('admin/add_invoice_row', {
                order_id: getOrderId(this),
                value: $(this).is(":checked") ? 1 : 0
            });
        });

        const datatable = $('#datatable');

        $("span.loadNum").attr("contenteditable", true);

        datatable.dataTable({
            "order": [[0, "desc"]],
            "pageLength": 50,
            "oLanguage": {
                "sSearch": "Filtreeri ridu"
            },

        });

        const filterContainer = $('#datatable_wrapper > div:nth-child(1) > div:nth-child(2)')
        filterContainer.prepend('<div style="text-align:center; color: black; font-weight: bold;"><input type="checkbox" style="display:inline-block;" id="showAllOrders" title="Vaikimisi kuvatakse tellimusi, mille toimumispäev oli viimase 30 päeva sees või on tulevikus."> <label title="Vaikimisi kuvatakse tellimusi, mille toimumispäev oli viimase 30 päeva sees või on tulevikus." for="showAllOrders">Kuva kõik tellimused</label></div>');
        filterContainer.prepend('<div style="text-align:center;"><input style="display:inline-block;" id="search" placeholder="Andmebaasi otsing"></div>');
        const search = $('#search');
        const showAllOrders = $('#showAllOrders');
        let url = new URL(window.location.href);
        let params = new URLSearchParams(url.search);

        $(function () {
            $('[data-toggle="title"]').tooltip()
        });

        // Attach a change event to showAllOrders checkbox to change URL
        showAllOrders.on('change', function () {
            if ($(this).is(":checked")) {
                window.location.href = 'admin/orders?showAllOrders=1';
            } else {
                window.location.href = 'admin/orders?showAllOrders=0';
            }
        });

        // Check the 'showAllOrders' checkbox if 'showAllOrders=1' is in URL
        let showAllOrdersUrlParameter = params.get('showAllOrders');
        if (showAllOrdersUrlParameter === '1') {
            showAllOrders.prop('checked', true);
        } else {
            showAllOrders.prop('checked', false);
        }

        // Attach a change event to search input to change URL
        search.on('change', function () {
            window.location.href = 'admin/orders?search=' + $(this).val();
        });

        // Check the 'search' input if 'search=' is in URL and it's not empty
        let searchParam = params.get('search');
        if (searchParam !== null && searchParam !== '') {
            search.val(decodeURIComponent(searchParam));
        }

    });


    // Database update
    var switchToInput = function () {
        var that = $(this).text();

        var $input = $("<input>", {
            val: that,
            type: "text",
            placeholder: '<?= __('Start typing') ?>'
        });
        $input.addClass("loadNum");
        $input.data('id', $(this).data('id'));
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };

    var switchToSpan = function () {

        $.post('admin/edit_order', {
                order_id: getOrderId(this),
                key: $(this).parent().attr('class'),
                value: $(this).val()
            }, function (response) {
                if (response !== "Ok") {
                    console.log(response.result);
                } else {
                    //swal("Edited!", "Selected booking has been edited.", "success");
                    //window.location.href = 'admin/bookings';
                }
            }
        );
        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $span.data('id', $(this).data('id'));
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        $("span.loadNum").attr("contenteditable", true);
    };

    $(".loadNum").on("click", switchToInput);


    $(".delete-order").click(function () {
        var that = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this booking!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.post("admin/delete_order", {
                    order_id: getOrderId(that)
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                        swal("Problem!", "There was an error saving your changes", "error");
                    } else {
                        swal("Deleted!", "Selected booking has been deleted.", "success");
                        location.reload();
                    }
                }
            );
        });
    });

    $(".selectStatus").change(function () {
        ajax('admin/edit_order_status', {
            order_id: getOrderId(this),
            payment_status_id: $(this).val()
        });
    });

    $(".generate-and-send-final-invoice").click(function () {
        const order_id = getOrderId(this)
        $.post("admin/generate_and_send_final_invoice", {
                order_id
            }, function (response) {
                if (response !== 'notpaid') {
                    var res = tryParseJSON(response);
                    if (res !== false) {
                        if (res.status !== 200) {
                            swal("Problem!", "There was an error creating the invoice. Server said: " + res.data, "error");
                        } else {
                            swal("Created!", "Invoice created.", "success");

                            // Update payment status
                            const select = $('#datatable > tbody > tr[data-order_id=' + order_id + '] > td.payment_status_id > select');
                            select.val(3);
                            select.trigger('change');

                            // Remove buttons by emptying the cell
                            $('#datatable > tbody > tr[data-order_id=' + order_id + '] > td.invoices').empty();

                        }
                    } else {
                        swal("Problem!", "There was an error creating the invoice. Server said: " + response, "error");
                    }
                } else {
                    swal("Problem!", "Deposit invoice hasn't been paid!", "error");

                }
            }
        );
    });

    $(".deposit-credit").click(function () {
        $.post("admin/generate_deposit_credit", {
                order_id: getOrderId(this)
            }, function (response) {
                var res = tryParseJSON(response);
                if (res !== false) {
                    if (res.status != 200) {
                        var errors = JSON.stringify(res.errors);
                        swal("Problem!", "There was an error creating the credit bill. Server said: " + errors, "error");
                    } else {
                        swal("Created!", "Credit bill created.", "success");
                    }
                } else {
                    console.log(response);
                    swal("Problem!", "There was an error creating the credit bill. Server said: " + response, "error");
                }
            }
        );
    });

    function getOrderId(element) {
        return $(element).parents('tr').data('order_id');
    }

    function tryParseJSON(jsonString) {
        try {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns null, and typeof null === "object",
            // so we must check for that, too. Thankfully, null is falsey, so this suffices:
            if (o && typeof o === "object") {
                return o;
            }
        } catch (e) {
        }

        return false;
    }

</script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>