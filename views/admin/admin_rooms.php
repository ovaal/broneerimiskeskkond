<style>
    .divImage {
        margin-bottom: 10px;
    }

    .deleteImage {
        position: absolute !important;
        top: 0;
        right: 0;
    }

    .room-invisible {
        background-color: lightgrey !important;
    }
</style>

<div class="row">
    <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Rooms') ?></h4>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Room ID') ?></th>
                        <th><?= __('Room name') ?></th>
                        <th><?= __('Room max persons') ?></th>
                        <th><?= __('Room description') ?></th>
                        <th><?= __('Room slogan') ?></th>
                        <th><?= __('Room message') ?></th>
                        <th><?= __('Room price') ?></th>
                        <th><?= __('Room size') ?></th>
                        <th><?= __('Room seats') ?></th>
                        <th><?= __('Room seat type') ?></th>
                        <th><?= __('Room class') ?></th>
                        <th><?= __('Delete Room') ?></th>
                        <th><?= __('Edit Room') ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($rooms as $room): ?>
                        <tr class="<?= $room['room_visible'] ? '' : 'room-invisible' ?>">
                            <td class="room_id"><?= $room['room_id'] ?></td>
                            <td><?= $room['room_name'] ?></td>
                            <td><?= $room['room_max_persons'] ?></td>
                            <td><?= $room['room_description'] ?></td>
                            <td><?= $room['room_slogan'] ?></td>
                            <td><?= $room['room_message'] ?></td>
                            <td><?= $room['room_price'] ?></td>
                            <td><?= $room['room_size'] ?></td>
                            <td><?= $room['room_seats'] ?></td>
                            <td><?= $room['room_seat_type'] ?></td>
                            <td><?= $room['room_class'] ?></td>
                            <td>
                                <button id="<?= $room['room_id'] ?>"
                                        class="delete-room btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                            <td>
                                <button id="<?= $room['room_id'] ?>"
                                        class="room-edit-modal btn btn-icon waves-effect waves-light btn-danger m-b-5"
                                        data-toggle="modal">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

</div>
<button class="addRoom pull-right btn btn-success waves-effect w-md waves-light m-b-5" data-toggle="modal"
        href="#add-room-modal"><?= __('Add room'); ?></button>
<button class="edit-extra-modal btn btn-primary pull-right"><?= __('Edit extras'); ?></button>
<button class="edit-supply-modal btn btn-primary pull-right"><?= __('Edit supplies'); ?></button>
<button class="edit-designs-modal btn btn-primary pull-right"><?= __('Edit designs'); ?></button>


<div class="modal fade" id="add-room-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add room'); ?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" role="form" id="formRoomAdd">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Name') ?></label>
                                <div class="col-md-10">
                                    <input name="room_name" class="form-control" type="text" id="room_name"
                                           placeholder="<?= __('Name') ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"
                                       for="example-email"><?= __('Max persons') ?></label>
                                <div class="col-md-10">
                                    <input name="room_max_persons" class="form-control" type="text"
                                           id="room_max_persons"
                                           placeholder="<?= __('Max persons') ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Area size') ?></label>
                                <div class="col-md-10">
                                    <input name="room_size" class="form-control" type="text" id="room_size"
                                           placeholder="<?= __('Area size') ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Description') ?></label>
                                <div class="col-md-10">
                                    <textarea name="room_description" class="form-control" id="room_description"
                                              rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Slogan') ?></label>
                                <div class="col-md-10">
                                    <input name="room_slogan" class="form-control" type="text" id="room_slogan"
                                           placeholder="<?= __('Slogan') ?>"/><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Message') ?></label>
                                <div class="col-md-10">
                                    <input name="room_message" class="form-control" type="text" id="room_message"
                                           placeholder="<?= __('Message') ?>"/><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Price') ?></label>
                                <div class="col-md-10">
                                    <input name="room_price" class="form-control" type="text" id="room_price"
                                           placeholder="<?= __('Price') ?>"/><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Seat count') ?></label>
                                <div class="col-md-10">
                                    <input name="room_seats" class="form-control" type="text" id="room_seats"
                                           placeholder="<?= __('Seat count') ?>"/><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Seat type') ?></label>
                                <div class="col-md-10">
                                    <input name="room_seat_type" class="form-control" type="text"
                                           id="room_seat_type"
                                           placeholder="<?= __('Seat type') ?>"/><br>
                                </div>
                            </div>
                    </form>
                </div>
            </div>

            <div class="row imagesRow">

            </div>
        </div>

        <div class="modal-footer">

            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                <input type="file" class="sr-only btnAddImage" id="inputImage" name="file"
                       accept="image/*">
                Lisa uus pilt
            </label>

            <button type="button" id="add-room"
                    class="btn btn-primary add-room"><?= __('Add room'); ?></button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Edit modal -->

<div class="modal fade" id="edit-room-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div id="edit-modal-content" class="modal-content">
        </div>
    </div>
</div>
<script src="assets/components/tinymce 2/js/tinymce/tinymce.min.js"></script>

<script>

    // Cropper
    var cropBoxData;
    var canvasData;
    var $btnAddImage = $('.btnAddImage');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    var modalName;
    var room_id;

    $(document).on('click', '.deleteImage', function () {
        $(this).parent().parent().remove();
    });

    $('.addRoom').on('click', function () {
        $('<div>').addClass('divImages').appendTo('.imagesRow');
    });

    // Choose image
    if (URL) {
        $(document).on('change', '.btnAddImage', function () {

            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {

                    blobURL = URL.createObjectURL(file);

                    // Duplicate image row div
                    var divImage = $('<div/>').addClass('divImage col-md-12')
                        .html('<div class="gal-detail"><img src="" class="image cropper" style="max-width: 100%"/><button class="deleteImage">X</button></div>')
                        .appendTo('.divImages').one('built.cropper', function () {

                            // Revoke when load complete
                            URL.revokeObjectURL(blobURL);

                        });

                    divImage.find('img').cropper('reset').cropper('replace', blobURL).cropper("setCropBoxData", {
                        width: 500,
                        height: 300
                    });

                    // Reset file button value so that btnAddImage change event would fire again if user selects the same image
                    $(this).val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $btnAddImage.prop('disabled', true).parent().addClass('disabled');
    }


    function save_room(action) {

        var formData = $('#formRoom' + action).serialize();

        if (action == 'Edit') {
            formData = formData + '&room_id=' + room_id;
        }

        $.post("rooms/" + action.toLowerCase(),
            formData
            , function (responseText) {

                if (action == 'Add') {
                    // Parse response
                    try {
                        data = JSON.parse(responseText);
                    } catch (e) {
                        console.log("error: " + e);
                        return;
                    }

                    if (data.result == 'FAIL') {
                        alert('There was an error saving your changes');
                        console.log('data', data);
                        return;
                    }
                    room_id = data.room_id;
                }

                saveImages(room_id);

            });
    }


    $(document).ready(function () {

        $("#add-room").click(function () {
            save_room('Add');
        });

        $(document).on('click', ".saveEdit", function () {
            save_room('Edit');
        });

        $(".delete-room").click(function () {
            var that = this;
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this room!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                var id = $(that).attr("id");
                ajax("admin/delete_room", {
                        room_id: id
                    }, function () {
                        swal("Problem!", "There was an error saving your changes", "error");
                    }, function () {
                        swal("Deleted!", "Selected room has been deleted.", "success");
                        window.location.href = 'admin/rooms';
                    }
                );
            });
        });

        $(window).on('hidden.bs.modal', function () {
            $('textarea').each(function () {
                id = $(this).attr('id');
                tinymce.EditorManager.execCommand('mceRemoveEditor', true, id);

            });
        });

        $(window).on('shown.bs.modal', function () {
            tinymce.init({
                selector: "textarea",
                content_css: '<?= BASE_URL?>assets/css/content.css?v<?= PROJECT_VERSION ?>',
                block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3;Header 4=h4',
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                }
            });

        });

        $(".room-edit-modal").click(function () {
            room_id = $(this).attr("id");
            $.ajax({
                url: 'admin/edit_room',
                type: "post",
                data: {
                    room_id: room_id,
                    room_type: "usual"
                },
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-room-modal').modal('show');

                }
            });
        });

        $(".edit-extra-modal").click(function () {
            $.ajax({
                url: 'admin/edit_extras',
                type: "post",
                data: {},
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-room-modal').modal('show');
                }
            });
        });


        $(".edit-supply-modal").click(function () {
            $.ajax({
                url: 'admin/edit_supplies',
                type: "post",
                data: {},
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-room-modal').modal('show');
                }
            });
        });

        $(".edit-designs-modal").click(function () {
            $.ajax({
                url: 'admin/edit_designs',
                type: "post",
                data: {},
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-room-modal').modal('show');
                }
            });
        });


        $(document).on('click', '.supplyAddNew', function () {
            var counter = parseInt($(this).data('counter'));
            $('#addRow tr').last().after('<tr><td>' + (counter + 1) + '</td><td><input type="text" name="edit[' + (counter + 1) + '][name]" class="form-control"></td><td><input type="number" name="edit[' + (counter + 1) + '][price]" class="form-control"></td><td><span class="glyphicon glyphicon-remove removeAddSupplyRow"></span></td></tr>');
            $('.supplyAddNew').data('counter', counter + 1);
        });

        $(document).on('click', '.designAddNew', function () {
            var counter = parseInt($(this).data('counter'));
            $('#addRow tr').last().after('<tr><td>' + (counter + 1) + '</td><td><input type="text" name="edit[' + (counter + 1) + '][name]" class="form-control"></td><td><input type="number" name="edit[' + (counter + 1) + '][price]" class="form-control"></td><td><span class="glyphicon glyphicon-remove removeAddSupplyRow"></span></td></tr>');
            $('.designAddNew').data('counter', counter + 1);
        });

        $(document).on('click', '.extraAddNew', function () {
            var counter = parseInt($(this).data('counter'));
            $('#addRow tr').last().after('<tr><td>' + (counter + 1) + '</td><td><input type="text" name="edit[' + (counter + 1) + '][name]" class="form-control"></td><td><input type="number" name="edit[' + (counter + 1) + '][price]" class="form-control"></td><td><select name="edit[' + (counter + 1) + '][unit]" class="form-control"> <option value="once">once</option> <option value="hour">hour</option></select></td><td><span class="glyphicon glyphicon-remove removeRow"></span></td></tr>');
            $('.extraAddNew').data('counter', counter + 1);
        });

        $(document).on('click', '.removeRow', function () {
            var id = $(this).attr('id');
            $.ajax({
                url: 'admin/remove_extra',
                type: "post",
                data: {id: id},
                success: function (data) {
                    $(this).closest('td').parent('tr').remove();
                }
            });

            $(this).closest('td').parent('tr').remove();
        });

        $(document).on('click', '.removeSupplyRow', function () {
            var id = $(this).attr('id');
            $.ajax({
                url: 'admin/remove_supply',
                type: "post",
                data: {id: id},
                success: function (data) {
                    $(this).closest('td').parent('tr').remove();
                }
            });

            $(this).closest('td').parent('tr').remove();
        });

        $(document).on('click', '.removeDesignRow', function () {
            var id = $(this).attr('id');
            $.ajax({
                url: 'admin/remove_design',
                type: "post",
                data: {id: id},
                success: function (data) {
                    $(this).closest('td').parent('tr').remove();
                }
            });

            $(this).closest('td').parent('tr').remove();
        });

        $(document).on('click', '.removeAddRow', function () {
            $(this).closest('td').parent('tr').remove();
            $('.extraAddNew').data('counter', counter--);
        });

        $(document).on('click', '.removeAddSupplyRow', function () {
            $(this).closest('td').parent('tr').remove();
            $('.supplyAddNew').data('counter', counter--);
        });

        $(document).on('click', '.removeAddDesignRow', function () {
            $(this).closest('td').parent('tr').remove();
            $('.designAddNew').data('counter', counter--);
        });
    });

    function saveImages(room_id) {
        var images = $('.image');
        var unsavedImages = images.length;

        // Reload page immediately if there aren't any images added
        if (!unsavedImages) {
            window.location.href = 'admin/rooms';
            return;
        }

        $.each(images, function (index, image) {

            var data = $(image).cropper('getCroppedCanvas', {width: 1600, height: 900});
            var image_data = data.toDataURL('image/jpeg');

            $.post('pictures/save', {
                room_id: room_id,
                image_data: image_data,
                room_type: 'usual'
            }, function (response) {
                if (response == '{"status":200}') {

                    unsavedImages--;
                    if (unsavedImages == 0) {
                        window.location.href = 'admin/rooms';
                    }
                } else {
                    console.log(response);
                    alert("Image was not uploaded.");
                }
            });

        });

    }

</script>

<link href="assets/components/cropper/cropper.min.css" rel="stylesheet">
<script src="assets/components/cropper/cropper.min.js"></script>
