<!-- end col -->
<style>
    /* Scale service group images to their container's size */
    #add-edit-service-group-modal-img, td.service-group-image img, #service-groups-table > tbody td:nth-child(1) > img {
        max-width: 100%;
        height: auto;
    }

    #service-groups-table > tbody td:nth-child(1) {
        max-width: 100px;
    }
</style>
<div class="container">
    <div class="card-box">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="header-title m-t-0 m-b-30"><?= __('Service groups') ?></h2>
            </div>
            <div class="col-sm-6">
                <button class="pull-right btn btn-success waves-effect w-md waves-light m-b-5 btn-add-service-group"
                        data-keyboard="true" data-toggle="modal"
                        href="#add-edit-service-group-modal"><?= __('Add service group'); ?></button>
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped" id="service-groups-table">
                    <thead>
                    <tr>
                        <th><?= __('Name') ?></th>
                        <th><?= __('Description') ?></th>
                        <th><?= __('Services') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($service_groups as $service_group): ?>
                        <tr data-service_group_id="<?= $service_group['service_group_id'] ?>">
                            <td><?= $service_group['service_group_name'] ?><?php if (!empty($service_group['service_group_image'])): ?>
                                    <img src="<?= $service_group['service_group_image'] ?>" alt="Image">
                                <?php endif ?></td>
                            <td><?= $service_group['service_group_description'] ?></td>
                            <td>
                                <?php foreach ($service_group['services'] as $service): ?>
                                    <span class="label label-info"><?= $service['service_name'] ?></span>
                                <?php endforeach ?>
                            </td>
                            <td>
                                <button class="btn-delete-service-group btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                            <td>
                                <button class="service-group-edit-modal btn btn-icon waves-effect waves-light btn-danger m-b-5"
                                        data-keyboard="true" data-toggle="modal">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->
</div><!-- SERVICE GROUP MODAL --> <?php require 'admin_services_service_group_modal.php' ?>
<!-- SERVICE MODAL       --> <?php require 'admin_services_service_modal.php' ?>
<script>
    var service_id;
    var service_group_id;
    var serviceGroupModalServicesTableTbody = $("#service-group-modal-services-table tbody");

    $(document).on('shown.bs.modal', '#add-edit-service-group-modal', function () {
        if (service_group_id) {

            $('#service-group-modal-services-section').show();

        } else {

            $('#service-group-modal-services-section').hide();

        }
    });

    function getImageOrFallback(path, fallback) {
        return new Promise(function (resolve) {
            var img = new Image();
            img.src = path;

            img.onload = function () {
                return resolve(path);
            };

            img.onerror = function () {
                return resolve(fallback);
            };
        });
    }


    $(document).ready(function () {

        $(".btn-save-service").on('click', function () {

            // Set service_group_id
            $('#service_group_id').val(service_group_id);

            // Set action
            var action = service_id > 0 ? "edit" : "add";

            // Checkbox state is based on quantity options value
            if (!$('#service_quantity_checkbox').prop('checked')) {
                $('#service_quantity_options').val('');
            }

            // Serialize form
            var formData = $('#formService').serialize();

            if (action === "edit") {
                formData = formData + '&service_id=' + service_id;
            }

            ajax(`admin/${action}_service`, formData, function (json) {
                addNewServiceToServicesTable(action, json.data);
                $('#add-edit-service-modal').modal('hide');
            });

        });

        $(".btn-add-service-group").on('click', function () {

            // Reset service group id (to hide services table in service group modal)
            service_group_id = 0;

        });

        $(".btn-save-service-group").on('click', function () {

            var action = service_group_id > 0 ? "edit" : "add";
            var formData = $('#formServiceGroup').serialize();
            if (action === "edit") {
                formData = formData + '&service_group_id=' + service_group_id;
            }


            ajax("admin/" + action + "_service_group", formData, function (AddEditServiceGroupResponse) {

                //alert('DEBUG: uploading image');

                var formUploadImage = new FormData();
                var files = $('#file')[0].files[0];

                // Cancel image upload if no image was selected
                if (typeof files === 'undefined') {

                    // Reload browser
                    history.go();
                    return;

                }

                // Get new service_group_id (only when adding new service)
                if (action === 'add') {
                    service_group_id = AddEditServiceGroupResponse.data;
                    if (!service_group_id > 0) {
                        alert('Operation failed. The server did not return a proper service_group_id');
                    }
                }

                //alert('DEBUG: service_group_id=' + service_group_id);

                formUploadImage.append('file', files);

                console.log(formUploadImage);

                var data = {"formData": formUploadImage, "service_group_id": service_group_id};

                //alert('DEBUG: performing ajax request to upload image');
                $.ajax({
                    url: 'admin/upload_service_group_image?service_group_id=' + service_group_id,
                    type: 'post',
                    data: formUploadImage,
                    contentType: false,
                    processData: false,
                    success: function (uploadServiceGroupImageResponse, status, jqXHR) {

                        //alert('DEBUG: upload image ajax request callback');

                        //alert('DEBUG: trying to parse uploadServiceGroupImageResponse which has value of ' + uploadServiceGroupImageResponse);
                        json = tryToParseJSON(uploadServiceGroupImageResponse);

                        if (typeof json === false) {
                            alert("The Unexpected response from the server. \n" + uploadServiceGroupImageResponse);
                        }

                        //alert('DEBUG: json parsed successfully. json.status=' + json.status);

                        if (json.status !== 200) {
                            console.log(json);
                            alert('Error ' + json.status + ': ' + json.data);
                            return false;
                        }

                        // Reload browser
                        history.go();
                    },
                    error: function (jqXHR, status, error) {

                        // Hopefully we should never reach here
                        console.log(jqXHR);
                        console.log(status);
                        console.log(error);

                        alert('There was a problem uploading the image to the server. Check console!');
                    }
                });


            });
        });


        serviceGroupModalServicesTableTbody.on('click', '.btn-delete-service', function () {

            var clickedButton = this;
            var service_id = $(clickedButton).parents('tr').data('service_id');
            var service_name = $(clickedButton).parents('tr').find('td:first-child').html();
            swal({
                title: 'Are you sure you want to delete the service "' + service_name + '"?',
                text: "You will not be able to recover this service!",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#FF0000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            }, function () {
                ajax("admin/delete_service", {
                    service_id: service_id
                }, function () {
                    $('#service-group-modal-services-table tbody tr[data-service_id="' + service_id + '"]').remove();

                    // Restore modal scrollability
                    $('.modal:visible').length && $(document.body).addClass('modal-open');
                });
            });
        });

        $(".btn-delete-service-group").click(function () {
            var clickedButton = this;
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this service!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                ajax("admin/delete_service_group", {
                    service_group_id: $(clickedButton).parents('tr').data("service_group_id")
                }, RELOAD);
            });
        });

        $(window).on('hidden.bs.modal', function () {

            // Reset service_id to 0 to prevent firing admin/edit_service request instead of add_service next time
            service_id = 0;
        });





        // Edit service group button
        $(".service-group-edit-modal").click(function () {

            service_group_id = $(this).parents('tr').data("service_group_id");

            ajax('admin/get_service_group', {service_group_id: service_group_id}, function (response) {

                var serviceGroup = response.data;
                $('#service-group-modal-title').html('Edit service group <i>' + serviceGroup.service_group_name + '</i>');
                $('#service-group-modal-services-table-title').html('<i>' + serviceGroup.service_group_name + '</i> services');
                $("#service_group_name").val(serviceGroup.service_group_name);
                $("#service_group_description").val(serviceGroup.service_group_description);
                $("#service_group_multiselect").prop('checked', !!serviceGroup.service_group_multiselect);
                $("#service_group_unit_id").val(serviceGroup.service_group_unit_id);
                $("#service_group_group_id").val(serviceGroup.service_group_group_id);
                $("#service_group_services").val(serviceGroup.service_group_price);

                // Populate services
                console.log(serviceGroup.services);

                // Empty the services table
                serviceGroupModalServicesTableTbody.html('');

                if (serviceGroup.services) {


                    for (const [index, service] of Object.entries(serviceGroup.services)) {
                        console.log('fieldName=' + index, 'fieldValue=' + JSON.stringify(service));

                        addNewServiceToServicesTable('add', service);
                    }
                }

                var link = getImageOrFallback(serviceGroup.service_group_image, 'https://via.placeholder.com/150').then(function (result) {
                    $("#add-edit-service-group-modal-img").attr('src', result);
                });

                $('#add-edit-service-group-modal').modal('show');
            });
        });

        // Reset modal title back to "Add service group" when the modal is closed
        $('#add-edit-service-group-modal').on('hide.bs.modal', function () {
            history.go();
        });

        function addNewServiceToServicesTable(action, row) {

            // TODO: Add correct units

            let tr = `<tr data-service_id="${row.service_id}">
                        <td>${row.service_name}</td>
                        <td>${row.service_description}</td>
                        <td>${row.service_unit_name}</td>
                        <td>${row.service_price}</td>
                        <td>
                                <button class="btn-delete-service btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <button class="btn-edit-service-modal btn btn-icon waves-effect waves-light btn-danger m-b-5" data-toggle="modal" data-keyboard="true">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </td>
                        </tr>`;

            if (action === 'add') {
                $('#service-group-modal-services-table tbody').append(tr);
            } else if (action === 'edit') {
                $('#service-group-modal-services-table tbody tr[data-service_id="' + row.service_id + '"]').replaceWith(tr);
            } else {
                alert('There was a problem adding the service to the services table');
            }
        }

        serviceGroupModalServicesTableTbody.on("click", ".btn-edit-service-modal", function () {

            // Get service id
            service_id = $(this).parents('tr').data('service_id');

            // Obtain service data from the backend
            ajax('admin/get_service', {service_id: service_id}, function (json) {

                // Extract data from response
                let data = json.data;

                // Fill the modal with selected service's data
                $("#service_name").val(data.service_name);
                $("#service_description").val(data.service_description);
                $("#service_group_id").val(data['service_group_id']);
                $("#service_quantity_options").val(data['service_quantity_options']);
                $("#service_unit_singular").val(data['service_unit_singular']);
                $("#service_unit_plural").val(data['service_unit_plural']);
                $("#service_time").val(data.service_time);
                $("#service_price").val(data.service_price);
                $("#service_name_in_order_summary").val(data.service_name_in_order_summary);
                $('#service_quantity_checkbox').prop('checked', data['service_quantity_options']);

                // Set up options options
                toggleUnitsUsage(!!$("#service_quantity_options").val());

                // Replace the modal title
                $('#add-edit-service-modal > div > div > div.modal-header > h4').html('Edit service <i>' + data.service_name + '</i>');

                // Show the modal
                $('#add-edit-service-modal').modal('show');

            });

        });

        // Show new service group image when user chooses new image
        $("#file").change(function () {

            console.log('inside change');
            if (this.files && this.files[0]) {

                console.log('file exists');
                var reader = new FileReader();


                reader.onload = function (e) {
                    console.log('reader onload invoked');
                    $('#add-edit-service-group-modal-img').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }


        });
    });
</script>
