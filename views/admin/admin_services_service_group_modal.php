<style>
    #service-group-modal-services-table td:last-child {
        text-align: right;
    }
</style>
<div class="modal fade" id="add-edit-service-group-modal" tabindex="-1">
    <div class="modal-dialog modal-lg box-shadow">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="service-group-modal-title"><?= __('Add service group'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" id="formServiceGroup">
                        <div class="col-md-12">
                            <!-- SERVICE GROUP NAME -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Name') ?></label>
                                <div class="col-md-10">
                                    <input name="service_group_name" class="form-control" type="text"
                                           id="service_group_name" placeholder="<?= __('Name') ?>"/>
                                </div>
                            </div>
                            <!-- SERVICE GROUP DESCRIPTION -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Description') ?></label>
                                <div class="col-md-10">
                                    <textarea name="service_group_description" class="form-control" type="text"
                                              id="service_group_description"
                                              placeholder="<?= __('Description') ?>"></textarea>
                                    <div class="alert alert-info" style="margin-top: 5px">Inserted images to the
                                        description will be linked, not copied.
                                    </div>
                                </div>
                            </div>

                            <!-- SERVICE GROUP IS MULTISELECT -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Multiselect?') ?></label>
                                <div class="col-md-10">
                                    <label for="service_group_multiselect">
                                    <input name="service_group_multiselect"  type="checkbox"
                                           id="service_group_multiselect"/>
                                        Customer can select multiple services from this service group
                                    </label>
                                </div>
                            </div>

                            <!-- SERVICE GROUP IMAGE-->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Image') ?></label>
                                <div class="col-md-10">
                                    <img id="add-edit-service-group-modal-img" src=""/>
                                    <input type="file" accept="image/*" id="file" name="file"/>
                                </div>
                            </div>
                            <!-- SERVICE GROUP IMAGE LOCATION -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Image location') ?></label>
                                <div class="col-md-10">
                                    <label>
                                        <select name="service_group_image_location" id="service_group_image_location"
                                                class="form-control">
                                            <option value="left">left</option>
                                            <option value="right">right</option>
                                        </select> </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-primary btn-save-service-group"><?= __('Save service group'); ?></button>
            </div>

            <!-- SERVICES SECTION-->

            <div class="modal-body" id="service-group-modal-services-section">
                <div class="row">
                    <div class="col-md-12" style="padding: 20px">
                        <div class="row" style="background-color: rgba(250,250,250,0.98); border: 1px solid #dadada;">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6 pull-left">
                                            <h4 id="service-group-modal-services-table-title"><?= __('Services') ?></h4>
                                        </div>
                                        <div class="col-sm6 pull-right">
                                            <a class="btn btn-secondary btn-add-edit-service-modal pull-left"
                                               style="margin-left: -15px" data-keyboard="true" data-toggle="modal"
                                               href="#add-edit-service-modal"><?= __('Add service'); ?></a>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="service-group-modal-services-table">
                                            <thead>
                                            <tr>
                                                <th><?= __('Name') ?></th>
                                                <th><?= __('Description') ?></th>
                                                <th><?= __('Unit') ?></th>
                                                <th><?= __('Price') ?></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
