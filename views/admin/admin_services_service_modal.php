<style>
    .box-shadow {
        box-shadow: 0 3px 15px rgba(0, 0, 0, 0.2);
    }

    ::-webkit-input-placeholder { /* Edge */
        color: #dedede;
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #dedede;
    }

    ::placeholder {
        color: #dedede;
    }
    #service_unit_plural{
        margin-top:5px;
    }
</style>
<div class="modal fade" id="add-edit-service-modal" style="width:fit-content !important;" tabindex="-2">
    <div class="modal-dialog modal-lg box-shadow">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Add service'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" role="form" id="formService" data-parsley-validate>
                        <div class="col-md-12">

                            <!-- SERVICE NAME -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Name') ?></label>
                                <div class="col-md-10">
                                    <input name="service_name" class="form-control" type="text" id="service_name"
                                           placeholder="<?= __('Service name') ?>"/>
                                </div>
                            </div>

                            <!-- SERVICE NAME IN ORDER SUMMARY -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Name in ordersummary') ?></label>
                                <div class="col-md-10">
                                    <input name="service_name_in_order_summary" class="form-control" type="text" id="service_name_in_order_summary"
                                           placeholder="<?= __('Service name in ordersummary') ?>"/>
                                </div>
                            </div>

                            <!-- SERVICE DESCRIPTION -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Description') ?></label>
                                <div class="col-md-10">
                                    <textarea name="service_description" class="form-control" type="text"
                                              id="service_description"
                                              placeholder="<?= __('Service description') ?>"></textarea>
                                </div>
                            </div>

                            <!-- SERVICE UNIT -->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?= __('Quantity selection') ?></label>
                                <div class="col-md-10">

                                    <!-- On/Off checkbox -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input class="" type="checkbox" id="service_quantity_checkbox"
                                                   name="service_quantity_checkbox">
                                            <label class="control-label"
                                                   for="service_quantity_checkbox"><?= __('Enable') ?></label>
                                        </div>
                                    </div>

                                    <!-- Quantity options -->
                                    <div class="row" id="service_quantity_options_section">

                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label"
                                                           for="service_quantity_options"><?= __('Options') ?></label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label"
                                                           for="service_unit_singular">
                                                        <?= __('Unit') ?></label>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <!-- List -->
                                                <div class="col-sm-6">
                                                    <input type="text" id="service_quantity_options"
                                                           name="service_quantity_options" placeholder="1,2,3,4,5"
                                                           style="width: 100%"/><br>
                                                    <sup><?= __('separate by commas') ?></sup>
                                                </div>

                                                <!-- Units -->
                                                <div class="col-sm-6">

                                                    <!-- Singular -->
                                                    <input type="text" id="service_unit_singular"
                                                           name="service_unit_singular" placeholder="Singual form (e.g. person)"
                                                           style="width: 100%"/>

                                                    <!-- Plural -->
                                                    <input type="text" id="service_unit_plural"
                                                           name="service_unit_plural" placeholder="Plural form (e.g. persons)"
                                                           style="width: 100%"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- SERVICE GROUP -->
                            <input type="hidden" name="service_group_id" id="service_group_id" value="">

                            <!-- SERVICE PRICE -->
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <span id="price"><?= __('Unit price') ?></span>
                                </label>
                                <div class="col-md-10">
                                    <input name="service_price" class="form-control" type="number" id="service_price"
                                           placeholder="<?= __('Service price') ?>"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save-service"><?= __('Save service'); ?></button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#service_quantity_checkbox').on('change', function () {
        toggleUnitsUsage(this.checked)
    });

    function toggleUnitsUsage(showQuantityOptions) {

        // Disable/enable the quantity options field
        if (showQuantityOptions) {
            $('#service_quantity_options_section').show();
        } else {
            $('#service_quantity_options_section').hide();
        }

        // Show Unit price instead of price when units are in use
        $('#price').html(showQuantityOptions ? '<?=__('Unit price')?>' : '<?=__('Price')?>');

    }

</script>