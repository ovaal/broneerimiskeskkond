<div class="row">
    <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Settings') ?></h4>

            <label>
                <input type="checkbox" data-setting="100_PERCENT_INVOICE_OPTION"
                    <?= $settings['100_PERCENT_INVOICE_OPTION'] ? 'checked' : '' ?>>
                <?= __('Show 100% invoice option') ?>
            </label><br>

            <label>
                <input type="checkbox" data-setting="WEEKENDS_CAN_BE_BOOKED"
                    <?= $settings['WEEKENDS_CAN_BE_BOOKED'] ? 'checked' : '' ?>>
                <?= __('Weekends can be booked') ?>
            </label>

        </div>
    </div>

</div>

<script>
    $(':checkbox').on('click', function () {
        ajax('admin/update_setting', {
            setting: $(this).data('setting'),
            value: $(this).is(':checked')
        });
    });
</script>