<style>
    .divImage {
        margin-bottom: 10px;
    }

    .deleteImage {
        position: absolute !important;
        top: 0;
        right: 0;
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15"></div>
        <h4 class="page-title"><?= __('Special rooms') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Special rooms') ?></h4>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Room ID') ?></th>
                        <th><?= __('Room name') ?></th>
                        <th><?= __('Room price') ?></th>
                        <th><?= __('Room max persons') ?></th>
                        <th><?= __('Room description') ?></th>
                        <th><?= __('Edit Room') ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($special_rooms as $room): ?>
                        <tr>
                            <td class="room_id"><?= $room['special_room_id'] ?></td>
                            <td><?= $room['special_room_name'] ?></td>
                            <td><?= $room['special_room_price'] ?></td>
                            <td><?= $room['special_room_max_persons'] ?></td>
                            <td><?= $room['special_room_description'] ?></td>
                            <td>
                                <button id="<?= $room['special_room_id'] ?>"
                                        class="room-edit-modal btn btn-icon waves-effect waves-light btn-danger m-b-5"
                                        data-toggle="modal">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-room-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div id="edit-modal-content" class="modal-content">
        </div>
    </div>
</div>
<script src="assets/components/tinymce 2/js/tinymce/tinymce.min.js"></script>

<script>

    // Cropper
    var cropBoxData;
    var canvasData;
    var $btnAddImage = $('.btnAddImage');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    var modalName;
    var room_id;

    $(document).on('click', '.deleteImage', function () {
        $(this).parent().parent().remove();
    });

    $('.addRoom').on('click', function () {
        $('<div>').addClass('divImages').appendTo('.imagesRow');
    });

    // Choose image
    if (URL) {
        $(document).on('change', '.btnAddImage', function () {

            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {

                    blobURL = URL.createObjectURL(file);

                    // Duplicate image row div
                    var divImage = $('<div/>').addClass('divImage col-md-12')
                        .html('<div class="gal-detail"><img src="" class="image cropper" style="max-width: 100%"/><button class="deleteImage">X</button></div>')
                        .appendTo('.divImages').one('built.cropper', function () {

                            // Revoke when load complete
                            URL.revokeObjectURL(blobURL);

                        });

                    divImage.find('img').cropper('reset').cropper('replace', blobURL).cropper("setCropBoxData", {
                        width: 500,
                        height: 300
                    });

                    // Reset file button value so that btnAddImage change event would fire again if user selects the same image
                    $(this).val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $btnAddImage.prop('disabled', true).parent().addClass('disabled');
    }

    function save_room(action) {
        var formData = $('#formRoom' + action).serialize();

        if (action == 'Edit') {
            formData = formData + '&room_id=' + room_id;
        }

        $.post("rooms/" + action.toLowerCase() + "_special",
            formData
            , function (responseText) {
                saveImages(room_id);
            }
        );
    }

    $(document).ready(function () {
        $(document).on('click', ".saveEdit", function () {
            save_room('Edit');
        });

        $(window).on('hidden.bs.modal', function () {
            $('textarea').each(function () {
                id = $(this).attr('id');
                tinymce.EditorManager.execCommand('mceRemoveEditor', true, id);
            });
        });

        $(window).on('shown.bs.modal', function () {
            tinymce.init({
                selector: "textarea",
                content_css: '<?= BASE_URL?>assets/css/content.css?v<?= PROJECT_VERSION ?>',
                block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3;Header 4=h4',
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                }
            });
        });

        $(".room-edit-modal").click(function () {
            room_id = $(this).attr("id");
            $.ajax({
                url: 'admin/edit_room',
                type: "post",
                data: {
                    room_id: room_id,
                    room_type: "special"
                },
                success: function (data) {
                    $("#edit-modal-content").html(data);
                    $('#edit-room-modal').modal('show');

                }
            });
        });
    });

    function saveImages(room_id) {
        var images = $('.image');
        var unsavedImages = images.length;

        // Reload page immediately if there aren't any images added
        if (!unsavedImages) {
            window.location.href = 'admin/special_rooms';
            return;
        }

        $.each(images, function (index, image) {

            var data = $(image).cropper('getCroppedCanvas', {width: 1600, height: 900});
            var image_data = data.toDataURL('image/jpeg');

            $.post('pictures/save', {
                room_id: room_id,
                image_data: image_data,
                room_type: 'special'
            }, function (response) {
                if (response == '{"status":200}') {

                    unsavedImages--;
                    if (unsavedImages == 0) {
                        window.location.href = 'admin/special_rooms';
                    }
                } else {
                    console.log(response);
                    alert("Image was not uploaded.");
                }
            });

        });

    }

</script>

<link href="assets/components/cropper/cropper.min.css" rel="stylesheet">
<script src="assets/components/cropper/cropper.min.js"></script>
