<?php namespace Broneerimiskeskkond; ?>
<style>
    span[contenteditable] {
        display: inline-block;
    }

    span[contenteditable]:empty::before {
        content: '<?= __('Add translation') ?>';
        display: inline-block;
        color: lightgrey;
    }

    .nav-pills > li > button,
    .nav-pills > li > button:focus {
        margin: 3px;
        margin-left: 0;
        border-radius: 4px;
        color: #23527c;
        background-color: #f4f8fb;
    }

    .nav-pills > li > button:hover {
        color: #23527c;
        background-color: #dfeef0;
    }

    .nav-pills > li.active > button,
    .nav-pills > li.active > button:focus,
    .nav-pills > li.active > button:hover {
        color: #fff;
        background-color: #337ab7;
    }
</style>
<!-- DataTables -->
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/fixedHeader.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>


<div class="card-box">

    <div class="row">
        <div class="col-lg-12">
            <h2 class="header-title m-t-0 m-b-30"><?= __('Translations') ?></h2>
            <span class="pull-right"><?= __('To edit a translation, click on a cell and insert a value. To save the value, click on any other cells. Only the first 255 characters of phrases are shown. Translations can be up to 32768 characters long.') ?></span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <ul id="pills-languages" class="nav nav-pills">
                <?php foreach (Language::getAll() as $language): ?>
                    <li role="presentation" <?= $language['language_name'] == $this->params[0] ? 'class="active"' : '' ?>>

                        <button class="select-language btn"
                                data-language_id="<?= $language['language_id'] ?>"
                                data-href="admin/translations/<?= $language['language_name'] ?>">

                            <input type="checkbox" <?= $language['language_enabled'] ? 'checked' : '' ?>>

                            <?= ucfirst($language['language_name']) ?>

                        </button>

                    </li>
                <?php endforeach ?>
            </ul>
        </div>

        <div class="col-xs-6">
            <ul id="pills-translatedness" class="nav nav-pills pull-right">
                <li role="presentation" <?= $this->params[1] == 'untranslated' ? 'class="active"' : '' ?>><a
                            href="admin/translations/<?= $this->params[0] ?>/untranslated"><?= __('Untranslated') ?></a>
                </li>
                <li role="presentation" <?= $this->params[1] == 'translated' ? 'class="active"' : '' ?>><a
                            href="admin/translations/<?= $this->params[0] ?>/translated"><?= __('Translated') ?></a>
                </li>
                <li role="presentation" <?= $this->params[1] == 'all' ? 'class="active"' : '' ?>><a
                            href="admin/translations/<?= $this->params[0] ?>/all"><?= __('All') ?></a></li>

            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">&nbsp;</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Appeared') ?></th>
                        <th><?= __('Language') ?></th>
                        <th><?= __('Phrase') ?></th>
                        <th><?= __('Translation') ?></th>
                        <th><?= __('Last active') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($translations as $translation): ?>
                        <tr data-translation_id="<?= $translation['translation_id'] ?>">
                            <td><?= substr($translation['appeared'], 0, 10) ?></td>
                            <td><?= $translation['language'] ?></td>
                            <td><?= htmlentities($translation['phrase']) ?></td>
                            <td class="translation">
                                <span class="loadNum"
                                      data-id="<?= $translation['translation_id'] ?>"><?= $translation['translation'] ?></span>
                            </td>
                            <td><?= $translation['last_active'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <button id="delete-selected-rows" class="btn btn-danger">Delete selected</button>
            </div>
        </div>
    </div><!-- end col -->
</div>


<script>
    $(document).ready(function () {
        $('#datatable').dataTable();

        $('#datatable tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });

        $("span.loadNum").attr("contenteditable", true);

        $('#delete-selected-rows').on('click', function () {
            var selectedTranslationIds = $("tr.selected").map(function () {
                return $(this).data('translation_id');
            }).get();
            ajax("admin/delete_translations", {translation_ids: selectedTranslationIds}, RELOAD)
        })
    });


    // Database update
    var switchToInput = function () {
        var that = $(this).text();

        var $input = $("<textarea>", {
            val: that,
            type: "text",
            placeholder: '<?= __('Start typing') ?>'
        });
        $input.css("width", "100%");
        $input.css("height", "100%");
        $input.addClass("loadNum");
        $input.data('id', $(this).data('id'));
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };

    var switchToSpan = function () {

        $.post('admin/edit_translation', {
                translation_id: $(this).data('id'),
                key: $(this).parent().attr('class'),
                value: $(this).val()
            }, function (response) {
                if (response !== "Ok") {
                    console.log(response.result);
                } else {
                    //swal("Edited!", "Selected user has been edited.", "success");
                    //window.location.href = 'admin/users';
                }
            }
        );

        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $span.data('id', $(this).data('id'));
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        $("span.loadNum").attr("contenteditable", true);
    };

    $(".loadNum").on("click", switchToInput);

    $('#pills-languages input').on('click', function (e) {
        var checked = null;
        ajax('admin/toggle_language', {
            language_id: $(this).parents('button').data('language_id'),
            state: $(this).is(':checked') ? 1 : 0
        });

        // Prevent the button from being clicked
        e.stopPropagation();
    });

    $('.select-language').on('click', function () {
        location.href = $(this).data('href');
    });

</script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>