<style>
    span[contenteditable] {
        display: inline-block;
    }

    span[contenteditable]:empty::before {
        content: '<?= __('Edit user') ?>';
        display: inline-block;
        color: lightgrey;
    }
</style>

<!-- DataTables -->
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/fixedHeader.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.css"
      rel="stylesheet" type="text/css"/>
<link href="assets/components/adminto/Horizontal/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
      type="text/css"/>


<div class="row">
    <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">

            <h4 class="header-title m-t-0 m-b-30"><?= __('Users') ?>
                <span
                        class="pull-right"><?= __('To edit a user, click on a cell and insert a value. To save the value, click on any other cells.') ?></span>
            </h4>

            <div class="table-responsive">
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('User ID') ?></th>
                        <th><?= __('Is admin') ?></th>
                        <th><?= __('Email') ?></th>
                        <th><?= __('Full name') ?></th>
                        <th><?= __('Phone') ?></th>
                        <th><?= __('Company') ?></th>
                        <th><?= __('State institution') ?></th>
                        <th><?= __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= $user['user_id'] ?></td>

                            <td>
                                <input class="is_admin"
                                       type="checkbox" <?= $user['is_admin'] == 1 ? 'checked' : '' ?>
                                       data-id="<?= $user['user_id'] ?>">
                            </td>

                            <td class="email">
                                <span class="loadNum" data-id="<?= $user['user_id'] ?>"><?= $user['email'] ?></span>
                            </td>

                            <td class="first_and_last_name">
                                <span class="loadNum" data-id="<?= $user['user_id'] ?>">
                                    <?= $user['first_and_last_name'] ?>
                                </span>
                            </td>

                            <td class="phone">
                                <span class="loadNum" data-id="<?= $user['user_id'] ?>"><?= $user['phone'] ?></span>
                            </td>

                            <td class="phone">
                                <?= $user['company_names'] ?>
                            </td>

                            <td>
                                <input class="state_institution"
                                       type="checkbox" <?= $user['state_institution'] == 1 ? 'checked' : '' ?>
                                       data-id="<?= $user['user_id'] ?>">
                            </td>

                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <?= __('Action') ?>
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                                        <li>
                                            <a class="switch-user" id="<?= $user['user_id'] ?>" onclick="return false;"
                                               href="#">
                                                <?= __('Switch user') ?>
                                            </a>
                                        </li>

                                        <li role="separator" class="divider"></li>

                                        <li>
                                            <a class="delete-user" id="<?= $user['user_id'] ?>" onclick="return false;"
                                               href="#">
                                                <?= __('Delete user') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

</div>

<script>

    $(document).ready(function () {
        $('#datatable').dataTable();
        $("span.loadNum").attr("contenteditable", true);
    });

    $('.state_institution').click(function () {
        if ($(this).is(':checked')) {
            $.post('admin/edit_user', {
                    user_id: $(this).data('id'),
                    key: 'state_institution',
                    value: 1
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                    }
                }
            );
        } else {
            $.post('admin/edit_user', {
                    user_id: $(this).data('id'),
                    key: 'state_institution',
                    value: 0
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                    }
                }
            );
        }

    });

    $('.is_admin').click(function () {
        if ($(this).is(':checked')) {
            $.post('admin/edit_user', {
                    user_id: $(this).data('id'),
                    key: 'is_admin',
                    value: 1
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                    }
                }
            );
        } else {
            $.post('admin/edit_user', {
                    user_id: $(this).data('id'),
                    key: 'is_admin',
                    value: 0
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                    }
                }
            );
        }

    });

    // Database update
    var switchToInput = function () {
        var that = $(this).text();

        var $input = $("<input>", {
            val: that,
            type: "text",
            placeholder: '<?= __('Start typing') ?>'
        });
        $input.addClass("loadNum");
        $input.data('id', $(this).data('id'));
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();

    };

    var switchToSpan = function () {

        $.post('admin/edit_user', {
                user_id: $(this).data('id'),
                key: $(this).parent().attr('class'),
                value: $(this).val()
            }, function (response) {
                if (response !== "Ok") {
                    console.log(response.result);
                }
            }
        );

        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $span.data('id', $(this).data('id'));
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        $("span.loadNum").attr("contenteditable", true);

    };

    $(".loadNum").on("click", switchToInput);

    $(".delete-user").click(function () {
        var that = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            var id = $(that).attr("id");
            $.post("admin/delete_user", {
                    user_id: id,
                    deleted: 1
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                        swal("Problem!", "There was an error saving your changes", "error");
                    } else {
                        swal("Deleted!", "Selected user has been deleted.", "success");
                        window.location.href = 'admin/users';
                    }
                }
            );
        });
    });

    $(".switch-user").click(function () {
        var that = this;
        swal({
            title: "Are you sure?",
            text: "Your user will be switched to selected one!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, switch user!",
            closeOnConfirm: false
        }, function () {
            var id = $(that).attr("id");
            $.post("admin/switch_user", {
                    user_id: id
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response.result);
                        swal("Problem!", "There was an error with switching a user", "error");
                    } else {
                        swal("Switched!", "Reloading page...", "success");

                        setTimeout(function () {
                            window.location.href = '<?=BASE_URL?>';
                        }, 1500);
                    }
                }
            );
        });
    });

</script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>