<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?= __('Designs') ?></h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <h2><?= __('Edit designs') ?></h2>

            <form class="form-horizontal" role="form" id="saveEditDesign" method="post" action="admin/edit_design">

                <div class="well">

                    <table role="form" class="table">
                        <tr>
                            <th>#</th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($designs as $design): ?>
                            <tr>
                                <td><?= $design['design_id'] ?></td>
                                <td><input type="text" class="form-control"
                                           name="edit[<?= $design['design_id'] ?>][name]"
                                           value="<?= $design['design_name'] ?>"></td>
                                <td><input type="number" class="form-control"
                                           name="edit[<?= $design['design_id'] ?>][price]"
                                           value="<?= $design['design_price'] ?>"></td>
                                <td><span id="<?= $design['design_id'] ?>"
                                          class="glyphicon glyphicon-remove removeDesignRow"></span></td>
                            </tr>
                        <?php endforeach ?>
                    </table>

                </div>
                <button type="submit" class="btn btn-primary pull-right"><?= __('Save changes') ?></button>
            </form>
        </div>
        <div class="col-md-12">
            <h2><?= __('Add new') ?></h2>

            <form class="form-horizontal" role="form" method="post" action="admin/add_design">

                <div class="well">
                    <table role="form" id="addRow" class="table">
                        <tr>
                            <th>#</th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><input type="text" name="edit[1][name]" class="form-control"></td>
                            <td><input type="number" name="edit[1][price]" class="form-control"></td>
                            <td><span class="glyphicon glyphicon-remove removeAddDesignRow"></span></td>
                        </tr>
                    </table>

                </div>
                <button type="submit" class="btn btn-primary pull-right"><?= __('Save') ?></button>
                <button type="button" data-counter="1"
                        class="btn btn-success pull-right designAddNew"><?= __('Add new') ?></button>
            </form>
        </div>
    </div>
</div>

<div class="modal-footer">
    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
</div>
