<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?= __('Extras') ?></h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <h2><?= __('Edit extras') ?></h2>

            <form class="form-horizontal" role="form" id="saveEditExtra" method="post" action="admin/edit_extra">

                <div class="well">

                    <table role="form" class="table">
                        <tr>
                            <th>#</th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Unit') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach($extras as $extra):?>
                        <tr>
                            <td><?= $extra['extra_id']?></td>
                            <td><input type="text" class="form-control" name="edit[<?= $extra['extra_id']?>][name]" value="<?= $extra['extra_name']?>"></td>
                            <td><input type="number" class="form-control" name="edit[<?= $extra['extra_id']?>][price]" value="<?= $extra['extra_price']?>"></td>
                            <td><select name="edit[<?= $extra['extra_id']?>][unit]" class="form-control">
                                    <option value="once" <?= $extra['extra_unit'] == "once" ? 'selected' : ''?>>once</option>
                                    <option value="hour"<?= $extra['extra_unit'] == "hour" ? 'selected' : ''?>>hour</option>
                                </select>
                            </td>
                            <td><span id="<?= $extra['extra_id']?>" class="glyphicon glyphicon-remove removeRow"></span></td>
                        </tr>
                        <?php endforeach?>
                    </table>

                </div>
                <button type="submit" class="btn btn-primary pull-right"><?= __('Save changes') ?></button>
            </form>
        </div>
        <div class="col-md-12">
            <h2><?= __('Add new') ?></h2>

            <form class="form-horizontal" role="form" method="post" action="admin/add_extra">

                <div class="well">
                    <table role="form" id="addRow" class="table">
                        <tr>
                            <th>#</th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Unit') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><input type="text" name="edit[1][name]" class="form-control"></td>
                            <td><input type="number" name="edit[1][price]" class="form-control"></td>
                            <td><select name="edit[1][unit]" class="form-control">
                                    <option value="once">once</option>
                                    <option value="hour">hour</option>
                                </select>
                            </td>
                            <td><span class="glyphicon glyphicon-remove removeAddRow"></span></td>
                        </tr>
                    </table>

                </div>
                <button type="submit" class="btn btn-primary pull-right"><?= __('Save') ?></button>
                <button type="button" data-counter="1" class="btn btn-success pull-right extraAddNew"><?= __('Add new') ?></button>
            </form>
        </div>
    </div>
</div>

<div class="modal-footer">
    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
</div>
