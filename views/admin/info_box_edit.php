<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?= __('Edit info box') ?> <?= $edit['info_box_name'] ?>
    </h4>
</div>

<div class="modal-body">
    <div class="row">
        <form class="form-horizontal" role="form" id="formInfoBoxEdit" method="post">
            <div class="col-md-12">
                <h2><?= __('Overview') ?></h2>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __('Info box name') ?></label>

                    <div class="col-md-10">
                        <input type="hidden" name="id" value="<?= $edit['info_box_id'] ?>">
                        <input class="form-control" type="text" name="info_box[info_box_name]"
                               value="<?= $edit['info_box_name'] ?>"
                               id="info_box_name_edit"
                               placeholder="<?= __('Info box name') ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __('Info box text') ?></label>

                    <div class="col-md-10">
                        <textarea class="form-control" name="info_box[info_box_text]" id="info_box_text_edit"
                                  rows="5"><?= $edit['info_box_text'] ?></textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>

<div class="modal-footer">
    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
    <button type="submit" class="btn btn-success saveEdit"><?= __('Save changes') ?></button>
</div>