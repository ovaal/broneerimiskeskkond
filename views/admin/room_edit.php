<style>
    .room-edit-images {
        position: relative;
    }

    .room-image {
        margin-bottom: 10px;
    }

    .delete-image {
        position: absolute !important;
        top: 0;
        right: 0;
        border: 1px solid black !important;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?= __('Edit room') ?> <?= $edit['room_name'] ?>
    </h4>
</div>

<div class="modal-body">
    <div class="row">
        <form class="form-horizontal" role="form" id="formRoomEdit" method="post">
            <div class="col-md-6">
                <h2><?= __('Overview') ?></h2>

                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Name') ?></label>

                    <div class="col-md-9">
                        <input type="hidden" name="id" value="<?= $edit['room_id'] ?>">
                        <input class="form-control" type="text" name="room[room_name]" value="<?= $edit['room_name'] ?>"
                               id="room_name_edit"
                               placeholder="<?= __('Name') ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Description') ?></label>

                    <div class="col-md-9">
                        <textarea class="form-control" name="room[room_description]" id="room_description_edit"
                                  rows="5"><?= $edit['room_description'] ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Price') ?></label>

                    <div class="col-md-9">
                        <input type="number" class="form-control" name="room[room_price]" id="room_price_edit"
                               value="<?= $edit['room_price'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Area size') ?></label>

                    <div class="col-md-9">
                        <input type="number" class="form-control" name="room[room_size]"
                               value="<?= $edit['room_size'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room max persons') ?></label>

                    <div class="col-md-9">
                        <input type="number" class="form-control" name="room[room_max_persons]"
                               value="<?= $edit['room_max_persons'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room slogan') ?></label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" name="room[room_slogan]"
                               value="<?= $edit['room_slogan'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room message') ?></label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" name="room[room_message]"
                               value="<?= $edit['room_message'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room seats') ?></label>

                    <div class="col-md-9">
                        <input type="number" class="form-control" name="room[room_seats]"
                               value="<?= $edit['room_seats'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room seat type') ?></label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" name="room[room_seat_type]"
                               value="<?= $edit['room_seat_type'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= __('Room class') ?></label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" name="room[room_class]"
                               value="<?= $edit['room_class'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="room-visible" class="col-md-3 control-label"><?= __('Room is visible') ?></label>

                    <div class="col-md-9">
                        <input id="room-visible" type="checkbox" class="form-control" name="room[room_visible]"
                            <?= empty($edit['room_visible']) ? '' : 'checked'?>>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <h2><?= __('Extras') ?></h2>
                <ol>
                    <?php foreach ($extras as $extra): ?>
                        <li>
                            <input type="checkbox" name="extras[]"
                                   value="<?= $extra['extra_id'] ?>" <?= isset($roomExtra[$extra['extra_id']]) ? 'checked' : '' ?>> <?= $extra['extra_name'] ?>
                        </li>
                    <?php endforeach ?>
                </ol>
            </div>

            <div class="col-md-6">
                <h2><?= __('Designs') ?></h2>
                <ol>
                    <?php foreach ($designs as $design): ?>
                        <li>
                            <input type="checkbox" name="designs[]"
                                   value="<?= $design['design_id'] ?>" <?= isset($roomDesign[$design['design_id']]) ? 'checked' : '' ?>> <?= $design['design_name'] ?>
                        </li>
                    <?php endforeach ?>
                </ol>
            </div>

        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2><?= __('Images') ?></h2>
            <?php foreach ($images as $image): ?>
                <div class="room-edit-images">
                    <img src="assets/img/rooms/<?= $room_id ?>/<?= $image ?>" class="img-responsive room-image">
                    <button id="<?= $room_id ?>/<?= $image ?>"
                            class="delete-image btn btn-icon waves-effect waves-light btn-danger m-b-5">
                        <i class="fa fa-remove"></i>
                    </button>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 divImages">

        </div>
    </div>
</div>

<div class="modal-footer">
    <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
        <input type="file" class="btn btn-primary btn-upload sr-only btnAddImage" id="inputImage" name="file"
               accept="image/*">
        <?= __('Add new image') ?>
    </label>
    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
    <button type="submit" class="btn btn-success saveEdit"><?= __('Save changes') ?></button>
</div>

<script>
    $(".delete-image").click(function () {
        var that = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this image!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            var filepath = $(that).attr("id");
            $.post("pictures/delete", {
                filepath: filepath,
                room_type: 'usual'
                }, function (response) {
                    if (response !== "Ok") {
                        console.log(response);
                        swal("Problem!", "There was an error saving your changes", "error");
                    } else {
                        swal("Deleted!", "Selected room has been deleted.", "success");
                        window.location.href = 'admin/rooms';
                    }
                }
            );
        });
    });

</script>