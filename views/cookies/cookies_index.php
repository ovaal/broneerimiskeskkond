<div class="roomsindex">
    <div class="row">
        <h1><?= __('Cookies') ?></h1>
        <div class="row info-block">
            <div class="col-md-12">
                <p><strong><?= __('Mis on küpsised?') ?></strong></p>
                <p><?= __('Küpsis on sinu veebilehitsejas väike tekstilõik, mis laetakse sinu arvutisse, kui sa mingit
                        kindlat veebilehte külastad. Küpsised võimaldavad veebilehel kasutaja arvuti ära tunda. Lisaks
                        aitab veebilehel meelde jätta teavet sinu külastuse kohta, näiteks eelistatud keelt ja muid
                        seadeid. See võib muuta järgmise külastuse lihtsamaks ja lehekülje sinu isikupärastatud
                        andmetega kasulikumaks just sulle. Küpsistel on oluline roll. Nendeta oleks veebi kasutamine
                        palju häirivam kogemus.') ?></p>
                <p><strong><?= __('Mis tüüpi küpsiseid ovaal.ee kasutab?') ?></strong></p>
                <p><?= __('ovaal.ee kasutab:') ?></p>
                <p><?= __('- seansiküpsiseid, mis kustutatakse automaatselt pärast igat külastust.') ?></p>
                <p><?= __('- püsivaid küpsiseid, mis jäävad alles veebilehe korduval kasutamisel.') ?></p>
                <p><?= __('- kolmandate osapoolte küpsised, mida kasutavad meie partnerite veebilehed, mis on meie lehele
                        manustatud või millele me lingime. Nende küpsiste tekkimist me ei kontrolli, seega soovitame sul
                        uurida kolmandate osapoolte lehtedelt nende küpsiste kohta, kuidas neid hallata.') ?></p>
                <p><strong><?= __('Milleks me kasutame küpsiseid ja millist teavet me kogume?') ?></strong></p>
                <p><?= __('Küpsised aitavad meil teenuseid vastavalt sinu soovile ja harjumusele edastada. Küpsiseid
                        kasutatakse tavaliselt statistiliste andmete kogumiseks, jälgides kasutajate eelistusi
                        võrgukülgede külastamisel. Näiteks kasutame neid turvalise otsingu eelistuste meeldejätmiseks,
                        profiilide loomist sihitatud reklaamide esitamiseks, nähtavate reklaamide asjakohasemaks
                        muutmiseks, lehe külastajate loendamiseks, meie teenustesse registreerumisel abi pakkumiseks ja
                        sinu andmete kaitsmiseks. Kui külastad meie veebilehte, võivad lisaks meie küpsistele sinu
                        arvutisse küpsiseid salvestada ja nendele juurde pääseda ning sinu internetitegevuse ja
                        veebiteenuste kasutamise kohta teavet koguda ka meie analüüside ja reklaamide esitamisega seotud
                        partnerid.') ?></p>
                <p><strong><?= __('Kas küpsiste kasutamist saab keelata?') ?></strong></p>
                <p><?= __('Jah, veebilehitsejal on võimalik keelata küpsiste kasutamine.') ?></p>
            </div>
        </div>
    </div>
</div>