<style>
    .info_box_container {
        text-align: center;
    }
</style>
<div class="info_box_container">
    <?php foreach ($info_boxes as $info_box): ?>
        <a href="info_boxes/<?= $info_box['info_box_id'] ?>">
            <h2><?= __($info_box['info_box_name']) ?></h2>
        </a>
    <?php endforeach; ?>
</div>
