<style>
    .info_box_container {
        margin: 50px;
    }
</style>
<div class="info_box_container">
    <a href="info_boxes/info_box_pdf/<?= $info_box['info_box_id'] ?>"
       class="btn btn-default print-pdf"><?= __('Print PDF') ?></a>
    <h2><?= $info_box['info_box_name'] ?></h2>
    <?= __($info_box['info_box_text']) ?>
</div>