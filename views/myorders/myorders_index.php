<style>
    .change-password {
        max-width: 300px;
        margin-top: 10px;
    }

    .cursor-pointer:hover {
        cursor: pointer;
    }
</style>

<div class="roomsindex suitablerooms">
    <h1><?= __('My Orders'); ?></h1>

    <div class="table-responsive">
        <table id="datatable" class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Order ID') ?></th>
                <th><?= __('Room Name') ?></th>
                <th><?= __('Date and time') ?></th>
                <th><?= __('Manage') ?></th>
                <th><?= __('Summary PDF') ?></th>
                <th><?= __('Key person') ?></th>
                <th><?= __('General conditions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order): ?>
                <tr>
                    <td class="order-id"><?= $order['order_id'] ?></td>

                    <td><?= $order['service_id'] == '3' ? __('Exhibition (') . $order['room_names'] . ')' : $order['room_names'] ?></td>

                    <td><?= $order['event_date'] ?>, <?= $order['booking_time'] ?></td>

                    <td class="catering">
                        <a title="<?= __('Change catering options') ?>" class="btn btn-default"
                           href="myorders/<?= $order['order_id']; ?>"
                           style="padding-top: 2px; padding-bottom: 2px;">
                            <img src="assets/img/esitlustehnika.png" style="height: 28px">
                        </a>
                    </td>

                    <td>
                        <a title="View order as PDF" class="btn btn-default"
                           href="pdf/order_summary/<?= $order['order_id'] ?>" download>
                            <i class="text-danger fa fa-file-pdf-o"></i>
                        </a>
                    </td>

                    <td>
                        <span><?= isset($order['coordinator_name']) ? $order['coordinator_name'] : '' ?></span>
                        <a class="edit-coordinator-button" href="#edit-organiser" data-toggle="modal">&nbsp;<i
                                    class="fa fa-pencil"></i></a>
                    </td>

                    <td>
                        <span class="cursor-pointer show-conditions-modal"><?= __('View conditions') ?></span>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="edit-organiser">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <input class="form-control coordinator-name" type="text"
                           placeholder="<?= __('Coordinator name') ?>"
                           style="display: inline; width: 50%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save-organiser" data-dismiss="modal">Save changes
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="change-password">
        <div class="row">
            <div class="col-md-6">
                <label for="old-password"><?= __('Old password') ?></label>
            </div>
            <div class="col-md-6">
                <input id="old-password" type="password">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="new-password"><?= __('New password') ?></label>
            </div>
            <div class="col-md-6">
                <input id="new-password" type="password">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input class="btn btn-ovaal" type="button" id="change-password" value="<?= __('Change password') ?>">
            </div>
        </div>
    </div>
</div>

<script src="assets/components/adminto/Horizontal/assets/plugins/moment/min//moment.min.js"></script>

<!-- Datatables-->
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="assets/components/adminto/Horizontal/assets/pages/datatables.init.js"></script>

<script>
    var orderId;
    $(document).ready(function () {

        $('#change-password').on('click', function () {
            $.post('myorders/change_password', {
                old_pw: $("#old-password").val(),
                new_pw: $("#new-password").val()
            }, function (res) {
                alert(res)
            });
        });

        // Let the user change coordinator name
        $(".save-organiser").on('click', function () {
            $.post('myorders/update_coordinator_name', {
                coordinator_name: $(".coordinator-name").val(),
                order_id: orderId
            });

            // Refresh the page to reflect changes
            location.reload()
        });
        $(".edit-coordinator-button").on("click", function () {

            // Get organiser name
            var name = $(this).parents('td').find('span').html();

            // Write coordinator name to input in the modal
            $(".coordinator-name").val(name);

            // Write order id to global variable
            orderId = $(this).parents('td').siblings('.order-id').html()
        });
    });
</script>