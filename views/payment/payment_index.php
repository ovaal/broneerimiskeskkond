<style>
    .select2-container {
        width: 100% !important;
    }

    body {
        background: #fff;
    }

    .the-header {
        background: #fff !important;
        height: 170px;
    }

    #navbar-collapse-1 {
        float: right;
    }

    .the-navbar .the-navbar-logo {
        background: url('assets/img/logo_purple.png') no-repeat;
        background-size: 98px 76px;
    }

    .the-navbar .the-navbar-menu li {
        display: none;
    }

    .the-navbar .the-navbar-menu li.the-navbar-user {
        display: inline-block;
    }

    .the-navbar .the-navbar-menu li.the-navbar-user a {
        display: inline-block;
        color: #000;
    }

    .the-navbar .the-navbar-menu li.the-navbar-user {
        color: #000;
    }

    .paymentextras {
        padding-bottom: 20px;
    }

    .paymentextras p {
        font-size: 16px;
    }

    .righttext .paymentextras {
        text-align: right;
    }

    .sums {
        text-align: right;
    }

    hr {
        margin-top: 10px;
        margin-bottom: 15px;
    }

    .roomrentingrow {
        font-size: 16px;
        margin-top: 10px;
        font-weight: 400;
    }

    h3 {
        font-size: 22px;
        color: #000;
        font-weight: 300;
    }

    @media (max-width: 768px) {
        .paymentbig {
            padding-left: 15px;
            padding-right: 15px;
        }
    }

    .conditionsBox {
        display: inline-block;
        border-radius: 0;
        padding: 15px 30px 15px 30px;
        color: #606060;
        text-transform: uppercase;
        background: #dadcdf;
        border: 0;
        font-size: 11px;
        margin: auto;
    }

    .show-conditions {
        cursor: pointer;
        text-decoration: underline;
    }

    #coordinator {
        padding-left: 15px;
        border: 1px solid black;
        font-size: 14px;
        color: #555555;
        height: 30px;
        margin-top: 1px;
        font-family: 'Lato', Sans-Serif, Arial;
        font-weight: 400;
        line-height: 16px;
        margin-left: -3px;
        outline: none;
        border-radius: 5px;
    }

    @media screen and (max-width: 768px) {
        .paymentbig {
            margin-top: 15px;
        }
    }

    .h1, .h2, .h3, h1, h2, h3 {
        margin-top: 10px;
        margin-bottom: 5px;
    }

    p {
        margin: 0 0 5px;
    }

    #coordinator {
        margin-left: 5px;
    }

    .table {
        display: table;
        margin: 0 auto;
    }

    .table-cell {
        display: table-cell;
        float: none;
        text-align: center;
        vertical-align: middle;
    }

    .payment-sideinfo {
        text-align: center;
        padding-top: 10px;
    }

    .payment-sideinfo a:hover {
        text-decoration: none;
    }

    .glyphicon-circle-arrow-right {
        color: #e5cf88;
    }

    .navbar .container {
        padding-left: 0px !important;
    }
</style>

<?php require 'views/additional_services/payment_index_order_summary_css.php' ?>
<?php require 'system/js_constants.php' ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<div class="container" style="margin-bottom: 200px;">
    <div class="row">
        <a href="additional_services" class="goback"><?= __('Cancel and go back'); ?></a>
    </div>

    <div class="row paymentinfo">
        <div class="col-md-3" style="padding: 0;">
            <div class="col-md-12 paymentside">
                <p class="upper"><?= __('SERVICE PROVIDER') ?></p>

                <h2>Aktoseidon OÜ</h2>

                <p><?= __('BRAND') ?>: Ovaalstuudio</p><br>

                <p class="upper"><?= __('CUSTOMER') ?></p>

                <h3><?= $user['company_name'] !== null ? $user['company_name'] : $user['email'] ?></h3>
            </div>

            <div class="col-md-12 payment-sideinfo">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="word-read" data-toggle="modal" href="#payment-conditions-modal">
                                    <?= __('READ') ?>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a class="word-read" data-toggle="modal" href="#payment-conditions-modal"
                                   style="color:#808080;">
                                    <?= __('Booking') ?>/<?= __('cancellation') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <a data-toggle="modal" href="#renter-keyperson-conditions-modal" class="word-read">
                                    <?= __('READ') ?>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a style="color:#808080;" data-toggle="modal" href="#renter-keyperson-conditions-modal">
                                    <?= __('Renter') ?>/<?= __('Keyperson') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 paymentbig">

            <div class="payment-quote">
                <?php if (!empty($message)) : ?>
                    <td colspan="5" class="<?= isset($message['success']) ? 'bg-success' : 'bg-danger' ?>">
                        <?php foreach ($message as $text) : ?>
                            <?= $text; ?>
                        <?php endforeach ?>
                    </td>
                <?php else : ?>
                    <td colspan="5" style="padding: 40px;"><?= __('Excellent, almost there. Let\'s summarize.'); ?></td>
                <?php endif ?>
            </div>

            <h2><?= __('Order summary'); ?></h2>
            <div style="margin-bottom: 25px;">
                <?php require 'templates/_partials/order_summary.php' ?>
            </div>


            <?php if ($this->auth->state_institution == 0) : ?>
                <p><strong><?= __('To confirm the booking, 25% of the room rent must be paid.') ?></strong></p>

                <p><?= __('By confirming, you are bound to pay the sum based on the following calculation') ?>:</p>
            <?php elseif ($this->auth->state_institution == 1) : ?>
                <p>
                    <strong>
                        <?= __('To confirm the booking, 25% of the room rent must be paid or by letter of guarantee.') ?>
                    </strong>
                </p>

                <p>
                    <?= __('By confirming, you agree to send the letter of guarantee or pay the sum based on the following calculation:') ?>
                </p>
            <?php endif; ?>

            <ul>
                <li>
                    <?= __('Room(s)') ?> "<?= $room_names ?>":
                    <?= __('rent') ?> <?= $rent ?> <?= __('EUR, excluding VAT') ?>
                </li>

                <li>
                    <?= __('25% of the sum') ?>: <?= $rent * 0.25 ?> <?= __('EUR, excluding VAT') ?>
                </li>
            </ul>

            <hr class="line">

            <p class="infotext">
                <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;&nbsp;
                <?= __('Rent time includes arriving and leaving time.') ?>
            </p>

            <p class="infotext">
                <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;&nbsp;
                <?= __('We kindly ask the key person to arrive first to introduce the studio, the key person will be given studio\'s door card.') ?>
            </p>

            <?= __('Coordinators name ') ?> :
            <input name="coordinator" id="coordinator" style="margin-bottom:12px;"
                   value="<?= isset($_SESSION['coordinator']) ? $_SESSION['coordinator'] : '' ?>">

            <p class="infotext" style="color:#808080;">
                <span class="glyphicon glyphicon-circle-arrow-right"></span>&nbsp;&nbsp;
                <?= __('By clicking "Receive order summary (PDF) to your email", you get an offer, but it\'s <strong>not</strong> a confirmation.') ?>
            </p>

            <p class="infotext" style="color:#808080;">
                <span class="glyphicon glyphicon-circle-arrow-right"></span>&nbsp;&nbsp;
                <?= __("By clicking 'Confirm booking', you confirm your order.") ?>
            </p>

            <p class="infotext">
                <?= __("Coordinator enters Ovaalstuudio first and get a doorcard.") ?>
            </p>

            <hr class="line">

            <div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="conditionsBox" style="width: 100%; text-align: center">
                            <?= __('I have read and agree to') ?>

                            <span class="show-conditions">
                                <?= __('purchase conditions') ?>
                            </span>

                            <input class="conditionsCheckbox" type="checkbox" id="conditions-checkbox">
                        </p>
                    </div>
                </div>

                <div class="row paybuttons">
                    <div class="col-md-6">
                        <a class="emailoffer" style="width: 100%;" data-toggle="modal" href="#company-name-modal">
                            <?= __("Receive order summary (PDF) to your email"); ?>
                        </a>
                    </div>

                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#paymentmodal" class="confirmorder" id="confirm-order"
                           style="width: 100%;">
                            <?= __('Confirm booking'); ?>
                        </a>
                    </div>
                </div>

                <p>
                    <?= __('Contact: (+372) 505 0277 or ovaal@ovaal.ee') ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentmodal" tabindex="false" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="padding-right: 20px;padding-left: 20px;">
                <div class="invoice-information"
                     style="padding-right: 45px;padding-left: 55px; padding-bottom: 30px; text-align: left;">
                    <h1 style="margin-bottom: 20px;"><?= __('Your info'); ?></h1>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="registration-organisation"><?= __('Company name'); ?></label>
                        </div>

                        <div class="col-md-12">
                            <select id="registration-organisation" class="form-control" required></select>
                        </div>
                    </div> <!-- Company name -->

                    <div class="row company-registry-number-container">
                        <div class="col-md-12">
                            <label for="registration-registry-number"><?= __('Registry number'); ?></label>
                        </div>

                        <div class="col-md-12">
                            <input type="number" class="form-control" id="registration-registry-number" required
                                   disabled>
                        </div>
                    </div> <!-- Registry number -->

                    <div class="row company-address-container">
                        <div class="col-md-12">
                            <label for="registration-address"><?= __('Address'); ?></label>
                        </div>

                        <div class="col-md-12">
                            <input type="text" class="form-control" id="registration-address" required disabled>
                        </div>
                    </div> <!-- Address -->

                    <div class="row">
                        <div class="col-md-12">
                            <label for="invoice-email"><?= __('Invoice email'); ?></label>
                        </div>

                        <div class="col-md-12">
                            <input type="email" id="invoice-email" class="form-control"
                                   value="<?= $user['invoice_email'] ? $user['invoice_email'] : $user['email'] ?>"
                                   placeholder="example@example.com">
                        </div>
                    </div> <!-- Invoice email -->

                    <div class="row">
                        <div class="col-md-12">
                            <label for="invoice-additional-info"><?= __('Additional info for invoice'); ?></label>
                        </div>

                        <div class="col-md-12">
                            <input type="text" id="invoice-additional-info" class="form-control">
                        </div>
                    </div> <!-- Additional info for invoice -->

                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <p style="margin-bottom: 0">
                                <?= __("Can't find your company?") ?>
                                <a style="cursor:pointer;" data-toggle="modal" href="#new-company-modal">
                                    <?= __('Add it here!') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="payment-methods" style="text-align: center">

                    <h1 style="padding-right: 45px;padding-left: 55px;"><?= __('Payment method'); ?></h1>

                    <div class="row table">
                        <div class="col-md-2 table-cell" style="padding-right: 0; color: #56a534">A)</div>
                        <div class="col-md-10 table-cell" style="padding-right: 45px; padding-left: 0;">
                            <a class="payment-method" id="btnPaymentByInvoice"><span
                                        class="text-uppercase"><?= __('Send me an invoice'); ?></span>
                                <span><?= __('as PDF/Print'); ?></span>
                            </a>
                        </div>
                    </div>

                    <?php if ($display_100_percent_invoice_opt && $auth->state_institution == 0) : ?>
                        <div class="row table">
                            <div class="col-md-2 table-cell" style="padding-right: 0; color: #56a534">B)</div>
                            <div class="col-md-10 table-cell" style="padding-right: 45px; padding-left: 0;">
                                <a class="payment-method"
                                   id="btnPaymentByInvoice100PercentNow"><?= __('Send me an invoice'); ?><br>
                                    <span><?= __('100% of the bill will be sent to email immediately, including 25% booking fee'); ?></span>
                                </a>
                            </div>
                        </div>
                    <?php elseif ($auth->state_institution == 1) : ?>
                        <div class="row table">
                            <div class="col-md-2 table-cell" style="padding-right: 0; color: #56a534">B)</div>
                            <div class="col-md-10 table-cell" style="padding-right: 45px; padding-left: 0;">
                                <a class="payment-method" id="btnGuaranteeLetter">
                                    <span class="text-uppercase"><?= __('To confirm the order, I will send a letter of guarantee to ovaal@ovaal.ee'); ?></span>
                                    <span><?= __('The invoice will be sent to Your email 1 day before the booking.'); ?></span>
                                </a>
                            </div>
                        </div>
                    <?php else : ?>
                    <?php endif; ?>

                    <?php if ($auth->state_institution == 0) : ?>
                        <div class="row table">
                            <div class="col-md-2 table-cell" style="padding-right: 0; color: #56a534"></div>
                            <div class="col-md-10 table-cell" style="padding-right: 45px; padding-left: 0;">
                                <p style="text-align: left;"><?= __('The booking will be cancelled automatically when not paying the invoice.'); ?></p>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>

                <a href="#" data-dismiss="modal" class="cancel"><?= __('Cancel and go back'); ?></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="errors-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="background-color: #F2DEDE; color: #A94442; border: 1px solid #A94442">
            <div class="modal-header" style="border-bottom: 0; background-color: transparent">

                <span>hei</span>
                <hr>

                <div class="text-center">
                    <button class="btn btn-danger" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div style="width: 100%" class="modal fade bs-example-modal-lg" id="notification-modal" tabindex="-1" role="dialog"
     aria-labelledby="myNoticeModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="padding-bottom: 20px;">
        <div class="modal-content">

            <div class="modal-body offertoemail"
                 style="text-align: center; font-weight: normal;white-space: nowrap; font-family: 'Open Sans' , sans-serif">
                <p><?= __("Perfect, all done!") ?></p>

                <p><?= __("Thanks for choosing Ovaalstuudio.") ?></p>

                <div style="width:150px; margin: 0 auto;">
                    <button style="background-color:#374054; color:#FFFFFF" type="button"
                            class="btn btn-block redirect-btn" data-dismiss="modal"><?= __("CLOSE") ?></button>
                </div>
            </div>


        </div>

    </div>
</div>

<div style="width: 100%" class="modal fade bs-example-modal-lg" id="email-offer-modal" tabindex="-1" role="dialog"
     aria-labelledby="myNoticeModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="padding-bottom: 20px;">
        <div class="modal-content">

            <div class="modal-body email-offer-modal-body offertoemail"
                 style="text-align: center; font-weight: normal;white-space: nowrap; font-family: 'Open Sans' , sans-serif"></div>
            <div class="modal-footer" style="text-align: center;">
                <button style="background-color:#374054; color:#FFFFFF" type="button"
                        class="btn btn-default redirect-btn"><?= __("Back to main page") ?></button>
                <button style="background-color:#374054; color:#FFFFFF" type="button" class="btn btn-primary"
                        data-dismiss="modal"><?= __("Stay here") ?></button>
            </div>

        </div>

    </div>
</div>

<div id="payment-conditions-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body master-info-box">
                <?= __(get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=5")) ?>
            </div>

            <div class="termsbutton"><a href="#" data-dismiss="modal"><?= __('Accept') ?></a>
            </div>

        </div>
    </div>
</div>

<div id="renter-keyperson-conditions-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body master-info-box">
                <?= __(get_one("SELECT info_box_text FROM info_boxes WHERE info_box_id=6")) ?>
            </div>

            <div class="termsbutton"><a href="#" data-dismiss="modal"><?= __('Accept') ?></a>
            </div>

        </div>
    </div>
</div>

<div id="room-already-booked" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

            </div>

            <div class="termsbutton">
                <a href="#" class="redirect-btn" data-dismiss="modal"><?= __('Go to main page.') ?></a>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="new-company-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <?= __("We couldn't find a company with that registry number.") ?>
                    <?= __('Please check that the entered registry number is correct and try again or add a new company below.') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="new-company-registry-number"
                           class="col-sm-2 control-label"><?= __('Registry Number') ?></label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="new-company-registry-number"
                               placeholder="<?= __('Registry Number') ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="new-company-name" class="col-sm-2 control-label"><?= __('Name') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="new-company-name"
                               placeholder="<?= __('Company name') ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="new-company-city" class="col-sm-2 control-label"><?= __('City') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="new-company-city"
                               placeholder="<?= __('Company city') ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="new-company-street" class="col-sm-2 control-label"><?= __('Street') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="new-company-street"
                               placeholder="<?= __('Company street address') ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="new-company-zip" class="col-sm-2 control-label"><?= __('Postal code') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="new-company-zip"
                               placeholder="<?= __('Comapny postal code') ?>">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary add-new-company-btn"><?= __('Add new company') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="company-name-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __('Please enter your company name') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control company-name">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary send-quote"><?= __('Send quote') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="assets/js/company-organisation-select2.js?v<?= PROJECT_VERSION ?>"></script>

<script>
    $(document).ready(function () {

        var confirmBtn = $(".confirmorder");
        var coordinator = $('#coordinator');
        var conditionsCheckbox = $('.conditionsCheckbox');
        var loading_modal = $("#loading-modal");
        var new_company_modal = $("#new-company-modal");

        // Initially disable confirm button
        confirmBtn.removeAttr('data-toggle');

        // Save coordinator to session
        coordinator.on('input change', function () {
            $.post('payment/coordinator_to_session', {
                'coordinator': $(this).val()
            });
        });

        // Add a new company
        $(".add-new-company-btn").on("click", function () {
            ajax("companies/add", {
                'company_registry_nr': $("#new-company-registry-number").val(),
                'company_name': $("#new-company-name").val(),
                'company_city': $("#new-company-city").val(),
                'company_street': $("#new-company-street").val(),
                'company_postal_code': $("#new-company-zip").val()
            }, "payment")
        });

        $(confirmBtn).click(function () {
            // Do not convert to ajax() - confirm modal will not show
            $.post('payment/check_date_availability', function (response) {
                if (response !== '{"status":200}') {
                    let json = tryToParseJSON(response);
                    $("#room-already-booked").modal('show');
                    $("#room-already-booked > div > div > div.modal-body")
                        .html('<?= __('Given room(s) is already booked on selected date and time') ?>:' + '</br>' + json.data);
                }
            });

            if (conditionsCheckbox.is(':checked') && coordinator.val() != '') {
                confirmBtn.attr('data-toggle', 'modal');
            } else if (!conditionsCheckbox.is(':checked')) {
                alert('<?= __('Read and accept our purchase conditions to continue.') ?>');

                confirmBtn.removeAttr('data-toggle');
                confirmBtn.click(function (e) {
                    e.preventDefault();
                });
            } else if (coordinator.val() == '') {
                alert('<?= __('Please fill out the coordinators name!') ?>');

                confirmBtn.removeAttr('data-toggle');
                confirmBtn.click(function (e) {
                    e.preventDefault();
                });
            }
        });


        $('.show-conditions').click(function () {
            $('#conditions-modal').modal('show');
        });

        // Prevent whitespace
        $("#registration-registry-number").on('keyup change', function () {
            $(this).val(function (_, v) {
                return v.replace(/\s+/g, '');
            });
        });


        $(".send-quote").on("click", function () {
            var company_name = $(".company-name").val();

            if (company_name.length < 2) {
                alert('<?= __('Please enter the correct company name') ?>');
            } else {
                // Show the loading modal until ajax finishes
                loading_modal.modal('show');

                ajax('payment/send_email_quote', {
                    company_name: company_name
                }, function () {
                    // Fill the modal body
                    $(".email-offer-modal-body").html(
                        '<p><?= __("Thanks for the inquiry!") ?></p>' +
                        '<p><?= __("Booking offer was sent to your email.") ?><p>');

                    $("#company-name-modal").modal('hide');

                    // Show the modal
                    $("#email-offer-modal").modal('show');
                });
            }
        });

        $("#invoice-email").on("input", function () {
            ajax("users/update_invoice_email", {
                invoice_email: $("#invoice-email").val()
            })
        });

    });

        $(document).on('click', '.redirect-btn', function () {
            window.location.href = '<?= BASE_URL ?>';
        });

        $(document).on('click', '.payment-method', function () {

            var clicked_element_id = $(this).attr('id');

            // Modals
            var loading_modal = $('#loading-modal');
            var payment_modal = $("#paymentmodal");
            var errors_modal = $("#errors-modal");

            // Fields
            var coordinator = $('#coordinator');
            var invoice_additional_info = $("#invoice-additional-info");
            var invoice_email = $("#invoice-email");

            // Check that the user has selected an company
            if (!company_id) {
                alert('<?= __('Please select or create a new company first.') ?>');
            } else {
                // Do not convert to ajax() - confirm modal will not show
                $.post('payment/check_date_availability', function (response) {
                    if (response !== '{"status":200}') {
                        $("#room-already-booked").modal('show');
                        $("#room-already-booked > div > div > div.modal-body")
                            .html('<?= __('Given room(s) is already booked on selected date and time') ?>:' + '</br>' + json.data);
                        payment_modal.modal('hide');
                    } else {
                        // Show loading modal
                        loading_modal.modal('show');

                        ajax('payment/confirm', {
                            button: clicked_element_id,
                            company_id: company_id,
                            coordinator: coordinator.val(),
                            invoice_additional_info: invoice_additional_info.val(),
                            invoice_email: invoice_email.val()
                        }, function () {
                            payment_modal.modal('hide');
                            $("#notification-modal").modal('show');
                        })
                    }
                });
                // Prevent navigation away from this page
                return false;
            }
        });
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
        };
</script>