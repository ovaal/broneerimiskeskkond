<style>

    body {
        font-family: "Lato", sans-serif;
        font-size: 11px;
    }

    h2 {
        font-family: "Lato-Light", sans-serif;
        font-size: 16px;
    }

    #ovaal {
        width: 35%;
        background-color: #374054;
        color: #fff;
        text-align: center;
        height: 224px;
        padding-top: 25px;
        float: left;
    }

    #ovaal p, #ovaal h2 {
        margin: 0;
    }

    #rooms {
        width: 65%;
        text-align: left;
        float: right;
    }

    .righttext, .prices {
        text-align: right;
    }

    tr#title td {
        font-size: 11px;
        text-transform: uppercase;
        color: #8a8a8a;
        border-bottom: 1px solid #8a8a8a;
    }

    table {
        border-collapse: collapse;
        line-height: 1.5;
    }

    .prices {
        vertical-align: bottom;
    }

    th, td {
        padding: 0;
    }

    .gray {
        color: #8a8a8a;
        text-transform: uppercase;
        padding-top: 5px;
        padding-bottom: 8px;
        font-family: "AvenirLT-Light", sans-serif;
    }

    #heading, table {
        color: #8a8a8a !important;
    }

    hr {
        color: #929292;
    }

    #tableTitle, #info {
        color: #000;
    }

    table, table p, #heading, a {
        font-size: 11px;
        color: #8a8a8a !important;
    }

    table, table p, #heading, #tableTitle {
        font-family: "AvenirLT-Light", sans-serif;
        font-weight: normal;
    }

    #heading, #tableTitle {
        font-size: 15px;
    }

    .padding {
        padding-top: 20px;
        padding-bottom: 10px;
    }

    #info {
        padding-top: 60px;
        padding-bottom: 60px;
    }

    .prices {
        color: #000;
        font-size: 11px;
    }

    .sectionTitle {
        color: #000;
        font-family: "AvenirLT-Medium", sans-serif;
        /* font-weight: bold;*/
    }

    tr.sectionRow td {
        padding-top: 30px;
    }

    tr.sectionRow-first td {
        padding-top: 10px
    }

    tr#contactRow td {
        font-family: "AvenirLT-Book", sans-serif;
    }


</style>
<h3 id="heading"><?= __('Ovaalstuudio innovatsiooni keskkond') ?></h3>

<div>
    <div id="ovaal">
        <p><?= __('SERVICE PROVIDER') ?></p>

        <h2>Aktoseidon OÜ</h2>

        <p><?= __('Brand') ?>: Ovaalstuudio</p>

        <br/>

        <p><?= __('CUSTOMER') ?></p>

        <h3><?= $company_name ? $company_name : $user['email'] ?></h3>
    </div>

    <div id="rooms">
        <img id="pdfImg" src="assets/img/pdf_offer_header.jpg" alt="Rooms">
    </div>
</div>

<table width="100%">
    <tr>
        <td colspan="2" class="padding">
            <h3 id="tableTitle"><?= __('Order summary'); ?>*</h3>
        </td>
    </tr>

    <tr>
        <td class="col-md-10 lefttext gray"><?= __('Service') ?></td>
        <td class="col-md-2 righttext gray"><?= __('Price') ?></td>
    </tr>

    <tr>
        <td colspan="2">
            <hr>
        </td>
    </tr>
    <?php $r = 0; ?>
    <?php if (isset($_SESSION["selected_exhibition_options"][0]) && $_SESSION["selected_exhibition_options"][0] == 3): ?>
        <tr class="sectionRow">
            <td class="lefttext">
                <p><?= __("Exhibition ") . "(" . $services["exhibition_time_sum"] . "h)" ?></p>
            </td>
            <td class="prices">
                <p><?= $services["exhibition_sum"] ?> €</p>
            </td>
        </tr>
    <?php endif ?>
    <?php if ($_SESSION['whole_studio'] == 1): ?>
        <tr <?= ($r == 1) ? 'class="sectionRow-first"' : 'class="sectionRow"' ?>>
            <td class="sectionTitle">
                <p><?= __('Whole studio') ?>,
                    (<?= $bookingDate . ' - ' . $_SESSION['booking_start_time'] . '-' . $_SESSION['booking_end_time'] ?>
                    )</p>
            </td>

            <?php if ($discount != 0): ?>
                <td class="prices">
                    <p><?= $roomprices * (1 - $user['user_discount_percent']) ?> €</p>
                </td>
            <?php else: ?>
                <td class="prices">
                    <p><?= $roomprices ?> €</p>
                </td>
            <?php endif; ?>

        </tr>
    <?php endif; ?>
    <?php if (!empty($_SESSION['selected_rooms'])): ?>
        <?php $n = 0; ?>
        <?php foreach ($_SESSION['selected_rooms'] as $room): ++$n ?>
            <tr <?= ($n == 1) ? 'class="sectionRow-first"' : 'class="sectionRow"' ?>>
                <td class="sectionTitle">

                    <p><?= __('Room rent') ?>,
                        "<?= $room['room_name'] ?>
                        " <?= $room['booking_start_time'] . ' - ' . $room['booking_end_time'] ?>
                        <?= $_SESSION['whole_studio'] ? '' : '(' . $bookingDate . ')' ?></p>
                </td>
                <td class="prices">
                    <?php if ($_SESSION['whole_studio'] != 1): ?>
                        <?php if ($discount != 0): ?>
                            <p><?= $room['room_price'] * $room['booking_duration'] * (1 - $user['user_discount_percent']) ?>
                                €</p>
                        <?php else: ?>
                            <p><?= $room['room_price'] * $room['booking_duration'] ?> €</p>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>

            <?php if (isset($_SESSION['coordinator'])): ?>
                <tr>
                    <td class="sectionTitle">
                        <?= __('Coordinator name') ?>: <?= $_SESSION['coordinator'] ?>
                    </td>
                </tr>
            <?php endif; ?>

            <?php if (isset($extras) && !empty($extras[$room['room_id']])): ?>
                <?php foreach ($extras[$room['room_id']] as $extra): ?>
                    <tr>
                        <td class="lefttext">
                            <p><?= $extra['info']['extra_name'] ?> (<?= $extra['amount'] ?>tk)</p>
                        </td>
                        <td class="prices">
                            <p><?= $extra['price'] ?> €</p>
                        </td>
                    </tr>
                <?php endforeach; ?> <!-- <tr> -->
            <?php endif ?>


            <?php if (isset($supplies) && !empty($supplies[$room['room_id']])): ?>
                <?php foreach ($supplies[$room['room_id']] as $supply): ?>
                    <tr>
                        <td class="lefttext">
                            <p><?= $supply['info']['supply_name'] ?> (<?= $supply['amount'] ?>tk)</p>
                        </td>
                        <td class="prices">
                            <p><?= $supply['price'] ?> €</p>
                        </td>
                    </tr>
                <?php endforeach; ?> <!-- <tr> -->
            <?php endif ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (isset($services) && $_SESSION["selected_exhibition_options"][0] != 3): ?>
        <tr class="sectionRow">
            <td class="lefttext">
                <p><?= __("Exhibition ") . "(" . $services["exhibition_time_sum"] . "h)" ?></p>
            </td>
            <td class="prices">
                <p><?= $services["exhibition_sum"] ?> €</p>
            </td>
        </tr>

    <?php endif ?>

    <tr>
        <td colspan="2" id="info">
            *<?= __('This file represents a summary of your order, but it is not a confirmation of a booking.') ?>
            <br/><?= __('To book a room, you must confirm the booking in e-Ovaal.') ?>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <hr>
        </td>
    </tr>

    <tr id="contactRow">
        <td>
            <?= __('Tel: (+372) 50 50 277') ?><br/>
            <?= __('E-mail:') ?> <a href="mailto:ovaal@ovaal.ee?Subject=Ovaal.ee" target="_top">ovaal@ovaal.ee</a><br/>
            <?= __('Website:') ?> <a href="http://ovaal.ee"> www.ovaal.ee </a>
        </td>
        <td class="righttext">
            <img width="60px" src="assets/img/logo_purple.png" alt="Ovaal.ee">
        </td>
    </tr>
</table>

