<style>

    .otherrooms {
        padding-top: 20px;
    }

    @media (max-width: 768px) {
        .searchback {
            padding-top: 10px;
        }

        #start_time, #end_time {
            width: 90%;
            display: block;
            margin: 10px auto 0;
            border-radius: 3px;
        }

        input[type="text"] {
            display: block;
            margin: 0 auto !important;
            margin-top: 10px !important;
            border-radius: 3px;

        }

        .searchback input[type="text"]:first-child {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        #ui-datepicker-div {
            left: 0 !important;
            right: 0 !important;
            margin-left: auto !important;
            margin-right: auto !important;
        }

        .ui-datepicker-calendar {
            width: 100% !important;
        }
    }

    .ui-datepicker-calendar {
        width: 214px;
    }

    <?php if(!\Broneerimiskeskkond\Settings::get('WEEKENDS_CAN_BE_BOOKED')):?>
    .ui-datepicker-week-end {
        display: none;
    }

    <?php endif?>

    .my-orders-button {
        background-color: #5b5b95 !important;
        color: white;
        min-width: 140px;
        text-align: center;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        padding: 15px 30px 15px 30px;
        margin-right: 10px;
        text-decoration: none;
    }

    .video-button {
        border-radius: 5px;
        margin-right: 10px;
        background-color: #2A4B7C !important;
    }

    body {
        background: #f4f4f4;
    }
</style>

<div class="searchback">

    <?php if ($user_has_orders): ?>
        <button type="button" onclick="location.href='myorders'"
                class="my-orders-button"><?= __('My orders') ?></button>
    <?php endif; ?>

    <button type="button" class="video-button" style="padding: 7px 15px 3px 20px;">
        <?= __('Instructional video') ?> <img src="assets/img/white-arrow.png"
                                              style="max-width: 30px; display: inline-block;">
    </button>

    <form action="?" style="display: inline;">
        <input type="text" name="booking_date" placeholder="Soovitav päev" id="datepicker"
               value="<?= $booking_date ?>"
               readonly="true">
        <input id="start_time" name="booking_start_time" type="hidden" value="08:00">
        <input id="end_time" name="booking_end_time" type="hidden" value="18:00">
        <input class="number_of_people" type="number" name="number_of_people"
               placeholder="<?= __('Number of people') ?>"
               value="<?= $number_of_people ?>">
        <button id="search-for-room"><?= __('Search for a room'); ?></button>
    </form>

</div>
<div class="roomsindex suitablerooms">
    <ul class="rooms" id="rooms">

        <li class="kogu_stuudio" style="margin-bottom: 78px;">
            <div class="wholestudio"
                 style="background-image:url(assets/img/special_rooms/<?= $whole_studio['special_room_id'] ?>/<?= $whole_studio['special_room_first_image_filename'] ?>)">
                <div class="room-layer" style="background: rgba(161, 171, 195, .8);">
                    <div class="content-row">
                        <div class="content-col-5">
                            <div class="room-layer-icon"></div>
                        </div>
                        <div class="content-col-5">
                            <div class="room-layer-info">
                                <span><?= $whole_studio['special_room_name'] ?></span></div>
                        </div>

                        <div class="viewmore">
                            <div class="content-col-10">
                                <?= __('Whole studio info') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="roomcapacity" style="height: 155px;">
                <p><?= __('Capacity') ?>: <?= $whole_studio['special_room_max_persons'] ?> <?= __('people') ?></p>
                <p></p>
                <p></p>
            </div>
        </li>
        <?php foreach ($rooms as $room): ?>
            <li class="<?= $room['room_class'] ?>"
                id="<?= $room['room_id'] ?>"
                data-price="<?= $room['room_price'] ?>"
                data-name="<?= $room['room_name'] ?>">
                <div class="roomsli"
                     style="background-image:url(assets/img/rooms/<?= $room['room_id'] ?>/<?= $room['room_first_image_filename'] ?>);">
                    <a class="roomlinkabs"></a>
                    <div class="room-layer">

                        <div class="content-row">

                            <div class="content-col-5">

                                <div class="room-layer-icon"></div>

                            </div>

                            <div class="content-col-5">

                                <div class="room-layer-info">
                                    <?= __('Room'); ?>
                                    <span><?= $room['room_name'] ?></span>
                                </div>
                            </div>

                        </div>

                        <div class="content-row">
                            <div class="viewmore">
                                <div class="content-col-10">

                                    <?= __('Room info'); ?>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="roomcapacity">
                    <p><?= __('Capacity'); ?>: <?= $room['room_max_persons'] ?> <?= __('people') ?></p>
                    <p><?= __('Vacant time to book:'); ?></p>
                    <?php if (isset($_SESSION['booking_start_time']) && isset($_SESSION['booking_end_time'])) {
                        \Broneerimiskeskkond\Room::get_available_bookings($room['room_id']);
                    } else {
                        echo __("Please select booking start and end times!");
                    } ?>
                </div>
            </li>

        <?php endforeach; ?>
        <li class="kogu_stuudio" data-exhibition="true" style="margin-bottom: 78px;">
            <div class="wholestudio"
                 style="background-image:url(assets/img/special_rooms/<?= $whole_studio_exhibition['special_room_id'] ?>/<?= $whole_studio_exhibition['special_room_first_image_filename'] ?>)">
                <div class="room-layer" style="background: rgba(161, 171, 195, .8);">
                    <div class="content-row">
                        <div class="content-col-5">
                            <div class="room-layer-icon"></div>
                        </div>
                        <div class="content-col-5">
                            <div class="room-layer-info">
                                <span><?= $whole_studio_exhibition['special_room_name'] ?></span></div>
                        </div>

                        <div class="viewmore">
                            <div class="content-col-10">
                                <?= __('Whole studio info') ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="roomcapacity" style="height: 155px;">
                <p><?= __('Capacity') ?>
                    : <?= $whole_studio_exhibition['special_room_max_persons'] ?> <?= __('people') ?></p>
                <p></p>
                <p></p>
            </div>
        </li>

    </ul>
</div>

<div class="roomsindex otherrooms">
    <h1><?= __('Other rooms'); ?></h1>
    <ul class="rooms">

    </ul>
</div>

<div class="modal fade" id="instructional-video-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Instructional video') ?></h4>
            </div>
            <div class="modal-body">
                <video style="width: 100%" controls>
                    <source src="assets/video/Broneerimisjuhis.mov">
                </video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="errormodal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body errormodal-body">

            </div>
            <div class="modal-footer">
                <a href="#" class="closeModal" data-dismiss="modal"><?= __('Ok') ?></a>
            </div>
        </div>
    </div>
</div>

<script src="assets/components/adminto/Horizontal/assets/plugins/moment/min//moment.min.js"></script>
<script>
    $(document).ready(function () {

        $("#datepicker").datepicker({
            <?php if(!\Broneerimiskeskkond\Settings::get('WEEKENDS_CAN_BE_BOOKED')):?>beforeShowDay: $.datepicker.noWeekends,<?php endif?>
            monthNames: ["Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
            monthNamesShort: ["Jan", "Veb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dets"],
            dayNamesMin: ['P', 'E', 'T', 'K', 'N', 'R', 'L'],
            firstDay: 1,
            minDate: 0
        });
        var datepicker = $("#datepicker").val();
        var selStartTime = $('#start_time');
        var selEndTime = $('#end_time');
        var optPreviouslySelectedStartTime = selStartTime.find(':selected');
        var optPreviouslySelectedEndTime = selEndTime.find(':selected');
        var otherRooms = $('div.otherrooms');
        var suitableRooms = $(".suitablerooms");
        var selected_rooms = <?= \Broneerimiskeskkond\Room::get_selected_rooms()?>;
        var whole_studio = $(".kogu_stuudio");
        var maximum_number_of_people = <?= MAX_NUM_OF_PEOPLE ?>;

        $(".number_of_people").on("change", function () {
            var input = $(this).val();
            if (input > maximum_number_of_people) {
                // Set the maximum number of people value
                $(this).val(maximum_number_of_people);

                // Inform the user
                alert('<?= __("Maximum number of people is ") ?>' + maximum_number_of_people);
            }
        });

        $.post("rooms/check_date", {
            info: 'less',
            clicked: 0
        }, function (response) {
            if (response !== "Ok") {
                $(".kogu_stuudio > .roomcapacity")
                    .append("<?= __('Vacant time to book') ?>:<br>" + response.substring(response.indexOf("Next")) + "<span class='choosing'><?= __('CHOOSE STUDIO') ?></span>");
            } else {
                $(".kogu_stuudio > .roomcapacity").append("<p><?= __('Vacant time to book') ?>:</p>07:00 - 23:59 <span class='choosing'><?= __('CHOOSE STUDIO') ?></span>")
            }
        });

        // Hide room "Õhk" from users
        $(".ohk").hide();

        // Initially hide other rooms
        otherRooms.hide();

        // Select suitable rooms for the user
        whole_studio.on('click', function () {
            var that = $(this);
            $.post("rooms/check_date", {
                info: 'more',
                clicked: 1
            }, function (response) {
                if (response !== "Ok") {
                    $("#errormodal").modal().show();
                    $(".errormodal-body").html(response);
                } else {
                    if (that.data('exhibition')) {
                        window.location.href = "rooms/2/special";
                    } else {
                        window.location.href = "rooms/1/special";
                    }

                }
            });

        });

        // Initially focus on the rooms and rooms search
        <?php if(ENV != ENV_DEVELOPMENT):?>
        $('html, body').animate({
            scrollTop: $('.searchback').offset().top
        }, 2000);
        <?php endif ?>

        $("select").click(function () {
            optPreviouslySelectedStartTime = selStartTime.find(':selected');
            optPreviouslySelectedEndTime = selEndTime.find(':selected');
        });

        $('#start_time, #end_time').change(function (e) {
            var selectedStartTime = new Date(moment(datepicker + ' ' + selStartTime.find(':selected').text()).format('MMMM D, YYYY h:mm:ss a')).getTime();
            var selectedEndTime = new Date(moment(datepicker + ' ' + selEndTime.find(':selected').text()).format('MMMM D, YYYY h:mm:ss a')).getTime();

            if (selectedStartTime > selectedEndTime || selectedStartTime == selectedEndTime) {
                optPreviouslySelectedStartTime.prop("selected", true);
                optPreviouslySelectedEndTime.prop("selected", true);
                alert('<?=__('Starting time must be lower than the ending time!')?>');
            }
        });

        function isEmpty(el) {
            return !$.trim(el.html())
        }

        // If a room is booked for the whole day, show it in other rooms section
        if (suitableRooms.has(".bookedAllDay")) {
            otherRooms.show();
            $("div:has(> .bookedAllDay)").parent().appendTo($('.otherrooms > ul.rooms'));
        }

        $(".rooms > li:not(.kogu_stuudio)").click(function (event) {

            var target = $(event.target);
            var room_id = $(this).attr('id');

            // If user clicked the room div
            if (!target.is("span.choosing")) {
                console.log('Vajutatud ei ole span.choosing');

                // Save room id's to session
                $.post('rooms/save_selected_rooms', {
                    id: room_id
                }, function (res) {

                    window.location.href = 'rooms/' + room_id;

                });
            } else if (target.is("span.choosing")) { // If user clicked "Choose room" inside room div

                // Remove the example room
                $('.selected-example').remove();
                var hidden_room = $(this);
                hidden_room.hide();

                // Save room id's to session
                $.post('rooms/save_selected_rooms', {
                    id: room_id
                });

            }
        });

        //Funktsioon, annab ruumidele vahekaugused sõltuvalt vacant time to book ridadele.
        var rooms = document.getElementById("rooms"),
            lis = rooms.getElementsByTagName("li"),
            l = lis.length,
            i = 0;
        var count = 0;
        for (i; i < l; i++) {
            Par = lis[i].getElementsByTagName("p"),

                P = Par.length;

            if (P > count) {
                count = P;
            }
        }
        if (count > 2) {
            marginB = (count) * 26;
            marginBottom = marginB.toString() + 'px';
            $('.rooms li').css('marginBottom', marginBottom);
        }

        // Make room divs the same size
        $('.rooms').each(function () {

            // Cache the highest
            var highestBox = 0;

            // Select and loop the elements you want to equalise
            $('.roomcapacity', this).each(function () {
                // If this box is higher than the cached highest then store it
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            });

            // Set the height of all those children to whichever was highest
            $('.roomcapacity', this).height(highestBox);

        });

    })
    ;
</script>
