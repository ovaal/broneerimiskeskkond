<?php use Broneerimiskeskkond\Cart;
use Broneerimiskeskkond\Settings; ?>

<style>
    #start_time, #end_time {
        border-top: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
        margin-left: 0;
        width: 100%;
        height: 43px;
        padding: 0 0 0 30px;
        font-size: 16px;
    }

    .fc-basic-view .fc-body .fc-row {
        min-height: 1.7em !important;
    }

    #calendar {
        margin-top: 10px;
        margin-bottom: 20px;
    }

    #calendar h2 {
        font-size: 16px;
        padding-top: 9px;
    }

    .fc-ltr .fc-basic-view .fc-day-number {
        text-align: center !important;
        padding-bottom: 0 !important;
        height: 25px;
    }

    .fc-ltr .fc-basic-view .fc-day-number:hover {
        background: #374054;
        color: #fff;
    }

    .fc-axis {
        width: 0 !important;
    }

    .fc-row .fc-bg {
        height: 28px !important;
    }

    .fc-past {
        color: #e6e6e6;
    }

    .fc-today {
        color: white !important;
        background: #737373 !important;
    }

    .bookedDay {
        background-color: #F0EDE5;
        color: #8c8c8c;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(1) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(2) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(3) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(4) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(5) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #calendar > div.fc-view-container > div > table > tbody > tr > td > div > div > div:nth-child(6) > div.fc-content-skeleton > table > tbody {
        display: none !important;
    }

    #clickedDay > div > div > div.modal-body > div > div.fc-view-container > div > table > tbody > tr > td > div.fc-scroller.fc-time-grid-container > div > div.fc-content-skeleton > table > tbody > tr > td:nth-child(2) > div > div:nth-child(2) {
        display: block !important;
    }

    .selectedDay {
        color: white;
        background: #e5cf88 !important;
    }

    .legend {
        border: 1px solid black;
        padding: 4px;
        display: inline-block;
        font-size: 12px;
        margin-bottom: 5px;
    }

    .today {
        color: white;
        background: #898e8c;
    }

    .fc-day-number:hover {
        color: white !important;
        background: #e5cf88 !important;
    }

    p {
        line-height: 1;
    }

    body {
        padding-right: 0 !important;
    }

    a:hover {
        text-decoration: none !important;
    }

    .fc-highlight {
        background: #e5cf88 !important;
        opacity: 1 !important;
    }

    @media (max-width: 768px) {
        span.extra, span.design {
            display: block;
        }

        input.extras {
            float: left;
        }

    }

    #prev {
        background: #000 url('<?= BASE_URL ?>assets/img/arrow.png') no-repeat center;
        transform: rotate(180deg);
        left: 0px;
    }

    #next {
        background: #000 url('<?= BASE_URL ?>assets/img/arrow.png') no-repeat center;
        right: 0px
    }

    .nvgt {
        opacity: 0.6;
        position: absolute;
        top: 50%;
        width: 50px;
        height: 50px;
    }

    .nvgt:hover {
        opacity: 0.9;
    }

    .the-navbar {
        top: 0px !important;
    }

    .navbar-header {
        top: 55px;
    }

    .search-result {
        background: rgba(8, 15, 25, .5);
        padding-right: 15px !important;
    }

    .the-header-room-bottom-bar {
        background: none !important;
    }

    .room-image {
        max-width: 820px;
        margin: 0 auto;
        overflow: hidden;
    }

    .images-container {
        width: 920px;
        position: relative;
        margin: auto;
    }

    .room-images {
        display: block;
    }

    .the-header {
        background-repeat: no-repeat !important;
        background-size: cover !important;
        background-position: 50% 80% !important;
        height: 368px !important;
    }

    @media (min-height: 700px) {
        .the-header {
            height: 400px !important;
        }
    }

    @media (min-height: 850px) {
        .the-header {
            height: 500px !important;
        }
    }

    @media (min-height: 1000px) {
        .the-header {
            height: 700px !important;
        }
    }

    @media (min-width: 744px) {
        @media (min-height: 700px) {
            .the-header {
                height: 500px !important;
            }
        }@media (min-height: 850px) {
        .the-header {
            height: 570px !important;
        }
    }@media (min-height: 1000px) {
        .the-header {
            height: 700px !important;
        }
    }
    }

    @media (min-width: 1128px) {
        @media (min-height: 700px) {
            .the-header {
                height: 552px !important;
            }
        }@media (min-height: 850px) {
        .the-header {
            height: 730px !important;
        }
    }@media (min-height: 1000px) {
        .the-header {
            height: 900px !important;
        }
    }
    }

    .other-offers {
        margin-top: 20px;
        padding-left: 0;
        font-size: 16px;
    }

    .extras-choosing, .supplies-choosing, .designs-choosing {
        font-size: 16px !important;
    }

    label.name {
        max-width: 90%;
        vertical-align: top;
    }

    .room-type-desc {
        line-height: 1.5;
        font-weight: 300;
        font-size: 16px;
        color: #808080;
        margin-top: 20px;
        margin-bottom: 20px;
    }

    .extras-checked-circle {
        font-size: 24px;
        color: #e5cf88;
        vertical-align: middle;
        margin-right: 5px;
        top: -1px;
    }

    .descriptive-p-tag {
        line-height: 1.5;
        font-weight: 300;
        color: #808080;
    }

    /* Custom checkboxes */
    #checked_checkbox, #unchecked_checkbox {
        top: -1px;
        width: 16px;
        height: 16px;
        display: inline-block;
        margin-right: 10px;
    }

    /* Color the checkboxes gray when their input is disabled */
    input[type=checkbox]:disabled + img.checkbox_image, img.gray {
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        opacity: 0.5;
    }

    /* Golden checkbox (make circle img yellow) */
    input[type=checkbox], .goldentext + img.checkbox_image, img.gray {
        -webkit-filter: grayscale(100%) brightness(120%) sepia(90%) hue-rotate(5deg) saturate(500%) contrast(0.7);
        filter: grayscale(100%) brightness(120%) sepia(90%) hue-rotate(5deg) saturate(500%) contrast(0.7);
    }

    .font-14px {
        font-size: 14px;
    }

    .legends {
        margin-bottom: 10px;
    }

    @media (min-width: 992px) {
        span.the-room-datetime-arrow {
            margin-left: -10px !important;
            z-index: 1;
        }
    }

    @media (max-width: 991px) {
        span.the-room-datetime-arrow {
            display: none;
        }
    }

    .timer span {
        margin-left: 5px;
    }

    .indent {
        display: block;
        margin-left: 28px;
    }

</style>
<div class="the-header-room-bottom-bar">

    <div class="the-wrap">

        <!-- room detail page -->
        <div class="the-left search-result">

            <a href="#"><?= __('Room search'); ?></a> -
            <a href="#" class="search-parameters">
                <?= __('Search result ') . '(' . $_SESSION['booking_date'] . ' - ' . $_SESSION['booking_start_time'] . '-' . $_SESSION['booking_end_time'] . ')' ?></a>
            -
            <?= __('Room') ?> <?= $room['room_name'] ?>

        </div>
        <!-- room detail page -->

    </div>

</div> <!-- .the-header-room-bottom-bar -->

<div class="the-room-view">
    <div class="container">
        <div class="row">
            <div class="the-wrap">

                <div class="the-room-view-content">

                    <div class="the-room-view-quote" style="margin-bottom:15px">
                        <?= __($room['room_message']) ?>
                    </div>

                    <div class="content-row" id="the-room-view-header">

                        <div class="content-col-4">
                            <h1><?= __('Room') ?> "<?= __($room['room_name']) ?>"</h1>
                            <span class="h1-sub"><?= __($room['room_slogan']) ?> </span>
                        </div>

                        <div class="content-col-3">
                            <input type="hidden" name="price" value="<?= $room['room_price'] ?>">
                        </div>

                        <div class="content-col-3">

                            <span class="price_quant"><?= __('Space:'); ?></span><br>
                            <span class="greentext">
                                    <strong><?= $room['room_size'] ?> m<sup>2</sup></strong>
                                </span>

                            <br>

                            <span class="price_quant"><?= __('Capacity:'); ?></span><br>
                            <span class="greentext">
                                    <strong><?= $room['room_max_persons'] ?> <?= __('people') ?></strong>
                                </span>

                        </div>

                    </div> <!-- .content-row -->

                    <h2 class="the-room-content-title">
                        <?= __('About ' . ($special_room_view ? 'whole studio' : 'room')); ?>
                    </h2>

                    <p><?= __($room['room_description']) ?></p>

                    <?php if (!$special_room_view): ?>
                        <div class="content-row" id="the-room-extra">
                            <div class="content-col-5 extras-choosing">
                                <i class="fa fa-check-circle-o extras-checked-circle"></i>

                                <img src="assets/img/esitlustehnika.png" style="max-width: 40px; display: inline-block">

                                <h2 class="the-room-content-title" style="display: inline-block;">
                                    <?= __('Extra possibilities'); ?>
                                </h2>

                                <br>

                                <?php if (!empty($extras)) foreach ($extras as $extra): ?>
                                    <span class="extra" style="display: inherit;">
                                    <input class="extras" type="checkbox" id="extra_checbox"
                                           name="<?= $extra['extra_id'] ?>"
                                           value="1"
                                           <?= isset($_SESSION['selected_rooms'][$room['room_id']]['extras'][$extra['extra_id']]) ? 'checked' : '' ?>>

                                    <label class="name" for="extra_checbox">
                                        <span class="extra_name">
                                            <?= __($extra['extra_name']); ?>
                                        </span>

                                        <span class="hili">
                                            <span class="price"><?= $extra['extra_price'] * 1; ?></span>€
                                        </span>
                                    </label>
                                </span>

                                    <br>
                                <?php endforeach; ?>
                            </div>

                            <?php if (!empty($supplies)): ?>
                                <div class="content-col-5 supplies-choosing">
                                    <h2 class="the-room-content-title"><?= __('Office supplies'); ?></h2>
                                    <?php foreach ($supplies as $supply): ?>
                                        <div class="data-input-toggle-number supply">
                                        <span class="input-minus">
                                            <i class="fa fa-minus"></i>
                                        </span>

                                            <input type="text" name="<?= $supply['supply_id'] ?>"
                                                   value="<?= isset($_SESSION['selected_rooms'][$room['room_id']]['supplies'][$supply['supply_id']]) ? $_SESSION['selected_rooms'][$room['room_id']]['supplies'][$supply['supply_id']] : '0' ?>"
                                                   class="quantity"
                                                   onkeypress="return isNumber(event)"/>

                                            <span class="input-plus">
                                            <i class="fa fa-plus"></i>
                                        </span>

                                            <span class="name"
                                                  data-id="<?= $supply['supply_id'] ?>"><?= __($supply['supply_name']) ?>
                                        </span>

                                            <span class="hili">
                                            <span class="price"><?= $supply['supply_price'] ?></span>
                                        </span>

                                            <br>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($designs)): ?>
                                <div class="content-col-5 designs-choosing" style="font-size: 16px;">
                                    <h2 class="the-room-content-title"><?= __('Room designs'); ?></h2>
                                    <?php foreach ($designs as $design): ?>

                                        <span class="design">
                                    <input class="designs" type="checkbox" id="design_checkbox"
                                           name="<?= $design['design_id'] ?>"
                                           value="1"
                                           <?= isset($_SESSION['selected_rooms'][$room['room_id']]['designs'][$design['design_id']]) ? 'checked' : '' ?>>
                                    <label class="name" for="design_checkbox">
                                        <?= __($design['design_name']); ?>
                                        <span class="hili">
                                            <span class="price hidden"><?= $design['design_price']; ?></span>
                                        </span>
                                    </label>
                                </span>
                                        <br>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>


                        </div> <!-- .content-row -->
                    <?php endif; ?>
                    <?php include 'templates/_partials/partner_status.php'; ?>
                </div> <!-- .the-room-view-content -->

                <div class="the-room-view-sidebar">

                    <?php if ($special_room): ?>

                        <span class="currentroom heading-whole-studio whole-studio-block">
                            <?= __('Studio (5 rooms)') ?>
                        </span>

                        <?php foreach (array_keys($_SESSION['selected_rooms']) as $room_id): ?>
                            <?php if ($room_id == $room['room_id'] && !$special_room_view): ?>
                                <div class="currentroom" data-room-id="<?= $room['room_id'] ?>">
                                    <span><?= \Broneerimiskeskkond\Room::get_rooms_accordion($room_id); ?></span>
                                </div>
                            <?php else: ?>
                                <a data-href="rooms/<?= $room_id ?>" class="room-block">
                                    <button class="anotherroom" id="room-<?= $room_id ?>"
                                            data-room-id="<?= $room_id ?>">
                                        <?= \Broneerimiskeskkond\Room::get_rooms_accordion($room_id); ?>
                                    </button>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>

                    <?php else: ?>
                        <?php foreach (array_keys($_SESSION['selected_rooms']) as $room_id): ?>
                            <?php if ($room_id == $room['room_id']): ?>
                                <div class="currentroom" data-room-id="<?= $room['room_id'] ?>">
                                    <span><?= __('Room') ?> "<?= __($room['room_name']) ?>"</span>
                                </div>

                            <?php else: ?>
                                <a data-href="rooms/<?= $room_id ?>" class="room-block">
                                    <button class="anotherroom" id="room-<?= $room_id ?>"
                                            data-room-id="<?= $room_id ?>">
                                        <?= \Broneerimiskeskkond\Room::get_rooms_accordion($room_id); ?>
                                    </button>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <div id='calendar'></div>

                    <div class="legends">
                        <span class="legend today"><?= __('Today') ?></span>
                        <span class="legend bookedDay"><?= __('Day with booking(s)') ?></span>
                        <span class="legend"><?= __('Available') ?></span>
                        <span class="legend selectedDay"><?= __('Selected') ?></span>
                    </div>

                    <p>
                        <i class="fa fa-check-circle-o extras-checked-circle"></i><?= __('Your booking'); ?>:
                    </p>
                    <div class="the-room-datetime" style="width: calc(100% + 80px); margin-left: -40px;"
                         data-toggle="tooltip"
                         title="<?= __('The rental time is the time from arrival to departure'); ?>">

                        <div class="row text-center" style="margin-left: 0; margin-right:0;">

                            <div class="col-md-3 no-padding">
                                <input class="yourDate" disabled style="margin-bottom:5px;">
                                <p class="font-14px"><?= __('Date') ?></p>
                            </div>

                            <div class="col-md-3 no-padding">
                                <select id="start_time" style="margin-bottom:5px;" class="glow">
                                    <?php foreach ($booking_start_time_options as $start): ?>
                                        <option <?= $start == $booking_start ? 'selected' : '' ?>><?= $start ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="the-room-datetime-arrow">
                                    <i class="fa fa-long-arrow-right"></i>
                                </span>
                                <p class="font-14px"><?= __('Check-in') ?></p>
                            </div>

                            <div class="col-md-3 no-padding">
                                <select id="end_time" style="margin-bottom:5px;" class="glow">
                                    <?php foreach ($booking_end_time_options as $end): ?>
                                        <option <?= $end == $booking_end ? 'selected' : '' ?>><?= $end ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="font-14px"><?= __('Check-out') ?></p>
                            </div>

                            <div class="col-md-3 no-padding">
                                <input id="textTotalNumOfPeople" type="number" style="margin-bottom:5px;">
                                <p class="font-14px"><?= __('People') ?></p>
                            </div>

                        </div>

                    </div>

                    <p class="descriptive-p-tag"><?= __('Rent time includes time arriving, preparing and time concluding.'); ?></p>

                    <br>

                    <ul class="the-room-booking-list" style="margin-top:0;">

                    </ul>

                    <hr>

                    <ul class="the-room-booking-list">

                        <?php if ($user['user_status'] > 1): ?>
                            <li>
                                <div class="the-left"><strong><?= __('Room rent discount'); ?></strong>
                                    (<?= __('from order'); ?> #<?= $user['current_discount_qualifying_order_id'] ?>)
                                </div>
                                <div class="the-right total-discount">0€</div>
                                <div class="the-clear"></div>
                            </li>
                        <?php endif; ?>

                        <li>
                            <div class="the-left"><strong><?= __('Price'); ?></strong></div>
                            <div class="the-right total-no-tax">0€</div>
                            <div class="the-clear"></div>
                        </li>

                        <li>
                            <div class="the-left"><strong><?= __('VAT') . ' ' . (VAT_PERCENT * 100) . ' %'; ?></strong>
                            </div>
                            <div class="the-right total-tax">0€</div>
                            <div class="the-clear"></div>
                        </li>

                        <li>
                            <div class="the-left"><strong><?= __('Total'); ?></strong></div>
                            <div class="the-right total-sum">0€</div>
                            <div class="the-clear"></div>
                        </li>

                    </ul>

                    <button type="button" class="the-room-booking-next-btn" id="next-step">
                        <?= __('Additional services'); ?><span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                    <a href="rooms">
                        <button class="the-room-booking-previous-btn"><?= __('Previous step'); ?></button>
                    </a>
                </div> <!-- .the-room-view-sidebar -->

            </div>
        </div>
    </div>
</div> <!-- .the-room-view -->


<!-- Fullcalendar -->
<script src="assets/components/adminto/Horizontal/assets/plugins/moment/min/moment.min.js"></script>
<script src="assets/components/adminto/Horizontal/assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
<link rel="stylesheet"
      href="assets/components/adminto/Horizontal/assets/plugins/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" media="print"
      href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.8.0/fullcalendar.print.css">

<!-- Date.js -->
<script src="assets/components/datejs/date.js"></script>

<script src="assets/js/jquery.countdown.js"></script>

<script>
    var image = $(".header-image");
    var calendar = $("#calendar");
    var events = <?= $bookings_js?>;
    var selEndTime = $('#end_time');
    var otherrooms = $(".anotherroom");
    var selStartTime = $('#start_time');
    var selectedDay = '<?= $booking_date ?>';
    var total_max_persons = <?= $max_people ?>;
    var images = <?= json_encode($images) ?>;
    var btnNextStep = $(".the-room-booking-next-btn");
    var textTotalNumOfPeople = $('#textTotalNumOfPeople');
    var booking_duration = <?= $_SESSION['booking_duration']?>;
    var optPreviouslySelectedEndTime = selEndTime.find(':selected');
    var optPreviouslySelectedStartTime = selStartTime.find(':selected');
    var start_end_people = $("select#start_time, select#end_time, input#textTotalNumOfPeople");
    var special_room_view = <?= $special_room_view ? 1 : 0 ?>;


    $(document).ready(function () {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        if (!special_room_view) {
            // Remove duplicate room from accordion
            $('#room-<?= $room['room_id']?>').remove();
        }

        // Show first image of images array

        if (special_room_view) {
            image.attr("style", "background-image:url('assets/img/special_rooms/<?=$special_room['room_id']?>/" + images[2] + "')");
        } else {
            image.attr("style", "background-image:url('assets/img/rooms/<?= $room['room_id']?>/" + images[2] + "')");
        }

        var imagesIndex = 2; // First image in array

        if (!(typeof images[3] == 'undefined')) { // Check if images array has more than one image

            $("#next").click(function () {

                var nextIndex = function (index, length) {
                    if (index + 1 > length + 1) {
                        return 2; // First image
                    } else {
                        return (index + 1); // Next image
                    }
                };

                imagesIndex = nextIndex(imagesIndex, Object.keys(images).length);
                image.fadeOut('slow', function () {
                    image.attr("style", "background-image:url('assets/img/<?= $special_room_view === true ? "special_rooms" : "rooms" ?>/<?= $room['room_id']?>/" + images[imagesIndex] + "')");
                    image.fadeIn('slow');
                });

            });

            $("#prev").click(function () {

                var previousIndex = function (index, length) {
                    if (index <= 2) {
                        return length + 1; // Last image
                    } else {
                        return index - 1; // Prev image
                    }
                };

                imagesIndex = previousIndex(imagesIndex, Object.keys(images).length);
                image.fadeOut('slow', function () {
                    image.attr("style", "background-image:url('assets/img/<?= $special_room_view === true ? "special_rooms" : "rooms" ?>/<?= $room['room_id']?>/" + images[imagesIndex] + "')");
                    image.fadeIn('slow');
                });
            });

        }

        updateCheckboxImages();

        // When a custom checkbox image is clicked then click the actual checkbox
        $(document).on('click', ".checkbox_image", function () {
            $(this).siblings(":checkbox").click();
        });

        $(".whole-studio-block").on("click", function () {
            window.location = 'rooms/<?=$special_room['room_id']?>/special';
        });

        $(".room-block").on("click", function () {
            window.location = $(this).data('href');
        });

        // Add glow on focus
        start_end_people.focusin(function () {
            if ($(this).prop('nodeName') == "SELECT") {
                $("select").addClass("glow");
            }
            $(this).addClass("glow");
        });

        // Remove glow on input
        start_end_people.on("change", function () {
            if ($(this).prop('nodeName') == "SELECT") {
                $("select").removeClass("glow");
            }
            $(this).removeClass("glow");
        });

        // Remove glow on focus out
        start_end_people.focusout(function () {
            if ($(this).prop('nodeName') == "SELECT") {
                $("select").removeClass("glow");
            }
            $(this).removeClass("glow");
        });

        // Initially calculate the bill
        calculateBill();

        // When an extra is clicked, re-calculate the bill
        $('.fa.fa-plus, span.extra').click(function () {
            calculateBill();
        });

        $('input.yourDate').val(selectedDay);

        textTotalNumOfPeople.val('<?= $_SESSION['number_of_people'] ?>' > total_max_persons ? total_max_persons : '<?= $_SESSION['number_of_people'] ?>');
        textTotalNumOfPeople.addClass('glow');

        // When dropdowns are clicked, record their previous values
        $("select").click(function () {
            optPreviouslySelectedStartTime = selStartTime.find(':selected');
            optPreviouslySelectedEndTime = selEndTime.find(':selected');
        });

        // Check if inserted number of people is valid
        $(textTotalNumOfPeople).on("input keyup", function () {
            enableOrDisableNextStep();
            empty_num_of_ppl(textTotalNumOfPeople, total_max_persons);
            $(this).val() == '' ? textTotalNumOfPeople.addClass('glow') : textTotalNumOfPeople.removeClass('glow');
        });

        function empty_num_of_ppl(type, max) {
            var n = type.val();
            type.val(n > max || n < 1 ? '' : n);
            if (n > max) {
                swal("<?= __('Maximum number of people for selected room(s) is ') ?> " + max);
            }
        }

        var tempVar = "";
        // Initialize the calendar
        $('#calendar').fullCalendar({
            dayClick: function (date, jsEvent, view) {

                selectedDay = formatDate(date);
                var today = formatDate(new Date());

                // Set all calendar numbers the default color
                $('.fc-past').css({"color": "#e6e6e6"});
                $('.fc-future').css({"color": "#606060"});

                // Make selected day white
                $('[data-date=' + selectedDay + ']').css({"color": "white"});

                // Selected cell background color
                if (tempVar == "") {
                    $(this).addClass('selectedDay');
                    tempVar = this;
                } else {
                    $(this).addClass('selectedDay');
                    $(tempVar).removeClass('selectedDay');
                    tempVar = this;
                }

                enableOrDisableNextStep();


                // Previous days will be unselectable
                if (selectedDay >= today) {

                    var startTimeText = $('#start_time :selected').text();
                    var endTimeText = $('#end_time :selected').text();
                    var start = $.trim(startTimeText);
                    var end = $.trim(endTimeText);

                    // Update clicked date
                    $('input.yourDate').val(selectedDay);

                    syncRoomSearchBox();

                    var loading_modal = $('#loading-modal');
                    loading_modal.modal('show');

                    // Save booking data to session
                    ajax("rooms/save_booking", {
                            booking_date: selectedDay,
                            booking_start_time: start,
                            booking_end_time: end,
                            number_of_people: textTotalNumOfPeople.val(),
                            room_id: '<?=$this->params[0]?>'
                        }, function () {
                            // Update the bill to reflect changed booking information
                            calculateBill();
                            close_modal(loading_modal);

                        }
                    );


                }
            },
            firstDay: 1, // Monday
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventRender: function (event, element) {
                var dataToFind = moment(event.start).format('YYYY-MM-DD');
                $("td[data-date='" + dataToFind + "']").addClass('bookedDay');
            },
            weekends: <?= Settings::get('WEEKENDS_CAN_BE_BOOKED') ? 'true' : 'false'?>,
            events: function (start, end, tz, callback) {
                callback(events);
            },
            eventLimit: true, // Limit visible events to 1
            monthNames: ['Jaanuar', 'Veebruar', 'Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember'],
            monthNamesShort: ['jan.', 'veb.', 'märts', 'apr.', 'mai', 'juuni', 'juuli', 'august', 'sept.', 'okt.', 'nov.', 'dets.'],
            dayNames: ['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
            dayNamesShort: ['P', 'E', 'T', 'K', 'N', 'R', 'L']
        }).fullCalendar('gotoDate', selectedDay);
        $('#calendar').fullCalendar('select', selectedDay);

        // Show bookings in title if there are any
        $('td').on('mouseover', '.bookedDay', function () {

            var existingBookings = '';
            var clickedDay = $(this).data('date');
            var existingBookingsOnGivenDay = events.filter(function (booking) {
                return booking.start.substr(0, 10) === clickedDay;
            });

            $.each(existingBookingsOnGivenDay, function (index, booking) {
                existingBookings += booking.start.substr(11, 5) + " - " + booking.end.substr(11, 5);
            });

            // Add title to event td
            $(this).attr('title', existingBookings);
            $(this).attr('title', existingBookings);
        });

        // Initially enable or disable previous month button on calendar
        enableOrDisablePrevMonth();

        $('.fc-prev-button, .fc-next-button').on('click', function () {
            enableOrDisablePrevMonth();
        });

        function enableOrDisablePrevMonth() {
            var chosen_date = $("#calendar").fullCalendar('getDate');
            var current_month = new Date().getMonth() + 1;
            var current_year = new Date().getFullYear();

            if (current_year == chosen_date.format("YYYY") && current_month == chosen_date.format("MM")) {
                $('.fc-prev-button').attr('disabled', true);
            } else {
                $('.fc-prev-button').attr('disabled', false);
            }
        }

        // Start continuous polling of events from server
        updateEvents();
    });

    var startTimeText = $('#start_time :selected').text();
    var endTimeText = $('#end_time :selected').text();
    var start = $.trim(startTimeText);
    var end = $.trim(endTimeText);


    // When booking times or number of participants are changed, perform time validation check and update session
    $('#start_time, #end_time, #textTotalNumOfPeople').on('input', function () {
        var selectedStartTime = new Date(selectedDay + ' ' + selStartTime.find(':selected').text()).getTime();
        var selectedEndTime = new Date(selectedDay + ' ' + selEndTime.find(':selected').text()).getTime();

        if (selectedStartTime > selectedEndTime || selectedStartTime == selectedEndTime) {
            optPreviouslySelectedStartTime.prop("selected", true);
            optPreviouslySelectedEndTime.prop("selected", true);
        }

        <?php if (!empty($_SESSION['selected_rooms'])): ?>
        // Check if the booking time is atleast 4 hours
        calculateMinimumTime();

        // Save booking data to session
        var start = $("#start_time option:selected").text();
        var end = $("#end_time option:selected").text();
        var loading_modal = $('#loading-modal');

        loading_modal.modal('show');

        ajax("rooms/save_booking", {
                booking_start_time: start,
                booking_end_time: end,
                number_of_people: textTotalNumOfPeople.val(),
                room_id: '<?=$this->params[0]?>'
            }, function () {
                // Update the bill to reflect changed booking information
                calculateBill();
            }
        );
        <?php endif; ?>

        calculateBill();
        enableOrDisableNextStep();

    });

    function updateEvents() {
        // do some work here

        ajax("rooms/get_booking/", {room_id: <?= $room['room_id']?>}, function (json) {

            // Update events
            events = JSON.parse(json.data);
            $("td[data-date]").removeClass('bookedDay');
            $("div.fc-day-grid-container td").attr("title", "");
            calendar.fullCalendar('removeEvents');
            calendar.fullCalendar('addEventSource', events);
            calendar.fullCalendar('rerenderEvents');

            enableOrDisableNextStep();

            setTimeout(updateEvents, 60000);
        });

    }

    function updateCheckboxImages() {
        // Use custom checkbox images
        $(":checkbox").each(function () {
            var checkbox = $(this);
            checkbox.hide();
            checkbox.siblings('img').remove();

            var image_class;

            // Add class "gray" if either of these checkboxes
            if (checkbox.hasClass("client-visited-studio") || checkbox.hasClass("client-has-birthday")) {
                image_class = "class='checkbox_image gray'";
            } else {
                image_class = "class='checkbox_image'";
            }

            if (checkbox.is(':checked')) {
                checkbox.after("<img id='checked_checkbox' " + image_class + " src='<?= BASE_URL ?>assets/img/checked.png'/>");
            } else {
                checkbox.after("<img id='unchecked_checkbox' " + image_class + " src='<?= BASE_URL ?>assets/img/unchecked.png'/>");
            }
        });
    }

    function calculateMinimumTime() {

        var time1 = $('#start_time :selected').val().split(':'), time2 = $('#end_time :selected').val().split(':');
        var hours1 = parseInt(time1[0], 10),
            hours2 = parseInt(time2[0], 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;

        // get hours
        if (hours < 0) hours = 24 + hours;

        // get minutes
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        } else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        // convert to fraction of 60
        mins = mins / 60;

        hours += mins;
        hours = hours.toFixed(2);

        <?php if($room['room_id'] == 1 || $room['room_id'] == 6): ?>

        // Minimum 4 hours for main rooms
        if (hours < 4) {
            $('#start_time').val('08:00');
            $('#end_time').val('18:00');
            alert('<?= __("Please select the correct time to book (minimum 4 hours)") ?>');
            return false;
        } else {
            return true;
        }

        <?php else: ?>

        // Minimum 1 hour if room selected as extra
        if ($('.extra-room').is(':checked')) {
            if (hours < 1) {
                $('#start_time').val('08:00');
                $('#end_time').val('18:00');
                alert('<?= __("Please select the correct time to book (minimum 4 hours for main room and 1 hour for groupwork room)") ?>');
                return false;
            } else {
                return true;
            }
        }

        // Minimum 4 hours if room selected as main
        if ($('.main-room').is(':checked')) {
            if (hours < 4) {
                $('#start_time').val('08:00');
                $('#end_time').val('18:00');
                alert('<?= __("Please select the correct time to book (minimum 4 hours for main room and 1 hour for groupwork room)") ?>');
                return false;
            } else {
                return true;
            }
        }

        <?php endif; ?>
    }

    function bookingDuration(startTimeText, endTimeText) {
        var randomDate = formatDate(new Date());
        var startTime = new Date(moment(randomDate + ' ' + startTimeText).format('MMMM D, YYYY h:mm:ss a')).getTime();
        var endTime = new Date(moment(randomDate + ' ' + endTimeText).format('MMMM D, YYYY h:mm:ss a')).getTime();

        return (endTime - startTime) / 3600000;
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function selectedTimeOverlapsWithExistingTime() {

        var overlap = false;
        var selectedStartTime = new Date(selectedDay + ' ' + $('#start_time :selected').text()).getTime();
        var selectedEndTime = new Date(selectedDay + ' ' + $('#end_time :selected').text()).getTime();
        var existingBookingsOnGivenDay = events.filter(function (booking) {
            return booking.start.substr(0, 10) === selectedDay;
        });

        $.each(existingBookingsOnGivenDay, function (index, existingBooking) {

                var existingBookingStart = new Date(existingBooking.start).getTime();
                var existingBookingEnd = new Date(existingBooking.end).getTime();

                // Add 1 hour
                existingBookingEnd = existingBookingEnd + 3600000;
                if ((selectedStartTime < existingBookingEnd) && (selectedEndTime > existingBookingStart)) {
                    overlap = true;
                }
            }
        )
        ;

        console.log("overlap:" + overlap);
        return overlap;
    }

    function numberOfPeopleIsValid(people) {
        return people.val() !== '' && people.val() !== '0' && people.val() <= total_max_persons;
    }

    function enableOrDisableNextStep() {

        if (selectedTimeOverlapsWithExistingTime()) {
            swal("<?= __('Selected time overlaps with booked time(s)! Please select an available date to continue. At least 1 hour must be in between the bookings.')?>");

            // Disable buttons
            console.log('Selected time overlaps with existing time. Disable other rooms and next step.');
            btnNextStep.addClass('disable-next-step');
            btnNextStep.prop('title', '<?= __("Selected time overlaps with existing booking.") ?>');
            btnNextStep.tooltip('fixTitle');
            btnNextStep.tooltip('enable');
            otherrooms.prop('disabled', true);
            return;
        }

        if (!numberOfPeopleIsValid(textTotalNumOfPeople)) {
            // Disable next step
            console.log('Total number of people isn\'t correct, disable next step.');
            btnNextStep.addClass('disable-next-step');
            btnNextStep.prop('title', '<?= __('The number of people has not been entered') ?>');
            btnNextStep.tooltip('fixTitle');
            btnNextStep.tooltip('enable');
            return;
        }

        // Enable buttons
        console.log('Selected time and number of people are correct, enable the buttons.');
        otherrooms.prop('disabled', false);
        btnNextStep.tooltip('disable');

        // Disable next step if no rooms are selected
        <?php if (!empty($_SESSION['selected_rooms'])): ?>
        btnNextStep.removeClass('disable-next-step');
        <?php endif; ?>
    }

    function buildExtras(type) {

        var extras = [];
        var selector = type == 'supplies' ? "#the-room-extra div.supply" : "#the-room-extra span.extra";

        $.each($(selector), function (i, element) {

            // Determine whether the item was selected, based on item type
            var item_is_selected = type == 'extras' ?
                $(element).find(':checkbox')[0].checked :
                ($(element).find('input.quantity').get(0).value) > 0;


            // Add item to array if it was selected
            if (item_is_selected) {
                extras.push({
                    name: $(element).find('.extra_name').html(),
                    quantity: type == 'extras' ? 1 : $(element).find('input.quantity').get(0).value,
                    price: $(element).find('span.price').html()
                });

            }

        });

        return extras;
    }

    function buildRentPriceRow() {

        var rent = [];
        var roomPrice = $("[name='price']").val();
        var room = parseFloat(roomPrice).toFixed();

        booking_duration = bookingDuration($('#start_time :selected').text(), $('#end_time :selected').text());

        rent.push({
            <?php if ($special_room): ?>
            name: '<?= __("Whole Studio") . ' ' . $special_room['room_price'] ?>€ × ' + booking_duration + ' <?= __('hour(s)') ?>',
            price: <?= (float)$special_room['room_price']  ?>,
            <?php else: ?>
            name: room + '€ × ' + booking_duration + ' <?= __('hour(s)') ?>',
            price: room,
            <?php endif; ?>
            quantity: booking_duration
        });

        return rent;
    }

    function calculateBill() {
        var rent = buildRentPriceRow();
        var extras = buildExtras('extras');
        var supplies = buildExtras('supplies');
        var items = rent.concat(extras).concat(supplies);
        var list = $('ul.the-room-booking-list:first');
        var discount = 0;

        // Check if theres a discount and calculate it(only from the rent)
        <?php if ($user['user_status'] == USER_STATUS_GOLD): ?>
        discount = 0.1;
        <?php elseif ($user['user_status'] == USER_STATUS_SILVER): ?>
        discount = 0.05;
        <?php else: ?>
        discount = 0;
        <?php endif; ?>
        discount *= (rent[0].price * rent[0].quantity);

        // Empty the bill
        $(list).children().remove();

        var total = 0;

        // Build the bill
        $.each(items, function (i, extra) {

            // Calculate sums
            var item_sum = extra.price * extra.quantity;
            total += item_sum;

            // Add new row
            var li = $('<li/>').appendTo(list);

            // Add item name to row
            $('<div/>')
                .addClass('the-left')
                .html('<i class="fa fa-check-circle-o"></i>' + extra.name)
                .appendTo(li);

            // Add item's sum to row
            $('<div/>')
                .addClass('the-right')
                .text(item_sum + '€')
                .appendTo(li);

            // Add newline
            $('<div/>')
                .addClass('the-clear')
                .appendTo(li);

        });

        // Add totals
        <?php if($_SESSION['whole_studio_exhibition'] == 1): ?>
        $('div.total-discount').text('-' + round(discount) + '€');
        $('div.total-no-tax').text(round(total + exhibition_sum - discount) + '€');
        $('div.total-tax').text(round((total + exhibition_sum - discount) * <?= VAT_PERCENT ?>) + '€');
        $('div.total-sum').text(round((total + exhibition_sum - discount) * <?= (1 + VAT_PERCENT) ?>) + '€');
        <?php else: ?>
        $('div.total-discount').text('-' + round(discount) + '€');
        $('div.total-no-tax').text(round(total - discount) + '€');
        $('div.total-tax').text(round((total - discount) * <?= VAT_PERCENT ?>) + '€');
        $('div.total-sum').text(round((total - discount) * <?= (1 + VAT_PERCENT) ?>) + '€');
        <?php endif; ?>

    }

    $('.extras').on('change', function () {
        <?php if (!empty($_SESSION['selected_rooms'])): ?>
        $.post('additional_services/save_extra_selection', {
            room_id: <?= $room['room_id']?>,
            extra_id: $(this).attr('name'),
            selected: this.checked
        });
        changeCheckboxImage($(this));

        <?php endif; ?>
    });

    $('.designs').on('change', function () {
        <?php if (!empty($_SESSION['selected_rooms'])): ?>
        $.post('rooms/save_designs', {
            room_id: <?= $room['room_id']?>,
            design_id: $(this).attr('name'),
            value: $(this).val()
        });
        changeCheckboxImage($(this));
        <?php endif; ?>
    });

    // Function is in use at custom.js
    function suppliesToSession(elem) {
        <?php if (!empty($_SESSION['selected_rooms'])): ?>
        $.post('rooms/save_supplies', {
            room_id: <?= $room['room_id']?>,
            supply_id: elem.attr('name'),
            quantity: elem.val()
        });
        changeCheckboxImage($(this));
        <?php endif; ?>
    }

    function syncRoomSearchBox() {
        var start = $("#start_time option:selected").text();
        var end = $("#end_time option:selected").text();
        var date = $("input.yourDate").val();

        $('.search-parameters').html('Search result (' + date + ' - ' + start + '-' + end + ')');
    }

    function changeCheckboxImage(clicked_item) {
        if (clicked_item.is(":checked")) {
            clicked_item.siblings(".checkbox_image").attr({
                "id": "checked_checkbox",
                "src": "<?= BASE_URL ?>assets/img/checked.png"
            });
        } else {
            clicked_item.siblings(".checkbox_image").attr({
                "id": "unchecked_checkbox",
                "src": "<?= BASE_URL ?>assets/img/unchecked.png"
            });
        }
    }

    btnNextStep.on('click', function () {
        if ($(this).hasClass('disable-next-step')) {
            return false;
        }
        location.href = 'additional_services'
    });

    // Gold/silver partner discount valid countdown
    var lastOrderDate = '<?= $user['user_status_ends'] ?>';
    $('.timer').countdown(lastOrderDate).on('update.countdown', function (event) {
        var $this = $(this).html(event.strftime(''
            + '<span>%m</span> <sub>%!m:<?= __('month,months')?>;</sub> '
            + '<span>%n</span> <sub>%!n:<?= __('day,days')?>;</sub> '
            + '<span>%H</span> <sub>%!H:<?= __('hour,hours')?>;</sub> '
            + '<span>%M</span> <sub>%!M:<?= __('minute,minutes')?>;</sub> '
            + '<span>%S</span> <sub>%!S:<?= __('second,seconds')?>;</sub> '
        ));
    });

    function round(number) {
        if (number % 0.1 !== 0) {
            return Math.round(number * 100) / 100
        } else if (number % 1 !== 0) {
            return Math.round(number * 10) / 10
        } else {
            return Math.round(number)
        }
    }
</script>